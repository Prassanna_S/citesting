package library
import com.lowagie.text.Row;
import org.apache.poi.hssf.usermodel.*
import java.util.List;
import java.util.Iterator;
import java.util.HashMap;
import java.lang.reflect.*;
import org.apache.poi.hssf.usermodel.HSSFCell;

import library.Constants.*;

class ReadDataObjQuery {
	def sourceFile
	InputStream inStr
	HSSFWorkbook workbook
	
	/**
	 * Constructors for the class
	 */
	public def ReadDataObjQuery(){
		sourceFile = Constant.PROJECTLOCATION + 'ResponseVerification.xls'
		inStr = new FileInputStream(sourceFile)
		workbook = new HSSFWorkbook(inStr)
	}
	public def ReadDataObjQuery (def file) {
		sourceFile = file
		inStr = new FileInputStream(file)
		workbook = new HSSFWorkbook(inStr)
	}
	
	/**
	 * Get the list of value of a single column(from response verification file of a test suite)
	 * @param columnName name of the column from which the data is to be retrieved
	 * @return list of column data
	 * @author vasanth.manickam
	 */
	List getColumnList(String columnName) {
		String dataName = 'null'
		List dataList = []
		int colIndex
		HSSFSheet sheet = workbook.getSheet(Constant.TESTSUITENAME)
		
		// Identify the column index 
		HSSFRow row1 = sheet.getRow(0)
		Iterator<HSSFCell> cells = row1.cellIterator()
		while(cells.hasNext()) {
			HSSFCell cell = cells.next()
			if(getCellValue(cell).equalsIgnoreCase(columnName)){
				colIndex = cell.getColumnIndex()
			}
		}
		
		// Iterate through all the rows to fetch data
		Iterator<HSSFRow> rows = sheet.rowIterator();
		HSSFRow row = rows.next ();
		row = rows.next ();
		dataName = row.getCell(colIndex).toString()
		dataList.add(dataName)
		while (rows.hasNext ())	{
			row = rows.next ();
			String rowData = row.getCell(colIndex).toString()
			// if the cell is empty with no data, have the last know value as the data and append to the list
			if(rowData.toString().equals("")) {
				dataList.add(dataName)
			} else {
				dataName = rowData
				dataList.add(dataName)
			}			
		}
		return dataList
	}
	
	/**
	 * Get the verification data for the particular test step of the test case
	 * @param testCaseName Name of the test case
	 * @param testStepName Name of the test step under the test case
	 * @return Response data map(xPath as keys and the expected data as value of the map)
	 * @author vasanth.manickam
	 */
	LinkedHashMap getVerificationData(String testCaseName, String testStepName) {
		List tcName = getColumnList('TestCase')
		List tsName = getColumnList('TestStep')
		List xPath = getColumnList('Node_XPath')
		List expValue = getColumnList('Expected_Value')
		LinkedHashMap responseMap = new LinkedHashMap();
		for(int i=0; i<tcName.size(); i++) {
			if(tcName.get(i).toString().equalsIgnoreCase(testCaseName) && tsName.get(i).toString().equalsIgnoreCase(testStepName)){
				
				// Get the equivalent xPath from the constant groovy file
				String xPathData = getXPath(xPath.get(i).toString())
				responseMap.put xPathData, expValue.get(i).toString()
			}
		}
		return responseMap
	}

	void replaceXpathNameSpace() {
		String dataName = 'null'
		List dataList = []
		int colIndex
		HSSFSheet sheet = workbook.getSheet(Constant.TESTSUITENAME)
		
		// Identify the column index
		HSSFRow row1 = sheet.getRow(0)
		Iterator<HSSFCell> cells = row1.cellIterator()
		while(cells.hasNext()) {
			HSSFCell cell = cells.next()
			if(getCellValue(cell).equalsIgnoreCase("Expected_Value")){
				colIndex = cell.getColumnIndex()
			}
		}
		
		// Iterate through all the rows to fetch data
		Iterator<HSSFRow> rows = sheet.rowIterator();
		HSSFRow row = rows.next ();
		
		while (rows.hasNext ())	{
			row = rows.next ();
			String rowData = row.getCell(colIndex).toString()
			if(rowData.contains("~namespace~")) {
				rowData = rowData.replace("~namespace~", Constant.NAMESPACE)
				row.getCell(colIndex).setCellValue(rowData)				
			}
		}
			
		FileOutputStream fileOut = new FileOutputStream(sourceFile);
		workbook.write(fileOut);
		fileOut.close();
	}
	
	/**
	 * Get the cell value based on the cell type
	 * @param cell HSSFCell cell for which value is to be retrieved 
	 * @return cell value
	 * @author vasanth.manickam
	 */
	String getCellValue(HSSFCell cell) {		
		switch ( cell.getCellType() ) {
			case HSSFCell.CELL_TYPE_NUMERIC:
				cell.getNumericCellValue().toString()
				break;
			case HSSFCell.CELL_TYPE_STRING:
				cell.getStringCellValue().toString()
				break;
		}
	}

	/**
	 * Get all the test cases that has to be executed
	 * @return List of test case name
	 * @author vasanth.manickam
	 */
	List getTCSelection() {
		def inFile = Constant.PROJECTLOCATION + 'TestCase_Selection.xls'
		
		InputStream inStr = new FileInputStream(inFile)
		HSSFWorkbook workbook = new HSSFWorkbook(inStr)
		HSSFSheet sheet = workbook.getSheet(Constant.TESTSUITENAME)
		HSSFRow headerRow = sheet.getRow(0)
		Iterator<HSSFRow> rows = sheet.rowIterator ();
		HSSFRow row = rows.next();
		def listTestCaseName = []
		while (rows.hasNext ())	{
						
			row = rows.next ();			
			if(row.getCell(1).toString().toUpperCase() != "NO"){
				listTestCaseName.add(row.getCell(0).toString())	
			}
		}
		inStr.close()
		return listTestCaseName
	}
	
	/**
	 * Get the respective xPath from the constants defined in 'ExPath.groovy'
	 * @param constNameReference reference name for which the respective xPath should be returned
	 * @return equivalent xPath as per defined in 'ExPath.groovy'
	 * @author vasanth.manickam
	 */
	String getXPath(String constNameReference) {
		def objxPath
		String xPath
		
		if(Constant.WSDLDEFINITION.contains(Constant.WSDLTYPE_DOCLITERAL) || (Constant.WSDLDEFINITION.contains(Constant.WSDLTYPE_DOCLITERALWRAPPED))) {
			objxPath = new ExPathDoc()
		} else if (Constant.WSDLDEFINITION.contains(Constant.WSDL_RPCLITERAL) || (Constant.WSDLDEFINITION.contains(Constant.WSDLTYPE_RPCENCODING))) {
			objxPath = new ExPathRpc()
		}
		
		for(Field field : objxPath.getClass().getFields()) {
			if(field.getName().toString().equalsIgnoreCase(constNameReference)) {
				Field fieldName = objxPath.getClass().getDeclaredField(field.getName().toString());
				xPath = fieldName.get(objxPath)
				break
			}						  
		}
		return xPath
	}
}	


