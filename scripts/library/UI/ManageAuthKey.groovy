package library.UI


import java.io.File;
import java.lang.reflect.Method
import java.sql.ResultSet
import java.sql.ResultSetMetaData
import java.util.concurrent.TimeUnit;
import java.text.DecimalFormat

import library.ConnectDB;
import library.LogResult;
import library.UsageVerificationMethods
import library.ReadData
//import library.VerificationMethods


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Proxy
import org.openqa.selenium.TakesScreenshot
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.Proxy.ProxyType

import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.firefox.*
import org.openqa.selenium.ie.InternetExplorerDriver
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.remote.CapabilityType
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.support.ui.Select
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.support.ui.ExpectedConditions

import library.Constants.Constant
import library.Constants.ExpValueConstants
import library.Constants.ExPathRpc;
import library.Constants.ExPathRpcEnc;
import library.Constants.ExPathAdminToolsApi;
import library.Constants.Constant.RESULT_TYPE;

import org.openqa.selenium.support.PageFactory;
import groovy.util.XmlParser

class ManageAuthKey extends StatementTemplates {
	public static WebDriver driver = null;
	public static boolean appflag = false;
	//Proxy proxy = new Proxy();
	public DesiredCapabilities dc = null;
	String winHandleMain
	LogResult logi = new LogResult(Constant.logFilePath)
	UsageVerificationMethods u=new UsageVerificationMethods(db)
	By contentTitle = By.id ("title")
	
	public boolean invokeUIMethod(String methodname,String param)
	{
		boolean r
		try
		{
			Class[] parametersTypes = new Class[1]
			parametersTypes[0] = String.class;
			Object[] parameters = new Object[1]
			parameters[0]=param
			Method m= this.getClass().getDeclaredMethod(methodname,parametersTypes)
			r=m.invoke(this,parameters)
			logi.logInfo "Result from method "+r.toString()
		}catch(e)
		{

			logi.logInfo  e
		}
		return r
	}
	public String invokeUIMethod_AUT(String methodname)
	{
		logi.logInfo "Calling invokeUIMethod for method" +methodname
		String r=this."$methodname"()
		logi.logInfo "Result from method "+r
		return r
	}

	public void quitDriver()
	{
		try
		{
			//driver.close()
			driver.quit()
		}
		catch(Exception e)
		{
			logi.logInfo("quitDriver Method Exception: "+e.printStackTrace());
		}

	}
	public String getValueFromUI(String xpath){

		String splitted_xpath = null;
		splitted_xpath = xpath.split('#')[1]
		String value
		logi.logInfo("Splitted xpath is : "+splitted_xpath)

		try {
			value = driver.findElement(By.xpath(splitted_xpath)).getText()

			logi.logInfo("Got Value is  : "+value)

		} catch (Exception e) {
			e.printStackTrace()
			value ="NO VALUE"

		}

		return value
	}
	
	public boolean moveToWebServices( ) {
		logi.logInfo("Calling moveToWebServices");
		boolean success = false;

		//Store the current window handle
		winHandleMain = driver.getWindowHandle();
		logi.logInfo( "TESTCASEID ON "+Constant.TESTCASEID)

		String baseUrl = "https://secure.unstable.qa.ariasystems.net/ui/app.php"
		String[] acctSearch = baseUrl.split("/ui")
		String accountSearchURL = acctSearch[0]+"/ui/app.php/WebServiceAdministration"

		driver.get(accountSearchURL)
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		logi.logInfo("Navigated to Web Services API page");
		Thread.sleep(7000)
		success=true

		return success;
	}
	
	public String generateNewAuthKey()
	{
		
		logi.logInfo( "Calling Generate new auth key method")
		String newAutuKey = null
		boolean flg_search=false
		try {
			moveToWebServices()
			waitForTheElement(contentTitle)
			String header  = driver.findElement(contentTitle).getText()
			if(header.equals("Web Service API"))
			{
				logi.logInfo("Web services API page opened in the right panel")
				int count = 1
				while(count <= 2){
					logi.logInfo("Web services API 1"+driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText())
					if(driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText().equalsIgnoreCase("(none)")){
						logi.logInfo("going into break statement");
						break;
					}
					else
					{
						count++
					}
					
				}
				logi.logInfo("In exit count value: "+count);
				if(count>2){
					logi.logInfo("Client already having the maximum of auth key(2 no's). So regenerating in old auth key")
					return keyRegeneration(count-1)
				}else{
				driver.findElement(By.xpath("(//a[contains(text(),'Generate Key')])[" +count +"]")).click()
				logi.logInfo("Clicked on the Generate key button")
				Thread.sleep(5000)
				for(int i=0;i<2;i++){
					if(!driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText().equalsIgnoreCase("(none)")){
						flg_search = true
						break;
					}
					else
					Thread.sleep(2000)
				}
				
				if(flg_search){
					newAutuKey = driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText()
					logi.logInfo("Newly generated Auth key: "+newAutuKey)
				}
				}
			}
			else
				{
					logi.logInfo("Web service page not moved")
				}
			
		} catch (Exception  e) {
			logi.logInfo ("MethodEXception"+e.printStackTrace())
			flg_search = false;
		}
		return newAutuKey
	}
	
	
	public String keyRegeneration(int count){
		logi.logInfo( "Calling Rerenerate auth key method "+count)
		String newAuthKey = null
		String oldAuthKey = null
		boolean flg_search=false
		if(count<=2){
			oldAuthKey = driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText()
			logi.logInfo("Old key : "+driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText())
			driver.findElement(By.xpath("(//a[contains(text(),'Generate Key')])[" +count +"]")).click()
			logi.logInfo("Clicked on the Generate key button")
			Thread.sleep(2000)
			driver.findElement(By.xpath("(//button[@type='button'])[3]")).click()
			Thread.sleep(5000)
			
				if((!driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText().equalsIgnoreCase("(none)")) ||
				(!driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText().equalsIgnoreCase(oldAuthKey))){
					flg_search = true
				}
				
			
			
			if(flg_search){
				newAuthKey = driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText()
				logi.logInfo("Re-generated Auth key: "+newAuthKey)
			}
		}
		return newAuthKey
	}
	
	public String enableAuthKey(){
		logi.logInfo( "Calling enable auth key method ")
		String checkedprop = null
		int i=1
		boolean flg_search=false
		for(i=1;i<=2;i++){
			checkedprop = driver.findElement(By.xpath("(//th[@id='enabled']/input)["+i+"]")).getAttribute("checked")
			logi.logInfo("Test1 "+driver.findElement(By.xpath("(//th[@id='enabled']/input)["+i+"]")).getAttribute("checked"))
			if(checkedprop == null ||checkedprop == 'null'){
				if((driver.findElement(By.xpath("(//th[@id='authkey'])[" +i +"]")).getText().equalsIgnoreCase("(none)"))){
                                  driver.findElement(By.xpath("(//a[contains(text(),'Generate Key')])[" +i +"]")).click()
                                  logi.logInfo("Clicked on the Generate key button")
                                  Thread.sleep(2000)
                           }
				else{
				Actions action = new Actions(driver);
				action.moveToElement(driver.findElement(By.xpath("(//th[@id='enabled']/input)["+i+"]"))).click().build().perform();
				logi.logInfo("Clicked on the check box to enable the Auth key")
				Thread.sleep(5000)			
				}	
				logi.logInfo("Enabled Auth key: "+driver.findElement(By.xpath("(//th[@id='authkey'])[" +i +"]")).getText())
				return driver.findElement(By.xpath("(//th[@id='authkey'])[" +i +"]")).getText()
			}
			
		}
		if(i>2){
			logi.logInfo("Auth keys are already enabled")
			return driver.findElement(By.xpath("(//th[@id='authkey'])[" +(i-1) +"]")).getText()
		}
		return "trtse"
	}
	
	public String disableAuthKey(){
		logi.logInfo( "Calling disable auth key method ")
		String checkedprop = null
		int i=1
		boolean flg_search=false
		for(i=1;i<=2;i++){
			checkedprop = driver.findElement(By.xpath("(//th[@id='enabled']/input)["+i+"]")).getAttribute("checked")
			logi.logInfo("Test1 "+driver.findElement(By.xpath("(//th[@id='enabled']/input)["+i+"]")).getAttribute("checked"))
			if(checkedprop == null ||checkedprop == 'null'){
				
				logi.logInfo("Already an Auth key was disabled. So returned the disabled auth key")
				
				logi.logInfo("Enabled Auth key: "+driver.findElement(By.xpath("(//th[@id='authkey'])[" +i +"]")).getText())
				return driver.findElement(By.xpath("(//th[@id='authkey'])[" +i +"]")).getText()
			}
			
		}
		if(i>2){
			logi.logInfo("Both Auth keys are already enabled. So disabling a auth key and return it")
			driver.findElement(By.xpath("(//th[@id='enabled']/input)["+(i-1)+"]")).click()
			logi.logInfo("Clicked on the check box for disabling a auth key")
			return driver.findElement(By.xpath("(//th[@id='authkey'])[" +(i-1) +"]")).getText()
		}
		return "trtse"
	}
	
	
	public LinkedHashMap auth_verifyAuthKeyDB(String tcID){
		logi.logInfo( "Verify the Auth key DB")
		ConnectDB db = new ConnectDB()
		LinkedHashMap finalMap = new LinkedHashMap<String , HashMap<String,String>>()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		
		String query = "select auth_key,enabled_ind from ariacore.client_auth_keys where client_no="+clientNo
		ResultSet resultSet = db.executePlaQuery(query);
		int count =1
		while (resultSet.next()){
			HashMap temp = new HashMap<String,String>()
			temp.put(resultSet.getString(1),resultSet.getString(2));
			logi.logInfo("Test temp DB 1 : "+temp.toString())
			finalMap.put("Auth Key "+count+": ",temp)
			count++
		}
	return finalMap.sort()

	}
	
	public LinkedHashMap auth_verifyAuthKey_UI(String tcId){
		logi.logInfo( "Verify the Auth key UI")
		LinkedHashMap finalMap = new LinkedHashMap<String , HashMap<String,String>>()
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		int count =0
		String checkedprop = null
		for(int i=1;i<=2;i++){
			HashMap temp = new HashMap<String,String>()
			if((!driver.findElement(By.xpath("(//th[@id='authkey'])[" +i +"]")).getText().equalsIgnoreCase("(none)"))){
				
			count++
			checkedprop = driver.findElement(By.xpath("(//th[@id='enabled']/input)["+i+"]")).getAttribute("checked")
			logi.logInfo("Test1 "+driver.findElement(By.xpath("(//th[@id='enabled']/input)["+i+"]")).getAttribute("checked"))
			if(checkedprop == null ||checkedprop == 'null'){
				
				temp.put(driver.findElement(By.xpath("(//th[@id='authkey'])[" +i +"]")).getText(),"0");
				finalMap.put("Auth Key "+count+": ",temp)
				
				
			}else{
			temp.put(driver.findElement(By.xpath("(//th[@id='authkey'])[" +i +"]")).getText(),"1");
			logi.logInfo("Test temp UI 1 : "+temp.toString())
			finalMap.put("Auth Key "+count+": ",temp)
			}
			}
			else{
			logi.logInfo("Client Have having only one Auth key")
			}
		}
			
		return finalMap
		}
	
	
	public String getAuthKey(int keyIndex) {
		String authKey = driver.findElement(By.xpath("(//th[@id='authkey'])[" +keyIndex +"]")).getText()
		return authKey
	}
	
	public String getFirstAuthKey() {
		return getAuthKey(1)
	}
	
	public String getSecondAuthKey() {
		return getAuthKey(2)
	}
	
	public String disableAuthKey(int keyIndex) {
		driver.findElement(By.xpath("(//th[@id='enabled']/input)["+keyIndex+"]")).click()
		Thread.sleep(5000)
		return 'true'
	}
	
	public String disableFirstAuthKey() {
		return disableAuthKey(1)
	}
	
	public String disableSecondAuthKey() {
		return disableAuthKey(2)
	}
	
	public String keyRegeneration(){
		logi.logInfo( "Calling Rerenerate auth key method ")
		String newAuthKey = null
		String oldAuthKey = null
		boolean flg_search=false
		for(int count=1 ; count <=2 ; count++){
			oldAuthKey = driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText()
			driver.findElement(By.xpath("(//a[contains(text(),'Generate Key')])[" +count +"]")).click()
			logi.logInfo("Clicked on the Generate key button")
			Thread.sleep(2000)
			driver.findElement(By.xpath("(//button[@type='button'])[3]")).click()
			Thread.sleep(5000)
			
				if((!driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText().equalsIgnoreCase("(none)")) ||
				(!driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText().equalsIgnoreCase(oldAuthKey))){
					flg_search = true
				}
				
			
			
			if(flg_search){
				newAuthKey = driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText()
				logi.logInfo("Re-generated Auth key: "+newAuthKey)
			}
		}
	}
	
	
	public String keyRegenerationAndReturnOld(int count){
		logi.logInfo( "Calling Rerenerate auth key method ")
		String newAuthKey = null
		String oldAuthKey = null
		boolean flg_search=false
		if(count <= 1){
			oldAuthKey = driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText()
			driver.findElement(By.xpath("(//a[contains(text(),'Generate Key')])[" +count +"]")).click()
			logi.logInfo("Clicked on the Generate key button")
			Thread.sleep(2000)
			driver.findElement(By.xpath("(//button[@type='button'])[3]")).click()
			Thread.sleep(5000)
			
				if((!driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText().equalsIgnoreCase("(none)")) ||
				(!driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText().equalsIgnoreCase(oldAuthKey))){
					flg_search = true
				}
				
			
			
			if(flg_search){
				newAuthKey = driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText()
				logi.logInfo("Re-generated Auth key: "+newAuthKey)
			}
		}
		return oldAuthKey
	}
	
	public String keyRegenerateFirstAndReturnOldKey() {
		return keyRegenerationAndReturnOld(1)
	}
	
	public String keyRegenerateSecondAndReturnOldKey() {
		return keyRegenerationAndReturnOld(2)
	}
	
	public String deleteAuthKeyAndReturnOld(int count){
		logi.logInfo( "Calling Delete auth key method ")
		String newAuthKey = null
		String oldAuthKey = null
		boolean flg_search=false
		if(count <= 2){
			oldAuthKey = driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText()
			driver.findElement(By.xpath("(//th[@id='delete']/a)[" +count +"]")).click()
			logi.logInfo("Clicked on the Generate key button")
			Thread.sleep(2000)
			driver.findElement(By.xpath("(//button[@type='button'])[3]")).click()
			Thread.sleep(5000)
			
				if((driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText().equalsIgnoreCase("(none)")) ||
				(!driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText().equalsIgnoreCase(oldAuthKey))){
					flg_search = true
				}
				
			if(flg_search){
				newAuthKey = driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText()
				logi.logInfo("Re-generated Auth key: "+newAuthKey)
			}
		}
		return oldAuthKey
	}
	
	public String deleteFirstAuthkeyAndReturnOldKey() {
		deleteAuthKeyAndReturnOld(1)
	}
	
	public String deleteSecondAuthkeyAndReturnOldKey() {
		deleteAuthKeyAndReturnOld(2)
	}
	
	
	public String deleteAuthKeyNotSuccess(int count){
		logi.logInfo( "Calling Delete auth key method ")
		String newAuthKey = null
		String oldAuthKey = null
		boolean flg_search=false
		if(count <= 2){
			oldAuthKey = driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText()
			driver.findElement(By.xpath("(//th[@id='delete']/a)[" +count +"]")).click()
			logi.logInfo("Clicked on the delete key button")
			Thread.sleep(2000)
			logi.logInfo("Error message displayed as "+driver.findElement(By.cssSelector("div.aria_status_warning.action_status > p")).getText())
			Thread.sleep(5000)
			
				if((driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText().equalsIgnoreCase("(none)"))){
					flg_search = true
				}
				
			if(flg_search){
				newAuthKey = driver.findElement(By.xpath("(//th[@id='authkey'])[" +count +"]")).getText()
				logi.logInfo("Re-generated Auth key: "+newAuthKey)
			}
		}
		return oldAuthKey
	}
	
	
	public String deleteFirstAuthkeyNotSuccess() {
		deleteAuthKeyNotSuccess(1)
	}
	
	public String deleteSecondAuthkeyNotSuccess() {
		deleteAuthKeyNotSuccess(2)
	}
	
	}
