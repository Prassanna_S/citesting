public  LinkedHashMap md_VerifyMPIAllTransactionLoop_m(String input)
 {
				 logi.logInfo("md_VerifyMPIAllTransactionLoop_m")
				 ConnectDB db = new ConnectDB()
				 int j=1,i=1
				 ArrayList<String> list = new ArrayList<String>()
				 LinkedHashMap <String, String> row = new LinkedHashMap<String, String>();
				 LinkedHashMap<String, HashMap> outer = new LinkedHashMap<String, HashMap>();
				 def client_no=Constant.mycontext.expand('${Properties#Client_No}')
				 logi.logInfo("client_no" +client_no )
				 def plan_instance
				 if(input.contains("#")){
					 plan_instance = im.md_GET_ACCT_OR_INSTANCE_NO2_m(input,null);
				 }else{
				 plan_instance = im.md_GET_ACCT_OR_INSTANCE_NO_m(input,null);
				 }
				 logi.logInfo("plan_instance" +plan_instance )
				 String query ="select max(invoice_no) as invoice_no from ariacore.gl_detail where master_plan_instance_no = " + plan_instance + " and client_no = "+client_no
				 String invoice_no=db.executeQueryP2(query)
				 String query3 = "Select acct_no from ariacore.plan_instance where plan_instance_no = "+ plan_instance +"and client_no=" +client_no
				 String acct_no =db.executeQueryP2(query3)
				 logi.logInfo("acct_no " +acct_no )
				 logi.logInfo("invoice_no" +invoice_no )
				 String query1 = "Select STATEMENT_NO from ariacore.acct_statement where invoice_no =" +invoice_no+ " and acct_no = "+ acct_no +"and client_no=" +client_no
				 String statement_no=db.executeQueryP2(query1)
				 logi.logInfo("statement_no" +statement_no )
				 //event no retrieve
				 String query4 = "select event_no from ariacore.acct_transaction where acct_no = "+acct_no+" and transaction_type in (22,21,3,-3,15,-21,-10,-6,-2,2,6,10,12,13) and statement_no = "+statement_no
				 ResultSet rs=db.executePlaQuery(query4)
				 while(rs.next())
				 {
					 
					 list[i]=rs.getObject(1)
					 i++
				 }
				 int sizecount = list.size()
				 logi.logInfo("sizecount" + sizecount)
				 for(int f=1;f<=sizecount-1;f++)
				 {
				 //line item details query
					 logi.logInfo "event no "+list[f]
				 String query2="SELECT gld.MASTER_PLAN_INSTANCE_NO AS MPINo,CASE WHEN gld.PLAN_NO IS NOT NULL THEN pi.PLAN_INSTANCE_CDID ELSE NULL END  AS ClientMPIId,"+
								" at.EVENT_NO AS MPITxnNo,tt.DESCRIPTION AS MPITxnTypeDesc,TO_CHAR(TO_DATE(TO_CHAR(AT.CREATE_DATE,'MM/fmDD/YYYY'),'MM/DD/YYYY'),'FMMM/DD/YYYY') AS MPITxnDt,"+
								" CASE WHEN gld.SERVICE_NO IS NOT NULL THEN gld.SERVICE_NO ELSE 0 END AS ItemServiceNo,"+
								" gld.PLAN_NO AS ItemPlanNo, gld.PLAN_NAME AS ItemPlanName,CASE WHEN AT.TRANSACTION_TYPE IN (7,10,-10,11,-21,15,22,23,24,12,13,3,-3,-6,-2,2,-12,-13) THEN 'Credit' ELSE aid.COMMENTS END  AS ItemSimpleLabel,"+
								" case when aid.service_no is null then '0.00' else (to_char(case when "+
								" (select sum(debit) as taxsum from ariacore.gl_tax_detail where invoice_no = gld.invoice_no and taxed_seq_num = gld.seq_num and client_no = at.client_no) is null then aid.debit"+
								" else (aid.debit + (select sum(debit) as taxsum from ariacore.gl_tax_detail where invoice_no = gld.invoice_no and taxed_seq_num = gld.seq_num and client_no = at.client_no))"+
								" end,'fm999990.90')) end  as itemgrosstotal,"+
								" TO_CHAR(TO_DATE(TO_CHAR(gld.START_DATE,'MM/fmDD/YYYY'),'MM/DD/YYYY'),'FMMM/DD/YYYY') AS ItemStartDate,"+
								" TO_CHAR(TO_DATE(TO_CHAR(gld.END_DATE,'MM/fmDD/YYYY'),'MM/DD/YYYY'),'FMMM/DD/YYYY')   AS ItemEndDate,"+
								" (TRUNC(gld.END_DATE)-TRUNC(gld.START_DATE))+1 AS ITEMPERIODDAYS, gld.TIER_BASE_PLAN_UNITS AS ItemBaseUnits,"+
								" CASE WHEN gld.MASTER_PLAN_INSTANCE_NO != gld.PLAN_INSTANCE_NO THEN gld.PLAN_INSTANCE_NO  ELSE NULL  END AS SPINo,"+
								" (select (DEBIT-OFFSET_DISC)as net_price from ariacore.gl_detail_after_credit"+
								"	where client_no = at.client_no and invoice_no = at.invoice_no and invoice_detail_no = at.source_no) as ITEMNETPRICE,"+
								"	(select (DEBIT-OFFSET_DISC)as net_price from ariacore.gl_detail_after_credit"+
								"	where client_no = at.client_no and invoice_no = at.invoice_no and invoice_detail_no = at.source_no) as ITEMMONTHLYAMOUNT,"+
								" CASE WHEN gld.MASTER_PLAN_INSTANCE_NO != gld.PLAN_INSTANCE_NO  THEN ARIACORE.eom_planmgr.get_plan_inst_id (gld.client_no,gld.PLAN_INSTANCE_NO,at.acct_no)"+
								" ELSE NULL  END AS ClientSPIId,gld.COMMENTS AS ItemComments,CASE WHEN AID.PLAN_NAME IS NULL AND AID.COMMENTS IS NULL THEN 'Credit' else aid.plan_name||' '||aid.comments end AS ItemPlanandComments,"+
								" gld.USAGE_UNITS AS ItemUnits,CASE WHEN AID.USAGE_RATE IS NULL THEN '0.00' ELSE TO_CHAR(aid.USAGE_RATE,'fm99999.90')END AS ItemRate,"+
								" CASE WHEN gld.RATE_SCHEDULE_NO IS NOT NULL AND GLD.SURCHARGE_RATE_SEQ_NO IS NULL THEN '1+' ELSE NULL END AS ItemTierLimits,"+
								" CASE WHEN gld.SURCHARGE_RATE_SEQ_NO IS NOT NULL THEN (select SURCHARGE_NO from ariacore.CLIENT_SURCHARGE WHERE ALT_SERVICE_NO_2_APPLY = GLD.SERVICE_NO AND CLIENT_NO = GLD.CLIENT_NO)"+
								" ELSE NULL END AS ItemSurchargeNo,case when (select sum(debit) as taxsum from ariacore.gl_tax_detail where invoice_no = gld.invoice_no and taxed_seq_num = gld.seq_num and client_no = at.client_no) is null then '0.00'"+
								" else to_char((select sum(debit) as taxsum from ariacore.gl_tax_detail where invoice_no = gld.invoice_no and taxed_seq_num = gld.seq_num and client_no = at.client_no),'fm99990.90')end as ItemTaxTotal,"+
								" gld.TIER_BASE_PLAN_UNITS AS ItemTierBasePlanUnits,"+
								" case when ( select count(taxed_seq_num) from ariacore.gl_tax_detail where invoice_no = gld.invoice_no and taxed_seq_num = gld.seq_num and client_no = at.client_no  ) > 1"+
								" then '[Multiple Taxes]' ELSE (select tax_type from ariacore.gl_tax_detail where invoice_no = gld.invoice_no and taxed_seq_num = gld.seq_num and client_no = at.client_no) end as ItemTaxType,"+
								" case when gld.seq_num not in (select taxed_seq_num from ariacore.gl_tax_detail where invoice_no = gld.invoice_no and client_no = gld.client_no)then '0.00' else case when ( select count(taxed_seq_num) from ariacore.gl_tax_detail where invoice_no = gld.invoice_no and taxed_seq_num = gld.seq_num and client_no = at.client_no  ) > 1"+
								" then '[Many]' ELSE to_char((select (tax_rate*100) as taxrate from ariacore.gl_tax_detail where invoice_no = gld.invoice_no and taxed_seq_num = gld.seq_num and client_no = at.client_no),'fm99990.90') end end AS ItemTaxRate,CASE WHEN cs.TAXABLE_IND = 1 THEN '2' ELSE TO_CHAR(CS.TAXABLE_IND) END as ItemTaxTInd,"+
								" TO_CHAR(((gld.PRORATION_FACTOR*100)),'fm9999.90') AS ItemPercentProrationFactor,TO_CHAR(gld.PRORATION_FACTOR,'fm9.9000000000') AS ItemProrationFactor,"+
								" GLD.ORIG_CHARGE_DETAIL_NO AS ItemOrigChargeInvoiceNo, at.ORIG_EVENT_NO as ItemOrigChargeTxnNo,rs.SCHEDULE_NAME AS RateScheduleDescription,"+
								" rs.CLIENT_RATE_SCHEDULE_ID AS ClientRateScheduleId,GLD.ORIG_COUPON_CD AS ItemCouponCode,CP.COMMENTS AS ItemCouponDesc,CD.REASON_CD AS ItemCreditReasonCode,"+
								" CD.COMMENTS AS ItemCreditReasonText,"+
								" (select TO_CHAR(sum(DECODE (tt2.is_charge, 1, acct_txn.amount, -1 * acct_txn.amount)),'fm99990.90') as txnamount from ariacore.acct_transaction acct_txn left join ariacore.transaction_types tt2 on acct_txn.transaction_type = tt2.transaction_type where acct_no = at.acct_no and statement_no = at.statement_no) AS TotalBillingAmount,case when at.transaction_type in (3) then TO_CHAR((at.amount*-1),'fm99990.90') else TO_CHAR(at.amount,'fm99990.90')end AS MPITxnAmt"+
								" FROM ariacore.acct_transaction at"+
								" LEFT JOIN ariacore.gl_detail gld"+
								" ON at.client_no   = gld.client_no"+
								" AND at.invoice_no = gld.invoice_no"+
								" AND at.source_no  =gld.invoice_detail_no"+
								" LEFT OUTER JOIN ariacore.plan_instance pi"+
								" ON gld.MASTER_PLAN_INSTANCE_NO=pi.plan_instance_no"+
								" LEFT JOIN ariacore.all_invoice_details aid ON at.client_no   = aid.client_no AND at.invoice_no = aid.invoice_no and at.source_no  =aid.invoice_detail_no LEFT OUTER JOIN ariacore.transaction_types tt"+
								" ON at.TRANSACTION_TYPE =tt.TRANSACTION_TYPE"+
								" LEFT OUTER JOIN ariacore.client_service cs"+
								" on gld.service_no = cs.service_no"+
								" and gld.client_no = cs.client_no"+
								" LEFT OUTER JOIN ariacore.NEW_RATE_SCHEDULES rs"+
								" ON gld.RATE_SCHEDULE_NO =rs.schedule_no"+
								" AND gld.client_no       =rs.client_no"+
								" LEFT OUTER JOIN ARIACORE.COUPONS CP ON"+
								" CP.COUPON_CD = GLD.ORIG_COUPON_CD AND CP.CLIENT_NO = GLD.CLIENT_NO"+
								" LEFT OUTER JOIN ARIACORE.CREDITS CD ON"+
								" GLD.ORIG_CREDIT_ID = CD.CREDIT_ID AND CD.CLIENT_NO = GLD.CLIENT_NO"+
								" WHERE at.EVENT_NO ="+list[f]+
								" ORDER BY at.master_plan_instance_no,at.event_no"
				 ResultSet rs4=db.executePlaQuery(query2)
				 ResultSetMetaData md4 = rs4.getMetaData();
				 int columns4 = md4.getColumnCount();
				 while (rs4.next())
				 {
									   logi.logInfo "inside while"
								 for(int l=1; l<=columns4; l++)
								 {
															  // logi.logInfo "inside for"
												 if((rs4.getObject(l))!=null)
												 {
																 
																				 row.put((md4.getColumnName(l)),(rs4.getObject(l)).toString().trim());
																 
												 
												 }
												 /*if(rs4.getObject('ITEMSIMPLELABEL')=="" || rs4.getObject('ITEMSIMPLELABEL')==null)
																				 
															  {
																											//logi.logInfo "inside ITEMPLANANDCOMMENTS "
																			  row.put("ITEMPLANANDCOMMENTS","Credit")
																			  row.put("ITEMSIMPLELABEL","Credit")
																															  logi.logInfo "inside ITEMPLANANDCOMMENTS 22222"
																			  
															  }
																									  if(rs4.getObject('ITEMRATE')==null)
															  {
																											//logi.logInfo "inside ITEMPLANANDCOMMENTS 1"
																			  row.put("ITEMRATE","0.00")
															  }
																									  if(rs4.getObject('ITEMGROSSTOTAL')==null)
																									  {
																											//logi.logInfo "inside ITEMPLANANDCOMMENTS 1"
																															  row.put("ITEMGROSSTOTAL","0.00")
																									  }
															 if(rs4.getObject('ITEMTAXTOTAL')==null)
															  {
																											//logi.logInfo "inside ITEMPLANANDCOMMENTS 2"
																			  row.put("ITEMTAXRATE","0.00")
																			  row.put("ITEMTAXTOTAL","0.00")
															  }*/
															  
															  /*if(rs4.getObject('ITEMTIERLIMITS')!=null)
															  {
																  logi.logInfo "inside ITEMPLANANDCOMMENTS 3"
																  String senior_acct = (String)row.get("ITEMTIERLIMITS")
																  logi.logInfo "inside senior_acct 2"+senior_acct
																  row.put("ITEMTIERLIMITS",senior_acct+'+');
															  }*/
															  
								 }
								/* if(rs4.getObject('ITEMTIERLIMITS')!=null)
								 {
									 //logi.logInfo "inside ITEMPLANANDCOMMENTS 3"
									 String senior_acct = (String)row.get("ITEMTIERLIMITS")
									 logi.logInfo "inside senior_acct 2"+senior_acct
									 row.put("ITEMTIERLIMITS",senior_acct+'+');
								 }
								 if((rs4.getObject('TRANSACTION_TYPE')== -21)||(rs4.getObject('TRANSACTION_TYPE')== 3))
								 {
									 logi.logInfo "inside ITEMPLANANDCOMMENTS 2"
									 
									 
									 String senior_acct1 = (String)row.get("MPITXNAMT")
									 logi.logInfo "inside senior_acct 2"+senior_acct1
									 row.put("MPITXNAMT",'-'+senior_acct1);
									 
								 }*/
									 
								 
								 outer.put(list[f].toString().trim(),row.sort())
								 row = new LinkedHashMap<String, String>();
								 j++
				 
				 }
				 }
				 return outer.sort()
 }