package library

import java.awt.geom.Arc2D.Double;
import java.lang.reflect.Method
import java.sql.ResultSet;
import java.sql.ResultSetMetaData
import java.sql.SQLException
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.text.SimpleDateFormat
import java.text.DateFormat
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import groovy.xml.StreamingMarkupBuilder

import org.apache.ivy.core.module.descriptor.ExtendsDescriptor;

import library.Constants.Constant;
import library.Constants.ExpValueConstants;
import library.Constants.ExPathRpcEnc;
import library.Constants.Constant.RESULT_TYPE;
import library.VerificationMethods
import library.Constants.ExPathAdminToolsApi;
import library.ReadData


public class AdminToolsVerificationMethods extends  KeyAPIsVerificationMethods {
	LogResult logi = new LogResult(Constant.logFilePath)
	static ConnectDB db = new ConnectDB();
	public DecimalFormat df=new DecimalFormat("#.##")

	public AdminToolsVerificationMethods(ConnectDB db) {
		super(db)
	}

	def md_inventory_details_from_API(String testcaseid) {
		LinkedHashMap <String, String> resultHash = new LinkedHashMap<String, String>()
		String active_ind = getValueFromHttpRequestValue("create_inventory_item", "active_ind")
		String item_type = getValueFromHttpRequestValue("create_inventory_item", "item_type")
		String subunit_qty = getValueFromHttpRequestValue("create_inventory_item", "subunit_qty")
		String client_item_id = getValueFromHttpRequestValue("create_inventory_item", "client_item_id")
		String stock_level_track = getValueFromHttpRequestValue("create_inventory_item", "stock_level_track")
		logi.logInfo("stock_level_track: " + stock_level_track)
		String stock_level_adjust = getValueFromHttpRequestValue("create_inventory_item", "stock_level_adjust")
		//HashMap resultHash = new HashMap<String, String>()
		resultHash.put("Item No", getValueFromResponse("create_inventory_item", ExPathAdminToolsApi.CREATE_INVENTORY_ITEM_ITEM_NO1))
		resultHash.put("Label", getValueFromHttpRequestValue("create_inventory_item", "plan_name"))
		resultHash.put("Client Sku", getValueFromHttpRequestValue("create_inventory_item", "plan_name"))
		resultHash.put("Item type", (item_type.toInteger()?1:0))
		resultHash.put("Description", getValueFromHttpRequestValue("create_inventory_item", "item_desc"))
		resultHash.put("Service No", getValueFromHttpRequestValue("create_inventory_item", "service[service_no]"))
		resultHash.put("Active Index", (active_ind.toInteger()?0:1))
		resultHash.put("Subunit Qty", subunit_qty==null?'null':subunit_qty)
		resultHash.put("Subunit Label", subunit_qty==null?'null':getValueFromHttpRequestValue("create_inventory_item", "subunit_label"))
		resultHash.put("Client Defined ID", client_item_id==null?"OQNQU01_"+(getValueFromResponse("create_inventory_item", ExPathAdminToolsApi.CREATE_INVENTORY_ITEM_ITEM_NO1)):client_item_id)

		//NEW

		//resultHash.put("Stock Level Track", (stock_level_track==null)?'null':(stock_level_track.toInteger()?0:'null'))
		if(stock_level_track == "0")
		{
			resultHash.put("Stock Level Track", null)
		}
		else if (stock_level_track == "1" && stock_level_adjust == null)
		{
			resultHash.put("Stock Level Track", "0")
		}
		else if (stock_level_track == "1" && stock_level_adjust != null || stock_level_adjust != "0")
		{
			resultHash.put("Stock Level Track", stock_level_adjust)
		}
		logi.logInfo("Hashmap is : " + resultHash)
		return resultHash.sort()
	}

	def md_update_inventory_details_from_API(String testcaseid) {
		LinkedHashMap <String, String> resultHash = new LinkedHashMap<String, String>()
		String active_ind = getValueFromHttpRequestValue("update_inventory_item", "active_ind")
		String item_type = getValueFromHttpRequestValue("create_inventory_item", "item_type")
		String subunit_qty = getValueFromHttpRequestValue("update_inventory_item", "subunit_qty")
		String client_item_id = getValueFromHttpRequestValue("create_inventory_item", "client_item_id")
		String update_client_item_id = getValueFromHttpRequestValue("update_inventory_item", "client_item_id")
		String stock_level_track = getValueFromHttpRequestValue("update_inventory_item", "stock_level_track")
		String stock_level_adjust = getValueFromHttpRequestValue("update_inventory_item", "stock_level_adjust")
		logi.logInfo("stock_level_track: " + stock_level_track)
		//HashMap resultHash = new HashMap<String, String>()
		resultHash.put("Item No", getValueFromResponse("update_inventory_item", ExPathAdminToolsApi.UPDATE_INVENTORY_ITEM_ITEM_NO1))
		resultHash.put("Label", getValueFromHttpRequestValue("create_inventory_item", "plan_name"))
		resultHash.put("Client Sku", getValueFromHttpRequestValue("create_inventory_item", "plan_name"))
		resultHash.put("Item type", (item_type.toInteger()?1:0))
		resultHash.put("Description", getValueFromHttpRequestValue("update_inventory_item", "item_desc"))
		resultHash.put("Service No", getValueFromHttpRequestValue("update_inventory_item", "service[service_no]"))
		resultHash.put("Active Index", (active_ind.toInteger()?0:1))
		resultHash.put("Subunit Qty", subunit_qty==null?'null':subunit_qty)
		resultHash.put("Subunit Label", subunit_qty==null?'null':getValueFromHttpRequestValue("update_inventory_item", "subunit_label"))
		resultHash.put("Client Defined ID", client_item_id==null?"OQNQU01_"+(getValueFromResponse("create_inventory_item", ExPathAdminToolsApi.CREATE_INVENTORY_ITEM_ITEM_NO1)):client_item_id)

		//NEW
		if(update_client_item_id != null)
		{
			resultHash.put("Client Defined ID", getValueFromHttpRequestValue("update_inventory_item", "client_item_id"))
		}

		if(stock_level_track == "0")
		{
			resultHash.put("Stock Level Track", null)
		}
		else if (stock_level_track == "1" && stock_level_adjust == null)
		{
			resultHash.put("Stock Level Track", "0")
		}
		else if (stock_level_track == "1" && stock_level_adjust != null || stock_level_adjust != "0")
		{
			resultHash.put("Stock Level Track", stock_level_adjust)
		}
		//resultHash.put("Stock Level Track", (stock_level_track==null)?'null':(stock_level_track.toInteger()?0:'null'))
		logi.logInfo("Hashmap is : " + resultHash)
		return resultHash.sort()
	}


	def md_inventory_details_from_DB(String testcaseid) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()

		String item_no = getValueFromResponse("create_inventory_item", ExPathAdminToolsApi.CREATE_INVENTORY_ITEM_ITEM_NO1)
		String client_no = getValueFromHttpRequestValue("create_inventory_item", "client_no")
		logi.logInfo("Item and Client number : " + item_no + " :: " + client_no)

		ResultSet rs = db.executeQuery("SELECT * FROM ARIACORE.INVENTORY_ITEMS where ITEM_NO= " + item_no + " AND CLIENT_NO=" + client_no)
		while(rs.next()) {
			hashResult.put("Item No", rs.getString('ITEM_NO'))
			hashResult.put("Label", rs.getString('LABEL'))
			hashResult.put("Client Sku", rs.getString('CLIENT_SKU'))
			hashResult.put("Item type", rs.getString('ARC_IND'))
			hashResult.put("Description", rs.getString('LONG_DESC'))
			hashResult.put("Service No", rs.getString('SERVICE_NO'))
			hashResult.put("Active Index", rs.getString('NO_DISPLAY_IND'))
			hashResult.put("Subunit Qty", rs.getString('SUBUNIT_QTY'))
			hashResult.put("Subunit Label", rs.getString('SUBUNIT_LABEL'))
			hashResult.put("Client Defined ID", rs.getString('CLIENT_ITEM_ID'))
			hashResult.put("Stock Level Track", rs.getString('STOCK_LEVEL'))
		}
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}

	def md_inventory_price_details_from_API(String testcaseid) {
		String currency_cd = getValueFromHttpRequestValue("create_inventory_item", "currency_cd")
		String item_price = getValueFromHttpRequestValue("create_inventory_item", "item_price")
		String updated_item_price = getValueFromHttpRequestValue("update_inventory_item", "item_price")
		String updated_currency_cd = getValueFromHttpRequestValue("update_inventory_item", "currency_cd")
		if(updated_item_price == null)
		updated_item_price=item_price
		String tax_inclusive_ind = getValueFromHttpRequestValue("create_inventory_item", "tax_inclusive_ind")
		String updated_tax_inclusive_ind = getValueFromHttpRequestValue("update_inventory_item", "tax_inclusive_ind")
		logi.logInfo("Updated tax inclusive ind : " + updated_tax_inclusive_ind)
		if((updated_tax_inclusive_ind == null) || (updated_tax_inclusive_ind == null))
		updated_tax_inclusive_ind = tax_inclusive_ind
		logi.logInfo("Updated tax inclusive ind After :: " + updated_tax_inclusive_ind)
		HashMap <String, String> resultHash = new HashMap<String, String>()
		resultHash.put("TAX_INCLUSIVE_IND", (tax_inclusive_ind.toInteger() == updated_tax_inclusive_ind.toInteger())?tax_inclusive_ind:updated_tax_inclusive_ind)
		if(updated_currency_cd != null)
		currency_cd=updated_currency_cd
		resultHash.put("CURRENCY_CD", currency_cd)
		if(currency_cd == "eur")
		{
			updated_item_price = updated_item_price.replace(",", "comma")
		}
		resultHash.put("PRICE", (item_price.toString() == updated_item_price.toString())?item_price:updated_item_price)
		return resultHash.sort()
	}

	def md_inventory_price_details_from_DB(String testcaseid) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()

		String item_no = getValueFromResponse("create_inventory_item", ExPathAdminToolsApi.CREATE_INVENTORY_ITEM_ITEM_NO1)
		
		String client_no = getValueFromHttpRequestValue("create_inventory_item", "client_no")
		logi.logInfo("Item and Client number : " + item_no + " :: " + client_no)
		String currency_cd = getValueFromHttpRequestValue("create_inventory_item", "currency_cd")
		String item_price = getValueFromHttpRequestValue("create_inventory_item", "item_price")
		String updated_item_price = getValueFromHttpRequestValue("update_inventory_item", "item_price")
		String updated_currency_cd = getValueFromHttpRequestValue("update_inventory_item", "currency_cd")
		if(updated_item_price == null)
		updated_item_price=item_price
		if(updated_currency_cd != null)
		currency_cd=updated_currency_cd

		ResultSet rs = db.executeQuery("SELECT * from ariacore.inventory_item_prices where item_no=" + item_no + " AND CLIENT_NO=" + client_no)
		while(rs.next()) {
			String dbCurrency = rs.getString('CURRENCY_CD')
			String dbPrice = rs.getString('PRICE')
			if(dbCurrency == currency_cd)
			{
				hashResult.put("TAX_INCLUSIVE_IND", rs.getString('TAX_INCLUSIVE_IND'))
				hashResult.put("CURRENCY_CD", rs.getString('CURRENCY_CD'))
				if(dbCurrency == "eur")
				{
					dbPrice = dbPrice.replace(".", "comma")
				}
				hashResult.put("PRICE", dbPrice)
			}
		}
		//logi.logInfo("Hashmap is : " + hashResult)

		return hashResult.sort()
	}

	def md_inventory_image_details_from_API(String testcaseid) {
		String image_url = getValueFromHttpRequestValue("create_inventory_item", "image[0][main_image_url]")
		String thumb_image_url = getValueFromHttpRequestValue("create_inventory_item", "image[0][thumbnail_image_url]")
		String image_text = getValueFromHttpRequestValue("create_inventory_item", "image[0][image_text]")
		String default_image_index = getValueFromHttpRequestValue("create_inventory_item", "image[0][default_ind]")
		String active_image_index = getValueFromHttpRequestValue("create_inventory_item", "image[0][active]")

		HashMap <String, String> resultHash = new HashMap<String, String>()
		resultHash.put("IMAGE_URL0", image_url)
		resultHash.put("IMAGE_URL1", thumb_image_url)
		resultHash.put("IMAGE_TEXT0", image_text)
		resultHash.put("IMAGE_TEXT1", image_text)
		resultHash.put("DEFAULT_IND0", default_image_index)
		resultHash.put("DEFAULT_IND1", default_image_index)
		resultHash.put("DISPLAY_IND0", active_image_index)
		resultHash.put("DISPLAY_IND1", active_image_index)
		return resultHash.sort()
	}

	def md_update_inventory_image_details_from_API(String testcaseid) {
		String image_url1 = getValueFromHttpRequestValue("update_inventory_item", "image[0][main_image_url]")
		String thumb_image_url1 = getValueFromHttpRequestValue("update_inventory_item", "image[0][thumbnail_image_url]")
		String image_text1 = getValueFromHttpRequestValue("update_inventory_item", "image[0][image_text]")
		String default_image_index1 = getValueFromHttpRequestValue("update_inventory_item", "image[0][default_ind]")
		String active_image_index1 = getValueFromHttpRequestValue("update_inventory_item", "image[0][active]")

		HashMap <String, String> resultHash = new HashMap<String, String>()
		resultHash.put("IMAGE_URL0", image_url1)
		resultHash.put("IMAGE_URL1", thumb_image_url1)
		resultHash.put("IMAGE_TEXT0", image_text1)
		resultHash.put("IMAGE_TEXT1", image_text1)
		resultHash.put("DEFAULT_IND0", default_image_index1)
		resultHash.put("DEFAULT_IND1", default_image_index1)
		resultHash.put("DISPLAY_IND0", active_image_index1)
		resultHash.put("DISPLAY_IND1", active_image_index1)

		String image_url2 = getValueFromHttpRequestValue("update_inventory_item", "image[1][main_image_url]")
		logi.logInfo("2nd image URL : " + image_url2)
		if(image_url2 != null) {
			String thumb_image_url2 = getValueFromHttpRequestValue("update_inventory_item", "image[1][thumbnail_image_url]")
			String image_text2 = getValueFromHttpRequestValue("update_inventory_item", "image[1][image_text]")
			String default_image_index2 = getValueFromHttpRequestValue("update_inventory_item", "image[1][default_ind]")
			String active_image_index2 = getValueFromHttpRequestValue("update_inventory_item", "image[1][active]")

			resultHash.put("IMAGE_URL2", image_url2)
			resultHash.put("IMAGE_URL3", thumb_image_url2)
			resultHash.put("IMAGE_TEXT2", image_text2)
			resultHash.put("IMAGE_TEXT3", image_text2)
			resultHash.put("DEFAULT_IND2", default_image_index2)
			resultHash.put("DEFAULT_IND3", default_image_index2)
			resultHash.put("DISPLAY_IND2", active_image_index2)
			resultHash.put("DISPLAY_IND3", active_image_index2)
		}

		return resultHash.sort()
	}

	def md_inventory_image_details_from_DB(String testcaseid) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()

		String item_no = getValueFromResponse("create_inventory_item", ExPathAdminToolsApi.CREATE_INVENTORY_ITEM_ITEM_NO1)
		String client_no = getValueFromHttpRequestValue("create_inventory_item", "client_no")
		logi.logInfo("Item and Client number : " + item_no + " :: " + client_no)
		int i = 0
		ResultSet rs = db.executePlaQuery("SELECT * from ariacore.inventory_item_images where item_no=" + item_no + " AND CLIENT_NO=" + client_no + " order by image_class_seq_no")

		while(rs.next()) {
			hashResult.put("IMAGE_URL" + i, rs.getString('IMAGE_URL'))
			hashResult.put("IMAGE_TEXT" + i, rs.getString('IMAGE_TEXT'))
			hashResult.put("DEFAULT_IND" + i, rs.getString('DEFAULT_IND'))
			hashResult.put("DISPLAY_IND" + i, rs.getString('DISPLAY_IND'))
			i++
		}
		logi.logInfo("Hashmap is : " + hashResult)

		return hashResult.sort()
	}

	def md_inventory_resource_type_from_API(String testcaseid)
	{
		LinkedHashMap<String,String> resultHash = new LinkedHashMap<String, String>()

		String client_sku = getValueFromHttpRequestValue("create_inventory_item", "client_sku")
		String resource_type_no = getValueFromHttpRequestValue("create_inventory_item", "resource_type_no")
		String resource_type_no_updated = getValueFromHttpRequestValue("update_inventory_item", "resource_type_no")
		String resource_units = getValueFromHttpRequestValue("create_inventory_item", "resource_units")
		String resource_units_updated = getValueFromHttpRequestValue("update_inventory_item", "resource_units")

		if(resource_type_no_updated == null)
		{
			resource_type_no_updated = resource_type_no
			resource_units_updated = resource_units
		}

		String[] resource_type
		resultHash.put("Client SKU", client_sku)
		if(resource_type_no_updated.contains("|"))
		{
			resource_type = resource_type_no_updated.split("\\|")
			String[] resource_unit_numbers = resource_units_updated.split("\\|")
			for(int i = 0;  i<resource_type.size();i++)
			{
				logi.logInfo("Items are : " + resource_type[i] + " :: " + resource_unit_numbers[i])
				resultHash.put("Resource Type" + i, resource_type[i])
				resultHash.put("Resource Units" + i, resource_unit_numbers[i])
			}
		}
		else
		{
			resultHash.put("Resource Type0", resource_type_no_updated)
			resultHash.put("Resource Units0", resource_units_updated)
		}
		logi.logInfo("Hashmap is : " + resultHash)
		return resultHash.sort()
	}

	def md_inventory_resource_type_from_DB(String testcaseid) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		String item_no = getValueFromResponse("create_inventory_item", ExPathAdminToolsApi.CREATE_INVENTORY_ITEM_ITEM_NO1)
		String client_no = getValueFromHttpRequestValue("create_inventory_item", "client_no")
		String client_sku = getValueFromHttpRequestValue("create_inventory_item", "client_sku")
		logi.logInfo("Item and Client number : " + item_no + " :: " + client_no)
		hashResult.put("Client SKU", client_sku)
		MySqlDB sqlDB = new MySqlDB()
		ResultSet rs = sqlDB.executePlaQuery("SELECT resource_type_id,resource_units FROM sku_resource_map WHERE client_id = " + client_no + " AND sku='" + client_sku + "' ")
		int i=0
		while(rs.next()) {
			hashResult.put("Resource Type"+i, rs.getString('resource_type_id'))
			hashResult.put("Resource Units"+i, rs.getString('resource_units'))
			i++
		}
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}

	def md_product_fields_from_API(String testcaseid) {
		LinkedHashMap<String,String> resultHash = new LinkedHashMap<String, String>()
		String field_number0 =  getValueFromHttpRequestValue("create_inventory_item", "supplemental_obj_field[0][field_no]")
		String field_number1 =  getValueFromHttpRequestValue("create_inventory_item", "supplemental_obj_field[1][field_no]")
		String field_number2 =  getValueFromHttpRequestValue("create_inventory_item", "supplemental_obj_field[2][field_no]")
		String field_number3 =  getValueFromHttpRequestValue("create_inventory_item", "supplemental_obj_field[3][field_no]")
		String field_number4 =  getValueFromHttpRequestValue("create_inventory_item", "supplemental_obj_field[4][field_no]")
		String field_value0 =  getValueFromHttpRequestValue("create_inventory_item", "supplemental_obj_field[0][field_value][0]")
		String field_value10 =  getValueFromHttpRequestValue("create_inventory_item", "supplemental_obj_field[1][field_value][0]")
		String field_value11 =  getValueFromHttpRequestValue("create_inventory_item", "supplemental_obj_field[1][field_value][1]")
		String field_value20 =  getValueFromHttpRequestValue("create_inventory_item", "supplemental_obj_field[2][field_value][0]")
		String field_value21 =  getValueFromHttpRequestValue("create_inventory_item", "supplemental_obj_field[2][field_value][1]")
		String field_value30 =  getValueFromHttpRequestValue("create_inventory_item", "supplemental_obj_field[3][field_value][0]")
		String field_value31 =  getValueFromHttpRequestValue("create_inventory_item", "supplemental_obj_field[3][field_value][1]")
		String field_value40 =  getValueFromHttpRequestValue("create_inventory_item", "supplemental_obj_field[4][field_value][0]")

		resultHash.put("field_number0",field_number0)
		resultHash.put("field_number1",field_number1)
		resultHash.put("field_number2",field_number2)
		resultHash.put("field_number3",field_number3)
		resultHash.put("field_number4",field_number4)
		resultHash.put("field_value0",field_value0)
		resultHash.put("field_value1",field_value10)
		resultHash.put("field_value2",field_value11)
		resultHash.put("field_value3",field_value20)
		resultHash.put("field_value4",field_value21)
		resultHash.put("field_value5",field_value30)
		resultHash.put("field_value6",field_value31)
		resultHash.put("field_value7",field_value40)

		logi.logInfo("Hashmap is : " + resultHash)
		return resultHash.sort()
	}

	def md_product_fields_from_DB(String testcaseid) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		String item_no = getValueFromResponse("create_inventory_item", ExPathAdminToolsApi.CREATE_INVENTORY_ITEM_ITEM_NO1)
		String client_no = getValueFromHttpRequestValue("create_inventory_item", "client_no")
		ResultSet field_no = db.executePlaQuery("SELECT FIELD_NO from ARIACORE.CLIENT_OBJ_SUPP_FIELDS where client_no =" + client_no + " and field_name like '%nspf%' order by field_no")
		int field_no_cnt=0, field_value_cnt=0
		List<String> field_nos=[]
		while(field_no.next()) {
			hashResult.put("field_number"+field_no_cnt, field_no.getString(1))
			field_nos.add(field_no.getString(1))
			field_no_cnt++
		}
		ResultSet fld_vals
		for(n in field_nos) {
			fld_vals = db.executePlaQuery("SELECT VALUE_TEXT from ARIACORE.CLIENT_OBJ_SUPP_FIELDS_OBJ_VAL where client_no=" + client_no + " and field_no=" + n +" and object_id=" + item_no)
			while(fld_vals.next()) {
				hashResult.put("field_value"+field_value_cnt, fld_vals.getString(1))
				field_value_cnt++
			}
		}
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}

	def md_create_plan_details_from_API(String testcaseID) {
		LinkedHashMap<String,String> resultHash = new LinkedHashMap<String, String>()
		String apiname=testcaseID.split("-")[2]
		logi.logInfo("API Name:"+apiname)
		String rollAcctStatusCd = getValueFromHttpRequestValue(apiname, "rollover_acct_status_cd")
		String rollAcctStatusDays = getValueFromHttpRequestValue(apiname, "rollover_acct_status_days")
		def rollStatusCd = rollAcctStatusCd==null?1:rollAcctStatusCd
		def rollStatusDays = rollAcctStatusDays==null?0:rollAcctStatusDays
		logi.logInfo("Roll account status cd is " +rollStatusCd)
		logi.logInfo("Roll account status days is " +rollStatusDays)
		String plan_no = getValueFromResponse(apiname, ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
		String plan_name =  getValueFromHttpRequestValue(apiname, "plan_name")
		String item_name =  getValueFromHttpRequestValue(apiname, "item_name")
		String desc =  getValueFromHttpRequestValue(apiname, "plan_description")
		String client_plan_id =  getValueFromHttpRequestValue(apiname, "client_plan_id")
		String billing_interval =  getValueFromHttpRequestValue(apiname, "billing_interval")
		String usage_billing_interval =  getValueFromHttpRequestValue(apiname, "usage_billing_interval")
		String currency =  getValueFromHttpRequestValue(apiname, "currency")
		String currency_cd =  getValueFromHttpRequestValue(apiname, "currency")
		String status_cd =  getValueFromHttpRequestValue(apiname, "acct_status_cd")
		String how_to_apply_min_fee =  getValueFromHttpRequestValue(apiname, "how_to_apply_min_fee")
		String supp_plan_ind =  ((getValueFromHttpRequestValue(apiname, "parent_plans[0]"))==null)?0:1
		String rollover_acct_status_cd = getValueFromHttpRequestValue(apiname, "rollover_acct_status_cd")
		String rollover_acct_status_days = getValueFromHttpRequestValue(apiname, "rollover_acct_status_days")
		String rollover_plan_no = getValueFromHttpRequestValue(apiname, "rollover_plan_no")
		String rollover_months = getValueFromHttpRequestValue(apiname, "rollover_months")
		String template_no = getValueFromHttpRequestValue(apiname, "template_no")
		String notificationTmpIid = getValueFromHttpRequestValue(apiname, "notification_template_group_no")
		if(rollover_months==null)
		{
			rollover_plan_no = null;
		}
		String credit_note_template_no = getValueFromHttpRequestValue(apiname, "credit_note_template_no")
		String dunning_plan_no = getValueFromHttpRequestValue(apiname, "dunning_plan_no")
		String early_cancel_min_months = getValueFromHttpRequestValue(apiname, "plan_cancel_min_month")
		String initial_free_months = getValueFromHttpRequestValue(apiname, "initial_free_months")
		String smt_no = getValueFromHttpRequestValue(apiname, "template_no")
		//String notify_No = getValueFromHttpRequestValue(apiname, "notification_template_group_no")


		resultHash.put("Plan No", plan_no)
		resultHash.put("Plan Name", plan_name)
		resultHash.put("Item Name", item_name)
		resultHash.put("Desc", desc)
		//resultHash.put("Client Plan Id", (client_plan_id==null)?(getValueFromResponse(apiname, ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)):client_plan_id)
		resultHash.put("Billing Interval", billing_interval)
		resultHash.put("Usage Billing Interval", usage_billing_interval)
		resultHash.put("Currency", currency)
		resultHash.put("Currency Cd", currency_cd)
		resultHash.put("Status Cd", (status_cd==null)?1:status_cd)
		resultHash.put("How To Apply Min Fee", how_to_apply_min_fee)
		resultHash.put("Supp Plan Ind", supp_plan_ind)
		resultHash.put("Master Plan No", (getValueFromHttpRequestValue(apiname, "parent_plans[0]")))
		resultHash.put("Rollover Acct Status Cd", rollStatusCd)
		resultHash.put("Rollover Acct Status Days", rollStatusDays)
		resultHash.put("Rollover Plan No", rollover_plan_no)
		resultHash.put("Rollover Months", rollover_months)
		resultHash.put("Credit Note Template No", (credit_note_template_no==null)?0:credit_note_template_no)
		resultHash.put("Statement Template No", (smt_no==null)?0:smt_no)
		//resultHash.put("Notification Template No", notify_No)
		resultHash.put("Dunning Plan No", dunning_plan_no)
		resultHash.put("Early Cancel Min Months", early_cancel_min_months)
		resultHash.put("Initial Free Months", initial_free_months)
		resultHash.put("Statement Template No", template_no)
		resultHash.put("Notification Template Grp Id", notificationTmpIid)
		
		if(apiname.contains("edit_plan")) {
			resultHash.put("Rollover Acct Status Cd", rollover_acct_status_cd == null?1:rollover_acct_status_cd)
			//resultHash.put("Credit Note Template No", (credit_note_template_no==null)?null:credit_note_template_no)
		}

		logi.logInfo("Hashmap is : " + resultHash)
		library.Constants.Constant.resultHashCreateNewPlan = resultHash.sort()
		return resultHash.sort()
	}


	def md_create_plan_details_from_API1(String testcaseID) {
		LinkedHashMap<String,String> resultHash = new LinkedHashMap<String, String>()
		String apiname=testcaseID.split("-")[2]
		logi.logInfo("API Name:"+apiname)
		String plan_no = getValueFromResponse(apiname, ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
		String plan_name =  getValueFromHttpRequestValue(apiname, "plan_name")
		String item_name =  getValueFromHttpRequestValue(apiname, "item_name")
		String desc =  getValueFromHttpRequestValue(apiname, "plan_description")
		String client_plan_id =  getValueFromHttpRequestValue(apiname, "client_plan_id")
		String billing_interval =  getValueFromHttpRequestValue(apiname, "billing_interval")
		String usage_billing_interval =  getValueFromHttpRequestValue(apiname, "usage_billing_interval")
		String currency =  getValueFromHttpRequestValue(apiname, "currency")
		String currency_cd =  getValueFromHttpRequestValue(apiname, "currency")
		String status_cd =  getValueFromHttpRequestValue(apiname, "acct_status_cd")
		String how_to_apply_min_fee =  getValueFromHttpRequestValue(apiname, "how_to_apply_min_fee")
		String supp_plan_ind =  ((getValueFromHttpRequestValue(apiname, "parent_plans[0]"))==null)?0:1
		String rollover_acct_status_cd = getValueFromHttpRequestValue(apiname, "rollover_acct_status_cd")
		String rollover_acct_status_days = getValueFromHttpRequestValue(apiname, "rollover_acct_status_days")
		String rollover_plan_no = getValueFromHttpRequestValue(apiname, "rollover_plan_no")
		String rollover_months = getValueFromHttpRequestValue(apiname, "rollover_months")
		String credit_note_template_no = ((getValueFromHttpRequestValue(apiname, "credit_note_template_no"))==null)?0:1
		String dunning_plan_no = getValueFromHttpRequestValue(apiname, "dunning_plan_no")
		String early_cancel_min_months = getValueFromHttpRequestValue(apiname, "plan_cancel_min_month")
		String initial_free_months = getValueFromHttpRequestValue(apiname, "initial_free_months")


		resultHash.put("Plan No", plan_no)
		resultHash.put("Plan Name", plan_name)
		resultHash.put("Item Name", item_name)
		resultHash.put("Desc", desc)
		resultHash.put("Client Plan Id", (client_plan_id==null)?(getValueFromResponse(apiname, ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)):client_plan_id)
		resultHash.put("Billing Interval", billing_interval)
		resultHash.put("Usage Billing Interval", usage_billing_interval)
		resultHash.put("Currency", currency)
		resultHash.put("Currency Cd", currency_cd)
		resultHash.put("Status Cd", (status_cd==null)?1:status_cd)
		resultHash.put("Supp Plan Ind", supp_plan_ind)
		resultHash.put("Rollover Acct Status Cd", rollover_acct_status_cd)
		resultHash.put("Rollover Acct Status Days", rollover_acct_status_days)
		resultHash.put("Rollover Plan No", rollover_plan_no)
		resultHash.put("Rollover Months", rollover_months)
		resultHash.put("Credit Note Template No", (credit_note_template_no==null)?0:credit_note_template_no)

		if(apiname.contains("edit_plan")) {
			resultHash.put("Rollover Acct Status Cd", rollover_acct_status_cd == null?0:rollover_acct_status_cd)
			resultHash.put("Credit Note Template No", (credit_note_template_no==null)?null:credit_note_template_no)
		}

		logi.logInfo("Hashmap is : " + resultHash)
		return resultHash.sort()
	}

	
	def md_create_plan_details_from_DB(String testcaseID) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		def client_no = getValueFromHttpRequestValue("create_new_plan", "client_no")
		String plan_name =  getValueFromHttpRequestValue("create_new_plan", "plan_name")
		String plan_no = db.executeQueryP2("SELECT plan_no FROM ARIACORE.CLIENT_PLAN where plan_name='"+plan_name+"' and client_no="+client_no)
		String planDetailsQuery = "SELECT * FROM ARIACORE.CLIENT_PLAN where PLAN_NO= " + plan_no
		String dunningPlanQuery = "SELECT DUNNING_PLAN_NO from ariacore.dunning_plan_assignment where PLAN_NO=" + plan_no + " AND CLIENT_NO=" + client_no
		String masterPlanNoQuery = "SELECT MASTER_PLAN_NO FROM ARIACORE.PLAN_SUPP_PLAN_MAP WHERE SUPP_PLAN_NO = "+plan_no
		String templateNoQuery = "SELECT TEMPLATE_NO FROM ARIACORE.PLAN_NOTIFY_METHODS WHERE PLAN_NO= " + plan_no + " AND METHOD_ID = 0"
		hashResult.put("Dunning Plan No", db.executeQueryP2(dunningPlanQuery))
		logi.logInfo("Master plan No is "+db.executeQueryP2(masterPlanNoQuery))
		hashResult.put("Master Plan No", db.executeQueryP2(masterPlanNoQuery))
		hashResult.put("Statement Template No", db.executeQueryP2(templateNoQuery))
		ResultSet rs = db.executePlaQuery(planDetailsQuery)
		while(rs.next()) {
			hashResult.put("Plan No", plan_no)
			hashResult.put("Plan Name", rs.getString("plan_name"))
			hashResult.put("Item Name", rs.getString("plan_name"))
			hashResult.put("Desc", rs.getString("eu_html_long"))
			//hashResult.put("Client Plan Id", rs.getString("client_plan_id"))
			hashResult.put("Billing Interval", rs.getString("billing_interval"))
			hashResult.put("Usage Billing Interval", rs.getString("usage_billing_interval"))
			hashResult.put("Currency", rs.getString("currency"))
			hashResult.put("Currency Cd", rs.getString("currency_cd"))
			hashResult.put("Status Cd", rs.getString("new_acct_status_cd"))
			hashResult.put("How To Apply Min Fee", rs.getString("min_fee_rules_cd"))
			hashResult.put("Supp Plan Ind", rs.getString("supp_plan_ind"))
			hashResult.put("Rollover Acct Status Cd", rs.getString("rollover_acct_status_cd"))
			hashResult.put("Rollover Acct Status Days", rs.getString("rollover_acct_status_days"))
			hashResult.put("Rollover Plan No", rs.getString("rollover_plan_no"))
			hashResult.put("Rollover Months", rs.getString("rollover_months"))
			hashResult.put("Credit Note Template No", rs.getString("credit_note_template_no")==null?0:rs.getString("credit_note_template_no"))
			hashResult.put("Early Cancel Min Months", rs.getString("early_cancel_min_months"))
			hashResult.put("Initial Free Months", rs.getString("initial_free_months"))
			//hashResult.put("Notification Template No",rs.getString("NOTIFY_TMPLT_GRP_SEQ_ID"))
			hashResult.put("Notification Template Grp Id", rs.getString("NOTIFY_TMPLT_GRP_SEQ_ID"))
			if(testcaseID.contains("edit_plan")) {
				hashResult.put("Rollover Acct Status Cd", rs.getString("rollover_acct_status_cd")==null?0:rs.getString("rollover_acct_status_cd"))
			}
		}
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}
	/*def md_create_plan_details_from_DB(String testcaseID) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		def client_no = getValueFromHttpRequestValue("create_new_plan", "client_no")
		String plan_name =  getValueFromHttpRequestValue("create_new_plan", "plan_name")
		String plan_no = db.executeQueryP2("SELECT plan_no FROM ARIACORE.CLIENT_PLAN where plan_name='"+plan_name+"' and client_no="+client_no)
		String planDetailsQuery = "SELECT * FROM ARIACORE.CLIENT_PLAN where PLAN_NO= " + plan_no
		String dunningPlanQuery = "SELECT DUNNING_PLAN_NO from dunning_plan_assignment where PLAN_NO=" + plan_no + " AND CLIENT_NO=" + client_no
		hashResult.put("Dunning Plan No", db.executeQueryP2(dunningPlanQuery))
		ResultSet rs = db.executePlaQuery(planDetailsQuery)
		while(rs.next()) {
			hashResult.put("Plan No", plan_no)
			hashResult.put("Plan Name", rs.getString("plan_name"))
			hashResult.put("Item Name", rs.getString("plan_name"))
			hashResult.put("Desc", rs.getString("eu_html_long"))
			hashResult.put("Client Plan Id", rs.getString("client_plan_id"))
			hashResult.put("Billing Interval", rs.getString("billing_interval"))
			hashResult.put("Usage Billing Interval", rs.getString("usage_billing_interval"))
			hashResult.put("Currency", rs.getString("currency"))
			hashResult.put("Currency Cd", rs.getString("currency_cd"))
			hashResult.put("Status Cd", rs.getString("new_acct_status_cd"))
			hashResult.put("How To Apply Min Fee", rs.getString("min_fee_rules_cd"))
			hashResult.put("Supp Plan Ind", rs.getString("supp_plan_ind"))
			hashResult.put("Rollover Acct Status Cd", rs.getString("rollover_acct_status_cd"))
			hashResult.put("Rollover Acct Status Days", rs.getString("rollover_acct_status_days"))
			hashResult.put("Rollover Plan No", rs.getString("rollover_plan_no"))
			hashResult.put("Rollover Months", rs.getString("rollover_months"))
			hashResult.put("Credit Note Template No", rs.getString("credit_note_template_no")==null?0:rs.getString("credit_note_template_no"))
			hashResult.put("Early Cancel Min Months", rs.getString("early_cancel_min_months"))
			hashResult.put("Initial Free Months", rs.getString("initial_free_months"))
			//hashResult.put("Notification Template No",rs.getString("NOTIFY_TMPLT_GRP_SEQ_ID"))
			hashResult.put("Notification Template Grp Id", rs.getString("NOTIFY_TMPLT_GRP_SEQ_ID"))
			if(testcaseID.contains("edit_plan")) {
				hashResult.put("Rollover Acct Status Cd", rs.getString("rollover_acct_status_cd")==null?0:rs.getString("rollover_acct_status_cd"))
			}
		}
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}
*/

	def md_create_plan_details_from_DB1(String testcaseID) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		def client_no = getValueFromHttpRequestValue("create_new_plan", "client_no")
		String plan_name =  getValueFromHttpRequestValue("create_new_plan", "plan_name")
		String plan_no = db.executeQueryP2("SELECT plan_no FROM ARIACORE.CLIENT_PLAN where plan_name='"+plan_name+"' and client_no="+client_no)
		String planDetailsQuery = "SELECT * FROM ARIACORE.CLIENT_PLAN where PLAN_NO= " + plan_no
		String dunningPlanQuery = "SELECT DUNNING_PLAN_NO from dunning_plan_assignment where PLAN_NO=" + plan_no + " AND CLIENT_NO=" + client_no
		ResultSet rs = db.executePlaQuery(planDetailsQuery)
		while(rs.next()) {
			hashResult.put("Plan No", plan_no)
			hashResult.put("Plan Name", rs.getString("plan_name"))
			hashResult.put("Item Name", rs.getString("plan_name"))
			hashResult.put("Desc", rs.getString("eu_html_long"))
			hashResult.put("Client Plan Id", rs.getString("client_plan_id"))
			hashResult.put("Billing Interval", rs.getString("billing_interval"))
			hashResult.put("Usage Billing Interval", rs.getString("usage_billing_interval"))
			hashResult.put("Currency", rs.getString("currency"))
			hashResult.put("Currency Cd", rs.getString("currency_cd"))
			hashResult.put("Status Cd", rs.getString("new_acct_status_cd"))
			hashResult.put("Supp Plan Ind", rs.getString("supp_plan_ind"))
			hashResult.put("Rollover Acct Status Cd", rs.getString("rollover_acct_status_cd"))
			hashResult.put("Rollover Acct Status Days", rs.getString("rollover_acct_status_days"))
			hashResult.put("Rollover Plan No", rs.getString("rollover_plan_no"))
			hashResult.put("Rollover Months", rs.getString("rollover_months"))
			hashResult.put("Credit Note Template No", rs.getString("credit_note_template_no")==null?0:rs.getString("credit_note_template_no"))
			
			if(testcaseID.contains("edit_plan")) {
				hashResult.put("Rollover Acct Status Cd", rs.getString("rollover_acct_status_cd")==null?0:rs.getString("rollover_acct_status_cd"))
			}
		}
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}

	String md_VerifySuppFieldValuesExp(String testcaseid) {
		def plan_no = getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
		def client_no = getValueFromHttpRequestValue("create_new_plan", "client_no")
		String serviceDetailsQuery = "SELECT * FROM CLIENT_PLAN_SUPP_FIELDS_VAL WHERE CLIENT_NO="+client_no+" AND PLAN_NO = " + plan_no + " ORDER BY FIELD_NO";
		Map<String, List<String>> suppFieldValues = new TreeMap<String, List<String>>();
		logi.logInfo("Before query Flat Rate Psssssssssssssssssssssssser Tier")
		ResultSet rs = db.executePlaQuery(serviceDetailsQuery);
		logi.logInfo("Flat Rate Psssssssssssssssssssssssser Tier")
		while(rs.next()) {
			if(!suppFieldValues.containsKey(rs.getString("FIELD_NO"))) {
				List<String> valueList = new ArrayList<>();
				valueList.add(rs.getString("VALUE_TEXT"));
				suppFieldValues.put(rs.getString("FIELD_NO") , valueList);
			} else {
				suppFieldValues.get(rs.getString("FIELD_NO")).add(rs.getString("VALUE_TEXT"));
			}
		}
		suppFieldValues.each
		{k,v ->
			String values = v
			if (values.contains(",")) {
				values = values.replaceAll(", ", "&")
				suppFieldValues.put(k,values)
			}
		}
		logi.logInfo("After replace : " + suppFieldValues)
		return suppFieldValues.toString()
	}

	String md_VerifySuppFieldValuesAct(String testcaseid) {

		Map<String, List<String>> suppFieldValues = new TreeMap<String, List<String>>();

		int i = 0
		while(!getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_no]").equals(null)) {
			logi.logInfo(" Supp Field  - " + getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_no]"))
			int j =0
			List<String> valueList = new ArrayList<>();
			while(!getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_value][" + j + "]").equals(null)) {
				logi.logInfo(" Supp Values - " + getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_value][" + j + "]"))

				valueList.add(getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_value][" + j + "]"));
				j++
			}
			suppFieldValues.put(getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_no]"), valueList )
			i++
		}
		suppFieldValues.each
		{k,v ->
			String values = v
			if (values.contains(",")) {
				values = values.replaceAll(", ", "&")
				suppFieldValues.put(k,values)
			}
		}
		logi.logInfo("After replace : " + suppFieldValues)
		library.Constants.Constant.hashResultSuppObjFields = suppFieldValues
		return suppFieldValues.toString()
	}
	
	String md_VerifySuppFieldValuesActEdit(String testcaseid) {
		
				Map<String, List<String>> suppFieldValues = new TreeMap<String, List<String>>();
		
				int i = 0
				while(!getValueFromHttpRequestValue("edit_plan", "supplemental_obj_field[" + i + "][field_no]").equals(null)) {
					logi.logInfo(" Supp Field  - " + getValueFromHttpRequestValue("edit_plan", "supplemental_obj_field[" + i + "][field_no]"))
					int j =0
					List<String> valueList = new ArrayList<>();
					while(!getValueFromHttpRequestValue("edit_plan", "supplemental_obj_field[" + i + "][field_value][" + j + "]").equals(null)) {
						logi.logInfo(" Supp Values - " + getValueFromHttpRequestValue("edit_plan", "supplemental_obj_field[" + i + "][field_value][" + j + "]"))
		
						valueList.add(getValueFromHttpRequestValue("edit_plan", "supplemental_obj_field[" + i + "][field_value][" + j + "]"));
						j++
					}
					suppFieldValues.put(getValueFromHttpRequestValue("edit_plan", "supplemental_obj_field[" + i + "][field_no]"), valueList )
					i++
				}
				suppFieldValues.each
				{k,v ->
					String values = v
					if (values.contains(",")) {
						values = values.replaceAll(", ", "&")
						suppFieldValues.put(k,values)
					}
				}
				logi.logInfo("After replace : " + suppFieldValues)
				return suppFieldValues.toString()
			}

	def md_VerifyChildPlans_from_DB(String testcaseid) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		List planGroupsId = getPropertyListContains("plan_group", testcaseid)
		for(prp in planGroupsId) {
			logi.logInfo("****************************************************" + prp)
		}
		def client_no = getValueFromHttpRequestValue("create_new_plan", "client_no")
		String planMapQuery = "SELECT * FROM ARIACORE.PLAN_SUPP_PLAN_MAP WHERE CLIENT_NO=" + client_no + " AND MASTER_PLAN_NO = " + getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
		ResultSet result = db.executePlaQuery(planMapQuery)
		int i = 0
		while (result.next()) {
			hashResult.put("child_plans" + i , result.getString("SUPP_PLAN_NO"))
			i++
		}
		logi.logInfo(" Plan Group Query " + planMapQuery)
		return hashResult.sort()
	}
	
	def md_VerifyChildPlans_from_API(String testcaseid) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		List<String> child_plans=[]
		String apiname=testcaseid.split("-")[2]
		logi.logInfo("API Name:"+apiname)
		for(i in 0..3) {
			child_plans.add(getValueFromHttpRequestValue(apiname, "child_plans[" + i + "]"))
		}
		int i=0
		ResultSet fld_vals
		for(n in child_plans) {
			if(n != null) {
				hashResult.put("child_plans"+i, n)
			}
			i++
		}
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}
	
	def md_verify_plan_description(String testcaseid) {
		String plan_no = getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
		String desc =  db.executeQueryP2("SELECT EU_HTML_LONG from ariacore.client_plan where plan_no=" + plan_no)
		if(desc.length() <= 3000){
			return "Description length is lesser or equal to 3000 characters"
		}
		else
			return "Description length is greater than 3000 characters"
	}

	def md_VerifyServiceDetails_API(String testCaseId) {

		List testCaseCmb = Constant.lhmAllHttpInputRequest_Global.keySet().asList()
		List httpValues = Constant.lhmAllHttpInputRequest_Global.values().asList()
		List<String> propertyList = []
		String apiname=testCaseId.split("-")[2]
		logi.logInfo("API Name:"+apiname)

		for(int j=0; j<testCaseCmb.size(); j++) {
			logi.logInfo("case Cmb value is :: "+ testCaseCmb[j])
			logi.logInfo(testCaseId)
			if(testCaseCmb[j].toString().equals(testCaseId)) {
				com.eviware.soapui.impl.wsdl.teststeps.HttpTestRequest request = httpValues[j]
				logi.logInfo("Request is ::::" + request)
				for(prp in request.getPropertyList()) {
					if(prp.name.contains("[service_no]")) {
						propertyList.add(prp.name)
					}
				}
			}
		}
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		List<String> service_no=[]
		List<String> name=[]
		List<String> client_service_id=[]
		List<String> gl_cd=[]
		List<String> service_type=[]
		List<String> pricing_rule=[]
		List<String> usage_type=[]
		logi.logInfo("md_VerifyServiceDetails_APIaaaaaaa=>>>>>>>>")
		logi.logInfo("request Size : " + propertyList.size())
		for(i in 0..(propertyList.size()-1)) {
			logi.logInfo("md_VerifyServiceDetails_APIbbbbbb=>>>>>>>>"+i)
			hashResult.put("service[" + i + "][service_no]", getValueFromHttpRequestValue(apiname, "service[" + i + "][service_no]"))
			hashResult.put("service[" + i + "][name]", getValueFromHttpRequestValue(apiname, "service[" + i + "][name]"))
			hashResult.put("service[" + i + "][client_service_id]", getValueFromHttpRequestValue(apiname, "service[" + i + "][client_service_id]"))
			hashResult.put("service[" + i + "][gl_cd]", getValueFromHttpRequestValue(apiname, "service[" + i + "][gl_cd]"))
			hashResult.put("service[" + i + "][service_type]", getValueFromHttpRequestValue(apiname, "service[" + i + "][service_type]"))
			hashResult.put("service[" + i + "][pricing_rule]", (getValueFromHttpRequestValue(apiname, "service[" + i + "][pricing_rule]")==null)?"Standard":getValueFromHttpRequestValue(apiname, "service[" + i + "][pricing_rule]"))
			hashResult.put("service[" + i + "][usage_type]", getValueFromHttpRequestValue(apiname, "service[" + i + "][usage_type]"))
		}

		logi.logInfo("md_VerifyServiceDetails_API Hashmap is : " + hashResult)
		library.Constants.Constant.hashResultCreatePlanServices = hashResult.sort()
		return hashResult.sort()
	}
	
	def md_VerifyServiceDetailsNewFormat_API(String testCaseId) {
		
		List testCaseCmb = Constant.lhmAllHttpInputRequest_Global.keySet().asList()
		List httpValues = Constant.lhmAllHttpInputRequest_Global.values().asList()
		List<String> propertyList = []
		String apiname=testCaseId.split("-")[2]
		logi.logInfo("API Name:"+apiname)

		for(int j=0; j<testCaseCmb.size(); j++) {
			logi.logInfo("case Cmb value is :: "+ testCaseCmb[j])
			logi.logInfo(testCaseId)
			if(testCaseCmb[j].toString().equals(testCaseId)) {
				com.eviware.soapui.impl.wsdl.teststeps.HttpTestRequest request = httpValues[j]
				logi.logInfo("Request is ::::" + request)
				for(prp in request.getPropertyList()) {
					if(prp.name.contains("[service_no]")) {
						propertyList.add(prp.name)
					}
				}
			}
		}
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		List<String> service_no=[]
		List<String> name=[]
		List<String> client_service_id=[]
		List<String> gl_cd=[]
		List<String> service_type=[]
		List<String> pricing_rule=[]
		List<String> usage_type=[]
		logi.logInfo("md_VerifyServiceDetails_APIaaaaaaa=>>>>>>>>")
		logi.logInfo("request Size : " + propertyList.size())
		for(i in 0..(propertyList.size()-1)) {
			logi.logInfo("md_VerifyServiceDetails_APIbbbbbb=>>>>>>>>"+i)
			String serviceNo = getValueFromHttpRequestValue(apiname, "service[" + i + "][service_no]")
//			hashResult.put("service[" + i + "][service_no]", getValueFromHttpRequestValue(apiname, "service[" + i + "][service_no]"))
			hashResult.put(serviceNo + " Name", getValueFromHttpRequestValue(apiname, "service[" + i + "][name]"))
			hashResult.put(serviceNo + " ClientServiceId", getValueFromHttpRequestValue(apiname, "service[" + i + "][client_service_id]"))
			hashResult.put(serviceNo + " Gl_Code", getValueFromHttpRequestValue(apiname, "service[" + i + "][gl_cd]"))
			hashResult.put(serviceNo + " ServiceType", getValueFromHttpRequestValue(apiname, "service[" + i + "][service_type]"))
			hashResult.put(serviceNo + " PricingRule", (getValueFromHttpRequestValue(apiname, "service[" + i + "][pricing_rule]")==null)?"Standard":getValueFromHttpRequestValue(apiname, "service[" + i + "][pricing_rule]"))
			hashResult.put(serviceNo + " UsageType", getValueFromHttpRequestValue(apiname, "service[" + i + "][usage_type]"))
		}


		logi.logInfo("md_VerifyServiceDetails_API Hashmap is : " + hashResult)
		library.Constants.Constant.hashResultCreatePlanServicesNew = hashResult.sort()
		return hashResult.sort()
	}

def md_VerifyServiceDetailsNewFormat_Ed2API(String testCaseId) {

		List testCaseCmb = Constant.lhmAllHttpInputRequest_Global.keySet().asList()
		List httpValues = Constant.lhmAllHttpInputRequest_Global.values().asList()
		List<String> propertyList = []
		String apiname=testCaseId.split("-")[2]
		logi.logInfo("API Name:"+apiname)

		for(int j=0; j<testCaseCmb.size(); j++) {
			logi.logInfo("case Cmb value is :: "+ testCaseCmb[j])
			logi.logInfo(testCaseId)
			if(testCaseCmb[j].toString().equals(testCaseId)) {
				com.eviware.soapui.impl.wsdl.teststeps.HttpTestRequest request = httpValues[j]
				logi.logInfo("Request is ::::" + request)
				for(prp in request.getPropertyList()) {
					if(prp.name.contains("[service_no]")) {
						propertyList.add(prp.name)
					}
				}
			}
		}
		LinkedHashMap<String,String> hashResult = library.Constants.Constant.hashResultCreatePlanServicesNew
		List<String> service_no=[]
		List<String> name=[]
		List<String> client_service_id=[]
		List<String> gl_cd=[]
		List<String> service_type=[]
		List<String> pricing_rule=[]
		List<String> usage_type=[]
		logi.logInfo("md_VerifyServiceDetails_APIaaaaaaa=>>>>>>>>")
		logi.logInfo("request Size : " + propertyList.size())
		for(i in 0..(propertyList.size()-1)) {
			logi.logInfo("md_VerifyServiceDetails_APIbbbbbb=>>>>>>>>"+i)
			String serviceNo = getValueFromHttpRequestValue(apiname, "service[" + i + "][service_no]")
			if(serviceNo != null){
//			hashResult.put("service[" + i + "][service_no]", getValueFromHttpRequestValue(apiname, "service[" + i + "][service_no]"))
			hashResult.put(serviceNo + " Name", getValueFromHttpRequestValue(apiname, "service[" + i + "][name]"))
			hashResult.put(serviceNo + " ClientServiceId", getValueFromHttpRequestValue(apiname, "service[" + i + "][client_service_id]"))
			hashResult.put(serviceNo + " Gl_Code", getValueFromHttpRequestValue(apiname, "service[" + i + "][gl_cd]"))
			hashResult.put(serviceNo + " ServiceType", getValueFromHttpRequestValue(apiname, "service[" + i + "][service_type]"))
			hashResult.put(serviceNo + " PricingRule", (getValueFromHttpRequestValue(apiname, "service[" + i + "][pricing_rule]")==null)?"Standard":getValueFromHttpRequestValue(apiname, "service[" + i + "][pricing_rule]"))
			hashResult.put(serviceNo + " UsageType", getValueFromHttpRequestValue(apiname, "service[" + i + "][usage_type]"))
			}
		}


		logi.logInfo("md_VerifyServiceDetails_API Hashmap is : " + hashResult)
		library.Constants.Constant.hashResultCreatePlanServicesNew = hashResult.sort()
		return hashResult.sort()
	}

def md_VerifyServiceDetailsNewFormat_DB(String testcaseid) {
List planGroupsId = getPropertyListContains("plan_group", testcaseid)
def client_no = getValueFromHttpRequestValue("create_new_plan", "client_no")
def plan_no = getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
String serviceDetailsQuery = "SELECT * FROM ARIACORE.PLAN_SERVICES JOIN ARIACORE.SERVICES ON ARIACORE.PLAN_SERVICES.SERVICE_NO = ARIACORE.SERVICES.SERVICE_NO AND ARIACORE.PLAN_SERVICES.client_no = ARIACORE.SERVICES.custom_to_client_no JOIN ARIACORE.CHART_OF_ACCTS ON ARIACORE.SERVICES.COA_ID = ARIACORE.CHART_OF_ACCTS.COA_ID AND ARIACORE.SERVICES.custom_to_client_no = ARIACORE.CHART_OF_ACCTS.custom_to_client_no WHERE ARIACORE.PLAN_SERVICES.PLAN_NO = " + plan_no + " AND ARIACORE.PLAN_SERVICES.CLIENT_NO = " + client_no + " AND ARIACORE.CHART_OF_ACCTS.COA_ID IN (SELECT COA_ID FROM ARIACORE.SERVICES WHERE SERVICE_NO IN (SELECT SERVICE_NO FROM ARIACORE.PLAN_SERVICES WHERE PLAN_NO = " + plan_no + " AND CLIENT_NO = " + client_no + "))"
logi.logInfo(" Plan Group Query " + serviceDetailsQuery)
ResultSet result = db.executePlaQuery(serviceDetailsQuery)

List testCaseCmb = Constant.lhmAllHttpInputRequest_Global.keySet().asList()
List httpValues = Constant.lhmAllHttpInputRequest_Global.values().asList()
List<String> propertyList = []
String apiname=testcaseid.split("-")[2]
logi.logInfo("API Name:"+apiname)

for(int j=0; j<testCaseCmb.size(); j++) {
	logi.logInfo("case Cmb value is :: "+ testCaseCmb[j])
	logi.logInfo(testcaseid)
	if(testCaseCmb[j].toString().equals(testcaseid)) {
		com.eviware.soapui.impl.wsdl.teststeps.HttpTestRequest request = httpValues[j]
		logi.logInfo("Request is ::::" + request)
		for(prp in request.getPropertyList()) {
			if(prp.name.contains("[service_no]")) {
				propertyList.add(prp.name)
			}
		}
	}
}

int r = (propertyList.size())-1,i,s1,s2
while (result.next()) {
	i = r
	if(result.getString("COMMENTS").contains("Activation")) {
		s1 = r
		s2 = 0
		i=s2
		logi.logInfo("After swapping :: " + s1 + "::" + s2 + "iterator value : " + i)
	}
	else if(s1) {
		i=i+1
		logi.logInfo("iterator value : " + i)
	}
	logi.logInfo("After setting values :: " + i)
	logi.logInfo("Iteration ----------------------------------------------------------------------------------------")
	String service_no = result.getString("SERVICE_NO")
//	hashResult.put("service[" + i + "][service_no]" , service_no)
	hashResult.put(service_no + " Name" , result.getString("COMMENTS"))
	hashResult.put(service_no + " ClientServiceId" , result.getString("CLIENT_SERVICE_ID"))
	hashResult.put(service_no + " Gl_Code" , result.getString("CLIENT_COA_CODE"))
	String serType = getServiceType(result.getString("RECURRING"), result.getString("USAGE_BASED"), result.getString("IS_CANCELLATION"), result.getString("IS_MIN_FEE"), result.getString("IS_ORDER_BASED"))
	hashResult.put(service_no + " ServiceType" , serType)
	hashResult.put(service_no + " PricingRule" , getPricingRule(result.getString("TIERED_PRICING_RULE_NO")))
	hashResult.put(service_no + " UsageType" , result.getString("USAGE_TYPE"))
	r--
}
logi.logInfo("md_VerifyServiceDetails_DB Hashmap is : " + hashResult)
return hashResult.sort()
}

	def md_VerifyServiceDetails_DB(String testcaseid) {
		List planGroupsId = getPropertyListContains("plan_group", testcaseid)
		def client_no = getValueFromHttpRequestValue("create_new_plan", "client_no")
		def plan_no = getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		String serviceDetailsQuery = "SELECT * FROM ARIACORE.PLAN_SERVICES JOIN ARIACORE.SERVICES ON ARIACORE.PLAN_SERVICES.SERVICE_NO = ARIACORE.SERVICES.SERVICE_NO AND ARIACORE.PLAN_SERVICES.client_no = ARIACORE.SERVICES.custom_to_client_no JOIN ARIACORE.CHART_OF_ACCTS ON ARIACORE.SERVICES.COA_ID = ARIACORE.CHART_OF_ACCTS.COA_ID AND ARIACORE.SERVICES.custom_to_client_no = ARIACORE.CHART_OF_ACCTS.custom_to_client_no WHERE ARIACORE.PLAN_SERVICES.PLAN_NO = " + plan_no + " AND ARIACORE.PLAN_SERVICES.CLIENT_NO = " + client_no + " AND ARIACORE.CHART_OF_ACCTS.COA_ID IN (SELECT COA_ID FROM ARIACORE.SERVICES WHERE SERVICE_NO IN (SELECT SERVICE_NO FROM ARIACORE.PLAN_SERVICES WHERE PLAN_NO = " + plan_no + " AND CLIENT_NO = " + client_no + "))"
		logi.logInfo(" Plan Group Query " + serviceDetailsQuery)
		ResultSet result = db.executePlaQuery(serviceDetailsQuery)

		List testCaseCmb = Constant.lhmAllHttpInputRequest_Global.keySet().asList()
		List httpValues = Constant.lhmAllHttpInputRequest_Global.values().asList()
		List<String> propertyList = []
		String apiname=testcaseid.split("-")[2]
		logi.logInfo("API Name:"+apiname)
		if(apiname=="edit_plan"){
			if(getValueFromHttpRequestValue("edit_plan", "edit_directives")=="3")
			{
				testcaseid = testcaseid.split("-")[0]+"-"+testcaseid.split("-")[1]+"-create_new_plan"
			}
		}
		for(int j=0; j<testCaseCmb.size(); j++) {
			logi.logInfo("case Cmb value is :: "+ testCaseCmb[j])
			logi.logInfo(testcaseid)
			if(testCaseCmb[j].toString().equals(testcaseid)) {
				com.eviware.soapui.impl.wsdl.teststeps.HttpTestRequest request = httpValues[j]
				logi.logInfo("Request is ::::" + request)
				for(prp in request.getPropertyList()) {
					if(prp.name.contains("[service_no]")) {
						propertyList.add(prp.name)
					}
				}
			}
		}
		logi.logInfo("propertyList.size()________: "+propertyList.size())
		int r = (propertyList.size())-1,i,s1,s2
		if(apiname=="edit_plan"){
			if(getValueFromHttpRequestValue("edit_plan", "edit_directives")=="3")
			{
				propertyList = []
				testcaseid = testcaseid.split("-")[0]+"-"+testcaseid.split("-")[1]+"-edit_plan"
					for(int j=0; j<testCaseCmb.size(); j++) {
						logi.logInfo("case Cmb value is :: "+ testCaseCmb[j])
						logi.logInfo(testcaseid)
						if(testCaseCmb[j].toString().equals(testcaseid)) {
							com.eviware.soapui.impl.wsdl.teststeps.HttpTestRequest request = httpValues[j]
							logi.logInfo("Request is ::::" + request)
							for(prp in request.getPropertyList()) {
								if(prp.name.contains("[service_no]")) {
									propertyList.add(prp.name)
								}
							}
						}
					}
					logi.logInfo("propertyList.size() "+propertyList.size())
					r = r-(propertyList.size())
				}
				
			}
		
		while (result.next()) {
			i = r
			if(result.getString("COMMENTS").contains("Activation")) {
				s1 = r
				s2 = 0
				i=s2
				logi.logInfo("After swapping :: " + s1 + "::" + s2 + "iterator value : " + i)
			}
			else if(s1) {
				i=i+1
				logi.logInfo("iterator value : " + i)
			}
			logi.logInfo("After setting values :: " + i)
			logi.logInfo("Iteration ----------------------------------------------------------------------------------------")
			String service_no = result.getString("SERVICE_NO")
			hashResult.put("service[" + i + "][service_no]" , service_no)
			hashResult.put("service[" + i + "][name]" , result.getString("COMMENTS"))
			hashResult.put("service[" + i + "][client_service_id]" , result.getString("CLIENT_SERVICE_ID"))
			hashResult.put("service[" + i + "][gl_cd]" , result.getString("CLIENT_COA_CODE"))
			String serType = getServiceType(result.getString("RECURRING"), result.getString("USAGE_BASED"), result.getString("IS_CANCELLATION"), result.getString("IS_MIN_FEE"), result.getString("IS_ORDER_BASED"))
			hashResult.put("service[" + i + "][service_type]" , serType)
			hashResult.put("service[" + i + "][pricing_rule]" , getPricingRule(result.getString("TIERED_PRICING_RULE_NO")))
			hashResult.put("service[" + i + "][usage_type]" , result.getString("USAGE_TYPE"))
			r--
		}
		logi.logInfo("md_VerifyServiceDetails_DB Hashmap is : " + hashResult)
		return hashResult.sort()
	}


	def md_VerifyPlanGroups_DB(String testcaseid) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		String apiname=testcaseid.split("-")[2]
		logi.logInfo("API Name:"+apiname)
		def plan_no = getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
		def client_no = getValueFromHttpRequestValue(apiname, "client_no")
		String planGroupQuery = "select GROUP_NO from ARIACORE.PLAN_CHANGE_GRP WHERE CLIENT_NO = " + client_no + " AND GROUP_NO in (SELECT GROUP_NO from ARIACORE.PLAN_CHANGE_MAP WHERE CLIENT_NO = " + client_no + " and PLAN_NO = " +plan_no+ ") ORDER BY GROUP_NO"
		ResultSet result = db.executePlaQuery(planGroupQuery)
		int p = 0
		while (result.next()) {
			logi.logInfo(" Inside while ")
			hashResult.put("plan_group"+p, result.getString("GROUP_NO"))
			p++
		}

		logi.logInfo(" Plan Group DB HASHMAP " + hashResult)
		return hashResult.sort()
	}

	def md_VerifyPlanGroups_API(String testcaseid) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		List<String> plan_groups=[]
		String apiname=testcaseid.split("-")[2]
		logi.logInfo("API Name:"+apiname)
		for(i in 0..1) {
			plan_groups.add(getValueFromHttpRequestValue(apiname, "plan_group[" + i + "]"))
		}
		int i=0
		ResultSet fld_vals
		for(n in plan_groups) {
			if(n != null) {
				hashResult.put("plan_group"+i, n)
			}
			i++
		}
		logi.logInfo("Plan group API Hashmap is : " + hashResult)
		return hashResult.sort()

	}

	String md_VirtualInventory(String testcaseid) {
		MySqlDB mysql = new MySqlDB()
		String apiname=testcaseid.split("-")[2]
		logi.logInfo("API Name:"+apiname)
		String plan_no = getValueFromResponse(apiname, ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
		ResultSet rs = mysql.executePlaQuery("SELECT resource_type_id FROM plan_resource_map where client_plan_id = " + plan_no)
		String result
		if(rs.next()) {
			result = rs.getString("resource_type_id")
		} else {
			result = "No Value"
		}
		return result
	}


	HashMap getRateScheduleNo() {
		HashMap<String, String> schedResultSet = new HashMap<String, String>()
		String plan_no = getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
		String query = "SELECT * FROM ARIACORE.NEW_RATE_SCHEDULES JOIN ARIACORE.PLAN_RATE_SCHED_MAP ON ARIACORE.NEW_RATE_SCHEDULES.SCHEDULE_NO = ARIACORE.PLAN_RATE_SCHED_MAP.SCHEDULE_NO WHERE ARIACORE.PLAN_RATE_SCHED_MAP.PLAN_NO = " + plan_no
		ConnectDB database = new ConnectDB()
		ResultSet rs = database.executePlaQuery(query)
		while(rs.next()) {
			schedResultSet.put(rs.getString("SCHEDULE_NAME"), rs.getString("SCHEDULE_NO"))
		}
		return schedResultSet.sort()
	}

	String md_VerifyRateSchedAmountAct_1(String testcaseid)
	{
		logi.logInfo("Entering amount act 1")
		return md_VerifyRateSchedAmountAct(testcaseid,1)
	}
	
	String md_VerifyRateSchedAmountAct_2(String testcaseid)
	{
		logi.logInfo("Entering amount act 2")
		return md_VerifyRateSchedAmountAct(testcaseid,2)
	}
	
	
	String md_VerifyRateSchedAmountAct_3(String testcaseid)
	{
		logi.logInfo("Entering amount act 3")
		return md_VerifyRateSchedAmountAct(testcaseid,3)
	}
	
	
	String md_VerifyRateSchedAmountAct_4(String testcaseid)
	{
		logi.logInfo("Entering amount act 4")
		return md_VerifyRateSchedAmountAct(testcaseid,4)
	}
	
	String md_VerifyRateSchedAmountAct(String testcaseid, int n) {
		logi.logInfo("md_VerifyRateSchedAmountAct "+n)
		Map<String, String> suppFieldValues = new TreeMap<String, String>()
		Map<String, String> sortedSerivceHash = getSortedSerivceOrderIndex()
		int rateSchedCount = getRateScheduleNo().size()
		int serviceCount = sortedSerivceHash.size()
		if(sortedSerivceHash.size() == 0) {
			sortedSerivceHash = getSortedSerivceOrderFromDB()
		}

		logi.logInfo("Service Index " + sortedSerivceHash.toString())
		int k = 1
		for(int i = 0 ; i < rateSchedCount ; i++) {

			Iterator iterator = sortedSerivceHash.entrySet().iterator();
			while (iterator.hasNext())
			{
				Map.Entry item = (Map.Entry) iterator.next();
				logi.logInfo(item.getKey() + " - " + item.getValue())
				for(int l = 0 ; l < n ; l++) {
					String key = "RateSched " + (i+1) +" Service No " + item.getKey() + " Rate tier "+(l+1)
					String value = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + l + "][schedule][" + i + "][amount]")
					logi.logInfo("Rate Sched " + key + " Value " + value)
					if(!value.equals(null)) {
						suppFieldValues.putAt(key, value)
						k++
					}
				}
			}
		}
		library.Constants.Constant.hashResultCreatePlanRateUnitsNew = suppFieldValues
		return suppFieldValues.toString()
	}

		
	String md_VerifyRateSchedAmountActEdit(String testcaseid, int n) {
		logi.logInfo("md_VerifyRateSchedAmountAct "+n)
		Map<String, String> suppFieldValues = new TreeMap<String, String>()
		Map<String, String> sortedSerivceHash = getSortedSerivceOrderIndex()
		int rateSchedCount = getRateScheduleNo().size()
		int serviceCount = sortedSerivceHash.size()
		if(sortedSerivceHash.size() == 0) {
			sortedSerivceHash = getSortedSerivceOrderFromDB()
		}

		logi.logInfo("Service Index " + sortedSerivceHash.toString())
		int k = 1
		for(int i = 0 ; i < rateSchedCount ; i++) {

			Iterator iterator = sortedSerivceHash.entrySet().iterator();
			while (iterator.hasNext())
			{
				Map.Entry item = (Map.Entry) iterator.next();
				logi.logInfo(item.getKey() + " - " + item.getValue())
				for(int l = 0 ; l < n ; l++) {
					String key = "RateSchedAmount " + k
					String value = getValueFromHttpRequestValue("edit_plan", "service[" + item.getValue() + "][tier][" + l + "][schedule][" + i + "][amount]")
					logi.logInfo("Rate Sched " + key + " Value " + value)
					if(!value.equals(null)) {
						suppFieldValues.putAt(key, value)
						k++
					}
				}
			}
		}
		return suppFieldValues.toString()
	}
	
	Map getSortedSerivceOrderIndex() {

		Map<String, String> schedResultSet = new HashMap<String, String>()
		int i = 0;
		while(!getValueFromHttpRequestValue("create_new_plan", "service[" + i + "][service_no]").equals(null)) {
			schedResultSet.putAt(getValueFromHttpRequestValue("create_new_plan", "service[" + i + "][service_no]"), i)
			i++
		}
		return schedResultSet.sort()
	}

	Map getSortedSerivceOrderFromDB() {

		Map<String, String> schedResultSet = new HashMap<String, String>()
		int i = 0;
		def plan_no = getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
		String rateSchedQuery = "SELECT * FROM ARIACORE.PLAN_SERVICES WHERE PLAN_NO=" + plan_no + " ORDER BY SERVICE_NO"
		ConnectDB database = new ConnectDB()
		ResultSet result = database.executePlaQuery(rateSchedQuery)

		while (result.next()) {
			schedResultSet.putAt(result.getInt("SERVICE_NO"), i)
			i++
		}
		return schedResultSet.sort()
	}
	
	String md_VerifyRateSchedAmountExpSeq_1(String testcaseid)
	{
		md_VerifyRateSchedAmountExpSeq(testcaseid,1)
	}
	
	String md_VerifyRateSchedAmountExpSeq_2(String testcaseid)
	{
		md_VerifyRateSchedAmountExpSeq(testcaseid,2)
	}
	
	String md_VerifyRateSchedAmountExpSeq_3(String testcaseid)
	{
		md_VerifyRateSchedAmountExpSeq(testcaseid,3)
	}
	
	String md_VerifyRateSchedAmountExpSeq(String testcaseid, int n) {
		
				Map<String, String> suppFieldValues = new TreeMap<String, String>()
		
				int rateSchedCount = getRateScheduleNo().size()
				def plan_no = getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
				String rateSchedQuery = "SELECT * FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SCHEDULE_NO IN (SELECT SCHEDULE_NO FROM (SELECT ROWNUM R, SCHEDULE_NO FROM ARIACORE.PLAN_RATE_SCHED_MAP WHERE PLAN_NO = " + plan_no + ")WHERE R = "+n+") ORDER BY SCHEDULE_NO, SERVICE_NO"
				ConnectDB database = new ConnectDB()
				ResultSet result = database.executePlaQuery(rateSchedQuery)
				int i=1;
				while (result.next()) {
					logi.logInfo("Schedule Amount " + i + " " +  result.getInt("RATE_PER_UNIT"))
					suppFieldValues.putAt("RateSchedAmount " + i, result.getInt("RATE_PER_UNIT"))
					i++
				}
				return suppFieldValues.toString()
			}
	
	
	String md_VerifyRateSchedAmountExp(String testcaseid) {

		Map<String, String> suppFieldValues = new TreeMap<String, String>()

		int rateSchedCount = getRateScheduleNo().size()
		def plan_no = getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
		String rateSchedQuery = "SELECT * FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SCHEDULE_NO IN (SELECT SCHEDULE_NO FROM ARIACORE.PLAN_RATE_SCHED_MAP WHERE PLAN_NO = " + plan_no + ") ORDER BY SCHEDULE_NO, SERVICE_NO"
		ConnectDB database = new ConnectDB()
		ResultSet result = database.executePlaQuery(rateSchedQuery)
		int i=0;
		String oldSchedNo=""
		while (result.next()) {
			if(oldSchedNo!=result.getString("SCHEDULE_NO")){
				oldSchedNo = result.getString("SCHEDULE_NO")
				i++
			}
			logi.logInfo("Schedule Amount " + i + " " +  result.getInt("RATE_PER_UNIT"))
			suppFieldValues.putAt("RateSched " + i +" Service No " + result.getString("SERVICE_NO") + " Rate tier "+result.getInt("RATE_SEQ_NO"), result.getInt("RATE_PER_UNIT"))
			
		}
		return suppFieldValues.toString()
	}
	
	String md_VerifyRateSchedAmountExpEdit(String testcaseid) {
		
				Map<String, String> suppFieldValues = new TreeMap<String, String>()
		
				int rateSchedCount = getRateScheduleNo().size()
				def plan_no = getValueFromResponse("edit_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
				String rateSchedQuery = "SELECT * FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SCHEDULE_NO IN (SELECT SCHEDULE_NO FROM ARIACORE.PLAN_RATE_SCHED_MAP WHERE PLAN_NO = " + plan_no + ") ORDER BY SCHEDULE_NO, SERVICE_NO"
				ConnectDB database = new ConnectDB()
				ResultSet result = database.executePlaQuery(rateSchedQuery)
				int i=1;
				while (result.next()) {
					logi.logInfo("Schedule Amount " + i + " " +  result.getInt("RATE_PER_UNIT"))
					suppFieldValues.putAt("RateSchedAmount " + i, result.getInt("RATE_PER_UNIT"))
					i++
				}
				return suppFieldValues.toString()
			}

	def md_VerifyRateSeqUnitsAct(String testcaseid) {
		Map<String, String> suppFieldValues = new TreeMap<String, String>()
		Map<String, String> rateSchedhash = getRateScheduleNo()
		Map<String, String> sortedSerivceHash = getSortedSerivceOrderIndex()
		if(sortedSerivceHash.size() == 0) {
			sortedSerivceHash = getSortedSerivceOrderFromDB()
		}

		Iterator iterator1 = rateSchedhash.entrySet().iterator();
		while (iterator1.hasNext())
		{
			Map.Entry item1 = (Map.Entry) iterator1.next();
			Iterator iterator = sortedSerivceHash.entrySet().iterator();

			while (iterator.hasNext())
			{
				Map.Entry item = (Map.Entry) iterator.next();
				for(int i = 1 ; i <= 3 ; i++ ) {

					String fromVal = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][from]")
					String toVal = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][to]")
					logi.logInfo(fromVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][from]")
					logi.logInfo(toVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][to]")
					if(toVal.equals(null)) { toVal = 0 }
					if(i == 1) {
						if(fromVal.equals(null)) {
							fromVal = 1
							suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + item.getKey() + " rate tier " + i, "from " + fromVal + " to " + toVal)
							logi.logInfo(item1.getValue() + " " + item.getKey() + " tier : " + i + " " + fromVal + " to " + toVal);
						} else {
							suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + item.getKey() + " rate tier " + i, "from " + fromVal + " to " + toVal)
							logi.logInfo(item1.getValue() + " " + item.getKey() + " tier : " + i + " " + fromVal + " to " + toVal);
						}
					} else {
						if (!fromVal.equals(null)) {
							suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + item.getKey() + " rate tier " + i, "from " + fromVal + " to " + toVal)
							logi.logInfo(item1.getValue() + " " + item.getKey() + " tier : " + i + " " + fromVal + " to " + toVal);
						}
					}
				}
			}
		}
		logi.logInfo("return_uk")
		return suppFieldValues
	}

	String md_VerifyRateSeqUnitsActSeq_1(String testcaseid)
	{
		md_VerifyRateSeqUnitsActSeq(testcaseid, 0)
	}
	
	String md_VerifyRateSeqUnitsActSeq_2(String testcaseid)
	{
		md_VerifyRateSeqUnitsActSeq(testcaseid, 1)
	}
	
	String md_VerifyRateSeqUnitsActSeq_3(String testcaseid)
	{
		md_VerifyRateSeqUnitsActSeq(testcaseid, 2)
	}
	
	String md_VerifyRateSeqUnitsActSeq(String testcaseid, int n) {
		Map<String, String> suppFieldValues = new TreeMap<String, String>()
		Map<String, String> rateSchedhash = getRateScheduleNo()
		Map<String, String> sortedSerivceHash = getSortedSerivceOrderIndex()
		if(sortedSerivceHash.size() == 0) {
			sortedSerivceHash = getSortedSerivceOrderFromDB()
		}

		Iterator iterator1 = rateSchedhash.entrySet().iterator();
		while (iterator1.hasNext())
		{
			Map.Entry item1 = (Map.Entry) iterator1.next();
			Iterator iterator = sortedSerivceHash.entrySet().iterator();

			while (iterator.hasNext())
			{
				Map.Entry item = (Map.Entry) iterator.next();
				for(int i = 1 ; i < 2 ; i++ ) {

					String fromVal = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + n + "][from]")
					String toVal = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + n + "][to]")
					logi.logInfo(fromVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + n + "][from]")
					logi.logInfo(toVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + n + "][to]")
					if(toVal.equals(null)) { toVal = 0 }
					if(i == 1) {
						if(fromVal.equals(null)) {
							fromVal = 1
							suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + item.getKey() + " rate tier " + i, "from " + fromVal + " to " + toVal)
							logi.logInfo(item1.getValue() + " " + item.getKey() + " tier : " + i + " " + fromVal + " to " + toVal);
						} else {
							suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + item.getKey() + " rate tier " + i, "from " + fromVal + " to " + toVal)
							logi.logInfo(item1.getValue() + " " + item.getKey() + " tier : " + i + " " + fromVal + " to " + toVal);
						}
					} else {
						if (!fromVal.equals(null)) {
							suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + item.getKey() + " rate tier " + i, "from " + fromVal + " to " + toVal)
							logi.logInfo(item1.getValue() + " " + item.getKey() + " tier : " + i + " " + fromVal + " to " + toVal);
						}
					}
				}
			}
		}
		return suppFieldValues.toString()
	}
	
	
	String md_VerifyRateSeqUnitsExp(String testcaseid) {

		Map<String, String> suppFieldValues = new TreeMap<String, String>()
		int rateSchedCount = getRateScheduleNo().size()
		def plan_no = getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
		String rateSchedQuery = "SELECT * FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SCHEDULE_NO IN (SELECT SCHEDULE_NO FROM ARIACORE.PLAN_RATE_SCHED_MAP WHERE PLAN_NO = " + plan_no + ") ORDER BY SCHEDULE_NO, SERVICE_NO"
		ConnectDB database = new ConnectDB()
		ResultSet result = database.executePlaQuery(rateSchedQuery)
		int i=1;
		while (result.next()) {
			logi.logInfo(result.getInt("SCHEDULE_NO") + " " + result.getInt("SERVICE_NO") + " rate tier " + result.getInt("RATE_SEQ_NO") + "From - " + result.getInt("FROM_UNIT") + " to -" + result.getInt("TO_UNIT"))
			suppFieldValues.putAt("Schedule No " + result.getInt("SCHEDULE_NO") + " Service No " + result.getInt("SERVICE_NO") + " rate tier " + result.getInt("RATE_SEQ_NO"), "from " + result.getInt("FROM_UNIT") + " to " + result.getInt("TO_UNIT"))
			i++
		}
		return suppFieldValues.toString()
	}
	
	String md_VerifyRateSeqUnitsExpSeq_1(String testcaseid)
	{
		md_VerifyRateSeqUnitsExpSeq(testcaseid, 1)
	}
	
	String md_VerifyRateSeqUnitsExpSeq_2(String testcaseid)
	{
		md_VerifyRateSeqUnitsExpSeq(testcaseid, 2)
	}
	
	String md_VerifyRateSeqUnitsExpSeq_3(String testcaseid)
	{
		md_VerifyRateSeqUnitsExpSeq(testcaseid, 3)
	}
	
	String md_VerifyRateSeqUnitsExpSeq(String testcaseid, int n) {
		
				Map<String, String> suppFieldValues = new TreeMap<String, String>()
				int rateSchedCount = getRateScheduleNo().size()
				def plan_no = getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
				String rateSchedQuery = "SELECT * FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SCHEDULE_NO IN (SELECT SCHEDULE_NO FROM (SELECT ROWNUM R, SCHEDULE_NO FROM ARIACORE.PLAN_RATE_SCHED_MAP WHERE PLAN_NO = " + plan_no + ")WHERE R = "+n+") ORDER BY SCHEDULE_NO, SERVICE_NO"
				ConnectDB database = new ConnectDB()
				ResultSet result = database.executePlaQuery(rateSchedQuery)
				int i=1;
				while (result.next()) {
					logi.logInfo(result.getInt("SCHEDULE_NO") + " " + result.getInt("SERVICE_NO") + " rate tier " + result.getInt("RATE_SEQ_NO") + "From - " + result.getInt("FROM_UNIT") + " to -" + result.getInt("TO_UNIT"))
					suppFieldValues.putAt("Schedule No " + result.getInt("SCHEDULE_NO") + " Service No " + result.getInt("SERVICE_NO") + " rate tier " + result.getInt("RATE_SEQ_NO"), "from " + result.getInt("FROM_UNIT") + " to " + result.getInt("TO_UNIT"))
					i++
				}
				return suppFieldValues.toString()
			}

	HashMap md_VerifyCouponCTdetails_API0(String tcid)
	{
		return  md_VerifyCouponCTdetails_API(tcid, 0)
	}

	HashMap md_VerifyCouponCTdetails_API1(String tcid)
	{
		return  md_VerifyCouponCTdetails_API(tcid, 1)
	}

	HashMap md_VerifyCouponCTdetails_DB1(String tcid)
	{
		return  md_VerifyCouponCTdetails_DB(tcid, 1)
	}

	HashMap md_VerifyCouponCTdetails_DB2(String tcid)
	{
		return  md_VerifyCouponCTdetails_DB(tcid, 2)
	}


	HashMap md_VerifyCouponCTdetails_DB3(String tcid)
	{
		return  md_VerifyCouponCTdetails_DB(tcid, 3)
	}

	String md_VerifyCouponExCTDetails_DB1(String tcid)
	{
		return  md_VerifyCouponExCTDetails_DB(tcid, 1)
	}

	String md_VerifyCouponExCTDetails_DB2(String tcid)
	{
		return  md_VerifyCouponExCTDetails_DB(tcid, 2)
	}

	String md_VerifyCouponExDRDetails_DB1(String tcid)
	{
		return  md_VerifyCouponExDRDetails_DB(tcid, 1)
	}

	String md_VerifyCouponExDRDetails_DB2(String tcid)
	{
		return  md_VerifyCouponExDRDetails_DB(tcid, 2)
	}

	String md_VerifyCouponExDRDetails_DB3(String tcid)
	{
		return  md_VerifyCouponExDRDetails_DB(tcid, 3)
	}

	HashMap md_VerifyCouponDRdetails_API0(String tcid)
	{
		return  md_VerifyCouponDRdetails_API(tcid, 0)
	}

	HashMap md_VerifyCouponDRdetails_API1(String tcid)
	{
		return  md_VerifyCouponDRdetails_API(tcid, 1)
	}

	HashMap md_VerifyCouponDBdetails_API0(String tcid)
	{
		return  md_VerifyCouponDBdetails_API(tcid, 0)
	}

	HashMap md_VerifyCouponDBdetails_API1(String tcid)
	{
		return  md_VerifyCouponDBdetails_API(tcid, 1)
	}

	HashMap md_VerifyCouponDRdetails_DB1(String tcid)
	{
		return  md_VerifyCouponDRdetails_DB(tcid, 1)
	}

	HashMap md_VerifyCouponDRdetails_DB2(String tcid)
	{
		return  md_VerifyCouponDRdetails_DB(tcid, 2)
	}

	HashMap md_VerifyCouponDRdetails_DB3(String tcid)
	{
		return  md_VerifyCouponDRdetails_DB(tcid, 3)
	}


	/**
	 * Verifying the Coupon Details API
	 * @param testCaseId
	 * @return Coupon Details API HashMap
	 */
	def md_VerifyCoupondetails_API(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]
		HashMap couponHash = new HashMap<String,String>()

		couponHash.put("COUPON_CD", getValueFromHttpRequestValue(apiname, "coupon_cd"))
		couponHash.put("COUPON_DESC", getValueFromHttpRequestValue(apiname, "coupon_desc"))
		couponHash.put("COUPON_MSG", getValueFromHttpRequestValue(apiname, "coupon_msg"))
		couponHash.put("STATUS", getValueFromHttpRequestValue(apiname, "status_ind"))
		couponHash.put("NO_OF_USES", getValueFromHttpRequestValue(apiname, "no_of_uses"))
		couponHash.put("START_DATE", getValueFromHttpRequestValue(apiname, "start_date"))
		couponHash.put("END_DATE", getValueFromHttpRequestValue(apiname, "end_date"))

		return couponHash.sort()
	}

	/**
	 * Verifying the Coupon Details DB
	 * @param testCaseId
	 * @return Coupon Details DB HashMap
	 */

	def md_VerifyCoupondetails_DB(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		def coupon_cd
		if (apiname == 'create_credit_template')
		{
			String credit_template_no = getValueFromResponse(apiname, ExPathAdminToolsApi.CREATE_CREDIT_TEMPLATE_NUMBER)
			String query1 = "Select * from ARIACORE.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE RECURRING_CREDIT_TEMPLATE_NO = " + credit_template_no + " AND CLIENT_NO = " + client_no + ""
			ResultSet rs1 = db.executePlaQuery(query1)
			while(rs1.next())
			{
				coupon_cd = rs1.getString('COUPON_CD')
			}
			logi.logInfo "coupon_cd ==="+ coupon_cd
		}
		else
		{
			coupon_cd = getValueFromResponse(apiname, ExPathAdminToolsApi.CREATE_COUPON_COUPON_CODE1)
		}


		HashMap coupondbHash = new HashMap<String, String>()
		String query1 = "SELECT * FROM ARIACORE.COUPONS WHERE COUPON_CD ='" + coupon_cd + "' AND CLIENT_NO=" + client_no
		ResultSet rs = db.executePlaQuery(query1)
		while(rs.next())
		{
			coupondbHash.put("COUPON_CD", rs.getString('COUPON_CD'))
			coupondbHash.put("COUPON_DESC", rs.getString('COMMENTS'))
			coupondbHash.put("COUPON_MSG", rs.getString('ONSCREEN_COUPON_USE_MSG'))
			coupondbHash.put("NO_OF_USES", rs.getString('MAX_USES'))
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			def startdate = rs.getDate('START_DATE');
			def enddate = rs.getDate('EXP_DATE');
			if (startdate == null)
			{
				coupondbHash.put("START_DATE", "null")
			}
			else
			{
				startdate = format.format(startdate);
				coupondbHash.put("START_DATE", startdate)
			}

			if (enddate == null)
			{
				coupondbHash.put("END_DATE", "null")
			}
			else
			{
				enddate = format.format(enddate).toString()
				coupondbHash.put("END_DATE", enddate)
			}
			/*                   if (apiname == 'create_credit_template')
			 {
			 if (rs.getString('NO_DISPLAY_IND') == '0')
			 {
			 coupondbHash.put("STATUS", "0")
			 }
			 else
			 {
			 coupondbHash.put("STATUS", "1")
			 }
			 }
			 else
			 {*/
			if (rs.getString('NO_DISPLAY_IND') == '0')
			{
				coupondbHash.put("STATUS", "1")
			}
			else
			{
				coupondbHash.put("STATUS", "0")
			}

		}
		return coupondbHash.sort()
	}


	/**
	 * Verifying the Coupon Count in DB
	 * @param testCaseId
	 * @return Coupon Count
	 */
	def md_VerifyCouponCount(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]
		def client_no = getValueFromHttpRequestValue(apiname, "client_no")
		def coupon_cd = getValueFromHttpRequestValue(apiname, "coupon_cd")
		def couponcount
		String query1 = "select count(*) from ariacore.coupons where CLIENT_NO ="+client_no+" and COUPON_CD = '"+coupon_cd+"'"
		couponcount=db.executeQueryP2(query1)
		return df.format(couponcount.toDouble()).toString()
	}


	/**
	 * Verifying the Discount Rule Count in DB
	 * @param testCaseId
	 * @return Discount Rule count
	 */
	def md_VerifyCouponDRCount(String testcaseid)
	{

		String apiname = testcaseid.split("-")[2]
		def client_no = getValueFromHttpRequestValue(apiname, "client_no")
		def coupon_cd = getValueFromHttpRequestValue(apiname, "coupon_cd")
		def couponDRcount
		String query1 = "select count(*) FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP Where CLIENT_NO=" + client_no +" AND COUPON_CD ='" + coupon_cd + "'"
		logi.logInfo("query1 "+query1)
		couponDRcount=db.executeQueryP2(query1)
		return df.format(couponDRcount.toDouble()).toString()
	}


	/**
	 * Verifying the Coupon Credit Template Details API
	 * @param testCaseId, line item
	 * @return Coupon Credit Template Details API HashMap
	 */
	def md_VerifyCouponCTdetails_API(String testcaseid, int line_item_no)
	{
		String apiname = testcaseid.split("-")[2]

		HashMap ctHash = new HashMap<String,String>()

		ctHash.put("CREDIT_TEMPLATE_NAME", getValueFromHttpRequestValue(apiname, "template[" + line_item_no + "][credit_template_name]"))
		ctHash.put("CREDIT_TEMPLATE_ID", getValueFromHttpRequestValue(apiname, "template[" + line_item_no + "][client_credit_template_id]"))
		ctHash.put("ELIGIBLE_PLAN_NO", getValueFromHttpRequestValue(apiname, "template[" + line_item_no + "][eligible_plan_no]"))
		ctHash.put("ELIGIBLE_SERVICE_NO", getValueFromHttpRequestValue(apiname, "template[" + line_item_no + "][eligible_service_no]"))
		ctHash.put("DISCOUNT_AMOUNT", getValueFromHttpRequestValue(apiname, "template[" + line_item_no + "][discount_amt]"))
		ctHash.put("PERCENTAGE_PLAN", getValueFromHttpRequestValue(apiname, "template[" + line_item_no + "][percentage_plan_no]"))
		ctHash.put("PERCENTAGE_SERVICE", getValueFromHttpRequestValue(apiname, "template[" + line_item_no + "][percentage_service_no]"))
		ctHash.put("ALTERNATE_SERVICE", getValueFromHttpRequestValue(apiname, "template[" + line_item_no + "][alt_service_no]"))

		String discountType = getValueFromHttpRequestValue(apiname, "template[" + line_item_no + "][discount_type]").toString()
		def currency = getValueFromHttpRequestValue(apiname, "template[" + line_item_no + "][currency_cd]").toString()
		String creditinterval = getValueFromHttpRequestValue(apiname, "template[" + line_item_no + "][credit_interval_months]").toString()
		String noofcredits = getValueFromHttpRequestValue(apiname, "template[" + line_item_no + "][no_of_credits]").toString()

		ctHash.put("CURRENCY", getValueFromHttpRequestValue(apiname, "template[" + line_item_no + "][currency_cd]"))

		if (discountType == "1")
		{
			ctHash.put("DISCOUNT_TYPE", "FLAT")
		}
		else
		{
			ctHash.put("DISCOUNT_TYPE", "PERCENTAGE")
		}

		if (creditinterval.isEmpty() || creditinterval == "null")
		{
			ctHash.put("CREDIT_INTERVAL", "1")
		}
		else
		{
			ctHash.put("CREDIT_INTERVAL", getValueFromHttpRequestValue(apiname, "template[" + line_item_no + "][credit_interval_months]"))
		}

		if (noofcredits.isEmpty() || noofcredits == "null")
		{
			ctHash.put("NO_OF_CREDITS", "1")
		}
		else
		{
			ctHash.put("NO_OF_CREDITS", getValueFromHttpRequestValue(apiname, "template[" + line_item_no + "][no_of_credits]"))
		}

		return ctHash.sort()
	}

	/**
	 * Verifying the Coupon Credit Template Details DB
	 * @param testCaseId, line item
	 * @return Coupon Credit Template Details DB HashMap
	 */
	def md_VerifyCouponCTdetails_DB(String testcaseid, int item_no)
	{
		String apiname = testcaseid.split("-")[2]

		String coupon_cd = getValueFromResponse(apiname, ExPathAdminToolsApi.CREATE_COUPON_COUPON_CODE1)
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		String currency = getValueFromHttpRequestValue(apiname, "template[" + (item_no-1) + "][currency_cd]")
		String discountType = getValueFromHttpRequestValue(apiname, "template[" + (item_no-1) + "][discount_type]")
		String eligiblePlanNo
		String eligibleServiceNo
		HashMap ctHash = new HashMap<String, String>()
		String query1 = "SELECT * FROM ARIACORE.RECURRING_CREDIT_TEMPLATES where RECURRING_CREDIT_TEMPLATE_NO in (SELECT RECURRING_CREDIT_TEMPLATE_NO from (select RECURRING_CREDIT_TEMPLATE_NO,row_number() over (order by RECURRING_CREDIT_TEMPLATE_NO) as sno FROM ARIACORE.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE COUPON_CD ='" + coupon_cd + "' AND CLIENT_NO=" + client_no +") where sno=" + item_no + ")"
		ResultSet rs = db.executePlaQuery(query1)
		while(rs.next())
		{

			ctHash.put("CREDIT_TEMPLATE_NAME", rs.getString('LABEL'))
			ctHash.put("CREDIT_TEMPLATE_ID", rs.getString('CLIENT_CREDIT_TEMPLATE_ID'))
			ctHash.put("ELIGIBLE_PLAN_NO",rs.getString('ELIGIBLE_PLAN_NO'))
			ctHash.put("ELIGIBLE_SERVICE_NO", rs.getString('ELIGIBLE_SERVICE_NO'))
			ctHash.put("NO_OF_CREDITS", rs.getString('NUM_CREDITS_REQUIRED'))
			ctHash.put("CREDIT_INTERVAL", rs.getString('CREDIT_INTERVAL_MONTHS'))
			ctHash.put("ALTERNATE_SERVICE", rs.getString('ALT_SERVICE_NO_2_APPLY'))

			if (discountType == "1")
			{
				ctHash.put("CURRENCY", rs.getString('CURRENCY_CD'))
				ctHash.put("DISCOUNT_AMOUNT", rs.getString('FLAT_AMOUNT'))
				ctHash.put("PERCENTAGE_PLAN", rs.getString('PERCENT_EVAL_PLAN_NO'))
				ctHash.put("PERCENTAGE_SERVICE", rs.getString('PERCENT_EVAL_SERVICE_NO'))
				ctHash.put("DISCOUNT_TYPE", "FLAT")
			}

			if (discountType == "2")
			{
				ctHash.put("CURRENCY", "null")
				ctHash.put("DISCOUNT_AMOUNT", rs.getString('PERCENT_AMOUNT'))
				ctHash.put("PERCENTAGE_PLAN", rs.getString('PERCENT_EVAL_PLAN_NO'))
				ctHash.put("PERCENTAGE_SERVICE", rs.getString('PERCENT_EVAL_SERVICE_NO'))
				ctHash.put("DISCOUNT_TYPE", "PERCENTAGE")
			}

			else
			{
				ctHash.put("CURRENCY", rs.getString('CURRENCY_CD'))
				ctHash.put("DISCOUNT_AMOUNT", rs.getString('FLAT_AMOUNT'))
				ctHash.put("PERCENTAGE_PLAN", rs.getString('PERCENT_EVAL_PLAN_NO'))
				ctHash.put("PERCENTAGE_SERVICE", rs.getString('PERCENT_EVAL_SERVICE_NO'))
				ctHash.put("DISCOUNT_TYPE", "FLAT")
			}
		}

		logi.logInfo "coupon CT db dETAILS "+ ctHash.toString()
		return ctHash.sort()
	}


	/**
	 * Verifying the Coupon Existing Credit Template Details DB
	 * @param testCaseId, line item
	 * @return Coupon Existing Credit Template Details DB HashMap
	 */
	String md_VerifyCouponExCTDetails_DB(String testcaseid, int item_no)
	{
		def apiname = testcaseid.split("-")[2]
		logi.logInfo("couponcount "+ apiname)
		def client_no = getValueFromHttpRequestValue(apiname, "client_no")
		def coupon_cd = getValueFromHttpRequestValue(apiname, "coupon_cd")
		String couponCTNO
		String query1 = "SELECT * FROM ARIACORE.RECURRING_CREDIT_TEMPLATES where CLIENT_NO=" + client_no +" AND RECURRING_CREDIT_TEMPLATE_NO in (SELECT RECURRING_CREDIT_TEMPLATE_NO from (select RECURRING_CREDIT_TEMPLATE_NO,row_number() over (order by RECURRING_CREDIT_TEMPLATE_NO) as sno FROM ARIACORE.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE COUPON_CD ='" + coupon_cd + "' AND CLIENT_NO=" + client_no +") where sno=" + item_no + ")"
		ResultSet rs = db.executePlaQuery(query1)
		while(rs.next())
		{
			couponCTNO = rs.getString('RECURRING_CREDIT_TEMPLATE_NO')
		}

		return couponCTNO
	}


	/**
	 * Verifying the Coupon Existing Discount Rule Details DB
	 * @param testCaseId, line item
	 * @return Coupon Existing Discount Rule Details DB HashMap
	 */
	String md_VerifyCouponExDRDetails_DB(String testcaseid, int item_no)
	{
		def apiname = testcaseid.split("-")[2]
		logi.logInfo("couponcount "+ apiname)
		def client_no = getValueFromHttpRequestValue(apiname, "client_no")
		def coupon_cd = getValueFromHttpRequestValue(apiname, "coupon_cd")
		String couponDRNO
		String query1 = "SELECT * FROM ARIACORE.CLIENT_DISCOUNT_RULES where CLIENT_NO=" + client_no +" AND RULE_NO in (SELECT RULE_NO from (select RULE_NO,row_number() over (order by RULE_NO) as sno FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP WHERE COUPON_CD ='" + coupon_cd + "' AND CLIENT_NO=" + client_no +") where sno=" + item_no + ")"
		ResultSet rs = db.executePlaQuery(query1)
		while(rs.next())
		{
			couponDRNO = rs.getString('RULE_NO')
		}

		return couponDRNO
	}


	/**
	 * Verifying the Coupon Existing Discount Bundle Details DB
	 * @param testCaseId
	 * @return Coupon Existing Discount Bundle Details DB HashMap
	 */
	String md_VerifyCouponExDBDetails_DB(String testcaseid)
	{
		def apiname = testcaseid.split("-")[2]
		logi.logInfo("couponcount "+ apiname)
		def client_no = getValueFromHttpRequestValue(apiname, "client_no")
		def coupon_cd = getValueFromHttpRequestValue(apiname, "coupon_cd")
		String couponDBNO
		String query1 = "SELECT * FROM ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP where CLIENT_NO=" + client_no +" AND COUPON_CD ='" + coupon_cd + "'"
		ResultSet rs = db.executePlaQuery(query1)
		while(rs.next())
		{
			couponDBNO = rs.getString('BUNDLE_NO')
		}

		return couponDBNO
	}


	/**
	 * Verifying the Coupon Discount Rule Details API
	 * @param testCaseId, line item
	 * @return Coupon Discount Rule Details API HashMap
	 */
	def md_VerifyCouponDRdetails_API(String testcaseid, int line_item_no)
	{
		String apiname = testcaseid.split("-")[2]

		String discountType
		String scope
		String tranScope
		String clientPlanId
		String nthSubRule
		String serviceCode

		HashMap drHash = new HashMap<String,String>()

		drHash.put("LABEL", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][label]"))
		drHash.put("RULE_ID", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][rule_id]"))
		drHash.put("RULE_DESCRIPTION", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][description]"))
		drHash.put("RULE_EXT_DESCRIPTION", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][ext_description]"))
		drHash.put("DISCOUNT_TYPE", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][flat_percent_ind]"))
		drHash.put("DISCOUNT_AMOUNT", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][amount]"))
		drHash.put("CURRENCY", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][currency]"))
		drHash.put("DURATION", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][duration_type_ind]"))
		drHash.put("MAX_APPLICABLE_MONTHS", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][max_applicable_months]"))
		drHash.put("MAX_APPLICATIONS_USES", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][max_applications_per_acct]"))
		drHash.put("INVOICE_DISPLAY_METHOD", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][inline_offset_ind]"))
		drHash.put("SCOPE", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][scope_no]"))
		drHash.put("SCOPE_PLAN_NO", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][plan_no]"))
		drHash.put("SCOPE_SERVICE_NO", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][service_no]"))
		drHash.put("SCOPE_ITEM_NO", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][item_no]"))
		drHash.put("SERVICE_CODE", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][service_code_to_use]"))
		drHash.put("ALT_SERVICE_NO", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][alt_service_no_2_apply]"))
		tranScope = getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][applicable_trans_scope]")
		scope = getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][scope_no]")
		nthSubRule = getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][use_all_or_nth_subs_rule]")
		discountType = getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][flat_percent_ind]").toString()
		serviceCode = getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][service_code_to_use]")
		if (discountType == "F")
		{
			drHash.put("DISCOUNT_TYPE", "FLAT")
		}
		else
		{
			drHash.put("DISCOUNT_TYPE", "PERCENTAGE")
		}

		if(serviceCode == null)
		{
			drHash.put("ALT_SERVICE_NO", "null")
		}

		if (tranScope == null)
		{
			drHash.put("APPLICABLE_TRNASACTION_SCOPE", "E")
		}
		else
		{
			drHash.put("APPLICABLE_TRNASACTION_SCOPE", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][applicable_trans_scope]"))
		}

		if (scope =="0")
		{
			drHash.put("USE ALL Nth SUB RULE", "FALSE")
		}
		else
		{
			if (nthSubRule == null)
			{
				drHash.put("USE ALL Nth SUB RULE", "FALSE")
			}

			else
			{
				if (nthSubRule == "TRUE" && scope =="11" && discountType == "P" )
				{
					drHash.put("USE ALL Nth SUB RULE", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][use_all_or_nth_subs_rule]"))
					drHash.put("APPLICABLE_TRNASACTION_SCOPE", "E")
				}
				else
				{
					drHash.put("USE ALL Nth SUB RULE", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][use_all_or_nth_subs_rule]"))
				}
			}
		}

		if (scope =="11")
		{
			clientPlanId = getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][client_plan_id][" + line_item_no + "]")
			if (clientPlanId == null)
			{
				drHash.put("SCOPE_PLAN_NO", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][plan_no][" + line_item_no + "]"))
			}
			else
			{
				drHash.put("SCOPE_PLAN_NO", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][client_plan_id][" + line_item_no + "]"))
			}
		}

		if (scope =="31")
		{
			drHash.put("SCOPE_ITEM_NO", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][item_no][" + line_item_no + "]"))
		}

		if (scope =="12")
		{
			drHash.put("SCOPE_SERVICE_NO", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][service_no][" + line_item_no + "]"))
		}

		if (scope =="13")
		{
			clientPlanId = getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][client_plan_id][" + line_item_no + "]")
			if (clientPlanId == null)
			{
				drHash.put("SCOPE_PLAN_NO", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][plan_no][" + line_item_no + "]"))
				drHash.put("SCOPE_SERVICE_NO", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][service_no][" + line_item_no + "][" + line_item_no + "]"))
			}
			else
			{
				drHash.put("SCOPE_PLAN_NO", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][client_plan_id][" + line_item_no + "]"))
				drHash.put("SCOPE_SERVICE_NO", getValueFromHttpRequestValue(apiname, "discount_rule[" + line_item_no + "][client_service_id][" + line_item_no + "][" + line_item_no + "]"))
			}
		}
		logi.logInfo "DISCOUNT RULE Details API " + drHash.toString()
		return drHash.sort()
	}


	/**
	 * Verifying the Coupon Discount Rule Details DB
	 * @param testCaseId, line item
	 * @return Coupon Discount Rule Details DB HashMap
	 */
	def md_VerifyCouponDRdetails_DB(String testcaseid, int item_no)
	{
		String apiname = testcaseid.split("-")[2]

		String coupon_cd = getValueFromResponse(apiname, ExPathAdminToolsApi.CREATE_COUPON_COUPON_CODE1)
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		String currency = getValueFromHttpRequestValue(apiname, "discount_rule[" + (item_no-1) + "][currency]")
		String discountType
		String scope
		String serviceCode = getValueFromHttpRequestValue(apiname, "discount_rule[" + (item_no-1) + "][service_code_to_use]")

		if (apiname == "update_coupon")
		{
			serviceCode = getValueFromHttpRequestValue(apiname, "discount_rule[" + (item_no-2) + "][service_code_to_use]")
		}

		HashMap drHash = new HashMap<String,String>()
		String query1 = "SELECT * FROM ARIACORE.CLIENT_DISCOUNT_RULES where CLIENT_NO=" + client_no +" AND RULE_NO in (SELECT RULE_NO from (select RULE_NO,row_number() over (order by RULE_NO) as sno FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP WHERE COUPON_CD ='" + coupon_cd + "' AND CLIENT_NO=" + client_no +") where sno=" + item_no + ")"
		ResultSet rs = db.executePlaQuery(query1)
		while(rs.next())
		{
			drHash.put("LABEL", rs.getString('LABEL'))
			drHash.put("RULE_ID", rs.getString('CLIENT_RULE_ID'))
			drHash.put("RULE_DESCRIPTION", rs.getString('DESCRIPTION'))
			drHash.put("RULE_EXT_DESCRIPTION", rs.getString('EXT_DESCRIPTION'))
			drHash.put("DISCOUNT_TYPE", rs.getString('FLAT_PERCENT_IND'))
			drHash.put("DISCOUNT_AMOUNT", rs.getString('AMOUNT'))
			drHash.put("CURRENCY", rs.getString('CURRENCY_CD'))
			drHash.put("DURATION", rs.getString('DURATION_TYPE_IND'))
			drHash.put("MAX_APPLICABLE_MONTHS", rs.getString('MAX_APPLICABLE_MONTHS'))
			drHash.put("MAX_APPLICATIONS_USES", rs.getString('MAX_APPLICATIONS_PER_ACCT'))
			drHash.put("INVOICE_DISPLAY_METHOD", rs.getString('INLINE_OFFSET_IND'))
			drHash.put("SCOPE", rs.getString('SCOPE_NO'))
			drHash.put("ALT_SERVICE_NO", rs.getString('ALT_SERVICE_NO_2_APPLY'))
			drHash.put("APPLICABLE_TRNASACTION_SCOPE", rs.getString('APPLICABLE_TRANS_SCOPE'))
			drHash.put("SERVICE_CODE", serviceCode)
			drHash.put("USE ALL Nth SUB RULE", rs.getString('USE_ALL_OR_NTH_SUBS_RULE'))
			discountType =  rs.getString('FLAT_PERCENT_IND')

			if (discountType == "F")
			{
				drHash.put("DISCOUNT_TYPE", "FLAT")
			}
			else
			{
				drHash.put("DISCOUNT_TYPE", "PERCENTAGE")
			}

			scope = rs.getString('SCOPE_NO')
		}

		if (scope =="0" || scope =="10" || scope =="21" || scope =="22" || scope =="23"|| scope =="30")
		{
			drHash.put("SCOPE_PLAN_NO", "null")
			drHash.put("SCOPE_SERVICE_NO", "null")
			drHash.put("SCOPE_ITEM_NO", "null")

		}
		else if (scope =="31")
		{
			String query2 = "SELECT * FROM ARIACORE.CLIENT_DR_COMPONENTS Where CLIENT_NO=" + client_no +" AND RULE_NO in (SELECT RULE_NO from (select RULE_NO,row_number() over (order by RULE_NO) as sno FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP WHERE COUPON_CD ='" + coupon_cd + "' AND CLIENT_NO=" + client_no +") where sno=" + item_no + ")"
			rs = db.executePlaQuery(query2)
			while(rs.next())
			{
				drHash.put("SCOPE_ITEM_NO", rs.getString('KEY_1_VAL'))
				drHash.put("SCOPE_SERVICE_NO", rs.getString('KEY_2_VAL'))
				drHash.put("SCOPE_PLAN_NO", rs.getString('KEY_3_VAL'))
			}
		}

		else if (scope =="12")
		{
			String query2 = "SELECT * FROM ARIACORE.CLIENT_DR_COMPONENTS Where CLIENT_NO=" + client_no +" AND RULE_NO in (SELECT RULE_NO from (select RULE_NO,row_number() over (order by RULE_NO) as sno FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP WHERE COUPON_CD ='" + coupon_cd + "' AND CLIENT_NO=" + client_no +") where sno=" + item_no + ")"
			rs = db.executePlaQuery(query2)
			while(rs.next())
			{
				drHash.put("SCOPE_ITEM_NO", rs.getString('KEY_3_VAL'))
				drHash.put("SCOPE_SERVICE_NO", rs.getString('KEY_1_VAL'))
				drHash.put("SCOPE_PLAN_NO", rs.getString('KEY_2_VAL'))
			}
		}
		else
		{
			String query2 = "SELECT * FROM ARIACORE.CLIENT_DR_COMPONENTS Where CLIENT_NO=" + client_no +" AND RULE_NO in (SELECT RULE_NO from (select RULE_NO,row_number() over (order by RULE_NO) as sno FROM ARIACORE.COUPON_DISCOUNT_RULE_MAP WHERE COUPON_CD ='" + coupon_cd + "' AND CLIENT_NO=" + client_no +") where sno=" + item_no + ")"
			rs = db.executePlaQuery(query2)
			while(rs.next())
			{
				drHash.put("SCOPE_PLAN_NO", rs.getString('KEY_1_VAL'))
				drHash.put("SCOPE_SERVICE_NO", rs.getString('KEY_2_VAL'))
				drHash.put("SCOPE_ITEM_NO", rs.getString('KEY_3_VAL'))
			}
		}

		return drHash.sort()
	}


	/**
	 * Verifying the Coupon Discount Bundle Details API
	 * @param testCaseId, line item
	 * @return Coupon Discount Bundle Details API HashMap
	 */
	def md_VerifyCouponDBdetails_API(String testcaseid, int line_item_no)
	{
		String apiname = testcaseid.split("-")[2]

		String allowOverlap

		HashMap dbHash = new HashMap<String,String>()
		dbHash.put("BUNDLE_NAME", getValueFromHttpRequestValue(apiname, "discount_bundle[" + line_item_no + "][bundle_name]"))
		dbHash.put("BUNDLE_ID", getValueFromHttpRequestValue(apiname, "discount_bundle[" + line_item_no + "][bundle_id]"))
		dbHash.put("BUNDLE_DESCRIPTION", getValueFromHttpRequestValue(apiname, "discount_bundle[" + line_item_no + "][bundle_description]"))
		dbHash.put("RULE_ID_1", getValueFromHttpRequestValue(apiname, "discount_bundle[" + line_item_no + "][rules][1]"))
		dbHash.put("RULE_ID_2", getValueFromHttpRequestValue(apiname, "discount_bundle[" + line_item_no + "][rules][2]"))

		allowOverlap = getValueFromHttpRequestValue(apiname, "discount_bundle[" + line_item_no + "][allow_overlap_ind]").toString()

		if (allowOverlap == "Y")
		{
			dbHash.put("ALLOW_OVERLAP", "Yes")
		}
		else
		{
			dbHash.put("ALLOW_OVERLAP", "NO")
		}

		return dbHash.sort()
	}


	/**
	 * Verifying the Coupon Discount Bundle Details DB
	 * @param testCaseId
	 * @return Coupon Discount Bundle Details DB HashMap
	 */
	def md_VerifyCouponDBdetails_DB(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]

		String coupon_cd = getValueFromResponse(apiname, ExPathAdminToolsApi.CREATE_COUPON_COUPON_CODE1)
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		String allowOverlap

		HashMap dbHash = new HashMap<String,String>()
		String query1 = "SELECT * FROM ARIACORE.CLIENT_DISCOUNT_RULE_BUNDLES WHERE BUNDLE_NO in (SELECT BUNDLE_NO from (select BUNDLE_NO FROM ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP WHERE COUPON_CD ='" + coupon_cd + "' AND CLIENT_NO=" + client_no +"))"
		ResultSet rs = db.executePlaQuery(query1)
		while(rs.next())
		{
			dbHash.put("BUNDLE_NAME", rs.getString('LABEL'))
			dbHash.put("BUNDLE_ID", rs.getString('CLIENT_BUNDLE_ID'))
			dbHash.put("BUNDLE_DESCRIPTION", rs.getString('DESCRIPTION'))

			allowOverlap = rs.getString('ALLOW_OVERLAP_IND')
			if (allowOverlap == "Y")
			{
				dbHash.put("ALLOW_OVERLAP", "Yes")
			}
			else
			{
				dbHash.put("ALLOW_OVERLAP", "NO")
			}
		}
		dbHash.sort()
		String query2 = "SELECT * FROM ARIACORE.CLIENT_DR_BUNDLE_MEMBERS Where CLIENT_NO=" + client_no +" AND BUNDLE_NO in (SELECT BUNDLE_NO from (select BUNDLE_NO FROM ARIACORE.COUPON_DISCOUNT_BUNDLE_MAP WHERE COUPON_CD ='" + coupon_cd + "' AND CLIENT_NO=" + client_no +"))"
		rs = db.executePlaQuery(query2)
		int i= 1
		while(rs.next())
		{
			dbHash.put("RULE_ID_"+i+"", rs.getString('RULE_NO'))
			i++
		}
		return dbHash
	}


	/**
	 * Verifying the Coupon Credit Template Details API
	 * @param testCaseId
	 * @return Coupon Credit Template Details API HashMap
	 */
	def md_get_coupon_details_API(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]

		HashMap ctHash = new HashMap<String,String>()

		ctHash.put("COUPON_CD", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_DETAILS_COUPON_CD))
		ctHash.put("COUPON_DESC", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_DETAILS_COUPON_DESC))
		ctHash.put("COUPON_MSG", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_DETAILS_COUPON_MSG))
		ctHash.put("START_DATE", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_DETAILS_COUPON_START_DATE))
		String end_date = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_DETAILS_COUPON_END_DATE)
		ctHash.put("END_DATE", end_date=='NoVal'?'null':end_date)
		ctHash.put("NO_OF_USES", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_DETAILS_COUPON_NO_OF_USES))
		ctHash.put("STATUS", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_DETAILS_COUPON_STATUS))
		ctHash.put("CREDIT_TEMPLATE_NAME", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_DETAILS_CREDIT_TEMPLATE_NAME))
		ctHash.put("CREDIT_TEMPLATE_ID", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_DETAILS_CREDIT_TEMPLATE_ID))
		String eligible_plan_no = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_DETAILS_ELIGIBLE_PLAN_NO)
		ctHash.put("ELIGIBLE_PLAN_NO", eligible_plan_no='NoVal'?'null':eligible_plan_no)
		String eligible_service_no = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_DETAILS_ELIGIBLE_SERVICE_NO)
		ctHash.put("ELIGIBLE_SERVICE_NO", eligible_service_no=='NoVal'?'null':eligible_service_no)
		ctHash.put("DISCOUNT_AMOUNT", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_DETAILS_DISCOUNT_AMOUNT))
		String percentage_plan = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_DETAILS_PERCENTAGE_PLAN)
		String percentage_service = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_DETAILS_PERCENTAGE_SERVICE)
		ctHash.put("PERCENTAGE_PLAN", percentage_plan=='NoVal'?'null':percentage_plan)
		ctHash.put("PERCENTAGE_SERVICE", percentage_service=='NoVal'?'null':percentage_service)
		String alt_service = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_DETAILS_ALTERNATE_SERVICE)
		ctHash.put("ALTERNATE_SERVICE", alt_service=='NoVal'?'null':alt_service)
		ctHash.put("DISCOUNT_TYPE", (getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_DETAILS_DISCOUNT_TYPE)=='2')?'PERCENTAGE':'FLAT')

		String creditinterval = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_DETAILS_CREDIT_INTERVAL)
		String noofcredits = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_DETAILS_NO_OF_CREDITS)

		if (creditinterval.isEmpty() || creditinterval == "null")
		{
			ctHash.put("CREDIT_INTERVAL", "1")
		}
		else
		{
			ctHash.put("CREDIT_INTERVAL", creditinterval)
		}

		if (noofcredits.isEmpty() || noofcredits == "null")
		{
			ctHash.put("NO_OF_CREDITS", "1")
		}
		else
		{
			ctHash.put("NO_OF_CREDITS", noofcredits)
		}
		logi.logInfo "coupon CT API dETAILS "+ ctHash.toString()
		return ctHash.sort()
	}

	/**
	 * Verifying the Coupon Credit Template Details DB
	 * @param testCaseId
	 * @return Coupon Credit Template Details DB HashMap
	 */
	def md_get_coupon_details_DB(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]

		String coupon_cd = getValueFromResponse(apiname, ExPathAdminToolsApi.CREATE_COUPON_COUPON_CODE1)
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		String discountType = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_DETAILS_DISCOUNT_TYPE)
		String eligiblePlanNo
		String eligibleServiceNo
		HashMap ctHash = new HashMap<String, String>()
		String query1 = "SELECT * FROM ARIACORE.RECURRING_CREDIT_TEMPLATES where RECURRING_CREDIT_TEMPLATE_NO in (SELECT RECURRING_CREDIT_TEMPLATE_NO from (select RECURRING_CREDIT_TEMPLATE_NO,row_number() over (order by RECURRING_CREDIT_TEMPLATE_NO) as sno FROM ARIACORE.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE COUPON_CD ='" + coupon_cd + "' AND CLIENT_NO=" + client_no +") where sno=1)"
		ResultSet rs = db.executePlaQuery(query1)
		while(rs.next())
		{

			ctHash.put("CREDIT_TEMPLATE_NAME", rs.getString('LABEL'))
			ctHash.put("CREDIT_TEMPLATE_ID", rs.getString('CLIENT_CREDIT_TEMPLATE_ID'))
			ctHash.put("ELIGIBLE_PLAN_NO",rs.getString('ELIGIBLE_PLAN_NO'))
			ctHash.put("ELIGIBLE_SERVICE_NO", rs.getString('ELIGIBLE_SERVICE_NO'))
			ctHash.put("NO_OF_CREDITS", rs.getString('NUM_CREDITS_REQUIRED'))
			ctHash.put("CREDIT_INTERVAL", rs.getString('CREDIT_INTERVAL_MONTHS'))
			ctHash.put("ALTERNATE_SERVICE", rs.getString('ALT_SERVICE_NO_2_APPLY'))

			if (discountType == "1")
			{
				ctHash.put("DISCOUNT_AMOUNT", rs.getString('FLAT_AMOUNT'))
				ctHash.put("PERCENTAGE_PLAN", rs.getString('PERCENT_EVAL_PLAN_NO'))
				ctHash.put("PERCENTAGE_SERVICE", rs.getString('PERCENT_EVAL_SERVICE_NO'))
				ctHash.put("DISCOUNT_TYPE", "FLAT")
			}

			if (discountType == "2")
			{
				ctHash.put("DISCOUNT_AMOUNT", rs.getString('PERCENT_AMOUNT'))
				ctHash.put("PERCENTAGE_PLAN", rs.getString('PERCENT_EVAL_PLAN_NO'))
				ctHash.put("PERCENTAGE_SERVICE", rs.getString('PERCENT_EVAL_SERVICE_NO'))
				ctHash.put("DISCOUNT_TYPE", "PERCENTAGE")
			}

			else
			{
				ctHash.put("DISCOUNT_AMOUNT", rs.getString('FLAT_AMOUNT'))
				ctHash.put("PERCENTAGE_PLAN", rs.getString('PERCENT_EVAL_PLAN_NO'))
				ctHash.put("PERCENTAGE_SERVICE", rs.getString('PERCENT_EVAL_SERVICE_NO'))
				ctHash.put("DISCOUNT_TYPE", "FLAT")
			}
		}

		String query2 = "SELECT * FROM ARIACORE.COUPONS WHERE COUPON_CD ='" + coupon_cd + "' AND CLIENT_NO=" + client_no
		rs = db.executePlaQuery(query2)
		while(rs.next())
		{
			ctHash.put("COUPON_CD", rs.getString('COUPON_CD'))
			ctHash.put("COUPON_DESC", rs.getString('COMMENTS'))
			ctHash.put("COUPON_MSG", rs.getString('ONSCREEN_COUPON_USE_MSG'))
			ctHash.put("NO_OF_USES", rs.getString('MAX_USES'))
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			def startdate = rs.getDate('START_DATE');
			def enddate = rs.getDate('EXP_DATE');
			if (startdate == null)
			{
				ctHash.put("START_DATE", "null")
			}
			else
			{
				startdate = format.format(startdate);
				ctHash.put("START_DATE", startdate)
			}

			if (enddate == null)
			{
				ctHash.put("END_DATE", "null")
			}
			else
			{
				enddate = format.format(enddate).toString()
				ctHash.put("END_DATE", enddate)
			}
			if (rs.getString('NO_DISPLAY_IND') == '0')
			{
				ctHash.put("STATUS", "1")
			}
			else
			{
				ctHash.put("STATUS", "0")
			}
		}

		logi.logInfo "coupon CT db dETAILS "+ ctHash.toString()
		return ctHash.sort()
	}

	/**
	 * Get the recurring template number
	 * @param testCaseId, coupon code
	 * @return recurring template number
	 */
	String get_recurring_template_no(String testcaseid, String coupon)
	{
		ResultSet rs = db.executeQuery("SELECT recurring_credit_template_no from ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP where coupon_cd='"+ coupon + "'")
		while(rs.next())
		{
			return rs.getString(1)
		}
	}

	/**
	 * Verifying the Recurring Credit Template Details API
	 * @param testCaseId
	 * @return Recurring Credit Template Details API HashMap
	 */
	def md_recurring_credit_template_details_API(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]
		HashMap ctHash = new HashMap<String,String>()
		ctHash.put("CREDIT_TEMPLATE_NAME", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_DETAILS_CREDIT_TEMPLATE_NAME))
		ctHash.put("CREDIT_TEMPLATE_ID", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_CREDIT_TEMPLATE_ID))
		ctHash.put("CLIENT_CREDIT_TEMPLATE_ID", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_CLIENT_CREDIT_TEMPLATE_ID))
		String eligible_plan_no = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_ELIGIBLE_PLAN_NO)
		ctHash.put("ELIGIBLE_PLAN_NO", eligible_plan_no='NoVal'?'null':eligible_plan_no)
		String eligible_service_no = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_ELIGIBLE_SERVICE_NO)
		ctHash.put("ELIGIBLE_SERVICE_NO", eligible_service_no=='NoVal'?'null':eligible_service_no)
		String alt_service = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_ALTERNATE_SERVICE)
		ctHash.put("ALTERNATE_SERVICE", alt_service=='NoVal'?'null':alt_service)
		String discount_type = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_DISCOUNT_TYPE)
		ctHash.put("DISCOUNT_TYPE", discount_type)
		if(discount_type=="Flat")
		{
			ctHash.put("DISCOUNT_AMOUNT", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_DISCOUNT_AMOUNT))
		}
		else
		{
			ctHash.put("PERCENT_AMOUNT", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_DISCOUNT_AMOUNT))
		}

		ctHash.put("COUPON_CODE", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_COUPON_CD))
		ctHash.put("COUPON_DESC", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_COUPON_DESC))
		ctHash.put("COUPON_MSG", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_COUPON_MSG))
		ctHash.put("MAXIMUM_USES", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_MAX_USES))

		String creditinterval = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_CREDIT_INTERVAL)
		String noofcredits = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_NO_OF_CREDITS)

		if (creditinterval.isEmpty() || creditinterval == "null")
		{
			ctHash.put("CREDIT_INTERVAL", "1")
		}
		else
		{
			ctHash.put("CREDIT_INTERVAL", creditinterval)
		}

		if (noofcredits.isEmpty() || noofcredits == "null")
		{
			ctHash.put("NO_OF_CREDITS", "1")
		}
		else
		{
			ctHash.put("NO_OF_CREDITS", noofcredits)
		}
		logi.logInfo "coupon CT API dETAILS "+ ctHash.toString()
		return ctHash.sort()

	}

	/**
	 * Verifying the Recurring Credit Template Details DB
	 * @param testCaseId
	 * @return Recurring Credit Template Details API HashMap
	 */
	def md_recurring_credit_template_details_DB(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]
		String coupon_cd = Constant.mycontext.expand('${Properties#coupon_cd}')
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		String credit_template_no = getValueFromHttpRequestValue(apiname, "credit_template_no")
		def percent
		HashMap ctHash = new HashMap<String, String>()
		String query = "SELECT * from ariacore.coupons COUP,ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP CRC,ariacore.RECURRING_CREDIT_TEMPLATES RCT where COUP.coupon_cd='" + coupon_cd +"' and  RCT.recurring_credit_template_no=" + credit_template_no + " and CRC.recurring_credit_template_no=" + credit_template_no
		ResultSet rs = db.executePlaQuery(query)
		while(rs.next())
		{
			ctHash.put("CREDIT_TEMPLATE_NAME", rs.getString('LABEL'))
			ctHash.put("CREDIT_TEMPLATE_ID", rs.getString('RECURRING_CREDIT_TEMPLATE_NO'))
			ctHash.put("CLIENT_CREDIT_TEMPLATE_ID", rs.getString('CLIENT_CREDIT_TEMPLATE_ID'))
			ctHash.put("ELIGIBLE_PLAN_NO",rs.getString('ELIGIBLE_PLAN_NO'))
			ctHash.put("ELIGIBLE_SERVICE_NO", rs.getString('ELIGIBLE_SERVICE_NO'))
			ctHash.put("NO_OF_CREDITS", rs.getString('NUM_CREDITS_REQUIRED'))
			ctHash.put("CREDIT_INTERVAL", rs.getString('CREDIT_INTERVAL_MONTHS'))
			ctHash.put("ALTERNATE_SERVICE", rs.getString('ALT_SERVICE_NO_2_APPLY'))
			percent = rs.getString('PERCENT_AMOUNT')
			if(percent == null) {
				ctHash.put("DISCOUNT_TYPE", "Flat")
				ctHash.put("DISCOUNT_AMOUNT", rs.getString('FLAT_AMOUNT'))
			}
			else
			{
				ctHash.put("DISCOUNT_TYPE", "Percentage")
				ctHash.put("PERCENT_AMOUNT", rs.getString('PERCENT_AMOUNT'))
			}

			ctHash.put("COUPON_CODE", rs.getString('COUPON_CD'))
			ctHash.put("COUPON_DESC", rs.getString('COMMENTS'))
			ctHash.put("COUPON_MSG", rs.getString('ONSCREEN_COUPON_USE_MSG'))
			ctHash.put("MAXIMUM_USES", rs.getString('MAX_USES'))
		}

		logi.logInfo "coupon CT db dETAILS "+ ctHash.toString()
		return ctHash.sort()
	}

	/**
	 * Verifying the Credit Template Details API
	 * @param testCaseId
	 * @return Credit Template Details API HashMap
	 */
	def md_create_credit_template_API(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		String credit_template_no = getValueFromResponse(apiname, ExPathAdminToolsApi.CREATE_CREDIT_TEMPLATE_NUMBER)
		def credits, creditInterval
		HashMap ctHash = new HashMap<String,String>()
		ctHash.put("CREDIT_TEMPLATE_NUMBER", credit_template_no)
		ctHash.put("CREDIT_TEMPLATE_NAME",getValueFromHttpRequestValue(apiname, "credit_template_name"))
		ctHash.put("CLIENT_CREDIT_TEMPLATE_ID", getValueFromHttpRequestValue(apiname, "client_credit_template_id")==null?getValueFromHttpRequestValue(apiname, "credit_template_name") : getValueFromHttpRequestValue(apiname, "client_credit_template_id"))
		String discount_type=getValueFromHttpRequestValue(apiname, "discount_type")
		ctHash.put("DISCOUNT_TYPE", discount_type=='2'?"PERCENTAGE":"FLAT")
		ctHash.put("CURRENCY_CD",getValueFromHttpRequestValue(apiname, "currency_cd"))
		if(discount_type=='2')
		{
			ctHash.put("PERCENT_AMT", getValueFromHttpRequestValue(apiname, "discount_amt"))
		}
		else
		{
			ctHash.put("DISCOUNT_AMT", getValueFromHttpRequestValue(apiname, "discount_amt"))
		}
		ctHash.put("PERCENTAGE PLAN NO", getValueFromHttpRequestValue(apiname, "percentage_plan_no"))
		ctHash.put("PERCENTAGE SERVICE NO", getValueFromHttpRequestValue(apiname, "percentage_service_no"))
		credits = getValueFromHttpRequestValue(apiname, "no_of_credits")
		creditInterval = getValueFromHttpRequestValue(apiname, "credit_interval_months")
		ctHash.put("ALTERNATE_SERVICE", getValueFromHttpRequestValue(apiname, "alt_service_no"))
		if(credits == null && creditInterval == null)
		{
			ctHash.put("NUM_CREDITS_REQUIRED", "1")
			ctHash.put("CREDIT_INTERVAL_MONTHS", "1")
		}
		else
		{
			ctHash.put("NUM_CREDITS_REQUIRED", getValueFromHttpRequestValue(apiname, "no_of_credits"))
			ctHash.put("CREDIT_INTERVAL_MONTHS", getValueFromHttpRequestValue(apiname, "credit_interval_months"))
		}

		logi.logInfo "Credit template API Details "+ ctHash.toString()
		return ctHash.sort()
	}

	/**
	 * Verifying the Credit Template Details DB
	 * @param testCaseId
	 * @return Credit Template Details API HashMap
	 */
	def md_create_credit_template_DB(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		String credit_template_no = getValueFromResponse(apiname, ExPathAdminToolsApi.CREATE_CREDIT_TEMPLATE_NUMBER)
		HashMap ctHash = new HashMap<String, String>()
		ResultSet rs = db.executePlaQuery("SELECT * from ariacore.RECURRING_CREDIT_TEMPLATES where recurring_credit_template_no=" + credit_template_no +" AND client_no=" + client_no)
		def flat, percent
		while(rs.next())
		{
			flat = rs.getString('FLAT_AMOUNT')
			percent = rs.getString('PERCENT_AMOUNT')
			logi.logInfo(flat.toString() + " :: " + percent.toString())
			ctHash.put("CREDIT_TEMPLATE_NUMBER", credit_template_no)
			ctHash.put("CREDIT_TEMPLATE_NAME", rs.getString('LABEL'))
			ctHash.put("CLIENT_CREDIT_TEMPLATE_ID", rs.getString('CLIENT_CREDIT_TEMPLATE_ID'))
			ctHash.put("NUM_CREDITS_REQUIRED", rs.getString('NUM_CREDITS_REQUIRED'))
			ctHash.put("CREDIT_INTERVAL_MONTHS", rs.getString('CREDIT_INTERVAL_MONTHS'))
			ctHash.put("ALTERNATE_SERVICE", rs.getString('ALT_SERVICE_NO_2_APPLY'))
			ctHash.put("CURRENCY_CD",rs.getString('CURRENCY_CD'))
			if(percent == null)
			{
				ctHash.put("DISCOUNT_TYPE", "FLAT")
				ctHash.put("DISCOUNT_AMT", rs.getString('FLAT_AMOUNT'))
			}
			else
			{
				ctHash.put("DISCOUNT_TYPE", "PERCENTAGE")
				ctHash.put("PERCENT_AMT", rs.getString('PERCENT_AMOUNT'))
			}

			ctHash.put("PERCENTAGE PLAN NO", rs.getString('PERCENT_EVAL_PLAN_NO'))
			ctHash.put("PERCENTAGE SERVICE NO", rs.getString('PERCENT_EVAL_SERVICE_NO'))
		}

		logi.logInfo "Credit Template DB dETAILS "+ ctHash.toString()
		return ctHash.sort()
	}

	def md_create_credit_template_serviceType_API(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]
		HashMap ctHash = new HashMap<String,String>()
		if(apiname == 'get_recurring_credit_template_details')
		{
			for(int i=1; i<=6; i++)
			{
				String value = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_ELIGIBLE_SERVICE_TYPE + "[" + i + "]")
				ctHash.put("SERVICE_TYPES " + (i-1).toString(), value == 'NoVal'?'null' : value)
			}
		}
		else if(apiname == 'get_coupon_details')
		{
			for(int i=1; i<=6; i++)
			{
				logi.logInfo("API name : " + apiname + " :: " + ExPathAdminToolsApi.GET_COUPON_TEMP_ELIGIBLE_SERVICE_TYPE + "[" + i + "]")
				String value = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_COUPON_TEMP_ELIGIBLE_SERVICE_TYPE + "[" + i + "]")
				logi.logInfo("Value from response : " + value)
				ctHash.put("SERVICE_TYPES " + (i-1).toString(), value == 'NoVal'?'null' : value)
			}
		}
		else if(apiname == 'create_coupon' || apiname == 'update_coupon')
		{
			for(int i=0; i<=5; i++)
			{
				ctHash.put("SERVICE_TYPES " + i, getValueFromHttpRequestValue(apiname, "template[0][eligible_service_types][" + i + "]"))
			}
		}
		else
		{
			for(int i=0; i<=5; i++)
			{
				ctHash.put("SERVICE_TYPES " + i, getValueFromHttpRequestValue(apiname, "eligible_service_types[" + i + "]"))
			}
		}

		logi.logInfo "Credit template API Details "+ ctHash.toString()
		return ctHash.sort()
	}

	def md_create_credit_template_serviceType_DB(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		String credit_template_no
		if(apiname == 'get_recurring_credit_template_details')
		{
			credit_template_no = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_CREDIT_TEMPLATE_ID)
			logi.logInfo("credit_template_no ======= " + credit_template_no)
		}
		else if(apiname == 'create_coupon' || apiname == 'get_coupon_details')
		{
			String credit_template_id =  getValueFromHttpRequestValue("create_coupon", "template[0][client_credit_template_id]")
			String query1 = "select * from ariacore.RECURRING_CREDIT_TEMPLATES WHERE CLIENT_CREDIT_TEMPLATE_ID='" + credit_template_id + "' AND CLIENT_NO = " + client_no + ""
			ResultSet rs1 = db.executePlaQuery(query1)
			while(rs1.next())
			{
				credit_template_no = rs1.getString('RECURRING_CREDIT_TEMPLATE_NO')
			}
		}
		else
		{
			credit_template_no = getValueFromResponse(apiname, ExPathAdminToolsApi.CREATE_CREDIT_TEMPLATE_NUMBER)
		}
		HashMap ctHash = new HashMap<String, String>()
		for(int i=0; i<=5; i++)
		{
			ctHash.put("SERVICE_TYPES " + i, "null")
		}
		logi.logInfo(ctHash.toString())
		int i = 0

		ResultSet rs = db.executePlaQuery("SELECT SERVICE_TYPE_CD from ariacore.RECUR_CREDIT_TMPLT_SRVC_TP where recurring_credit_template_no=" + credit_template_no +" AND client_no=" + client_no)

		while(rs.next())
		{
			ctHash.put("SERVICE_TYPES " + i, rs.getString(1))
			i++
		}
		logi.logInfo "Credit Template DB dETAILS "+ ctHash.toString()
		return ctHash.sort()
	}

	/**
	 * Verifying the Recurring Credit Template Details API
	 * @param testCaseId
	 * @return Recurring Credit Template Details API HashMap
	 */
	def md_recurring_credit_template_details_NoCoupon_API(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]
		HashMap ctHash = new HashMap<String,String>()
		ctHash.put("CREDIT_TEMPLATE_NAME", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_DETAILS_CREDIT_TEMPLATE_NAME))
		ctHash.put("CREDIT_TEMPLATE_ID", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_CREDIT_TEMPLATE_ID))
		ctHash.put("CLIENT_CREDIT_TEMPLATE_ID", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_CLIENT_CREDIT_TEMPLATE_ID))
		String eligible_plan_no = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_ELIGIBLE_PLAN_NO)
		ctHash.put("ELIGIBLE_PLAN_NO", eligible_plan_no='NoVal'?'null':eligible_plan_no)
		String eligible_service_no = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_ELIGIBLE_SERVICE_NO)
		ctHash.put("ELIGIBLE_SERVICE_NO", eligible_service_no=='NoVal'?'null':eligible_service_no)
		String alt_service = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_ALTERNATE_SERVICE)
		ctHash.put("ALTERNATE_SERVICE", alt_service=='NoVal'?'null':alt_service)
		String discount_type = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_DISCOUNT_TYPE)
		ctHash.put("DISCOUNT_TYPE", discount_type)
		if(discount_type=="Flat")
		{
			ctHash.put("DISCOUNT_AMOUNT", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_DISCOUNT_AMOUNT))
		}
		else
		{
			ctHash.put("PERCENT_AMOUNT", getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_DISCOUNT_AMOUNT))
		}

		String creditinterval = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_CREDIT_INTERVAL)
		String noofcredits = getValueFromResponse(apiname, ExPathAdminToolsApi.GET_RECUR_TEMP_NO_OF_CREDITS)

		if (creditinterval.isEmpty() || creditinterval == "null")
		{
			ctHash.put("CREDIT_INTERVAL", "1")
		}
		else
		{
			ctHash.put("CREDIT_INTERVAL", creditinterval)
		}

		if (noofcredits.isEmpty() || noofcredits == "null")
		{
			ctHash.put("NO_OF_CREDITS", "1")
		}
		else
		{
			ctHash.put("NO_OF_CREDITS", noofcredits)
		}
		logi.logInfo "coupon CT API dETAILS "+ ctHash.toString()
		return ctHash.sort()

	}

	/**
	 * Verifying the Recurring Credit Template Details DB
	 * @param testCaseId
	 * @return Recurring Credit Template Details API HashMap
	 */
	def md_recurring_credit_template_details_NoCoupon_DB(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		String credit_template_no = getValueFromResponse("create_credit_template", ExPathAdminToolsApi.CREATE_CREDIT_TEMPLATE_NUMBER)
		HashMap ctHash = new HashMap<String, String>()
		String query = "select * from ariacore.RECURRING_CREDIT_TEMPLATES where recurring_credit_template_no=" + credit_template_no +" AND client_no=" + client_no
		ResultSet rs = db.executePlaQuery(query)
		def percent
		while(rs.next())
		{
			ctHash.put("CREDIT_TEMPLATE_NAME", rs.getString('LABEL'))
			ctHash.put("CREDIT_TEMPLATE_ID", rs.getString('RECURRING_CREDIT_TEMPLATE_NO'))
			ctHash.put("CLIENT_CREDIT_TEMPLATE_ID", rs.getString('CLIENT_CREDIT_TEMPLATE_ID'))
			ctHash.put("ELIGIBLE_PLAN_NO",rs.getString('ELIGIBLE_PLAN_NO'))
			ctHash.put("ELIGIBLE_SERVICE_NO", rs.getString('ELIGIBLE_SERVICE_NO'))
			ctHash.put("NO_OF_CREDITS", rs.getString('NUM_CREDITS_REQUIRED'))
			ctHash.put("CREDIT_INTERVAL", rs.getString('CREDIT_INTERVAL_MONTHS'))
			ctHash.put("ALTERNATE_SERVICE", rs.getString('ALT_SERVICE_NO_2_APPLY'))
			percent = rs.getString('PERCENT_AMOUNT')
			if(percent == null)
			{
				ctHash.put("DISCOUNT_TYPE", "Flat")
				ctHash.put("DISCOUNT_AMOUNT", rs.getString('FLAT_AMOUNT'))
			}
			else
			{
				ctHash.put("DISCOUNT_TYPE", "Percentage")
				ctHash.put("PERCENT_AMOUNT", rs.getString('PERCENT_AMOUNT'))
			}
		}

		logi.logInfo "coupon CT db dETAILS "+ ctHash.toString()
		return ctHash.sort()
	}

	def md_get_recurring_template_details_API(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]
		int i =1
		String response_value = "//*/*/*:e["
		ArrayList<String> arrValues = new ArrayList<>();
		while(i>0)
		{
			response_value = getValueFromResponse(apiname, "//*/*/*:e["+i+"]/*:recurring_credit_template_no")
			logi.logInfo("RV :"+ response_value)
			if(response_value != 'NoVal')
			{
				arrValues.add(response_value);
			}
			else
			{
				i=-1
				break
			}
			i++
		}
		logi.logInfo("Template Details from API :" + arrValues.toString())
		return arrValues.sort()
	}

	def md_get_recurring_template_details_DB(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		ArrayList<String> arrValues = new ArrayList<>();
		int i=1
		ResultSet rs = db.executePlaQuery("SELECT recurring_credit_template_no from ariacore.RECURRING_CREDIT_TEMPLATES where client_no=" + client_no)
		while(rs.next())
		{
			arrValues.add(rs.getString(1))
		}
		logi.logInfo("Template Details from DB :" + arrValues.toString())
		return arrValues.sort()
	}

	def md_verify_recurring_template_details_count_API(String testcaseid)
	{
		ArrayList<String> api_list = md_get_recurring_template_details_API(testcaseid)
		ArrayList<String> db_list = md_get_recurring_template_details_DB(testcaseid)
		int count=0
		for(int i=1; i<=api_list.size();i++)
		{
			if(api_list[i]==db_list[i]){
				count++
			}
		}
		return count.toString()
	}

	def md_verify_recurring_template_details_count_DB(String testcaseid)
	{
		ArrayList<String> api_list = md_get_recurring_template_details_API(testcaseid)
		ArrayList<String> db_list = md_get_recurring_template_details_DB(testcaseid)
		int count=0
		for(int i=1; i<=db_list.size();i++)
		{
			if(api_list[i]==db_list[i]){
				count++
			}
		}
		return count.toString()
	}

	/**
	 * Verifying the Recurring Credit Template and Coupon Details DB
	 * @param testCaseId
	 * @return Recurring Credit Template Details API HashMap
	 */
	def md_recurring_credit_template_coupon_details_DB(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		String credit_template_no = getValueFromResponse("create_credit_template", ExPathAdminToolsApi.CREATE_CREDIT_TEMPLATE_NUMBER)
		def coupon_cd
		String query1 = "Select * from ARIACORE.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE RECURRING_CREDIT_TEMPLATE_NO = " + credit_template_no + " AND CLIENT_NO = " + client_no + ""
		ResultSet rs1 = db.executePlaQuery(query1)
		while(rs1.next())
		{
			coupon_cd = rs1.getString('COUPON_CD')
		}

		def percent
		HashMap ctHash = new HashMap<String, String>()
		String query = "SELECT * from ariacore.coupons COUP,ariacore.COUPON_RECUR_CREDIT_TMPLT_MAP CRC,ariacore.RECURRING_CREDIT_TEMPLATES RCT where COUP.coupon_cd='" + coupon_cd +"' and  RCT.recurring_credit_template_no=" + credit_template_no + " and CRC.recurring_credit_template_no=" + credit_template_no
		ResultSet rs = db.executePlaQuery(query)
		while(rs.next())
		{
			ctHash.put("CREDIT_TEMPLATE_NAME", rs.getString('LABEL'))
			ctHash.put("CREDIT_TEMPLATE_ID", rs.getString('RECURRING_CREDIT_TEMPLATE_NO'))
			ctHash.put("CLIENT_CREDIT_TEMPLATE_ID", rs.getString('CLIENT_CREDIT_TEMPLATE_ID'))
			ctHash.put("ELIGIBLE_PLAN_NO",rs.getString('ELIGIBLE_PLAN_NO'))
			ctHash.put("ELIGIBLE_SERVICE_NO", rs.getString('ELIGIBLE_SERVICE_NO'))
			ctHash.put("NO_OF_CREDITS", rs.getString('NUM_CREDITS_REQUIRED'))
			ctHash.put("CREDIT_INTERVAL", rs.getString('CREDIT_INTERVAL_MONTHS'))
			ctHash.put("ALTERNATE_SERVICE", rs.getString('ALT_SERVICE_NO_2_APPLY'))
			percent = rs.getString('PERCENT_AMOUNT')
			if(percent == null)
			{
				ctHash.put("DISCOUNT_TYPE", "Flat")
				ctHash.put("DISCOUNT_AMOUNT", rs.getString('FLAT_AMOUNT'))
			}
			else
			{
				ctHash.put("DISCOUNT_TYPE", "Percentage")
				ctHash.put("PERCENT_AMOUNT", rs.getString('PERCENT_AMOUNT'))
			}

			ctHash.put("COUPON_CODE", rs.getString('COUPON_CD'))
			ctHash.put("COUPON_DESC", rs.getString('COMMENTS'))
			ctHash.put("COUPON_MSG", rs.getString('ONSCREEN_COUPON_USE_MSG'))
			ctHash.put("MAXIMUM_USES", rs.getString('MAX_USES'))
		}

		return ctHash.sort()
	}

	/**
	 * Verifying the Credit template Coupon Details API
	 * @param testCaseId
	 * @return Coupon Details API HashMap
	 */
	def md_VerifyCTwithCoupondetails_API(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]
		HashMap couponHash = new HashMap<String,String>()

		couponHash.put("COUPON_CD", getValueFromHttpRequestValue(apiname, "coupon[0][coupon_cd]"))
		couponHash.put("COUPON_DESC", getValueFromHttpRequestValue(apiname, "coupon[0][coupon_desc]"))
		couponHash.put("COUPON_MSG", getValueFromHttpRequestValue(apiname, "coupon[0][coupon_msg]"))
		couponHash.put("STATUS", getValueFromHttpRequestValue(apiname, "coupon[0][status_ind]"))
		couponHash.put("NO_OF_USES", getValueFromHttpRequestValue(apiname, "coupon[0][no_of_uses]"))
		couponHash.put("START_DATE", getValueFromHttpRequestValue(apiname, "coupon[0][start_date]"))
		couponHash.put("END_DATE", getValueFromHttpRequestValue(apiname, "coupon[0][end_date]"))

		return couponHash.sort()
	}

	/**
	 * Verifying the Credit template Coupon Details API
	 * @param testCaseId
	 * @return Coupon Details API HashMap
	 */
	def md_VerifyCTwithEXCoupon_API(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]
		HashMap couponHash = new HashMap<String,String>()
		for(int i=0; i<=0; i++)
		{
			couponHash.put("EXC COUPON_CD[" + i + "]", getValueFromHttpRequestValue(apiname, "existing_coupon[" + i + "]"))
		}
		return couponHash.sort()
	}

	/**
	 * Verifying the Credit template Coupon Details DB
	 * @param testCaseId
	 * @return Coupon Details API HashMap
	 */
	def md_VerifyCTwithEXCoupon_DB(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]
		HashMap couponHash = new HashMap<String,String>()
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		String credit_template_no = getValueFromResponse(apiname, ExPathAdminToolsApi.CREATE_CREDIT_TEMPLATE_NUMBER)
		def coupon_cd
		int i = 0
		String query = "Select COUPON_CD from ARIACORE.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE RECURRING_CREDIT_TEMPLATE_NO = " + credit_template_no + " AND CLIENT_NO = " + client_no + ""
		ResultSet rs = db.executePlaQuery(query)
		while(rs.next())
		{
			couponHash.put("EXC COUPON_CD[" + i + "]", rs.getString(1))
			i++
		}
		return couponHash.sort()
	}


	/**
	 * Verifying the Coupon Details - Service Credit  DB
	 * @param testCaseId
	 * @return Coupon Service Credit Details API HashMap
	 */

	def md_create_coupon_serviceType_DB(String testcaseid)
	{
		String apiname = testcaseid.split("-")[2]
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		String credit_template_no
		String credit_template_id =  getValueFromHttpRequestValue("update_coupon", "template[0][client_credit_template_id]")
		String query1 = "select * from ariacore.RECURRING_CREDIT_TEMPLATES WHERE CLIENT_CREDIT_TEMPLATE_ID='" + credit_template_id + "' AND CLIENT_NO = " + client_no + ""
		ResultSet rs1 = db.executePlaQuery(query1)
		while(rs1.next())
		{
			credit_template_no = rs1.getString('RECURRING_CREDIT_TEMPLATE_NO')
		}
		HashMap ctHash = new HashMap<String, String>()
		for(int i=0; i<=5; i++)
		{
			ctHash.put("SERVICE_TYPES " + i, "null")
		}
		logi.logInfo(ctHash.toString())
		int i = 0
		ResultSet rs = db.executePlaQuery("SELECT SERVICE_TYPE_CD from ariacore.RECUR_CREDIT_TMPLT_SRVC_TP where recurring_credit_template_no=" + credit_template_no +" AND client_no=" + client_no)

		while(rs.next())
		{
			ctHash.put("SERVICE_TYPES " + i, rs.getString(1))
			i++
		}
		return ctHash.sort()
	}

	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_service_from_DB(String testcaseid)
	{
		logi.logInfo("Enter the method from DB")
		LinkedHashMap<String,String> serviceHashResult = new LinkedHashMap<String, String>()

		String service_no = getValueFromResponse("create_service", ExPathAdminToolsApi.CREATE_SERVICE_SERVICE_NO1)
		String client_no = getValueFromHttpRequestValue("create_service", "client_no")
		logi.logInfo("Item and Client number : " + service_no + " :: " + client_no)

		ResultSet rs = db.executeQuery("SELECT * FROM ARIACORE.ALL_SERVICE WHERE SERVICE_NO = " + service_no + " AND CLIENT_NO=" + client_no)
		while(rs.next())
		{
			serviceHashResult.put("Service No",rs.getString('SERVICE_NO'))
			serviceHashResult.put("Service Name",rs.getString('SERVICE_NAME'))
			serviceHashResult.put("Service Type",rs.getString('SERVICE_TYPE'))
			if(rs.getString('SERVICE_TYPE').equalsIgnoreCase("US"))
			{
				logi.logInfo("Taxable Info By Uvais Create : "+rs.getInt('TAXABLE_IND'))
				String tax = rs.getString('TAXABLE_IND')
				logi.logInfo("Taxable Info By Uvais : "+tax)
				if(tax == "1")
				{
					serviceHashResult.put("Service Taxable Ind","1")
				}
				else if(tax == "0")
				{
					serviceHashResult.put("Service Taxable Ind","0")
				}
				else
				{
					serviceHashResult.put("Service Taxable Ind",rs.getString('TAXABLE_IND'))
				}
				serviceHashResult.put("Service Usage Type",rs.getString('USAGE_TYPE_NO'))
			}
			ResultSet rs1 = db.executePlaQuery("SELECT * FROM ARIACORE.CLIENT_CHART_OF_ACCOUNT WHERE COA_ID = "+rs.getString('COA_ID')+" AND CLIENT_NO=" + client_no)
			while(rs1.next())
			{
				serviceHashResult.put("COA ID",rs1.getString('COA_CODE'))
			}
		}
		logi.logInfo("Hashmap is : " + serviceHashResult)
		return serviceHashResult.sort()
	}
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_service_from_API(String testcaseid)
	{
		logi.logInfo("Enter the method from API")
		LinkedHashMap<String,String> serviceHashResult = new LinkedHashMap<String, String>()
		serviceHashResult.put("Service No", getValueFromResponse("create_service", ExPathAdminToolsApi.CREATE_SERVICE_SERVICE_NO1))
		serviceHashResult.put("Service Name", getValueFromHttpRequestValue("create_service", "service_name"))
		serviceHashResult.put("COA ID", getValueFromHttpRequestValue("create_service", "gl_cd"))
		String ser_type = getValueFromHttpRequestValue("create_service", "service_type")
		if(ser_type.equalsIgnoreCase("Recurring"))
		{
			serviceHashResult.put("Service Type","RC")
		}
		else if(ser_type.equalsIgnoreCase("Activation"))
		{
			serviceHashResult.put("Service Type","AC")
		}
		else if(ser_type.equalsIgnoreCase("Usage-Based"))
		{
			serviceHashResult.put("Service Type","US")
			serviceHashResult.put("Service Taxable Ind",getValueFromHttpRequestValue("create_service", "taxable_ind"))
			serviceHashResult.put("Service Usage Type",getValueFromHttpRequestValue("create_service", "usage_type"))
		}
		else if(ser_type.equalsIgnoreCase("Order-Based"))
		{
			serviceHashResult.put("Service Type","OR")
		}
		else if(ser_type.equalsIgnoreCase("Cancellation"))
		{
			serviceHashResult.put("Service Type","CN")
		}
		else if(ser_type.equalsIgnoreCase("Minimum Fee"))
		{
			serviceHashResult.put("Service Type","MN")
		}
		logi.logInfo("Hashmap is : " + serviceHashResult)
		return serviceHashResult.sort()
	}
	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_update_service_from_DB(String testcaseid)
	{
		logi.logInfo("Enter the method from DB")
		LinkedHashMap<String,String> serviceHashResult = new LinkedHashMap<String, String>()

		String service_no = getValueFromResponse("update_service", ExPathAdminToolsApi.UPDATE_SERVICE_NO1)
		String client_no = getValueFromHttpRequestValue("update_service", "client_no")
		logi.logInfo("Item and Client number : " + service_no + " :: " + client_no)

		ResultSet rs = db.executeQuery("SELECT * FROM ARIACORE.ALL_SERVICE WHERE SERVICE_NO= " + service_no + " AND CLIENT_NO=" + client_no)
		while(rs.next())
		{
			serviceHashResult.put("Service No",rs.getString('SERVICE_NO'))
			serviceHashResult.put("Service Name",rs.getString('SERVICE_NAME'))
			serviceHashResult.put("Service Type",rs.getString('SERVICE_TYPE'))
			if(rs.getString('SERVICE_TYPE').equalsIgnoreCase("US"))
			{
				logi.logInfo("Taxable Info By Uvais Update : "+rs.getInt('TAXABLE_IND'))
				String tax = rs.getString('TAXABLE_IND')
				logi.logInfo("Taxable Info By Uvais 1 : "+tax)
				if(tax == "1")
				{
					serviceHashResult.put("Service Taxable Ind","1")
				}
				else if(tax == "0")
				{
					serviceHashResult.put("Service Taxable Ind","0")
				}
				else
				{
					serviceHashResult.put("Service Taxable Ind",rs.getString('TAXABLE_IND'))
				}
				serviceHashResult.put("Service Usage Type",rs.getString('USAGE_TYPE_NO'))
			}
			ResultSet rs1 = db.executePlaQuery("SELECT * FROM ARIACORE.CLIENT_CHART_OF_ACCOUNT WHERE COA_ID = "+rs.getString('COA_ID')+" AND CLIENT_NO=" + client_no)
			while(rs1.next())
			{
				serviceHashResult.put("COA ID",rs1.getString('COA_CODE'))
			}
		}
		logi.logInfo("Hashmap is : " + serviceHashResult)
		return serviceHashResult.sort()
	}
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_update_service_from_API(String testcaseid)
	{
		logi.logInfo("Enter the method from API")
		LinkedHashMap<String,String> serviceHashResult = new LinkedHashMap<String, String>()
		serviceHashResult.put("Service No", getValueFromResponse("update_service", ExPathAdminToolsApi.UPDATE_SERVICE_NO1))
		serviceHashResult.put("Service Name", getValueFromHttpRequestValue("update_service", "service_name"))
		serviceHashResult.put("COA ID", getValueFromHttpRequestValue("update_service", "gl_cd"))
		String ser_type = getValueFromHttpRequestValue("update_service", "service_type")
		if(ser_type.equalsIgnoreCase("Recurring"))
		{
			serviceHashResult.put("Service Type","RC")
		}
		else if(ser_type.equalsIgnoreCase("Activation"))
		{
			serviceHashResult.put("Service Type","AC")
		}
		else if(ser_type.equalsIgnoreCase("Usage-Based"))
		{
			serviceHashResult.put("Service Type","US")
			serviceHashResult.put("Service Taxable Ind",getValueFromHttpRequestValue("update_service", "taxable_ind"))
			serviceHashResult.put("Service Usage Type",getValueFromHttpRequestValue("update_service", "usage_type"))
		}
		else if(ser_type.equalsIgnoreCase("Order-Based"))
		{
			serviceHashResult.put("Service Type","OR")
		}
		else if(ser_type.equalsIgnoreCase("Cancellation"))
		{
			serviceHashResult.put("Service Type","CN")
		}
		else if(ser_type.equalsIgnoreCase("Minimum Fee"))
		{
			serviceHashResult.put("Service Type","MN")
		}
		logi.logInfo("Hashmap is : " + serviceHashResult)
		return serviceHashResult.sort()
	}

	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_promotion_from_API(String testcaseid)
	{
			logi.logInfo("Enter the method from API")
			LinkedHashMap<String,String> promoHashResult = new LinkedHashMap<String, String>()
			promoHashResult.put("Promotion Name",getValueFromResponse("create_promotion",ExPathAdminToolsApi.CREATE_PROMOTION_CD))
			promoHashResult.put("Promotion Description",getValueFromHttpRequestValue("create_promotion", "promo_desc"))
			promoHashResult.put("Promotion Set No",getValueFromHttpRequestValue("create_promotion", "promo_plan_set_no"))
			promoHashResult.put("Promotion No Of Uses",getValueFromHttpRequestValue("create_promotion", "no_of_uses"))
			logi.logInfo("Hash map result is : " +promoHashResult)
			return promoHashResult.sort()
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_update_promotion_from_API(String testcaseid)
	{
			logi.logInfo("Enter the method from API")
			LinkedHashMap<String,String> promoHashResult = new LinkedHashMap<String, String>()
			promoHashResult.put("Promotion Name",getValueFromResponse("update_promotion",ExPathAdminToolsApi.CREATE_PROMOTION_CD))
			promoHashResult.put("Promotion Description",getValueFromHttpRequestValue("update_promotion", "promo_desc"))
			promoHashResult.put("Promotion Set No",getValueFromHttpRequestValue("update_promotion", "promo_plan_set_no"))
			promoHashResult.put("Promotion No Of Uses",getValueFromHttpRequestValue("update_promotion", "no_of_uses"))
			logi.logInfo("Hash map result is : " +promoHashResult)
			return promoHashResult.sort()
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_update_promotion_from_DB(String testcaseid)
	{
		LinkedHashMap<String,String> promoHashResult = new LinkedHashMap<String, String>()
		String promo_code = getValueFromResponse("update_promotion", ExPathAdminToolsApi.UPDATE_PROMO_CODE1)
		String client_no = getValueFromHttpRequestValue("update_promotion", "client_no")
		logi.logInfo("Promo and Client number : " + promo_code + " :: " + client_no)
		ResultSet rs = db.executeQuery("select * from ariacore.promotions where promo_cd ='"+promo_code+"' AND CLIENT_NO = "+ client_no)
		while(rs.next())
		 {
			promoHashResult.put("Promotion Name",rs.getString('PROMO_CD'))
			promoHashResult.put("Promotion Description",rs.getString('COMMENTS'))
			promoHashResult.put("Promotion Set No",rs.getString('PLAN_TYPE_NO'))
			promoHashResult.put("Promotion No Of Uses",rs.getString('MAX_REGISTRATIONS'))
		 }
		 logi.logInfo("Hash map result is : " +promoHashResult)
		return promoHashResult.sort()
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_promotion_from_DB(String testcaseid)
	{
		LinkedHashMap<String,String> promoHashResult = new LinkedHashMap<String, String>()
		String promo_code = getValueFromResponse("create_promotion", ExPathAdminToolsApi.CREATE_PROMOTION_CD)
		String client_no = getValueFromHttpRequestValue("create_promotion", "client_no")
		logi.logInfo("Promo and Client number : " + promo_code + " :: " + client_no)
		ResultSet rs = db.executeQuery("select * from ariacore.promotions where promo_cd ='"+promo_code+"' AND CLIENT_NO = "+ client_no)
		while(rs.next())
		 {
			promoHashResult.put("Promotion Name",rs.getString('PROMO_CD'))
			promoHashResult.put("Promotion Description",rs.getString('COMMENTS'))
			promoHashResult.put("Promotion Set No",rs.getString('PLAN_TYPE_NO'))
			promoHashResult.put("Promotion No Of Uses",rs.getString('MAX_REGISTRATIONS'))
		 }
		 logi.logInfo("Hash map result is : " +promoHashResult)
		return promoHashResult.sort()
	}

	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_promo_plan_set_from_DB(String testcaseid)
	{
		LinkedHashMap<String,String> promoPlanHashResult = new LinkedHashMap<String, String>()
		String promo_plan_set_no = getValueFromResponse("create_promo_plan_set", ExPathAdminToolsApi.CREATE_PROMO_PLAN_SET_NO1)
		String client_no = getValueFromHttpRequestValue("create_promo_plan_set", "client_no")
		logi.logInfo("Promotinal plan set No and Client number : " + promo_plan_set_no + " :: " + client_no)
		ResultSet rs = db.executeQuery("SELECT * from ariacore.PLAN_TYPES WHERE PLAN_TYPE_NO ='"+promo_plan_set_no+"'")
		while(rs.next())
		 {
			promoPlanHashResult.put("Promotional Plan Set No",rs.getString('PLAN_TYPE_NO'))
			promoPlanHashResult.put("Promotional Plan Set Name",rs.getString('KEY_NAME'))
			promoPlanHashResult.put("Promotional Plan Set Description",rs.getString('DESC_LONG'))
			promoPlanHashResult.put("Promotional Plan Set Client ID",rs.getString('CLIENT_PLAN_TYPE_ID'))
		 }
		 logi.logInfo("Hash map result is : " +promoPlanHashResult)
		return promoPlanHashResult.sort()
	}
	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_promo_plan_set_from_API(String testcaseid)
	{
			logi.logInfo("Enter the method from API")
			LinkedHashMap<String,String> promoPlanHashResult = new LinkedHashMap<String, String>()
			promoPlanHashResult.put("Promotional Plan Set No",getValueFromResponse("create_promo_plan_set", ExPathAdminToolsApi.CREATE_PROMO_PLAN_SET_NO1))
			promoPlanHashResult.put("Promotional Plan Set Name",getValueFromHttpRequestValue("create_promo_plan_set","promo_plan_set_name"))
			promoPlanHashResult.put("Promotional Plan Set Description",getValueFromHttpRequestValue("create_promo_plan_set", "promo_plan_set_desc"))
			promoPlanHashResult.put("Promotional Plan Set Client ID",getValueFromHttpRequestValue("create_promo_plan_set", "client_plan_type_id"))
			logi.logInfo("Hash map result is : " +promoPlanHashResult)
			return promoPlanHashResult.sort()
			
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_credit_template_from_DB(String testcaseid)
	{
		LinkedHashMap<String,String> CTHashResult = new LinkedHashMap<String, String>()
		String ctNo = getValueFromResponse("create_credit_template", ExPathAdminToolsApi.CREATE_CREDIT_TEMPLATE_NO1)
		String client_no = getValueFromHttpRequestValue("create_credit_template", "client_no")
		logi.logInfo("Credit template and Client number : " + ctNo + " :: " + client_no)
		ResultSet rs = db.executeQuery("select * from ariacore.RECURRING_CREDIT_TEMPLATES where RECURRING_CREDIT_TEMPLATE_NO ="+ctNo+" AND CLIENT_NO=" + client_no)
		while(rs.next())
		 {
			CTHashResult.put("CT Number",rs.getString('RECURRING_CREDIT_TEMPLATE_NO'))
			CTHashResult.put("CT Amount",rs.getString('FLAT_AMOUNT'))
			CTHashResult.put("CT Currency",rs.getString('CURRENCY_CD'))
			CTHashResult.put("CT No Of Credits",rs.getString('NUM_CREDITS_REQUIRED'))
			CTHashResult.put("CT Interval Months",rs.getString('CREDIT_INTERVAL_MONTHS'))
			CTHashResult.put("CT Template ID",rs.getString('CLIENT_CREDIT_TEMPLATE_ID'))
			
		 }
		 logi.logInfo("Hash map result is : " +CTHashResult)
		return CTHashResult.sort()
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_credit_template_from_API(String testcaseid)
	{
			logi.logInfo("Enter the method from API")
			LinkedHashMap<String,String> CTHashResult = new LinkedHashMap<String, String>()
			CTHashResult.put("CT Number",getValueFromResponse("create_credit_template", ExPathAdminToolsApi.CREATE_CREDIT_TEMPLATE_NO1))
			CTHashResult.put("CT Amount",getValueFromHttpRequestValue("create_credit_template", "discount_amt"))
			CTHashResult.put("CT Currency",getValueFromHttpRequestValue("create_credit_template", "currency_cd"))
			CTHashResult.put("CT No Of Credits",getValueFromHttpRequestValue("create_credit_template", "no_of_credits"))
			CTHashResult.put("CT Interval Months",getValueFromHttpRequestValue("create_credit_template", "credit_interval_months"))
			CTHashResult.put("CT Template ID",getValueFromHttpRequestValue("create_credit_template", "credit_template_name"))
			logi.logInfo("Hash map result is : " +CTHashResult)
			return CTHashResult.sort()
			
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_discount_rule_from_DB(String testcaseid)
	{
		LinkedHashMap<String,String> DRHashResult = new LinkedHashMap<String, String>()
		String drNo = getValueFromResponse("create_discount_rule", ExPathAdminToolsApi.CREATE_RULE_NO1)
		String client_no = getValueFromHttpRequestValue("create_discount_rule", "client_no")
		logi.logInfo("Discount Rule and Client number : " + drNo + " :: " + client_no)
		ResultSet rs = db.executeQuery("select * from ariacore.CLIENT_DISCOUNT_RULES where rule_no = "+drNo+" AND CLIENT_NO=" + client_no)
		while(rs.next())
		 {
			DRHashResult.put("DR Number",rs.getString('RULE_NO'))
			DRHashResult.put("DR Amount",rs.getString('AMOUNT'))
			DRHashResult.put("DR Currency",rs.getString('CURRENCY_CD'))
			DRHashResult.put("DR Inline Offset",rs.getString('INLINE_OFFSET_IND'))
			DRHashResult.put("DR Duration Type",rs.getString('DURATION_TYPE_IND'))
			DRHashResult.put("DR Scope",rs.getString('SCOPE_NO'))
			DRHashResult.put("DR ID",rs.getString('CLIENT_RULE_ID'))
			DRHashResult.put("DR Description",rs.getString('DESCRIPTION'))
			DRHashResult.put("DR External Description",rs.getString('EXT_DESCRIPTION'))
		 }
		 logi.logInfo("Hash map result is : " +DRHashResult)
		return DRHashResult.sort()
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_discount_rule_from_API(String testcaseid)
	{
		logi.logInfo("Enter the method from API")
		LinkedHashMap<String,String> DRHashResult = new LinkedHashMap<String, String>()
		DRHashResult.put("DR Number",getValueFromResponse("create_discount_rule", ExPathAdminToolsApi.CREATE_RULE_NO1))
			DRHashResult.put("DR Amount",getValueFromHttpRequestValue("create_discount_rule", "amount"))
			DRHashResult.put("DR Currency",getValueFromHttpRequestValue("create_discount_rule", "currency"))
			DRHashResult.put("DR Inline Offset",getValueFromHttpRequestValue("create_discount_rule", "inline_offset_ind"))
			DRHashResult.put("DR Duration Type",getValueFromHttpRequestValue("create_discount_rule", "duration_type_ind"))
			DRHashResult.put("DR Scope",getValueFromHttpRequestValue("create_discount_rule", "scope_no"))
			DRHashResult.put("DR ID",getValueFromHttpRequestValue("create_discount_rule", "rule_id"))
			DRHashResult.put("DR Description",getValueFromHttpRequestValue("create_discount_rule", "description"))
			DRHashResult.put("DR External Description",getValueFromHttpRequestValue("create_discount_rule", "ext_description"))
			
			logi.logInfo("Hash map result is : " +DRHashResult)
			return DRHashResult.sort()
		
	}
	
	
	/**
	 */
	def md_discount_bundle_from_API(String testcaseid)
	{
		logi.logInfo("Enter the method from API")
		
		LinkedHashMap<String,String> DBHashResult = new LinkedHashMap<String, String>()
		
		DBHashResult.put("DB Number",getValueFromResponse("create_discount_bundle", ExPathAdminToolsApi.CREATE_BUNDLE_NO1))
		DBHashResult.put("DB Name",getValueFromHttpRequestValue("create_discount_bundle","bundle_name"))
		DBHashResult.put("DB Description",getValueFromHttpRequestValue("create_discount_bundle","bundle_description"))
		DBHashResult.put("DB Allow overlap",getValueFromHttpRequestValue("create_discount_bundle","allow_overlap_ind"))
		DBHashResult.put("DB Client ID",getValueFromHttpRequestValue("create_discount_bundle","bundle_id"))
		//DBHashResult.put("DB Rule[1]",getValueFromHttpRequestValue("create_discount_bundle","rules[1]"))
		//DBHashResult.put("DB Rule[2]",getValueFromHttpRequestValue("create_discount_bundle","rules[2]"))
			
		logi.logInfo("Hash map result is : " +DBHashResult)
		return DBHashResult.sort()
	}
	
	
	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_discount_bundle_from_DB(String testcaseid)
	{
		LinkedHashMap<String,String> DBHashResult = new LinkedHashMap<String, String>()
		int i =1;
		
		String dbNo = getValueFromResponse("create_discount_bundle", ExPathAdminToolsApi.CREATE_BUNDLE_NO1)
		String client_no = getValueFromHttpRequestValue("create_discount_bundle", "client_no")
		logi.logInfo("Discount Bundle and Client number : " + dbNo + " :: " + client_no)
		ResultSet rs = db.executeQuery("select * from ARIACORE.CLIENT_DISCOUNT_RULE_BUNDLES where bundle_no = "+dbNo+" AND CLIENT_NO=" + client_no)
		while(rs.next())
		{
			DBHashResult.put("DB Number",rs.getString('BUNDLE_NO'))
			DBHashResult.put("DB Name",rs.getString('LABEL'))
			DBHashResult.put("DB Description",rs.getString('DESCRIPTION'))
			DBHashResult.put("DB Allow overlap",rs.getString('ALLOW_OVERLAP_IND'))
			DBHashResult.put("DB Client ID",rs.getString('CLIENT_BUNDLE_ID'))
		}
		
		/*ResultSet rs1 = db.executeQuery("select * from ARIACORE.CLIENT_DR_BUNDLE_MEMBERS where bundle_no ="+dbNo+" AND PRIMACY = "+i+" AND CLIENT_NO=" + client_no)
		 while(rs1.next())
		 {
			 DBHashResult.put("DB Rule["+i+"]",rs.getString('RULE_NO'));
			 i++;
		 }*/
		logi.logInfo("Hash map result is : " +DBHashResult)
		return DBHashResult.sort()
	}
	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_add_coa_DB(String testcaseid)
	{
		LinkedHashMap<String,String> coaHashResult = new LinkedHashMap<String, String>()
		String coaNo =  getValueFromResponse("add_coa", ExPathAdminToolsApi.ADD_COA_NO1)
		String client_no = getValueFromHttpRequestValue("add_coa", "client_no")
		logi.logInfo("Add COA and Client number : " + coaNo + " :: " + client_no)
		ResultSet rs = db.executeQuery("select * from ARIACORE.CLIENT_CHART_OF_ACCOUNT WHERE COA_ID = "+coaNo+" AND CLIENT_NO=" + client_no)
		while(rs.next())
		{
			coaHashResult.put("COA ID",rs.getString('COA_ID'))
			coaHashResult.put("COA CODE",rs.getString('COA_CODE'))
			coaHashResult.put("COA DESCRIPTION",rs.getString('COA_DESCRIPTION'))
		}
		logi.logInfo("Hash map result is : " +coaHashResult)
		return coaHashResult.sort()
	}
	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_add_coa_API(String testcaseid)
	{
		LinkedHashMap<String,String> coaHashResult = new LinkedHashMap<String, String>()
		String coaNo =  getValueFromResponse("add_coa", ExPathAdminToolsApi.ADD_COA_NO1)
		coaHashResult.put("COA ID",getValueFromResponse("add_coa", ExPathAdminToolsApi.ADD_COA_NO1))
		coaHashResult.put("COA CODE",getValueFromHttpRequestValue("add_coa","coa_code"))
		coaHashResult.put("COA DESCRIPTION",getValueFromHttpRequestValue("add_coa","coa_description"))
		logi.logInfo("Hash map result is : " +coaHashResult)
		return coaHashResult.sort()
	}
	
	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_edit_coa_DB(String testcaseid)
	{
		LinkedHashMap<String,String> coaHashResult = new LinkedHashMap<String, String>()
		String coaNo =  getValueFromResponse("edit_coa", ExPathAdminToolsApi.EDIT_COA_ID1)
		String client_no = getValueFromHttpRequestValue("edit_coa", "client_no")
		logi.logInfo("COA Id and Client number : " + coaNo + " :: " + client_no)
		ResultSet rs = db.executeQuery("select * from ARIACORE.CLIENT_CHART_OF_ACCOUNT WHERE COA_ID = "+coaNo+" AND CLIENT_NO=" + client_no)
		while(rs.next())
		{
			coaHashResult.put("COA ID",rs.getString('COA_ID'))
			coaHashResult.put("COA CODE",rs.getString('COA_CODE'))
			coaHashResult.put("COA DESCRIPTION",rs.getString('COA_DESCRIPTION'))
		}
		logi.logInfo("Hash map result is : " +coaHashResult)
		return coaHashResult.sort()
	}
	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_edit_coa_API(String testcaseid)
	{
		LinkedHashMap<String,String> coaHashResult = new LinkedHashMap<String, String>()
		String coaNo =  getValueFromResponse("edit_coa", ExPathAdminToolsApi.EDIT_COA_ID1)
		coaHashResult.put("COA ID",getValueFromResponse("edit_coa", ExPathAdminToolsApi.EDIT_COA_ID1))
		coaHashResult.put("COA CODE",getValueFromHttpRequestValue("edit_coa","coa_code"))
		coaHashResult.put("COA DESCRIPTION",getValueFromHttpRequestValue("edit_coa","coa_description"))
		logi.logInfo("Hash map result is : " +coaHashResult)
		return coaHashResult.sort()
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_acct_field_from_DB(String testcaseid)
	{
		LinkedHashMap<String,String> acctFieldHashResult = new LinkedHashMap<String, String>()
		String accountFieldNo = getValueFromResponse("create_supp_field", ExPathAdminToolsApi.ACCT_FIELD_NO1)
		String client_no = getValueFromHttpRequestValue("create_supp_field", "client_no")
		logi.logInfo("Account Field and Client number : " + accountFieldNo + " :: " + client_no)
		ResultSet rs = db.executeQuery("SELECT * FROM ARIACORE.CLIENT_SUPP_FIELDS WHERE FIELD_NAME = '"+accountFieldNo+"' AND CLIENT_NO=" + client_no)
		while(rs.next())
		{
			acctFieldHashResult.put("Field Name",rs.getString('FIELD_NAME'))
			acctFieldHashResult.put("Field Description",rs.getString('FIELD_SUPP_TEXT'))
			acctFieldHashResult.put("Field Required",rs.getString('REQUIRED_IND'))
			acctFieldHashResult.put("Field Min Value",rs.getString('MIN_NUM_VALS'))
			acctFieldHashResult.put("Field Max Value",rs.getString('MAX_NUM_VALS'))
			acctFieldHashResult.put("Field Order",rs.getString('PRIMACY'))
			acctFieldHashResult.put("Field Input",rs.getString('FORM_INPUT_TYPE'))
		}
		logi.logInfo("Hash map result is : " +acctFieldHashResult)
		return acctFieldHashResult.sort()
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_acct_field_from_API(String testcaseid)
	{
		LinkedHashMap<String,String> acctFieldHashResult = new LinkedHashMap<String, String>()
		String accountFieldNo = getValueFromResponse("create_supp_field", ExPathAdminToolsApi.ACCT_FIELD_NO1)
		
		acctFieldHashResult.put("Field Name",getValueFromHttpRequestValue("create_supp_field","field_name"))
		acctFieldHashResult.put("Field Description",getValueFromHttpRequestValue("create_supp_field","field_desc"))
		acctFieldHashResult.put("Field Required",getValueFromHttpRequestValue("create_supp_field","required_ind"))
		acctFieldHashResult.put("Field Min Value",getValueFromHttpRequestValue("create_supp_field","min_no_sel"))
		acctFieldHashResult.put("Field Max Value",getValueFromHttpRequestValue("create_supp_field","max_no_sel"))
		acctFieldHashResult.put("Field Order",getValueFromHttpRequestValue("create_supp_field","field_order"))
		acctFieldHashResult.put("Field Input",getValueFromHttpRequestValue("create_supp_field","presentation_mode"))
		
		logi.logInfo("Hash map result is : " +acctFieldHashResult)
		return acctFieldHashResult.sort()
		
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_update_acct_field_from_DB(String testcaseid)
	{
		LinkedHashMap<String,String> acctFieldHashResult = new LinkedHashMap<String, String>()
		String accountFieldName = getValueFromResponse("update_supp_field", ExPathAdminToolsApi.UPDATE_SUPP_FIELD_NAME1)
		String client_no = getValueFromHttpRequestValue("update_supp_field", "client_no")
		logi.logInfo("Account Field and Client number : " + accountFieldName + " :: " + client_no)
		ResultSet rs = db.executeQuery("SELECT * FROM ARIACORE.CLIENT_SUPP_FIELDS WHERE FIELD_NAME = '"+accountFieldName+"' AND CLIENT_NO=" + client_no)
		while(rs.next())
		{
			acctFieldHashResult.put("Field Name",rs.getString('FIELD_NAME'))
			acctFieldHashResult.put("Field Description",rs.getString('FIELD_SUPP_TEXT'))
			acctFieldHashResult.put("Field Required",rs.getString('REQUIRED_IND'))
			acctFieldHashResult.put("Field Min Value",rs.getString('MIN_NUM_VALS'))
			acctFieldHashResult.put("Field Max Value",rs.getString('MAX_NUM_VALS'))
			acctFieldHashResult.put("Field Order",rs.getString('PRIMACY'))
			acctFieldHashResult.put("Field Input",rs.getString('FORM_INPUT_TYPE'))
		}
		logi.logInfo("Hash map result is : " +acctFieldHashResult)
		return acctFieldHashResult.sort()
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_update_acct_field_from_API(String testcaseid)
	{
		LinkedHashMap<String,String> acctFieldHashResult = new LinkedHashMap<String, String>()
		String accountFieldNo = getValueFromResponse("update_supp_field", ExPathAdminToolsApi.UPDATE_SUPP_FIELD_NAME1)
		
		acctFieldHashResult.put("Field Name",getValueFromHttpRequestValue("update_supp_field","field_name"))
		acctFieldHashResult.put("Field Description",getValueFromHttpRequestValue("update_supp_field","field_desc"))
		acctFieldHashResult.put("Field Required",getValueFromHttpRequestValue("update_supp_field","required_ind"))
		acctFieldHashResult.put("Field Min Value",getValueFromHttpRequestValue("update_supp_field","min_no_sel"))
		acctFieldHashResult.put("Field Max Value",getValueFromHttpRequestValue("update_supp_field","max_no_sel"))
		acctFieldHashResult.put("Field Order",getValueFromHttpRequestValue("update_supp_field","field_order"))
		acctFieldHashResult.put("Field Input",getValueFromHttpRequestValue("update_supp_field","presentation_mode"))
		
		logi.logInfo("Hash map result is : " +acctFieldHashResult)
		return acctFieldHashResult.sort()
		
	}
	
	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_update_credit_template_percent_API(String testcaseid)
	{
		LinkedHashMap<String,String> CTHashResult = new LinkedHashMap<String, String>()
		String updateCTNo = getValueFromHttpRequestValue("update_credit_template","client_credit_template_id")
		String client_no = getValueFromHttpRequestValue("update_credit_template", "client_no")
		CTHashResult.put("CT Client Id",getValueFromHttpRequestValue("update_credit_template","client_credit_template_id"))
		CTHashResult.put("CT Name",getValueFromHttpRequestValue("update_credit_template","credit_template_name"))
		CTHashResult.put("CT Discount Amount",getValueFromHttpRequestValue("update_credit_template","discount_amt"))
		CTHashResult.put("CT No Of Credits",getValueFromHttpRequestValue("update_credit_template","no_of_credits"))
		CTHashResult.put("CT Credit Interval Months",getValueFromHttpRequestValue("update_credit_template","credit_interval_months"))
		String value = getValueFromHttpRequestValue("update_credit_template","percentage_client_plan_id")
		CTHashResult.put("CT Percentage Service",getValueFromHttpRequestValue("update_credit_template","percentage_client_service_id"))
		ResultSet rs = db.executeQuery("SELECT * FROM ARIACORE.ALL_SERVICE WHERE CLIENT_NO = "+client_no+" AND SERVICE_NAME ='"+value)
		while(rs.next())
		{
			CTHashResult.put("CT Percentage Service",rs.getString('SERVICE_NO'))
		}
		logi.logInfo("Hash map result is : " +CTHashResult)
		return CTHashResult.sort()
		
	}
	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_update_credit_template_flat_API(String testcaseid)
	{
		LinkedHashMap<String,String> CTHashResult = new LinkedHashMap<String, String>()
		String updateCTNo = getValueFromHttpRequestValue("update_credit_template","client_credit_template_id")
		
		CTHashResult.put("CT Client Id",getValueFromHttpRequestValue("update_credit_template","client_credit_template_id"))
		CTHashResult.put("CT Name",getValueFromHttpRequestValue("update_credit_template","credit_template_name"))
		CTHashResult.put("CT Discount Amount",getValueFromHttpRequestValue("update_credit_template","discount_amt"))
		CTHashResult.put("CT No Of Credits",getValueFromHttpRequestValue("update_credit_template","no_of_credits"))
		CTHashResult.put("CT Credit Interval Months",getValueFromHttpRequestValue("update_credit_template","credit_interval_months"))
		
		logi.logInfo("Hash map result is : " +CTHashResult)
		return CTHashResult.sort()
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_update_credit_template_flat_DB(String testcaseid)
	{
		LinkedHashMap<String,String> CTHashResult = new LinkedHashMap<String, String>()
		String CTNo =getValueFromHttpRequestValue("update_credit_template","client_credit_template_id")
		String client_no = getValueFromHttpRequestValue("update_credit_template", "client_no")
		String planNo = null;
		String serviceNo = null;
		logi.logInfo("CT name and Client number : " + CTNo + " :: " + client_no)
		ResultSet rs = db.executeQuery("SELECT * FROM ARIACORE.RECURRING_CREDIT_TEMPLATES where CLIENT_NO = "+client_no+" AND CLIENT_CREDIT_TEMPLATE_ID = '"+CTNo+"'")
		while(rs.next())
		{
			CTHashResult.put("CT Client Id",rs.getString('CLIENT_CREDIT_TEMPLATE_ID'))
			CTHashResult.put("CT Name",rs.getString('LABEL'))
			CTHashResult.put("CT Discount Amount",rs.getString('FLAT_AMOUNT'))
			CTHashResult.put("CT No Of Credits",rs.getString('NUM_CREDITS_REQUIRED'))
			CTHashResult.put("CT Credit Interval Months",rs.getString('CREDIT_INTERVAL_MONTHS'))
			
		}
		logi.logInfo("Hash map result is : " +CTHashResult)
		return CTHashResult.sort()
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_update_credit_template_percent_DB(String testcaseid)
	{
		LinkedHashMap<String,String> CTHashResult = new LinkedHashMap<String, String>()
		String CTNo =getValueFromHttpRequestValue("update_credit_template","client_credit_template_id")
		String client_no = getValueFromHttpRequestValue("update_credit_template", "client_no")
		String planNo = null;
		String serviceNo = null;
		logi.logInfo("CT name and Client number : " + CTNo + " :: " + client_no)
		ResultSet rs = db.executeQuery("SELECT * FROM ARIACORE.RECURRING_CREDIT_TEMPLATES where CLIENT_NO = "+client_no+" AND CLIENT_CREDIT_TEMPLATE_ID = '"+CTNo+"'")
		while(rs.next())
		{
			CTHashResult.put("CT Client Id",rs.getString('CLIENT_CREDIT_TEMPLATE_ID'))
			CTHashResult.put("CT Name",rs.getString('LABEL'))
			CTHashResult.put("CT Discount Amount",rs.getString('PERCENT_AMOUNT'))
			CTHashResult.put("CT No Of Credits",rs.getString('NUM_CREDITS_REQUIRED'))
			CTHashResult.put("CT Credit Interval Months",rs.getString('CREDIT_INTERVAL_MONTHS'))
			CTHashResult.put("CT Percentage Service",rs.getString('PERCENT_EVAL_SERVICE_NO'))
			planNo = rs.getString('PERCENT_EVAL_PLAN_NO')
			
		}
		ResultSet rs1 = db.executeQuery("SELECT * FROM ARIACORE.CLIENT_PLAN WHERE CLIENT_NO = "+client_no+" AND PLAN_NO ="+planNo)
		while(rs1.next())
		{
			CTHashResult.put("CT Percentage Plan",rs1.getString('PLAN_NAME'))
		}
		
		logi.logInfo("Hash map result is : " +CTHashResult)
		return CTHashResult.sort()
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_update_inventory_details_from_DB(String testcaseid) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()

		String item_no = getValueFromResponse("update_inventory_item", ExPathAdminToolsApi.CREATE_INVENTORY_ITEM_ITEM_NO1)
		String client_no = getValueFromHttpRequestValue("update_inventory_item", "client_no")
		logi.logInfo("Item and Client number : " + item_no + " :: " + client_no)

		ResultSet rs = db.executeQuery("SELECT * FROM ARIACORE.INVENTORY_ITEMS where ITEM_NO= " + item_no + " AND CLIENT_NO=" + client_no)
		while(rs.next()) {
			hashResult.put("Item No", rs.getString('ITEM_NO'))
			hashResult.put("Item type", rs.getString('ARC_IND'))
			hashResult.put("Description", rs.getString('LONG_DESC'))
			hashResult.put("Service No", rs.getString('SERVICE_NO'))
			hashResult.put("Active Index", rs.getString('NO_DISPLAY_IND'))
			hashResult.put("Subunit Qty", rs.getString('SUBUNIT_QTY'))
			hashResult.put("Subunit Label", rs.getString('SUBUNIT_LABEL'))
			
			
		}
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}
	
	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_update_inventory_details_from_update_API(String testcaseid) {
		LinkedHashMap <String, String> resultHash = new LinkedHashMap<String, String>()
		String active_ind = getValueFromHttpRequestValue("update_inventory_item", "active_ind")
		String item_type = getValueFromHttpRequestValue("update_inventory_item", "item_type")
		String subunit_qty = getValueFromHttpRequestValue("update_inventory_item", "subunit_qty")
		
		//HashMap resultHash = new HashMap<String, String>()
		resultHash.put("Item No", getValueFromResponse("update_inventory_item", ExPathAdminToolsApi.UPDATE_INVENTORY_ITEM_ITEM_NO1))
		resultHash.put("Item type", (item_type.toInteger()?1:0))
		resultHash.put("Description", getValueFromHttpRequestValue("update_inventory_item", "item_desc"))
		resultHash.put("Service No", getValueFromHttpRequestValue("update_inventory_item", "service[service_no]"))
		resultHash.put("Active Index", (active_ind.toInteger()?0:1))
		resultHash.put("Subunit Qty", subunit_qty==null?'null':subunit_qty)
		resultHash.put("Subunit Label", subunit_qty==null?'null':getValueFromHttpRequestValue("update_inventory_item", "subunit_label"))
		logi.logInfo("Hashmap is : " + resultHash)
		return resultHash.sort()
	}
	
	/**
	 *
	 */
	def md_update_promo_plan_set_from_DB(String testcaseid)
	{
		LinkedHashMap<String,String> promoPlanHashResult = new LinkedHashMap<String, String>()
		String promo_plan_set_no = getValueFromResponse("update_promo_plan_set", ExPathAdminToolsApi.UPDATE_PROMO_PLAN_SET_NO1)
		String client_no = getValueFromHttpRequestValue("update_promo_plan_set", "client_no")
		logi.logInfo("Promotinal plan set No and Client number : " + promo_plan_set_no + " :: " + client_no)
		ResultSet rs = db.executeQuery("SELECT * from ariacore.PLAN_TYPES WHERE PLAN_TYPE_NO ="+promo_plan_set_no)
		while(rs.next())
		 {
			promoPlanHashResult.put("Promotional Plan Set No",rs.getString('PLAN_TYPE_NO'))
			promoPlanHashResult.put("Promotional Plan Set Name",rs.getString('KEY_NAME'))
			promoPlanHashResult.put("Promotional Plan Set Description",rs.getString('DESC_LONG'))
			promoPlanHashResult.put("Promotional Plan Set Client ID",rs.getString('CLIENT_PLAN_TYPE_ID'))
		 }
		 logi.logInfo("Hash map result is : " +promoPlanHashResult)
		return promoPlanHashResult.sort()
	}
	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_update_promo_plan_set_from_API(String testcaseid)
	{
			logi.logInfo("Enter the method from API")
			LinkedHashMap<String,String> promoPlanHashResult = new LinkedHashMap<String, String>()
			promoPlanHashResult.put("Promotional Plan Set No",getValueFromResponse("update_promo_plan_set", ExPathAdminToolsApi.UPDATE_PROMO_PLAN_SET_NO1))
			promoPlanHashResult.put("Promotional Plan Set Name",getValueFromHttpRequestValue("update_promo_plan_set","promo_plan_set_name"))
			promoPlanHashResult.put("Promotional Plan Set Description",getValueFromHttpRequestValue("update_promo_plan_set", "promo_plan_set_desc"))
			promoPlanHashResult.put("Promotional Plan Set Client ID",getValueFromHttpRequestValue("update_promo_plan_set", "client_plan_type_id"))
			logi.logInfo("Hash map result is : " +promoPlanHashResult)
			return promoPlanHashResult.sort()
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_create_plan_group_from_DB(String testcaseid)
	{
		LinkedHashMap<String,String> planGrpHashResult = new LinkedHashMap<String, String>()
		String groupNo = getValueFromResponse("create_plan_group", ExPathAdminToolsApi.CREATE_PLAN_GRP_GROUPNO1)
		String client_no = getValueFromHttpRequestValue("create_plan_group", "client_no")
		logi.logInfo("Group No and Client number : " + groupNo + " :: " + client_no)
		ResultSet rs = db.executeQuery("SELECT * FROM ARIACORE.PLAN_CHANGE_GRP where CLIENT_NO= " + client_no + " AND GROUP_NO=" + groupNo)
		while(rs.next())
		 {
			planGrpHashResult.put("Group No",rs.getString('GROUP_NO'))
			planGrpHashResult.put("Group Name",rs.getString('GROUP_NAME'))
			planGrpHashResult.put("Group Description",rs.getString('DESCRIPTION'))
			planGrpHashResult.put("Group Client ID",rs.getString('CLIENT_PLAN_CHANGE_GROUP_ID'))
			int csr = rs.getInt('CSR_IND')
			int uss = rs.getInt('USS_IND')
			logi.logInfo("CSR && USS value is : " +csr + " And " + uss )
			if((csr == 1) && (uss == 1))
			{
				planGrpHashResult.put("Group Usage Type","Both CSR And USS")
				logi.logInfo("Both")
			}
			else if((csr == 1) && (uss == 0))
			{
				planGrpHashResult.put("Group Usage Type","CSR")
				logi.logInfo("CSR")
			}
			else if((csr == 0) && (uss == 1))
			{
				planGrpHashResult.put("Group Usage Type","USS")
				logi.logInfo("USS")
			}
		 }
		 logi.logInfo("Hash map result is : " +planGrpHashResult)
		return planGrpHashResult.sort()
	}
	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_create_plan_group_API(String testcaseid)
	{
			logi.logInfo("Enter the method from API")
			LinkedHashMap<String,String> planGrpHashResult = new LinkedHashMap<String, String>()
			planGrpHashResult.put("Group No",getValueFromResponse("create_plan_group", ExPathAdminToolsApi.CREATE_PLAN_GRP_GROUPNO1))
			planGrpHashResult.put("Group Name",getValueFromHttpRequestValue("create_plan_group","group_name"))
			planGrpHashResult.put("Group Description",getValueFromHttpRequestValue("create_plan_group", "group_desc"))
			planGrpHashResult.put("Group Client ID",getValueFromHttpRequestValue("create_plan_group", "client_plan_change_group_id"))
			String usageType = getValueFromHttpRequestValue("create_plan_group", "group_usage");
			if(usageType.equalsIgnoreCase("csr"))
			{
				planGrpHashResult.put("Group Usage Type","CSR")
			}
			else if(usageType.equalsIgnoreCase("uss"))
			{
				planGrpHashResult.put("Group Usage Type","USS")
			}
			else if(usageType.equalsIgnoreCase("both"))
			{
				planGrpHashResult.put("Group Usage Type","Both CSR And USS")
			}
			logi.logInfo("Hash map result is : " +planGrpHashResult)
			return planGrpHashResult.sort()
	}
	
	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_update_plan_group_from_DB(String testcaseid)
	{
		LinkedHashMap<String,String> planGrpHashResult = new LinkedHashMap<String, String>()
		String groupNo = getValueFromResponse("update_plan_group", ExPathAdminToolsApi.UPDATE_PLAN_GRP_GROUPNO1)
		String client_no = getValueFromHttpRequestValue("update_plan_group", "client_no")
		logi.logInfo("Group No and Client number : " + groupNo + " :: " + client_no)
		ResultSet rs = db.executeQuery("SELECT * FROM ARIACORE.PLAN_CHANGE_GRP where CLIENT_NO= " + client_no + " AND GROUP_NO=" + groupNo)
		while(rs.next())
		 {
			planGrpHashResult.put("Group No",rs.getString('GROUP_NO'))
			planGrpHashResult.put("Group Name",rs.getString('GROUP_NAME'))
			planGrpHashResult.put("Group Description",rs.getString('DESCRIPTION'))
			int csr = rs.getInt('CSR_IND')
			int uss = rs.getInt('USS_IND')
			logi.logInfo("CSR && USS value is : " +csr + " And " + uss )
			if((csr == 1) && (uss == 1))
			{
				planGrpHashResult.put("Group Usage Type","Both CSR And USS")
				logi.logInfo("Both")
			}
			else if((csr == 1) && (uss == 0))
			{
				planGrpHashResult.put("Group Usage Type","CSR")
				logi.logInfo("CSR")
			}
			else if((csr == 0) && (uss == 1))
			{
				planGrpHashResult.put("Group Usage Type","USS")
				logi.logInfo("USS")
			}
		 }
		 logi.logInfo("Hash map result is : " +planGrpHashResult)
		return planGrpHashResult.sort()
	}
	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_update_plan_group_API(String testcaseid)
	{
			logi.logInfo("Enter the method from API")
			LinkedHashMap<String,String> planGrpHashResult = new LinkedHashMap<String, String>()
			planGrpHashResult.put("Group No",getValueFromResponse("update_plan_group", ExPathAdminToolsApi.UPDATE_PLAN_GRP_GROUPNO1))
			planGrpHashResult.put("Group Name",getValueFromHttpRequestValue("update_plan_group","group_name"))
			planGrpHashResult.put("Group Description",getValueFromHttpRequestValue("update_plan_group", "group_desc"))
			String usageType = getValueFromHttpRequestValue("update_plan_group", "group_usage");
			if(usageType.equalsIgnoreCase("csr"))
			{
				planGrpHashResult.put("Group Usage Type","CSR")
			}
			else if(usageType.equalsIgnoreCase("uss"))
			{
				planGrpHashResult.put("Group Usage Type","USS")
			}
			else if(usageType.equalsIgnoreCase("both"))
			{
				planGrpHashResult.put("Group Usage Type","Both CSR And USS")
			}
			logi.logInfo("Hash map result is : " +planGrpHashResult)
			return planGrpHashResult.sort()
	}
	
	
	
	
	def md_edit_plan_details_from_DB(String testcaseID) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		def client_no = getValueFromHttpRequestValue("edit_plan", "client_no")
		String plan_name =  getValueFromHttpRequestValue("edit_plan", "plan_name")
		String plan_no = db.executeQueryP2("SELECT plan_no FROM ARIACORE.CLIENT_PLAN where plan_name='"+plan_name+"' and client_no="+client_no)
		String planDetailsQuery = "SELECT * FROM ARIACORE.CLIENT_PLAN where PLAN_NO= " + plan_no
		String dunningPlanQuery = "SELECT DUNNING_PLAN_NO from dunning_plan_assignment where PLAN_NO=" + plan_no + " AND CLIENT_NO=" + client_no
		ResultSet rs = db.executePlaQuery(planDetailsQuery)
		while(rs.next()) {
			hashResult.put("Plan No", plan_no)
			hashResult.put("Plan Name", rs.getString("plan_name"))
			hashResult.put("Desc", rs.getString("eu_html_long"))
			hashResult.put("Billing Interval", rs.getString("billing_interval"))
			hashResult.put("Usage Billing Interval", rs.getString("usage_billing_interval"))
			hashResult.put("Currency", rs.getString("currency"))
			hashResult.put("Status Cd", rs.getString("new_acct_status_cd"))
			hashResult.put("Supp Plan Ind", rs.getString("supp_plan_ind"))
			hashResult.put("Rollover Acct Status Cd", rs.getString("rollover_acct_status_cd"))
			hashResult.put("Rollover Acct Status Days", rs.getString("rollover_acct_status_days"))
			hashResult.put("Rollover Plan No", rs.getString("rollover_plan_no"))
			hashResult.put("Rollover Months", rs.getString("rollover_months"))
		}
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}

	
	def md_VerifyServiceDetailsAfterEdit_DB(String testcaseid) {
		List planGroupsId = getPropertyListContains("plan_group", testcaseid)
		def client_no = getValueFromHttpRequestValue("edit_plan", "client_no")
		def plan_no = getValueFromResponse("edit_plan", ExPathAdminToolsApi.EDIT_PLAN_NO1)
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		String serviceDetailsQuery = "SELECT * FROM ARIACORE.PLAN_SERVICES JOIN ARIACORE.SERVICES ON ARIACORE.PLAN_SERVICES.SERVICE_NO = ARIACORE.SERVICES.SERVICE_NO AND ARIACORE.PLAN_SERVICES.client_no = ARIACORE.SERVICES.custom_to_client_no JOIN ARIACORE.CHART_OF_ACCTS ON ARIACORE.SERVICES.COA_ID = ARIACORE.CHART_OF_ACCTS.COA_ID AND ARIACORE.SERVICES.custom_to_client_no = ARIACORE.CHART_OF_ACCTS.custom_to_client_no WHERE ARIACORE.PLAN_SERVICES.PLAN_NO = " + plan_no + " AND ARIACORE.PLAN_SERVICES.CLIENT_NO = " + client_no + " AND ARIACORE.CHART_OF_ACCTS.COA_ID IN (SELECT COA_ID FROM ARIACORE.SERVICES WHERE SERVICE_NO IN (SELECT SERVICE_NO FROM ARIACORE.PLAN_SERVICES WHERE PLAN_NO = " + plan_no + " AND CLIENT_NO = " + client_no + "))"
		logi.logInfo(" Plan services Query " + serviceDetailsQuery)
		ResultSet result = db.executePlaQuery(serviceDetailsQuery)

		List testCaseCmb = Constant.lhmAllHttpInputRequest_Global.keySet().asList()
		List httpValues = Constant.lhmAllHttpInputRequest_Global.values().asList()
		List<String> propertyList = []
		String apiname=testcaseid.split("-")[2]
		logi.logInfo("API Name:"+apiname)

		for(int j=0; j<testCaseCmb.size(); j++) {
			logi.logInfo("case Cmb value is :: "+ testCaseCmb[j])
			logi.logInfo(testcaseid)
			if(testCaseCmb[j].toString().equals(testcaseid)) {
				com.eviware.soapui.impl.wsdl.teststeps.HttpTestRequest request = httpValues[j]
				logi.logInfo("Request is ::::" + request)
				for(prp in request.getPropertyList()) {
					if(prp.name.contains("[service_no]")) {
						propertyList.add(prp.name)
					}
				}
			}
		}

		int r = (propertyList.size())-1,i,s1,s2
		while (result.next()) {
			i = r
			if(result.getString("COMMENTS").contains("Activation")) {
				s1 = r
				s2 = 0
				i=s2
				logi.logInfo("After swapping :: " + s1 + "::" + s2 + "iterator value : " + i)
			}
			else if(s1) {
				i=i+1
				logi.logInfo("iterator value : " + i)
			}
			logi.logInfo("After setting values :: " + i)
			logi.logInfo("Iteration ----------------------------------------------------------------------------------------")
			String service_no = result.getString("SERVICE_NO")
			hashResult.put("service[" + i + "][service_no]" , service_no)
			hashResult.put("service[" + i + "][name]" , result.getString("COMMENTS"))
			hashResult.put("service[" + i + "][client_service_id]" , result.getString("CLIENT_SERVICE_ID"))
			hashResult.put("service[" + i + "][gl_cd]" , result.getString("CLIENT_COA_CODE"))
			String serType = getServiceType(result.getString("RECURRING"), result.getString("USAGE_BASED"), result.getString("IS_CANCELLATION"), result.getString("IS_MIN_FEE"), result.getString("IS_ORDER_BASED"))
			hashResult.put("service[" + i + "][service_type]" , serType)
			hashResult.put("service[" + i + "][pricing_rule]" , getPricingRule(result.getString("TIERED_PRICING_RULE_NO")))
			hashResult.put("service[" + i + "][usage_type]" , result.getString("USAGE_TYPE"))
			r--
		}
		logi.logInfo("md_VerifyServiceDetails_DB Hashmap is : " + hashResult)
		return hashResult.sort()
	}

	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_Delete_Rules_DB(String testcaseid)
	{
		logi.logInfo("md_Delete_Rules_DB")
		def client_no = getValueFromHttpRequestValue("delete_rules", "client_no")
		String rule_no =  getValueFromHttpRequestValue("delete_rules", "rule_nos[0]")
		String del_rule_no = db.executeQueryP2("SELECT CASE WHEN COUNT(CLIENT_RULE_ID) = 0 THEN 'Discount Rule Deleted' ELSE 'Discount Rule Not Deleted' END AS RESULT FROM ARIACORE.CLIENT_DISCOUNT_RULES WHERE CLIENT_NO = client_no AND CLIENT_RULE_ID = '"+rule_no+"'")
		return del_rule_no
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_Delete_Bundles_DB(String testcaseid)
	{
		logi.logInfo("md_Delete_Bundles_DB")
		def client_no = getValueFromHttpRequestValue("delete_bundles", "client_no")
		String bundle_no =  getValueFromHttpRequestValue("delete_bundles", "bundle_nos[0]")
		String del_bundle_no = db.executeQueryP2("SELECT CASE WHEN COUNT(CLIENT_BUNDLE_ID) = 0 THEN 'Discount Bundle Deleted' ELSE 'Discount Bundle Not Deleted' END AS RESULT FROM ARIACORE.CLIENT_DISCOUNT_RULE_BUNDLES WHERE CLIENT_NO = client_no AND CLIENT_BUNDLE_ID = '"+bundle_no+"'")
		return del_bundle_no
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_Delete_Plan_DB(String testcaseid)
	{
		logi.logInfo("md_Delete_Plans_DB")
		def client_no = getValueFromHttpRequestValue("delete_plans", "client_no")
		String plan_no =  getValueFromHttpRequestValue("delete_plans", "plan_nos[0]")
		String del_plan_no = db.executeQueryP2("SELECT CASE WHEN COUNT(CLIENT_PLAN_ID) = 0 THEN 'Plan Deleted' ELSE 'Plan Not Deleted' END AS RESULT FROM ARIACORE.CLIENT_PLAN WHERE CLIENT_NO = client_no AND CLIENT_PLAN_ID = '"+plan_no+"'")
		return del_plan_no
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_Delete_Plan_Group_DB(String testcaseid)
	{
		logi.logInfo("md_Delete_Plan_Group_DB")
		def client_no = getValueFromHttpRequestValue("delete_plans", "client_no")
		String plan_grp_no =  getValueFromHttpRequestValue("delete_plans", "plan_nos[0]")
		String del_plangrp_no = db.executeQueryP2("SELECT CASE WHEN COUNT(CLIENT_PLAN_CHANGE_GROUP_ID) = 0 THEN 'Plan Group Deleted' ELSE 'Plan Group Not Deleted' END AS RESULT FROM ARIACORE.PLAN_CHANGE_GRP WHERE CLIENT_NO = client_no AND CLIENT_PLAN_CHANGE_GROUP_ID = '"+plan_grp_no+"'")
		return del_plangrp_no
	}
	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_Delete_Coupons_DB(String testcaseid)
	{
		logi.logInfo("md_Delete_Coupons_DB")
		def client_no = getValueFromHttpRequestValue("delete_coupons", "client_no")
		String coupon_cd =  getValueFromHttpRequestValue("delete_coupons", "coupon_cd[0]")
		String coupon_cd_res = db.executeQueryP2("SELECT CASE WHEN COUNT(COUPON_CD) = 0 THEN 'Coupon Deleted' ELSE 'Coupon Not Deleted' END AS RESULT FROM ARIACORE.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE CLIENT_NO = client_no AND COUPON_CD = '"+plan_grp_no+"'")
		return coupon_cd_res
	}
	
	
	def md_edit_plan_details_from_API(String testcaseID) {
		LinkedHashMap<String,String> resultHash = new LinkedHashMap<String, String>()
		String apiname = "edit_plan"
		logi.logInfo("API Name:"+apiname)
		String plan_no = getValueFromResponse(apiname, ExPathAdminToolsApi.EDIT_PLAN_NO1)
		String plan_name =  getValueFromHttpRequestValue(apiname, "plan_name")
		String item_name =  getValueFromHttpRequestValue(apiname, "item_name")
		String desc =  getValueFromHttpRequestValue(apiname, "plan_description")
		String client_plan_id =  getValueFromHttpRequestValue(apiname, "client_plan_id")
		String billing_interval =  getValueFromHttpRequestValue(apiname, "billing_interval")
		String usage_billing_interval =  getValueFromHttpRequestValue(apiname, "usage_billing_interval")
		String currency =  getValueFromHttpRequestValue(apiname, "currency")
		String status_cd =  getValueFromHttpRequestValue(apiname, "acct_status_cd")
		String how_to_apply_min_fee =  getValueFromHttpRequestValue(apiname, "how_to_apply_min_fee")
		String supp_plan_ind =  ((getValueFromHttpRequestValue(apiname, "parent_plans[0]"))==null)?0:1
		String rollover_acct_status_cd = getValueFromHttpRequestValue(apiname, "rollover_acct_status_cd")
		String rollover_acct_status_days = getValueFromHttpRequestValue(apiname, "rollover_acct_status_days")
		String rollover_plan_no = getValueFromHttpRequestValue(apiname, "rollover_plan_no")
		String rollover_months = getValueFromHttpRequestValue(apiname, "rollover_months")
		String credit_note_template_no = getValueFromHttpRequestValue(apiname, "credit_note_template_no")

		resultHash.put("Plan No", plan_no)
		resultHash.put("Plan Name", plan_name)
		resultHash.put("Desc", desc)
		resultHash.put("Billing Interval", billing_interval)
		resultHash.put("Usage Billing Interval", usage_billing_interval)
		resultHash.put("Currency", currency)
		resultHash.put("Status Cd", status_cd)
		resultHash.put("Supp Plan Ind", supp_plan_ind)
		resultHash.put("Rollover Acct Status Cd", rollover_acct_status_cd)
		resultHash.put("Rollover Acct Status Days", rollover_acct_status_days)
		resultHash.put("Rollover Plan No", rollover_plan_no)
		resultHash.put("Rollover Months", rollover_months)

		logi.logInfo("Hashmap is : " + resultHash)
		return resultHash.sort()
	}
	
	
	
	
	def md_VerifyUpdateCoupondetails_DB(String testcaseid)
	{
	
			String apiname = "update_coupon"
			String client_no = getValueFromHttpRequestValue(apiname, "client_no")
			def coupon_cd
			if (apiname == 'update_coupon')
			{
				String credit_template_no = getValueFromResponse(apiname, ExPathAdminToolsApi.UPDATE_COUPON_COUPON_CODE1)
				String query1 = "Select * from ARIACORE.COUPON_RECUR_CREDIT_TMPLT_MAP WHERE RECURRING_CREDIT_TEMPLATE_NO = " + credit_template_no + " AND CLIENT_NO = " + client_no + ""
				ResultSet rs1 = db.executePlaQuery(query1)
				while(rs1.next())
				{
					coupon_cd = rs1.getString('COUPON_CD')
				}
				logi.logInfo "coupon_cd ==="+ coupon_cd
			}
			else
			{
				coupon_cd = getValueFromResponse(apiname, ExPathAdminToolsApi.UPDATE_COUPON_COUPON_CODE1)
			}
	
	
			HashMap coupondbHash = new HashMap<String, String>()
			String query1 = "SELECT * FROM ARIACORE.COUPONS WHERE COUPON_CD ='" + coupon_cd + "' AND CLIENT_NO=" + client_no
			ResultSet rs = db.executePlaQuery(query1)
			while(rs.next())
			{
				coupondbHash.put("COUPON_CD", rs.getString('COUPON_CD'))
				coupondbHash.put("COUPON_DESC", rs.getString('COMMENTS'))
				coupondbHash.put("COUPON_MSG", rs.getString('ONSCREEN_COUPON_USE_MSG'))
				coupondbHash.put("NO_OF_USES", rs.getString('MAX_USES'))
				DateFormat format = new SimpleDateFormat("dd-MMM-yy");
				def startdate = rs.getDate('START_DATE');
				def enddate = rs.getDate('EXP_DATE');
				if (startdate == null)
				{
					coupondbHash.put("START_DATE", "null")
				}
				else
				{
					startdate = format.format(startdate);
					coupondbHash.put("START_DATE", startdate)
				}
	
				if (enddate == null)
				{
					coupondbHash.put("END_DATE", "null")
				}
				else
				{
					enddate = format.format(enddate).toString()
					coupondbHash.put("END_DATE", enddate)
				}
				/*                   if (apiname == 'update_coupon')
				 {
				 if (rs.getString('NO_DISPLAY_IND') == '0')
				 {
				 coupondbHash.put("STATUS", "0")
				 }
				 else
				 {
				 coupondbHash.put("STATUS", "1")
				 }
				 }
				 else
				 {*/
				if (rs.getString('NO_DISPLAY_IND') == '0')
				{
					coupondbHash.put("STATUS", "1")
				}
				else
				{
					coupondbHash.put("STATUS", "0")
				}
	
			}
			return coupondbHash.sort()
		}
	
	
	
	def md_VerifyUpdateCoupondetails_API(String testcaseid)
	{
		String apiname = "update_coupon"
		HashMap couponHash = new HashMap<String,String>()

		couponHash.put("COUPON_CD", getValueFromHttpRequestValue(apiname, "coupon_cd"))
		couponHash.put("COUPON_DESC", getValueFromHttpRequestValue(apiname, "coupon_desc"))
		couponHash.put("COUPON_MSG", getValueFromHttpRequestValue(apiname, "coupon_msg"))
		couponHash.put("STATUS", getValueFromHttpRequestValue(apiname, "status_ind"))
		couponHash.put("NO_OF_USES", getValueFromHttpRequestValue(apiname, "no_of_uses"))
		couponHash.put("START_DATE", getValueFromHttpRequestValue(apiname, "start_date"))
		couponHash.put("END_DATE", getValueFromHttpRequestValue(apiname, "end_date"))

		return couponHash.sort()
	}
	
	
	def md_VerifyCompanyProfileDetails_API(String testcaseid)
	{
		String apiname = "get_company_profile"
		HashMap companyHash = new HashMap<String,String>()
		
		companyHash.put("CLIENT_NAME", getValueFromResponse(apiname, ExPathAdminToolsApi.COMPANY_PROFILE_CLIENT_NAME))
		companyHash.put("ADDRESS 1", getValueFromHttpRequestValue(apiname,"address1"))
		companyHash.put("ADDRESS 2", getValueFromHttpRequestValue(apiname,"address2"))
		companyHash.put("CITY", getValueFromHttpRequestValue(apiname,"city"))
		companyHash.put("LOCALITY", getValueFromHttpRequestValue(apiname,"locality"))
		companyHash.put("STATE", getValueFromHttpRequestValue(apiname,"state_prov"))
		companyHash.put("COUNTRY", getValueFromHttpRequestValue(apiname,"country"))
		companyHash.put("POSTAL CODE", getValueFromHttpRequestValue(apiname,"postal_code"))
		companyHash.put("PHONE", getValueFromHttpRequestValue(apiname,"phone"))
				
		return companyHash.sort()
	}
	
	
	def md_VerifySetCompanyProfileDetails_API(String testcaseid)
	{
		String apiname = "set_company_profile"
		HashMap companyHash = new HashMap<String,String>()
		
		companyHash.put("CLIENT_NAME", getValueFromResponse(apiname, ExPathAdminToolsApi.COMPANY_PROFILE_CLIENT_NAME))
		companyHash.put("ADDRESS 1", "1400 N Providence Rd.")
		companyHash.put("ADDRESS 2", "Suite 200")
		companyHash.put("CITY", "Media")
		companyHash.put("LOCALITY", "United States")
		companyHash.put("STATE", "PA")
		companyHash.put("COUNTRY", "US")
		companyHash.put("POSTAL CODE", 19063)
		companyHash.put("PHONE", 1234567890)
				
		return companyHash.sort()
	}
	
	def md_VerifyCompanyProfileDetails_DB(String testcaseid)
	{
		String apiname = "get_company_profile"
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		logi.logInfo("CLIENT NO 1 IS " + client_no)
		HashMap companyHash = new HashMap<String,String>()
		String query1 = "SELECT * from ARIACORE.CLIENT WHERE CLIENT_NO = " + client_no + ""
		ResultSet rs = db.executePlaQuery(query1)
		while(rs.next())
		{
			companyHash.put("CLIENT_NAME", rs.getString('CLIENT_NAME'))
			companyHash.put("ADDRESS 1", rs.getString('ADDRESS1'))
			companyHash.put("ADDRESS 2", rs.getString('ADDRESS2'))
			companyHash.put("CITY", rs.getString('CITY'))
			companyHash.put("LOCALITY", rs.getString('LOCALITY'))
			companyHash.put("STATE", rs.getString('STATE_PROV'))
			companyHash.put("COUNTRY", rs.getString('COUNTRY'))
			companyHash.put("POSTAL CODE", rs.getString('POSTAL_CODE'))
			companyHash.put("PHONE", rs.getString('PHONE'))
		}
		return companyHash.sort()
	}
	
	def md_VerifySetCompanyProfileDetails_DB(String testcaseid)
	{
		String apiname = "set_company_profile"
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		logi.logInfo("CLIENT NO 1 IS " + client_no)
		HashMap companyHash = new HashMap<String,String>()
		String query1 = "SELECT * from ARIACORE.CLIENT WHERE CLIENT_NO = " + client_no + ""
		ResultSet rs = db.executePlaQuery(query1)
		while(rs.next())
		{
			companyHash.put("CLIENT_NAME", rs.getString('CLIENT_NAME'))
			companyHash.put("ADDRESS 1", rs.getString('ADDRESS1'))
			companyHash.put("ADDRESS 2", rs.getString('ADDRESS2'))
			companyHash.put("CITY", rs.getString('CITY'))
			companyHash.put("LOCALITY", rs.getString('LOCALITY'))
			companyHash.put("STATE", rs.getString('STATE_PROV'))
			companyHash.put("COUNTRY", rs.getString('COUNTRY'))
			companyHash.put("POSTAL CODE", rs.getString('POSTAL_CODE'))
			companyHash.put("PHONE", rs.getString('PHONE'))
		}
		return companyHash.sort()
	}
	
	def md_VerifyCompanyProfileContactDetails_API(String testcaseid)
	{
		String apiname = "get_company_profile"
		HashMap companyContactHash = new HashMap<String,String>()
		
		companyContactHash.put("CONTACT", getValueFromHttpRequestValue(apiname,"contact"))
		companyContactHash.put("CONTACT ADDRESS 1", getValueFromHttpRequestValue(apiname,"contact_address1"))
		companyContactHash.put("CONTACT ADDRESS 2", getValueFromHttpRequestValue(apiname,"contact_address2"))
		companyContactHash.put("CONTACT CITY", getValueFromHttpRequestValue(apiname,"contact_city"))
		companyContactHash.put("CONTACT STATE", getValueFromHttpRequestValue(apiname,"contact_state"))
		companyContactHash.put("CONTACT ZIP", getValueFromHttpRequestValue(apiname,"contact_zip"))
		companyContactHash.put("CONTACT PHONE", getValueFromHttpRequestValue(apiname,"contact_phone"))
		companyContactHash.put("CONTACT EMAIL", getValueFromHttpRequestValue(apiname,"contact_email"))
		companyContactHash.put("CONTACT COUNTRY", getValueFromHttpRequestValue(apiname,"contact_country"))
		companyContactHash.put("CONTACT LOCALITY", getValueFromHttpRequestValue(apiname,"contact_locality"))
		
		return companyContactHash.sort()
	}
	
	def md_VerifySetCompanyProfileContactDetails_API(String testcaseid)
	{
		String apiname = "set_company_profile"
		HashMap companyContactHash = new HashMap<String,String>()
		
		companyContactHash.put("CONTACT", "Aria")
		companyContactHash.put("CONTACT ADDRESS 1", "1400 N Providence Rd.")
		companyContactHash.put("CONTACT ADDRESS 2", "Suite 200")
		companyContactHash.put("CONTACT CITY", "Media")
		companyContactHash.put("CONTACT STATE", "PA")
		companyContactHash.put("CONTACT ZIP", 19063)
		companyContactHash.put("CONTACT PHONE", 1234567890)
		companyContactHash.put("CONTACT EMAIL", "blackhole@ariasystems.com")
		companyContactHash.put("CONTACT COUNTRY", "US")
		companyContactHash.put("CONTACT LOCALITY", "United States")
		
		return companyContactHash.sort()
	}
	
	def md_VerifyCompanyProfileContactDetails_DB(String testcaseid)
	{
		String apiname = "get_company_profile"
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		logi.logInfo("CLIENT NO IS 2 " + client_no)
		HashMap companyContactHash = new HashMap<String,String>()
		String query1 = "SELECT * from ARIACORE.CLIENT WHERE CLIENT_NO = " + client_no + ""
		ResultSet rs = db.executePlaQuery(query1)
		while(rs.next())
		{
			companyContactHash.put("CONTACT", rs.getString('CONTACT'))
			companyContactHash.put("CONTACT ADDRESS 1", rs.getString('CONTACT_ADDRESS1'))
			companyContactHash.put("CONTACT ADDRESS 2", rs.getString('CONTACT_ADDRESS2'))
			companyContactHash.put("CONTACT CITY", rs.getString('CONTACT_CITY'))
			companyContactHash.put("CONTACT STATE", rs.getString('CONTACT_STATE'))
			companyContactHash.put("CONTACT ZIP", rs.getString('CONTACT_ZIP'))
			companyContactHash.put("CONTACT PHONE", rs.getString('CONTACT_PHONE'))
			companyContactHash.put("CONTACT EMAIL", rs.getString('CONTACT_EMAIL'))
			companyContactHash.put("CONTACT COUNTRY", rs.getString('CONTACT_COUNTRY'))
			companyContactHash.put("CONTACT LOCALITY", rs.getString('CONTACT_LOCALITY'))
		}
		return companyContactHash.sort()
	}
	
	def md_VerifySetCompanyProfileContactDetails_DB(String testcaseid)
	{
		String apiname = "set_company_profile"
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		logi.logInfo("CLIENT NO IS 2 " + client_no)
		HashMap companyContactHash = new HashMap<String,String>()
		String query1 = "SELECT * from ARIACORE.CLIENT WHERE CLIENT_NO = " + client_no + ""
		ResultSet rs = db.executePlaQuery(query1)
		while(rs.next())
		{
			companyContactHash.put("CONTACT", rs.getString('CONTACT'))
			companyContactHash.put("CONTACT ADDRESS 1", rs.getString('CONTACT_ADDRESS1'))
			companyContactHash.put("CONTACT ADDRESS 2", rs.getString('CONTACT_ADDRESS2'))
			companyContactHash.put("CONTACT CITY", rs.getString('CONTACT_CITY'))
			companyContactHash.put("CONTACT STATE", rs.getString('CONTACT_STATE'))
			companyContactHash.put("CONTACT ZIP", rs.getString('CONTACT_ZIP'))
			companyContactHash.put("CONTACT PHONE", rs.getString('CONTACT_PHONE'))
			companyContactHash.put("CONTACT EMAIL", rs.getString('CONTACT_EMAIL'))
			companyContactHash.put("CONTACT COUNTRY", rs.getString('CONTACT_COUNTRY'))
			companyContactHash.put("CONTACT LOCALITY", rs.getString('CONTACT_LOCALITY'))
		}
		return companyContactHash.sort()
	}
	
	def md_VerifyCompanyProfileBillingDetails_API(String testcaseid)
	{
		String apiname = "get_company_profile"
		HashMap companyBillingHash = new HashMap<String,String>()
		
		companyBillingHash.put("BILLING CONTACT", getValueFromHttpRequestValue(apiname,"billing_contact"))
		companyBillingHash.put("BILLING ADDRESS 1", getValueFromHttpRequestValue(apiname,"billing_address1"))
		companyBillingHash.put("BILLING ADDRESS 2", getValueFromHttpRequestValue(apiname,"billing_address2"))
		companyBillingHash.put("BILLING CITY", getValueFromHttpRequestValue(apiname,"billing_city"))
		companyBillingHash.put("BILLING ZIP", getValueFromHttpRequestValue(apiname,"billing_zip"))
		companyBillingHash.put("BILLING PHONE", getValueFromHttpRequestValue(apiname,"billing_phone"))
		companyBillingHash.put("BILLING STATE", getValueFromHttpRequestValue(apiname,"billing_state"))
		companyBillingHash.put("BILLING COUNTRY", getValueFromHttpRequestValue(apiname,"billing_country"))
		companyBillingHash.put("BILLING LOCALITY", getValueFromHttpRequestValue(apiname,"billing_locality"))
		
		return companyBillingHash.sort()
	}
	
	
	def md_VerifySetCompanyProfileBillingDetails_API(String testcaseid)
	{
		String apiname = "set_company_profile"
		HashMap companyBillingHash = new HashMap<String,String>()
		
		companyBillingHash.put("BILLING CONTACT","Aria")
		companyBillingHash.put("BILLING ADDRESS 1","1400 N Providence Rd.")
		companyBillingHash.put("BILLING ADDRESS 2","Suite 200")
		companyBillingHash.put("BILLING CITY","Media")
		companyBillingHash.put("BILLING ZIP",19063)
		companyBillingHash.put("BILLING PHONE",1234567890)
		companyBillingHash.put("BILLING STATE","PA")
		companyBillingHash.put("BILLING COUNTRY","US")
		companyBillingHash.put("BILLING LOCALITY","United States")
		
		return companyBillingHash.sort()
	}
	
	def md_VerifyCompanyProfileBillingDetails_DB(String testcaseid)
	{
		String apiname = "get_company_profile"
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		logi.logInfo("CLIENT NO IS 2 " + client_no)
		HashMap companyBillingHash = new HashMap<String,String>()
		String query1 = "SELECT * from ARIACORE.CLIENT WHERE CLIENT_NO = " + client_no + ""
		ResultSet rs = db.executePlaQuery(query1)
		while(rs.next())
		{
			companyBillingHash.put("BILLING CONTACT", rs.getString('BILLING_CONTACT'))
			companyBillingHash.put("BILLING ADDRESS 1", rs.getString('BILLING_ADDRESS1'))
			companyBillingHash.put("BILLING ADDRESS 2", rs.getString('BILLING_ADDRESS2'))
			companyBillingHash.put("BILLING CITY", rs.getString('BILLING_CITY'))
			companyBillingHash.put("BILLING ZIP", rs.getString('BILLING_ZIP'))
			companyBillingHash.put("BILLING PHONE", rs.getString('BILLING_PHONE'))
			companyBillingHash.put("BILLING STATE", rs.getString('BILLING_STATE'))
			companyBillingHash.put("BILLING COUNTRY", rs.getString('BILLING_COUNTRY'))
			companyBillingHash.put("BILLING LOCALITY", rs.getString('BILLING_LOCALITY'))
		}
		return companyBillingHash.sort()
	}
	
	def md_VerifySetCompanyProfileBillingDetails_DB(String testcaseid)
	{
		String apiname = "set_company_profile"
		String client_no = getValueFromHttpRequestValue(apiname, "client_no")
		logi.logInfo("CLIENT NO IS 2 " + client_no)
		HashMap companyBillingHash = new HashMap<String,String>()
		String query1 = "SELECT * from ARIACORE.CLIENT WHERE CLIENT_NO = " + client_no + ""
		ResultSet rs = db.executePlaQuery(query1)
		while(rs.next())
		{
			companyBillingHash.put("BILLING CONTACT", rs.getString('BILLING_CONTACT'))
			companyBillingHash.put("BILLING ADDRESS 1", rs.getString('BILLING_ADDRESS1'))
			companyBillingHash.put("BILLING ADDRESS 2", rs.getString('BILLING_ADDRESS2'))
			companyBillingHash.put("BILLING CITY", rs.getString('BILLING_CITY'))
			companyBillingHash.put("BILLING ZIP", rs.getString('BILLING_ZIP'))
			companyBillingHash.put("BILLING PHONE", rs.getString('BILLING_PHONE'))
			companyBillingHash.put("BILLING STATE", rs.getString('BILLING_STATE'))
			companyBillingHash.put("BILLING COUNTRY", rs.getString('BILLING_COUNTRY'))
			companyBillingHash.put("BILLING LOCALITY", rs.getString('BILLING_LOCALITY'))
		}
		return companyBillingHash.sort()
	}
		
	
	def md_verifyGetServiceNo(String testcaseid)
	{
		String apiname = "get_service_details"
		String client_service_id = getValueFromHttpRequestValue(apiname, "client_service_id")
		def client_no = getValueFromHttpRequestValue(apiname, "client_no")
		String serviceNoQuery = "SELECT SERVICE_NO FROM ARIACORE.SERVICES WHERE CLIENT_SERVICE_ID = '" + client_service_id + "' AND CUSTOM_TO_CLIENT_NO = "+client_no+" "
		ResultSet rs = db.executePlaQuery(serviceNoQuery)
		while(rs.next())
		{
		return rs.getString("SERVICE_NO")
		}
		
	}
	
	def md_getPlanGroupCount_API(String testcaseid)
	{
		def count = getNodeCountFromResponse("get_plan_groups", "//*/*:plan_groups[1]/*:e")
		logi.logInfo("Node API count is " + count)
		return count
	}
	
	
	def md_getPlanGroupCount_DB(String testcaseid)
	{
		def client_no = getValueFromHttpRequestValue("get_plan_groups", "client_no")
		String plangrp_count = db.executeQueryP2("SELECT COUNT(*) FROM ARIACORE.PLAN_CHANGE_GRP WHERE CLIENT_NO = "+client_no+" ")
		logi.logInfo("Node DB count is " + plangrp_count)
		return plangrp_count
	}
	
	def md_getPlanGroupNo(String testcaseid)
	{
		def client_no = getValueFromHttpRequestValue("get_plan_group_details", "client_no")
		def client_Grp_Id = getValueFromHttpRequestValue("get_plan_group_details", "client_plan_change_group_id")
		String planGrpNo = db.executeQueryP2("SELECT GROUP_NO FROM ARIACORE.PLAN_CHANGE_GRP WHERE CLIENT_NO = "+client_no+" AND CLIENT_PLAN_CHANGE_GROUP_ID = '"+client_Grp_Id+"'")
		logi.logInfo("Node DB count is " + planGrpNo)
		return planGrpNo
	}
	
	
	def md_VerifyCreateUsageType_DB(String testcaseid)
	{
		HashMap usageHash = new HashMap<String,String>()
		String apiname = "create_usage_type"
		String usage_type_cd = getValueFromHttpRequestValue(apiname, "usage_type_code")
		String serviceNoQuery = "SELECT * FROM ARIACORE.CLIENT_USAGE_TYPE WHERE USAGE_TYPE_CD = '" + usage_type_cd + "' "
		ResultSet rs = db.executePlaQuery(serviceNoQuery)
		while(rs.next())
		{
			usageHash.put("USAGE TYPE NAME", rs.getString('SUMMARY_DESC'))
			usageHash.put("USAGE TYPE DESCRIPTION", rs.getString('DESCRIPTION'))
			usageHash.put("USAGE UNIT TYPE NO", rs.getString('USAGE_UNIT_TYPE_NO'))
			usageHash.put("USAGE TYPE CODE", rs.getString('USAGE_TYPE_CD'))
			usageHash.put("USAGE NAME", rs.getString('SUPP_DISPLAY_STRING'))
			usageHash.put("USAGE TYPE NO", rs.getString('USAGE_TYPE_NO'))
		}
		return usageHash.sort()
	}
	
	
	
	def md_VerifyCreateUsageType_API(String testcaseid)
	{
		HashMap usageHash = new HashMap<String,String>()
		String apiname = "create_usage_type"
		
			usageHash.put("USAGE TYPE NAME", getValueFromHttpRequestValue(apiname,'usage_type_name'))
			usageHash.put("USAGE TYPE DESCRIPTION", getValueFromHttpRequestValue(apiname,'usage_type_desc'))
			usageHash.put("USAGE UNIT TYPE NO", getValueFromHttpRequestValue(apiname,'usage_unit_type_no'))
			usageHash.put("USAGE TYPE CODE", getValueFromHttpRequestValue(apiname,'usage_type_code'))
			usageHash.put("USAGE NAME", getValueFromHttpRequestValue(apiname,'usage_type_display_string'))
			usageHash.put("USAGE TYPE NO", getValueFromResponse(apiname, ExPathAdminToolsApi.CREATE_USAGE_TYPE_NO1))
			return usageHash.sort()
	}
	
	
	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_prod_field_from_DB(String testcaseid)
	{
		LinkedHashMap<String,String> prodFieldHashResult = new LinkedHashMap<String, String>()
		String productFieldNo = getValueFromResponse("create_supp_obj_field", ExPathAdminToolsApi.PROD_FIELD_NO1)
		String client_no = getValueFromHttpRequestValue("create_supp_obj_field", "client_no")
		logi.logInfo("Product Field No and Client number : " + productFieldNo + " :: " + client_no)
		ResultSet rs = db.executeQuery("SELECT * FROM ARIACORE.CLIENT_OBJ_SUPP_FIELDS WHERE FIELD_NO = '"+productFieldNo+"' AND CLIENT_NO=" + client_no)
		while(rs.next())
		{
			prodFieldHashResult.put("Field Name",rs.getString('FIELD_NAME'))
			prodFieldHashResult.put("Field Description",rs.getString('DESCRIPTION'))
			prodFieldHashResult.put("Data Type",rs.getString('DATATYPE'))
			prodFieldHashResult.put("Field Min Value",rs.getString('MIN_NUM_VALS'))
			prodFieldHashResult.put("Field Max Value",rs.getString('MAX_NUM_VALS'))
			prodFieldHashResult.put("Field Input",rs.getString('FORM_INPUT_TYPE'))
		}
		logi.logInfo("Hash map result is : " +prodFieldHashResult)
		return prodFieldHashResult.sort()
	}
	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_prod_field_from_API(String testcaseid)
	{
		LinkedHashMap<String,String> prodFieldHashResult = new LinkedHashMap<String, String>()
		String accountFieldNo = getValueFromResponse("create_supp_obj_field", ExPathAdminToolsApi.PROD_FIELD_NO1)
		
		prodFieldHashResult.put("Field Name",getValueFromHttpRequestValue("create_supp_obj_field","field_name"))
		prodFieldHashResult.put("Field Description",getValueFromHttpRequestValue("create_supp_obj_field","description"))
		prodFieldHashResult.put("Data Type",getValueFromHttpRequestValue("create_supp_obj_field","datatype"))
		prodFieldHashResult.put("Field Min Value",getValueFromHttpRequestValue("create_supp_obj_field","min_no_sel"))
		prodFieldHashResult.put("Field Max Value",getValueFromHttpRequestValue("create_supp_obj_field","max_no_sel"))
		prodFieldHashResult.put("Field Input",getValueFromHttpRequestValue("create_supp_obj_field","form_input_type"))
		
		logi.logInfo("Hash map result is : " +prodFieldHashResult)
		return prodFieldHashResult.sort()
		
	}
	
	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_update_prod_field_from_DB(String testcaseid)
	{
		LinkedHashMap<String,String> prodFieldHashResult = new LinkedHashMap<String, String>()
		String productFieldNo = getValueFromResponse("update_supp_obj_field", ExPathAdminToolsApi.PROD_FIELD_NO1)
		String client_no = getValueFromHttpRequestValue("update_supp_obj_field", "client_no")
		logi.logInfo("Product Field No and Client number : " + productFieldNo + " :: " + client_no)
		ResultSet rs = db.executeQuery("SELECT * FROM ARIACORE.CLIENT_OBJ_SUPP_FIELDS WHERE FIELD_NO = '"+productFieldNo+"' AND CLIENT_NO=" + client_no)
		while(rs.next())
		{
			prodFieldHashResult.put("Field Name",rs.getString('FIELD_NAME'))
			prodFieldHashResult.put("Field Description",rs.getString('DESCRIPTION'))
			prodFieldHashResult.put("Data Type",rs.getString('DATATYPE'))
			prodFieldHashResult.put("Field Min Value",rs.getString('MIN_NUM_VALS'))
			prodFieldHashResult.put("Field Max Value",rs.getString('MAX_NUM_VALS'))
			prodFieldHashResult.put("Field Input",rs.getString('FORM_INPUT_TYPE'))
		}
		logi.logInfo("Hash map result is : " +prodFieldHashResult)
		return prodFieldHashResult.sort()
	}
	
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_update_prod_field_from_API(String testcaseid)
	{
		LinkedHashMap<String,String> prodFieldHashResult = new LinkedHashMap<String, String>()
		String accountFieldNo = getValueFromResponse("update_supp_obj_field", ExPathAdminToolsApi.PROD_FIELD_NO1)
		
		prodFieldHashResult.put("Field Name",getValueFromHttpRequestValue("update_supp_obj_field","field_name"))
		prodFieldHashResult.put("Field Description",getValueFromHttpRequestValue("update_supp_obj_field","description"))
		prodFieldHashResult.put("Data Type",getValueFromHttpRequestValue("update_supp_obj_field","datatype"))
		prodFieldHashResult.put("Field Min Value",getValueFromHttpRequestValue("update_supp_obj_field","min_no_sel"))
		prodFieldHashResult.put("Field Max Value",getValueFromHttpRequestValue("update_supp_obj_field","max_no_sel"))
		prodFieldHashResult.put("Field Input",getValueFromHttpRequestValue("update_supp_obj_field","form_input_type"))
		
		logi.logInfo("Hash map result is : " +prodFieldHashResult)
		return prodFieldHashResult.sort()
		
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_create_acct_complete_coupon_from_API(String testcaseid)
	{
		LinkedHashMap<String,String> couponHashResult = new LinkedHashMap<String, String>()
		String accountNo = getValueFromResponse("create_acct_complete", ExPathAdminToolsApi.CREATE_ACCT_COMPLETE_ACCT_NO)
		def coupon_amt= Math.abs(getValueFromResponse("create_acct_complete", ExPathAdminToolsApi.CREATE_ACCT_COMPLETE_COUPON).toInteger())
		logi.logInfo "Coupon AMT is " + coupon_amt
		couponHashResult.put("Coupon Code",getValueFromHttpRequestValue("create_acct_complete","coupon_codes"))
		couponHashResult.put("Coupon Amount",coupon_amt)
		
		logi.logInfo("API Hash map result is : " +couponHashResult)
		return couponHashResult.sort()
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_create_acct_complete_coupon_from_DB(String testcaseid)
	{
		LinkedHashMap<String,String> couponHashResult = new LinkedHashMap<String, String>()
		String accountNo = getValueFromResponse("create_acct_complete", ExPathAdminToolsApi.CREATE_ACCT_COMPLETE_ACCT_NO)
		String client_no = getValueFromHttpRequestValue("create_acct_complete", "client_no")
		logi.logInfo("Account No and Client number : " + accountNo + " :: " + client_no)
		ResultSet rs = db.executeQuery("SELECT * FROM (SELECT ROWNUM R, AMOUNT,ORIG_COUPON_CD FROM ARIACORE.ALL_CREDITS WHERE ACCT_NO = "+accountNo+" AND CLIENT_NO = "+client_no+")WHERE ROWNUM = 1")
		while(rs.next())
		{
			couponHashResult.put("Coupon Code",rs.getString('ORIG_COUPON_CD'))
			couponHashResult.put("Coupon Amount",rs.getString('AMOUNT'))
		}
		logi.logInfo("DB Hash map result is : " +couponHashResult)
		return couponHashResult.sort()
	}
	
	/**
	 *
	 * @param testcaseid
	 * @return
	 */
	def md_create_acct_complete_account_credit_DB(String testcaseid)
	{
		LinkedHashMap<String,String> couponHashResult = new LinkedHashMap<String, String>()
		String accountNo = getValueFromResponse("create_acct_complete", ExPathAdminToolsApi.CREATE_ACCT_COMPLETE_ACCT_NO)
		String client_no = getValueFromHttpRequestValue("create_acct_complete", "client_no")
		logi.logInfo("Account No and Client number : " + accountNo + " :: " + client_no)
		ResultSet rs = db.executeQuery("SELECT AMOUNT FROM (SELECT ROWNUM R, AMOUNT,ORIG_COUPON_CD FROM ARIACORE.ALL_CREDITS WHERE ACCT_NO = "+accountNo+" AND CLIENT_NO = "+client_no+")WHERE ROWNUM = 1")
		while(rs.next())
		{
			logi.logInfo("Inside the method md_create_acct_complete_coupon_from_DB1")
			logi.logInfo("Amount from DB 1 is "+rs.getString('AMOUNT'))
			return rs.getString('AMOUNT')
		}
	}
	
	def md_create_acct_complete_invoice_credits_DB(String testcaseid)
	{
		LinkedHashMap<String,String> couponHashResult = new LinkedHashMap<String, String>()
		String accountNo = getValueFromResponse("create_acct_complete", ExPathAdminToolsApi.CREATE_ACCT_COMPLETE_ACCT_NO)
		String client_no = getValueFromHttpRequestValue("create_acct_complete", "client_no")
		logi.logInfo("Account No and Client number : " + accountNo + " :: " + client_no)
		ResultSet rs = db.executeQuery("SELECT CREDIT FROM (SELECT ROWNUM R, CREDIT FROM ARIACORE.ALL_INVOICES WHERE ACCT_NO = "+accountNo+" AND CLIENT_NO = "+client_no+")WHERE ROWNUM = 1")
		while(rs.next())
		{
			logi.logInfo("Inside the method md_create_acct_complete_coupon_from_DB2")
			logi.logInfo("Amount from DB 2 is "+rs.getString('CREDIT'))
			return rs.getString('CREDIT')
		}
	}
	
	//Rev rec in create Service API
	def md_create_service_rev_rec_API(String testcaseid)
	{
		LinkedHashMap<String,String> revRecResult = new LinkedHashMap<String, String>()
		String service_no = getValueFromResponse("create_service", ExPathAdminToolsApi.CREATE_SERVICE_SERVICE_NO1)
		String client_no = getValueFromHttpRequestValue("create_service", "client_no")
		logi.logInfo("Service No and Client number : " + service_no + " :: " + client_no)
		String defGlCode = getValueFromHttpRequestValue("create_service","deferred_ind")
		logi.logInfo("Def Code "+defGlCode)
		int defInd = defGlCode.equals(null)?0:1
		logi.logInfo("Def Ind "+defInd)
		revRecResult.put("Service No",service_no)
		revRecResult.put("Account Receivable GL Code",getValueFromHttpRequestValue("create_service","ar_gl_cd"))
		if(defInd == 0)
		{
			revRecResult.put("Deferred GL Code",null)
		}
		else
		{
			revRecResult.put("Deferred GL Code",getValueFromHttpRequestValue("create_service","def_gl_cd"))
		}
		revRecResult.put("Rev Rec Ind",getValueFromHttpRequestValue("create_service","rev_rec_ind"))
		revRecResult.put("Deferred Ind",defInd)
		
		logi.logInfo("Rev rec from API "+revRecResult.sort())
		return revRecResult.sort()
	}
	
	//Rev rec in create Service DB
	def md_create_service_rev_rec_DB(String testcaseid)
	{
		LinkedHashMap<String,String> revRecResult = new LinkedHashMap<String, String>()
		String service_no = getValueFromResponse("create_service", ExPathAdminToolsApi.CREATE_SERVICE_SERVICE_NO1)
		String client_no = getValueFromHttpRequestValue("create_service", "client_no")
		logi.logInfo("Service No and Client number : " + service_no + " :: " + client_no)
		
		ResultSet rs = db.executeQuery("SELECT * FROM REVREC.CLIENT_RR_SERVICES_XFORM_MAP WHERE service_no = "+service_no+" AND CLIENT_NO=" + client_no)
		while(rs.next())
		{
			revRecResult.put("Service No",rs.getString('SERVICE_NO'))
			revRecResult.put("Account Receivable GL Code",rs.getString('AR_GL_CODE'))
			revRecResult.put("Deferred GL Code",rs.getString('UNEARNED_GL_CODE'))
			revRecResult.put("Rev Rec Ind",rs.getString('DO_AR_IND'))
			revRecResult.put("Deferred Ind",rs.getString('DO_DEFERRED_IND'))
		}
		logi.logInfo("Rev rec from API "+revRecResult.sort())
		return revRecResult.sort()
	}
	
	//Rev rec in Update Service API
	def md_update_service_rev_rec_API(String testcaseid)
	{
		logi.logInfo("Enter md_update_service_rev_rec_API method")
		LinkedHashMap<String,String> revRecResult = new LinkedHashMap<String, String>()
		String service_no = getValueFromResponse("update_service", ExPathAdminToolsApi.UPDATE_SERVICE_NO1)
		String client_no = getValueFromHttpRequestValue("update_service", "client_no")
		logi.logInfo("Service No and Client number : " + service_no + " :: " + client_no)
		String defGlCode = getValueFromHttpRequestValue("update_service","deferred_ind")
		logi.logInfo("Def Code "+defGlCode)
		int defInd = defGlCode.equals(null)?0:1
		logi.logInfo("Def Ind "+defInd)
		revRecResult.put("Service No",service_no)
		revRecResult.put("Account Receivable GL Code",getValueFromHttpRequestValue("update_service","ar_gl_cd"))
		if(defInd == 0)
		{
			revRecResult.put("Deferred GL Code",null)
		}
		else
		{
			revRecResult.put("Deferred GL Code",getValueFromHttpRequestValue("update_service","def_gl_cd"))
		}
		revRecResult.put("Deferred GL Code",getValueFromHttpRequestValue("update_service","def_gl_cd"))
		revRecResult.put("Rev Rec Ind",getValueFromHttpRequestValue("update_service","rev_rec_ind"))
		revRecResult.put("Deferred Ind",getValueFromHttpRequestValue("update_service","deferred_ind"))
		logi.logInfo("Rev rec from API "+revRecResult.sort())
		return revRecResult.sort()
	}
	
	//Rev rec in Update Service DB
	def md_update_service_rev_rec_DB(String testcaseid)
	{
		LinkedHashMap<String,String> revRecResult = new LinkedHashMap<String, String>()
		String service_no = getValueFromResponse("update_service", ExPathAdminToolsApi.UPDATE_SERVICE_NO1)
		String client_no = getValueFromHttpRequestValue("update_service", "client_no")
		logi.logInfo("Service No and Client number : " + service_no + " :: " + client_no)
		
		ResultSet rs = db.executeQuery("SELECT * FROM REVREC.CLIENT_RR_SERVICES_XFORM_MAP WHERE service_no = "+service_no+" AND CLIENT_NO=" + client_no)
		while(rs.next())
		{
			revRecResult.put("Service No",rs.getString('SERVICE_NO'))
			revRecResult.put("Account Receivable GL Code",rs.getString('AR_GL_CODE'))
			revRecResult.put("Deferred GL Code",rs.getString('UNEARNED_GL_CODE'))
			revRecResult.put("Rev Rec Ind",rs.getString('DO_AR_IND'))
			revRecResult.put("Deferred Ind",rs.getString('DO_DEFERRED_IND'))
		}
		logi.logInfo("Rev rec from API "+revRecResult.sort())
		return revRecResult.sort()
		
	}
	
	def md_create_service_without_rev_rec_DB(String testcaseid)
	{
		logi.logInfo("md_create_service_without_rev_rec_DB")
		String service_no = getValueFromResponse("create_service", ExPathAdminToolsApi.UPDATE_SERVICE_NO1)
		String client_no = getValueFromHttpRequestValue("create_service", "client_no")
		logi.logInfo("Service No and Client number : " + service_no + " :: " + client_no)
		
		ResultSet rs = db.executeQuery("SELECT * FROM REVREC.CLIENT_RR_SERVICES_XFORM_MAP WHERE service_no = "+service_no+" AND CLIENT_NO=" + client_no)
		if(!rs.next())
		{
			return "No Node"
		}
		else
		{
			return "Rev Rec Present"
		}
	}
	
	def md_update_service_without_rev_rec_DB(String testcaseid)
	{
		logi.logInfo("md_create_service_without_rev_rec_DB")
		String service_no = getValueFromResponse("update_service", ExPathAdminToolsApi.UPDATE_SERVICE_NO1)
		String client_no = getValueFromHttpRequestValue("update_service", "client_no")
		logi.logInfo("Service No and Client number : " + service_no + " :: " + client_no)
		
		ResultSet rs = db.executeQuery("SELECT * FROM REVREC.CLIENT_RR_SERVICES_XFORM_MAP WHERE service_no = "+service_no+" AND CLIENT_NO=" + client_no)
		if(!rs.next())
		{
			return "No Node"
		}
		else
		{
			return "Rev Rec Present"
		}
	}
	
	def md_VerifyChildPlans_from_Edit_Directive_API(String testcaseid) {
		logi.logInfo("Entering md_VerifyChildPlans_from_Edit_Directive_API")
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		List<String> child_plans=[]
		String apiname=testcaseid.split("-")[2]
		def directive = getValueFromHttpRequestValue("edit_plan", "edit_directives")
		logi.logInfo("Directive selected is " +directive)
		logi.logInfo("API Name:"+apiname)
		if(directive=="1" || directive==null)
		{
			logi.logInfo("Entering directive 1")
			for(i in 0..3) {
				child_plans.add(getValueFromHttpRequestValue("edit_plan", "child_plans[" + i + "]"))
			}
			int i=0
			ResultSet fld_vals
			for(n in child_plans) {
				if(n != null) {
					hashResult.put("child_plans"+i, n)
				}
				i++
			}
			logi.logInfo("Hashmap is : " + hashResult)
		}
		else if(directive=="2")
		{
			logi.logInfo("Entering directive 2")
			for(i in 0..3) {
				child_plans.add(getValueFromHttpRequestValue("edit_plan", "child_plans[" + i + "]"))
			}
			int i=0
			ResultSet fld_vals
			for(n in child_plans) {
				if(n != null) {
					hashResult.put("child_plans"+i, n)
				}
				i++
			}
			logi.logInfo("Hashmap is : " + hashResult)
		}
		else if(directive=="3")
		{
			logi.logInfo("Entering directive 3")
			for(i in 0..3) {
				logi.logInfo("Child Plans list " + getValueFromHttpRequestValue("create_new_plan", "child_plans[" + i + "]"))
				child_plans.add(getValueFromHttpRequestValue("create_new_plan", "child_plans[" + i + "]"))
			}
			for(i in 0..3) {
				logi.logInfo("Child Plans Removing list " + getValueFromHttpRequestValue("edit_plan", "child_plans[" + i + "]"))
				child_plans.remove(getValueFromHttpRequestValue("edit_plan", "child_plans[" + i + "]"))
			}
			int i=0
			for(n in child_plans) {
				if(n != null) {
					hashResult.put("child_plans"+i, n)
				}
				i++
			}
			logi.logInfo("Hashmap is : " + hashResult)
		}
		return hashResult.sort()
	}
	
	
	
	def md_verifyRollOverPlan_Directive_DB(String testcaseid)
	{
		logi.logInfo("Entering md_verifyRollOverPlan")
		String apiname=testcaseid.split("-")[2]
		def directive = getValueFromHttpRequestValue("edit_plan", "edit_directives")
		logi.logInfo("Directive selected is " +directive)
		logi.logInfo("API Name:"+apiname)
		def plan_no = getValueFromResponse("edit_plan", ExPathAdminToolsApi.EDIT_PLAN_NO1)
		def rollOverPlanNo = getValueFromHttpRequestValue("edit_plan", "rollover_plan_no")
		String planDetailsQuery = "SELECT * FROM ARIACORE.CLIENT_PLAN where PLAN_NO= " + plan_no
		ResultSet rs = db.executePlaQuery(planDetailsQuery)
		while(rs.next())
		{
			logi.logInfo("Roll over plan no is "+rs.getString("rollover_plan_no"))
			return rs.getString("rollover_plan_no")
		}
	}
	
	def md_verifyRollOverPlan_Directive_API(String testcaseid)
	{
		logi.logInfo("Entering md_verifyRollOverPlan")
		String apiname=testcaseid.split("-")[2]
		def directive = getValueFromHttpRequestValue("edit_plan", "edit_directives")
		def rollOverPlanNo
		logi.logInfo("Directive selected is " +directive)
		logi.logInfo("API Name:"+apiname)
		def plan_no = getValueFromResponse("edit_plan", ExPathAdminToolsApi.EDIT_PLAN_NO1)
		def editRollOverPlanNo = getValueFromHttpRequestValue("edit_plan", "rollover_plan_no")
		def createRollOverPlanNo = getValueFromHttpRequestValue("create_new_plan", "rollover_plan_no")
		List<String> rollOverPlans=[]
		
		rollOverPlans.add(getValueFromHttpRequestValue("create_new_plan", "rollover_plan_no"))
		
		if(directive=="1")
		{
			logi.logInfo("Entering directive 1")
			return editRollOverPlanNo
		}
		else if(directive=="2")
		{
			logi.logInfo("Entering directive 2")
			return editRollOverPlanNo
		}
		else if(directive=="3")
		{
			logi.logInfo("Entering directive 3")
			return null;
		}
	}
	
	def md_create_plan_details_In_Edit_API1(String testcaseID) {
		def directive = getValueFromHttpRequestValue("edit_plan", "edit_directives")
		logi.logInfo("Directive selected is " +directive)
		
		if(directive=="3")
		{
			logi.logInfo("Get Value "+library.Constants.Constant.resultHashCreateNewPlan.get("Statement Template No"));
			if(library.Constants.Constant.resultHashCreateNewPlan.get("Statement Template No")==getValueFromHttpRequestValue("edit_plan", "template_no")){
				library.Constants.Constant.resultHashCreateNewPlan.put("Statement Template No","null")
			}
			if(library.Constants.Constant.resultHashCreateNewPlan.get("Rollover Months")==getValueFromHttpRequestValue("edit_plan", "rollover_months")){
				library.Constants.Constant.resultHashCreateNewPlan.put("Rollover Months","null")
			}
			if(library.Constants.Constant.resultHashCreateNewPlan.get("Rollover Plan No")==getValueFromHttpRequestValue("edit_plan", "rollover_plan_no")){
				library.Constants.Constant.resultHashCreateNewPlan.put("Rollover Plan No","null")
			}
			if(library.Constants.Constant.resultHashCreateNewPlan.get("Dunning Plan No")==getValueFromHttpRequestValue("edit_plan", "dunning_plan_no")){
				library.Constants.Constant.resultHashCreateNewPlan.put("Dunning Plan No","null")
			}
			if(library.Constants.Constant.resultHashCreateNewPlan.get("Notification Template Grp Id")==getValueFromHttpRequestValue("edit_plan", "notification_template_group_no")){
				library.Constants.Constant.resultHashCreateNewPlan.put("Notification Template Grp Id","null")
			}
		}
		return library.Constants.Constant.resultHashCreateNewPlan.sort()
	}
	
	def md_create_plan_services_In_Edit_API1(String testcaseID) {
		
		return library.Constants.Constant.hashResultCreatePlanServices.sort()
	}
	
	def md_VerifyPlanGroup_from_Edit_Directive_API(String testcaseid) {
		logi.logInfo("Entering md_Verifyplan_group_from_Edit_Directive_API")
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		List<String> plan_group=[]
		String apiname=testcaseid.split("-")[2]
		def directive = getValueFromHttpRequestValue("edit_plan", "edit_directives")
		logi.logInfo("Directive selected is " +directive)
		logi.logInfo("API Name:"+apiname)
		if(directive=="1" || directive==null)
		{
			logi.logInfo("Entering directive 1")
			for(i in 0..3) {
				plan_group.add(getValueFromHttpRequestValue("edit_plan", "plan_group[" + i + "]"))
			}
			int i=0
			ResultSet fld_vals
			for(n in plan_group) {
				if(n != null) {
					hashResult.put("plan_group"+i, n)
				}
				i++
			}
			logi.logInfo("Hashmap is : " + hashResult)
		}
		else if(directive=="2")
		{
			logi.logInfo("Entering directive 2")
			for(i in 0..3) {
				plan_group.add(getValueFromHttpRequestValue("edit_plan", "plan_group[" + i + "]"))
			}
			int i=0
			ResultSet fld_vals
			for(n in plan_group) {
				if(n != null) {
					hashResult.put("plan_group"+i, n)
				}
				i++
			}
			logi.logInfo("Hashmap is : " + hashResult)
		}
		else if(directive=="3")
		{
			logi.logInfo("Entering directive 3")
			for(i in 0..3) {
				logi.logInfo("plan_group list " + getValueFromHttpRequestValue("create_new_plan", "plan_group[" + i + "]"))
				plan_group.add(getValueFromHttpRequestValue("create_new_plan", "plan_group[" + i + "]"))
			}
			for(i in 0..3) {
				logi.logInfo("Child Plans Removing list " + getValueFromHttpRequestValue("edit_plan", "plan_group[" + i + "]"))
				plan_group.remove(getValueFromHttpRequestValue("edit_plan", "plan_group[" + i + "]"))
			}
			int i=0
			for(n in plan_group) {
				if(n != null) {
					hashResult.put("plan_group"+i, n)
				}
				i++
			}
			logi.logInfo("Hashmap is : " + hashResult)
		}
		return hashResult.sort()
	}
	
	
	def md_VerifyServices_from_Edit_Directive_API(String testcaseid) {
		logi.logInfo("Entering md_VeriServices_from_Edit_Directive_API")
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		List<String> plan_group=[]
		String apiname=testcaseid.split("-")[2]
		List testCaseCmb = Constant.lhmAllHttpInputRequest_Global.keySet().asList()
		List httpValues = Constant.lhmAllHttpInputRequest_Global.values().asList()
		List<String> propertyList = []
		logi.logInfo("API Name:"+apiname)
		if(getValueFromHttpRequestValue("edit_plan", "edit_directives")=="3")
		{
			testcaseid = testcaseid.split("-")[0]+"-"+testcaseid.split("-")[1]+"-create_new_plan"
		}
		for(int j=0; j<testCaseCmb.size(); j++) {
			logi.logInfo("case Cmb value is :: "+ testCaseCmb[j])
			logi.logInfo(testcaseid)
			if(testCaseCmb[j].toString().equals(testcaseid)) {
				com.eviware.soapui.impl.wsdl.teststeps.HttpTestRequest request = httpValues[j]
				logi.logInfo("Request is ::::" + request)
				for(prp in request.getPropertyList()) {
					if(prp.name.contains("[service_no]")) {
						propertyList.add(prp.name)
					}
				}
			}
		}
		def directive = getValueFromHttpRequestValue("edit_plan", "edit_directives")
		logi.logInfo("Directive selected is " +directive)
		logi.logInfo("API Name:"+apiname)
		if(directive=="1" || directive==null)
		{
			logi.logInfo("Entering directive 1")
			for(i in 0..3) {
				plan_group.add(getValueFromHttpRequestValue("edit_plan", "plan_group[" + i + "]"))
			}
			int i=0
			ResultSet fld_vals
			for(n in plan_group) {
				if(n != null) {
					hashResult.put("plan_group"+i, n)
				}
				i++
			}
			logi.logInfo("Hashmap is : " + hashResult)
		}
		else if(directive=="2")
		{
			logi.logInfo("Entering directive 2")
			for(i in 0..3) {
				plan_group.add(getValueFromHttpRequestValue("edit_plan", "plan_group[" + i + "]"))
			}
			int i=0
			ResultSet fld_vals
			for(n in plan_group) {
				if(n != null) {
					hashResult.put("plan_group"+i, n)
				}
				i++
			}
			logi.logInfo("Hashmap is : " + hashResult)
		}
		else if(directive=="3")
		{
		
		List<String> service_no=[]
		List<String> name=[]
		List<String> client_service_id=[]
		List<String> gl_cd=[]
		List<String> service_type=[]
		List<String> pricing_rule=[]
		List<String> usage_type=[]
		logi.logInfo("md_VerifyServiceDetails_APIaaaaaaa=>>>>>>>>")
		logi.logInfo("request Size : " + propertyList.size())
		apiname = "create_new_plan"
		int j=0
		
		for(i in 0..(propertyList.size()-2)) {
			logi.logInfo("md_VerifyServiceDetails_APIbbbbbb=>>>>>>>>"+i)
			if(getValueFromHttpRequestValue(apiname, "service[" + (i+j) + "][service_no]")==getValueFromHttpRequestValue("edit_plan", "service[0][service_no]")){
				j++
				
			}
			hashResult.put("service[" + i + "][service_no]", getValueFromHttpRequestValue(apiname, "service[" + (i+j) + "][service_no]"))
			hashResult.put("service[" + i + "][name]", getValueFromHttpRequestValue(apiname, "service[" + (i+j) + "][name]"))
			hashResult.put("service[" + i + "][client_service_id]", getValueFromHttpRequestValue(apiname, "service[" + (i+j) + "][client_service_id]"))
			hashResult.put("service[" + i + "][gl_cd]", getValueFromHttpRequestValue(apiname, "service[" + (i+j) + "][gl_cd]"))
			hashResult.put("service[" + i + "][service_type]", getValueFromHttpRequestValue(apiname, "service[" + (i+j) + "][service_type]"))
			hashResult.put("service[" + i + "][pricing_rule]", getValueFromHttpRequestValue(apiname, "service[" + (i+j) + "][pricing_rule]"))
			hashResult.put("service[" + i + "][usage_type]", getValueFromHttpRequestValue(apiname, "service[" + (i+j) + "][usage_type]"))
		}


		logi.logInfo("md_VerifyServiceDetails_API Hashmap is : " + hashResult)
		library.Constants.Constant.hashResultCreatePlanServices = hashResult.sort()
		return hashResult.sort()
		}
		return library.Constants.Constant.hashResultCreatePlanServices.sort()
	}
	
	
	def md_VerifyRateTier_from_Edit_Directive_API(String testcaseid) {
		logi.logInfo("Entering md_VerifyRateTier_from_Edit_Directive_API")
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		List<String> plan_group=[]
		String apiname=testcaseid.split("-")[2]
		List testCaseCmb = Constant.lhmAllHttpInputRequest_Global.keySet().asList()
		List httpValues = Constant.lhmAllHttpInputRequest_Global.values().asList()
		List<String> propertyList = []
		logi.logInfo("API Name:"+apiname)
		
		for(int j=0; j<testCaseCmb.size(); j++) {
			logi.logInfo("case Cmb value is :: "+ testCaseCmb[j])
			logi.logInfo(testcaseid)
			if(testCaseCmb[j].toString().equals(testcaseid)) {
				com.eviware.soapui.impl.wsdl.teststeps.HttpTestRequest request = httpValues[j]
				logi.logInfo("Request is ::::" + request)
				for(prp in request.getPropertyList()) {
					if(prp.name.contains("[service_no]")) {
						propertyList.add(prp.name)
					}
				}
			}
		}
		def directive = getValueFromHttpRequestValue("edit_plan", "edit_directives")
		logi.logInfo("Directive selected is " +directive)
		logi.logInfo("API Name:"+apiname)
		if(directive=="1" || directive==null)
		{
			logi.logInfo("Entering directive 1")
		}
		else if(directive=="2")
		{
			logi.logInfo("Entering directive 2")
		}
		else if(directive=="3")
		{
		
		List<String> service_no=[]
		List<String> name=[]
		List<String> client_service_id=[]
		List<String> gl_cd=[]
		List<String> service_type=[]
		List<String> pricing_rule=[]
		List<String> usage_type=[]
		logi.logInfo("md_VerifyServiceRateTierDetails_APIaaaaaaa=>>>>>>>>")
		logi.logInfo("request Size : " + propertyList.size())
				int j=0
				Map<String, String> suppFieldValues = md_VerifyRateSeqUnitsAct(testcaseid)
				logi.logInfo("Values Test "+suppFieldValues)
		for(i in 0..(propertyList.size()-1)) {
			logi.logInfo("md_VerifyServiceDetails_APIbbbbbb=>>>>>>>>"+i)
			
			Iterator<Map.Entry<String, String>> iterator1 = suppFieldValues.iterator();
			while (iterator1.hasNext())
			{
				Map.Entry item1 = (Map.Entry) iterator1.next();
				logi.logInfo("Item : " + item1 );
				logi.logInfo("key : " +item1.getKey());
				if(item1.getKey().toString().contains((getValueFromHttpRequestValue("edit_plan", "service["+ i+"][service_no]")).toString()))
					{
						logi.logInfo("Removeed Item : " +item1.getKey());
						//suppFieldValues.remove(item1.getKey())
						iterator1.remove()
						logi.logInfo("Removeed")
					}
			}
		}


		logi.logInfo("md_VerifyServiceDetails_API Hashmap is : " + suppFieldValues)
		
		return suppFieldValues.sort()
		}
		return hashResult
	}
	
	String md_VerifySuppFieldValuesAct_Edit_Directive_3(String testcaseid) {
		
				Map<String, List<String>> suppFieldValues = new TreeMap<String, List<String>>();
		boolean status = true
				int i = 0
				
				while(!getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_no]").equals(null)) {
					logi.logInfo(" Supp Field  - " + getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_no]"))
					int j = 0
					List<String> valueList = new ArrayList<>();
					status = true
					while(!getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_value][" + j + "]").equals(null)) {
						logi.logInfo(" Supp Values - " + getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_value][" + j + "]"))
						int h= 0
						while(!getValueFromHttpRequestValue("edit_plan", "supplemental_obj_field[" + h + "][field_no]").equals(null)){
							if(getValueFromHttpRequestValue("edit_plan", "supplemental_obj_field[" + h + "][field_no]").equals(getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_no]"))){
								int u=0
								while(!getValueFromHttpRequestValue("edit_plan", "supplemental_obj_field[" + h + "][field_value][" + u + "]").equals(null)) {
									logi.logInfo(" Supp Values - " + getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_value][" + j + "]"))
									if(getValueFromHttpRequestValue("edit_plan", "supplemental_obj_field[" + h + "][field_value]["+u+"]").equals(getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_value]["+j+"]"))){
										status = false
									}
									u++
								}
							}
							h++
						}
						
						if(status){
							valueList.add(getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_value][" + j + "]"));
						}
						j++
					}
					logi.logInfo("Test valueList size "+valueList.size())
					if(valueList.size()>0){
						suppFieldValues.put(getValueFromHttpRequestValue("create_new_plan", "supplemental_obj_field[" + i + "][field_no]"), valueList )
					}
					i++
				}
				suppFieldValues.each
				{k,v ->
					String values = v
					if (values.contains(",")) {
						values = values.replaceAll(", ", "&")
						suppFieldValues.put(k,values)
					}
				}
				logi.logInfo("After replace : " + suppFieldValues)
				return suppFieldValues.toString()
			}
	
	//Omraj methods
	
	String md_VerifyRateSchedAmountActEdit_1(String testcaseid)
	{
		logi.logInfo("Entering amount act 1")
		return md_VerifyRateSchedAmountActEdit(testcaseid,1)
	}
	
	String md_VerifyRateSchedAmountActEdit_2(String testcaseid)
	{
		logi.logInfo("Entering amount act 2")
		return md_VerifyRateSchedAmountActEdit(testcaseid,2)
	}
	
	
	String md_VerifyRateSchedAmountActEdit_3(String testcaseid)
	{
		logi.logInfo("Entering amount act 3")
		return md_VerifyRateSchedAmountActEdit(testcaseid,3)
	}
	
	
	String md_VerifyRateSchedAmountActEdit_4(String testcaseid)
	{
		logi.logInfo("Entering amount act 4")
		return md_VerifyRateSchedAmountActEdit(testcaseid,4)
	}
	
	String md_VerifyRateSeqUnitsActNew_2(String testcaseid)
	{
		md_VerifyRateSeqUnitsActNew(testcaseid, 2)
	}
	
	String md_VerifyRateSeqUnitsActNew_3(String testcaseid)
	{
		md_VerifyRateSeqUnitsActNew(testcaseid, 3)
	}
	
	String md_VerifyRateSeqUnitsActNew(String testcaseid, int n) {
		Map<String, String> suppFieldValues = new TreeMap<String, String>()
		Map<String, String> rateSchedhash = getRateScheduleNo()
		Map<String, String> sortedSerivceHash = getSortedSerivceOrderIndex()
		if(sortedSerivceHash.size() == 0) {
			sortedSerivceHash = getSortedSerivceOrderFromDB()
		}
		String apiname=testcaseid.split("-")[2]
		Iterator iterator1 = rateSchedhash.entrySet().iterator();
		int rateSchIndex = 0
		while (iterator1.hasNext())
		{
			Map.Entry item1 = (Map.Entry) iterator1.next();
			Iterator iterator = sortedSerivceHash.entrySet().iterator();

			while (iterator.hasNext())
			{
				Map.Entry item = (Map.Entry) iterator.next();
				for(int i = 1 ; i <= n ; i++ ) {
					String fromVal, toVal
					if(rateSchIndex == 0) {
						/*logi.logInfo("Item  value ---- " + rateSchIndex)
						logi.logInfo(fromVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][from]")
						logi.logInfo(toVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][to]")
						
						fromVal = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][from]")
						toVal = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][to]")*/
						logi.logInfo("Item  value ---- " + rateSchIndex)
						logi.logInfo(fromVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][from]")
						logi.logInfo(toVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][to]")
						
						fromVal = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][from]")
						toVal = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][to]")
					} else {
						logi.logInfo("Item  value ---- " + rateSchIndex)
						logi.logInfo(fromVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][from]")
						logi.logInfo(toVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][to]")
						
						fromVal = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][from]")
						toVal = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][to]")
					}
					if(apiname == "edit_plan" && rateSchIndex == 2){
						logi.logInfo("Item  value ---- " + rateSchIndex)
						logi.logInfo(fromVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][from]")
						logi.logInfo(toVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][to]")
						
						fromVal = getValueFromHttpRequestValue("edit_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][from]")
						toVal = getValueFromHttpRequestValue("edit_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][to]")
					}

					if(toVal.equals(null)) { toVal = 0 }
					if(i == 1) {
						if(fromVal.equals(null)) {
							fromVal = 1
							suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + item.getKey() + " rate tier " + i, "from " + fromVal + " to " + toVal)
							logi.logInfo(item1.getValue() + " " + item.getKey() + " tier : " + i + " " + fromVal + " to " + toVal);
						} else {
							suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + item.getKey() + " rate tier " + i, "from " + fromVal + " to " + toVal)
							logi.logInfo(item1.getValue() + " " + item.getKey() + " tier : " + i + " " + fromVal + " to " + toVal);
						}
					} else {
						if (!fromVal.equals(null)) {
							suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + item.getKey() + " rate tier " + i, "from " + fromVal + " to " + toVal)
							logi.logInfo(item1.getValue() + " " + item.getKey() + " tier : " + i + " " + fromVal + " to " + toVal);
						}
					}
				}
			}
			rateSchIndex++
		}
		library.Constants.Constant.hashResultCreatePlanRateUnits = suppFieldValues
		return suppFieldValues.toString()
	}
	
	String md_VerifyRateSeqUnitsActEd2_1(String testcaseid) {
		return md_VerifyRateSeqUnitsActEd2(testcaseid, 1)
	}
	
	String md_VerifyRateSeqUnitsActEd2_2(String testcaseid) {
		return md_VerifyRateSeqUnitsActEd2(testcaseid, 2)
	}
	
	String md_VerifyRateSeqUnitsActEd2_3(String testcaseid) {
		return md_VerifyRateSeqUnitsActEd2(testcaseid, 3)
	}
	
	String md_VerifyRateSeqUnitsActEd2_4(String testcaseid) {
		return md_VerifyRateSeqUnitsActEd2(testcaseid, 4)
	}
	
	String md_VerifyRateSeqUnitsActEd2(String testcaseid, int n) {
		
		Map<String, String> suppFieldValues = library.Constants.Constant.hashResultCreatePlanRateUnits.sort()
		Map<String, String> rateSchedhash = getRateScheduleNo()
		Map<String, String> sortedSerivceHash = getSortedSerivceOrderIndexEditPlan()
		if(sortedSerivceHash.size() == 0) {
			sortedSerivceHash = getSortedSerivceOrderFromDB()
		}

		Iterator iterator1 = rateSchedhash.entrySet().iterator();
		int rateSchIndex = 0
		while (iterator1.hasNext())
		{
			Map.Entry item1 = (Map.Entry) iterator1.next();
			Iterator iterator = sortedSerivceHash.entrySet().iterator();

			while (iterator.hasNext())
			{
				Map.Entry item = (Map.Entry) iterator.next();
				for(int i = 1 ; i <= n ; i++ ) {
					String fromVal, toVal
					if(rateSchIndex == 0) {
						logi.logInfo("Item  value ---- " + rateSchIndex)
						logi.logInfo(fromVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][from]")
						logi.logInfo(toVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][to]")
						
						fromVal = getValueFromHttpRequestValue("edit_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][from]")
						toVal = getValueFromHttpRequestValue("edit_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][to]")
						/*logi.logInfo("Item  value ---- " + rateSchIndex)
						logi.logInfo(fromVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][from]")
						logi.logInfo(toVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][to]")
						
						fromVal = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][from]")
						toVal = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][to]")*/
					} else {
						logi.logInfo("Item  value ---- " + rateSchIndex)
						logi.logInfo(fromVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][from]")
						logi.logInfo(toVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][to]")
						
						fromVal = getValueFromHttpRequestValue("edit_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][from]")
						toVal = getValueFromHttpRequestValue("edit_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][to]")
					}
					/*String fromVal = getValueFromHttpRequestValue("edit_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][from]")
					String toVal = getValueFromHttpRequestValue("edit_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][to]")
					logi.logInfo(fromVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][from]")
					logi.logInfo(toVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][to]")*/
					if(toVal.equals(null)) { toVal = 0 }
					if(i == 1) {
						if(fromVal.equals(null)) {
							fromVal = 1
							suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + item.getKey() + " rate tier " + i, "from " + fromVal + " to " + toVal)
							logi.logInfo(item1.getValue() + " " + item.getKey() + " tier : " + i + " " + fromVal + " to " + toVal);
						} else {
							suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + item.getKey() + " rate tier " + i, "from " + fromVal + " to " + toVal)
							logi.logInfo(item1.getValue() + " " + item.getKey() + " tier : " + i + " " + fromVal + " to " + toVal);
						}
					} else {
						if (!fromVal.equals(null)) {
							suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + item.getKey() + " rate tier " + i, "from " + fromVal + " to " + toVal)
							logi.logInfo(item1.getValue() + " " + item.getKey() + " tier : " + i + " " + fromVal + " to " + toVal);
						}
					}
				}
			}
			rateSchIndex++
		}
//		suppFieldValues = library.Constants.Constant.hashResultCreatePlanRateUnits.sort()
		return suppFieldValues.sort().toString()
	}
	
	String md_VerifyNewRateSeqUnitsActEd2_2(String testcaseid) {
		return md_VerifyNewRateSeqUnitsActEd2(testcaseid, 2)
	}
	
	String md_VerifyNewRateSeqUnitsActEd2(String testcaseid, int n) {
		
		Map<String, String> suppFieldValues = library.Constants.Constant.hashResultCreatePlanRateUnits.sort()
		Map<String, String> rateSchedhash = getRateScheduleNo()
		Map<String, String> sortedSerivceHash = getSortedSerivceOrderIndexEditPlan()
		if(sortedSerivceHash.size() == 0) {
			sortedSerivceHash = getSortedSerivceOrderFromDB()
		}

		Iterator iterator1 = rateSchedhash.entrySet().iterator();
		int rateSchedIndex = 0
		while (iterator1.hasNext())
		{
			Map.Entry item1 = (Map.Entry) iterator1.next();
			Iterator iterator = sortedSerivceHash.entrySet().iterator();
			while (iterator.hasNext())
			{
				Map.Entry item = (Map.Entry) iterator.next();
				for(int i = 1 ; i <= n ; i++ ) {
					String fromVal, toVal
					
					if(rateSchedIndex > 0) {
						fromVal = getValueFromHttpRequestValue("edit_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][from]")
						toVal = getValueFromHttpRequestValue("edit_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][to]")
						logi.logInfo("RateSchedIndex " + rateSchedIndex)
						logi.logInfo("From Val " + fromVal +  " service[" + item.getValue() + "][tier][" + (i - 1) + "][from]")
						logi.logInfo("To Val " + toVal + " service[" + item.getValue() + "][tier][" + (i - 1) + "][to]")
					} else {
						fromVal = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][from]")
						toVal = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][to]")
						logi.logInfo("RateSchedIndex " + rateSchedIndex)
						logi.logInfo("From Val " + fromVal +  " service[" + item.getValue() + "][tier][" + (i - 1) + "][from]")
						logi.logInfo("To Val " + toVal + " service[" + item.getValue() + "][tier][" + (i - 1) + "][to]")
					}
					
					if(toVal.equals(null)) { toVal = 0 }
					if(i == 1) {
						if(fromVal.equals(null)) {
							fromVal = 1
							suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + item.getKey() + " rate tier " + i, "from " + fromVal + " to " + toVal)
							logi.logInfo(item1.getValue() + " " + item.getKey() + " tier : " + i + " " + fromVal + " to " + toVal);
						} else {
							suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + item.getKey() + " rate tier " + i, "from " + fromVal + " to " + toVal)
							logi.logInfo(item1.getValue() + " " + item.getKey() + " tier : " + i + " " + fromVal + " to " + toVal);
						}
					} else {
						if (!fromVal.equals(null)) {
							suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + item.getKey() + " rate tier " + i, "from " + fromVal + " to " + toVal)
							logi.logInfo(item1.getValue() + " " + item.getKey() + " tier : " + i + " " + fromVal + " to " + toVal);
						}
					}
				}
			}
			rateSchedIndex++
		}
//		suppFieldValues = library.Constants.Constant.hashResultCreatePlanRateUnits.sort()
		return suppFieldValues.sort().toString()
	}

	
	def md_VerifyRateTier_from_Edit_Directive_API_New(String testcaseid) {
		logi.logInfo("Entering md_VerifyRateTier_from_Edit_Directive_API")
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		List<String> plan_group=[]
		String apiname=testcaseid.split("-")[2]
		List testCaseCmb = Constant.lhmAllHttpInputRequest_Global.keySet().asList()
		List httpValues = Constant.lhmAllHttpInputRequest_Global.values().asList()
		List<String> propertyList = []
		logi.logInfo("API Name:"+apiname)
		
		for(int j=0; j<testCaseCmb.size(); j++) {
			logi.logInfo("case Cmb value is :: "+ testCaseCmb[j])
			logi.logInfo(testcaseid)
			if(testCaseCmb[j].toString().equals(testcaseid)) {
				com.eviware.soapui.impl.wsdl.teststeps.HttpTestRequest request = httpValues[j]
				logi.logInfo("Request is ::::" + request)
				for(prp in request.getPropertyList()) {
					if(prp.name.contains("[schedule_no]")) {
						propertyList.add(prp.name)
					}
				}
			}
		}
		def directive = getValueFromHttpRequestValue("edit_plan", "edit_directives")
		logi.logInfo("Directive selected is " +directive)
		logi.logInfo("API Name:"+apiname)
		if(directive=="1" || directive==null)
		{
			logi.logInfo("Entering directive 1")
		}
		else if(directive=="2")
		{
			logi.logInfo("Entering directive 2")
		}
		else if(directive=="3")
		{
		
		List<String> service_no=[]
		List<String> name=[]
		List<String> client_service_id=[]
		List<String> gl_cd=[]
		List<String> service_type=[]
		List<String> pricing_rule=[]
		List<String> usage_type=[]
		logi.logInfo("md_VerifyServiceRateTierDetails_APIaaaaaaa=>>>>>>>>")
		logi.logInfo("request Size : " + propertyList.size())
				int j=0
				Map<String, String> suppFieldValues = library.Constants.Constant.hashResultCreatePlanRateUnits
				logi.logInfo("Values Test "+suppFieldValues)
		for(i in 0..(propertyList.size()-1)) {
			logi.logInfo("md_VerifyServiceDetails_APIbbbbbb=>>>>>>>>"+i)
			
			Iterator<Map.Entry<String, String>> iterator1 = suppFieldValues.iterator();
			while (iterator1.hasNext())
			{
				Map.Entry item1 = (Map.Entry) iterator1.next();
				logi.logInfo("Item : " + item1 );
				logi.logInfo("key : " +item1.getKey());
				if(item1.getKey().toString().contains((getValueFromHttpRequestValue("edit_plan", "schedule["+ i+"][schedule_no]")).toString()))
					{
						logi.logInfo("Removeed Item : " +item1.getKey());
						//suppFieldValues.remove(item1.getKey())
						iterator1.remove()
						logi.logInfo("Removeed")
					}
			}
		}


		logi.logInfo("md_VerifyServiceDetails_API Hashmap is : " + suppFieldValues)
		
		return suppFieldValues.sort()
		}
		return hashResult
	}
	
	def md_create_plan_services_In_Edit_API_New(String testcaseID) {
		
		return library.Constants.Constant.hashResultCreatePlanServicesNew.sort()
	}
	
	
	def md_VerifyParentPlans_from_API(String testcaseid) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		List<String> child_plans=[]
		String apiname=testcaseid.split("-")[2]
		logi.logInfo("API Name:"+apiname)
		for(i in 0..3) {
			child_plans.add(getValueFromHttpRequestValue(apiname, "parent_plans[" + i + "]"))
		}
		int i=0
		ResultSet fld_vals
		for(n in child_plans) {
			if(n != null) {
				hashResult.put("parent_plans"+i, n)
			}
			i++
		}
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}
	
	
	def md_VerifyParentPlans_from_DB(String testcaseid) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		List planGroupsId = getPropertyListContains("plan_group", testcaseid)
		for(prp in planGroupsId) {
			logi.logInfo("****************************************************" + prp)
		}
		def client_no = getValueFromHttpRequestValue("create_new_plan", "client_no")
		String planMapQuery = "SELECT * FROM ARIACORE.PLAN_SUPP_PLAN_MAP WHERE CLIENT_NO=" + client_no + " AND SUPP_PLAN_NO = " + getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
		ResultSet result = db.executePlaQuery(planMapQuery)
		int i = 0
		while (result.next()) {
			hashResult.put("parent_plans" + i , result.getString("MASTER_PLAN_NO"))
			i++
		}
		logi.logInfo(" Plan Group Query " + planMapQuery)
		return hashResult.sort()
	}
	
	def md_VerifyParentPlans_from_Edit_Directive_API(String testcaseid) {
		logi.logInfo("Entering md_VerifyParentPlans_from_Edit_Directive_API")
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		List<String> child_plans=[]
		String apiname=testcaseid.split("-")[2]
		def directive = getValueFromHttpRequestValue("edit_plan", "edit_directives")
		logi.logInfo("Directive selected is " +directive)
		logi.logInfo("API Name:"+apiname)
		if(directive=="1" || directive==null)
		{
			logi.logInfo("Entering directive 3")
		}
		else if(directive=="2")
		{
			logi.logInfo("Entering directive 2")
			
		}
		else if(directive=="3")
		{
			logi.logInfo("Entering directive 3")
			for(i in 0..3) {
				logi.logInfo("Parent Plans list " + getValueFromHttpRequestValue("create_new_plan", "parent_plans[" + i + "]"))
				child_plans.add(getValueFromHttpRequestValue("create_new_plan", "parent_plans[" + i + "]"))
			}
			for(i in 0..3) {
				logi.logInfo("Child Plans Removing list " + getValueFromHttpRequestValue("edit_plan", "parent_plans[" + i + "]"))
				child_plans.remove(getValueFromHttpRequestValue("edit_plan", "parent_plans[" + i + "]"))
			}
			int i=0
			for(n in child_plans) {
				if(n != null) {
					hashResult.put("parent_plans"+i, n)
				}
				i++
			}
			logi.logInfo("Hashmap is : " + hashResult)
		}
		return hashResult.sort()
	}
	
	
	def md_VerifyPlansExclusion_from_API(String testcaseid) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		List<String> child_plans=[]
		String apiname=testcaseid.split("-")[2]
		logi.logInfo("API Name:"+apiname)
		for(i in 0..3) {
			child_plans.add(getValueFromHttpRequestValue(apiname, "exclusion_plans[" + i + "]"))
		}
		int i=0
		ResultSet fld_vals
		for(n in child_plans) {
			if(n != null) {
				hashResult.put("exclusion_plans"+i, n)
			}
			i++
		}
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}
	
	
	def md_VerifyPlansExclusion_from_DB(String testcaseid) {
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		
		def client_no = getValueFromHttpRequestValue("create_new_plan", "client_no")
		String planMapQuery = "select * from ariacore.SUPP_PLAN_EXCL_MAP WHERE CLIENT_NO=" + client_no + " AND SUPP_PLAN_NO = " + getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
		logi.logInfo(" Plan Group Query " + planMapQuery)
		ResultSet result = db.executePlaQuery(planMapQuery)
		int i = 0
		while (result.next()) {
			hashResult.put("exclusion_plans" + i , result.getString("GROUP_NO"))
			i++
		}
		logi.logInfo(" Plan Group Query " + planMapQuery)
		return hashResult.sort()
	}
	
	def md_VerifyPlansExclusion_from_Edit_Directive_API(String testcaseid) {
		logi.logInfo("Entering md_VerifyPlansExclusion_from_Edit_Directive_API")
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		List<String> child_plans=[]
		String apiname=testcaseid.split("-")[2]
		def directive = getValueFromHttpRequestValue("edit_plan", "edit_directives")
		logi.logInfo("Directive selected is " +directive)
		logi.logInfo("API Name:"+apiname)
		if(directive=="1" || directive==null)
		{
			logi.logInfo("Entering directive 3")
		}
		else if(directive=="2")
		{
			logi.logInfo("Entering directive 2")
			
		}
		else if(directive=="3")
		{
			logi.logInfo("Entering directive 3")
			for(i in 0..3) {
				logi.logInfo("Parent Plans list " + getValueFromHttpRequestValue("create_new_plan", "exclusion_plans[" + i + "]"))
				child_plans.add(getValueFromHttpRequestValue("create_new_plan", "exclusion_plans[" + i + "]"))
			}
			for(i in 0..3) {
				logi.logInfo("Child Plans Removing list " + getValueFromHttpRequestValue("edit_plan", "exclusion_plans[" + i + "]"))
				child_plans.remove(getValueFromHttpRequestValue("edit_plan", "exclusion_plans[" + i + "]"))
			}
			int i=0
			for(n in child_plans) {
				if(n != null) {
					hashResult.put("exclusion_plans"+i, n)
				}
				i++
			}
			logi.logInfo("Hashmap is : " + hashResult)
		}
		return hashResult.sort()
	}
	
		String md_VerifyRateSchedAmountActEditDir2_1(String testcaseid) {
			return md_VerifyRateSchedAmountActEditDir2(testcaseid, 1)
		}
		
		String md_VerifyRateSchedAmountActEditDir2_2(String testcaseid) {
			return md_VerifyRateSchedAmountActEditDir2(testcaseid, 2)
		}
		
		String md_VerifyRateSchedAmountActEditDir2_3(String testcaseid) {
			return md_VerifyRateSchedAmountActEditDir2(testcaseid, 3)
		}
		
		String md_VerifyRateSchedAmountActEditDir2(String testcaseid, int n) {
			logi.logInfo("md_VerifyRateSchedAmountAct "+n)
			Map<String, String> suppFieldValues = new TreeMap<String, String>()
			Map<String, String> sortedSerivceHash = getSortedSerivceOrderIndex()
			Map<String, String> sortedSerivceHashEdit = getSortedSerivceOrderIndexEditPlan()
			Map<String, String> editScheduleList = getRateScheduleNo()
			
			int rateSchedCount = getRateScheduleNo().size()
			int serviceCount = sortedSerivceHash.size()
			if(sortedSerivceHash.size() == 0) {
				sortedSerivceHash = getSortedSerivceOrderFromDB()
			}
	
			logi.logInfo("Service Index " + sortedSerivceHash.toString())
			logi.logInfo("Service Index " + sortedSerivceHashEdit.toString())
			int k = 1
			for(int i = 0 ; i < rateSchedCount ; i++) {
	
				Iterator iterator = sortedSerivceHash.entrySet().iterator();
				while (iterator.hasNext())
				{
					Map.Entry item = (Map.Entry) iterator.next();
					logi.logInfo(item.getKey() + " - " + item.getValue())
					logi.logInfo("editScheduleList "+editScheduleList.get("test"+(i+1)))
					logi.logInfo("Edit request schedule "+getValueFromHttpRequestValue("edit_plan", "schedule[0][schedule_no]"))
					if((sortedSerivceHashEdit.containsKey(item.getKey()))&&(editScheduleList.get("test"+(i+1))==getValueFromHttpRequestValue("edit_plan", "schedule[0][schedule_no]"))&&(item.getKey()==getValueFromHttpRequestValue("edit_plan", "service[0][service_no]"))) {
						logi.logInfo("Service contains in the edit request also " + sortedSerivceHashEdit.get(item.getKey()))
						String editPlanServiceIndex = sortedSerivceHashEdit.get(item.getKey())
						for(int l = 0 ; l < n ; l++) {
							String key = "RateSchedAmount " + k
							String value = getValueFromHttpRequestValue("edit_plan", "service[" + editPlanServiceIndex + "][tier][" + l + "][schedule][0][amount]")
							logi.logInfo("Rate Sched " + key + " Value " + value)
							if(!value.equals(null)) {
								suppFieldValues.putAt(key, value)
								k++
							}
							/*else{
								value = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + l + "][schedule][" + i + "][amount]")
								logi.logInfo("Rate Sched " + key + " Value " + value)
								if(!value.equals(null)) {
									suppFieldValues.putAt(key, value)
									k++
								}
							
							}*/
						}
					} else {
						for(int l = 0 ; l < n ; l++) {
							String key = "RateSchedAmount " + k
							String value = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + l + "][schedule][" + i + "][amount]")
							logi.logInfo("Rate Sched " + key + " Value " + value)
							if(!value.equals(null)) {
								suppFieldValues.putAt(key, value)
								k++
							}
						}
					}
					
				}
			}
			return suppFieldValues.toString()
		}
		
		String md_VerifyNewRateSchedAmountActEditDir2_2(String testcaseid) {
			return md_VerifyNewRateSchedAmountActEditDir2(testcaseid, 2)
		}
		
		String md_VerifyNewRateSchedAmountActEditDir2(String testcaseid, int n) {
			logi.logInfo("md_VerifyRateSchedAmountAct "+n)
			Map<String, String> suppFieldValues = new TreeMap<String, String>()
			Map<String, String> sortedSerivceHash = getSortedSerivceOrderIndex()
			Map<String, String> sortedSerivceHashEdit = getSortedSerivceOrderIndexEditPlan()
			
			int rateSchedCount = getRateScheduleNo().size()
			int serviceCount = sortedSerivceHash.size()
			if(sortedSerivceHash.size() == 0) {
				sortedSerivceHash = getSortedSerivceOrderFromDB()
			}
	
			logi.logInfo("Service Index " + sortedSerivceHash.toString())
			logi.logInfo("Service Index " + sortedSerivceHashEdit.toString())
			int k = 1
			for(int i = 0 ; i < rateSchedCount ; i++) {
				int schedIndex = 0
				Iterator iterator = sortedSerivceHash.entrySet().iterator();
				while (iterator.hasNext())
				{
					Map.Entry item = (Map.Entry) iterator.next();
					logi.logInfo(item.getKey() + " - " + item.getValue())
					if(i > 0) {
						logi.logInfo("Service contains in the edit request also " + sortedSerivceHashEdit.get(item.getKey()))
						String editPlanServiceIndex = sortedSerivceHashEdit.get(item.getKey())
						for(int l = 0 ; l < n ; l++) {
							String key = "RateSchedAmount " + k
							String value = getValueFromHttpRequestValue("edit_plan", "service[" + item.getValue() + "][tier][" + l + "][schedule][" + schedIndex + "][amount]")
							logi.logInfo("Rate Sched " + key + " Value " + value)
							if(!value.equals(null)) {
								suppFieldValues.putAt(key, value)
								k++
							}
						}
					} else {
						for(int l = 0 ; l < n ; l++) {
							String key = "RateSchedAmount " + k
							String value = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + l + "][schedule][" + schedIndex + "][amount]")
							logi.logInfo("Rate Sched " + key + " Value " + value)
							if(!value.equals(null)) {
								suppFieldValues.putAt(key, value)
								k++
							}
						}
					}
					
				}
			}
			return suppFieldValues.toString()
		}
	
		
		Map getSortedSerivceOrderIndexEditPlan() {
			
			Map<String, String> schedResultSet = new HashMap<String, String>()
			for(int i=0 ; i < 9 ; i++) {
				if(!getValueFromHttpRequestValue("edit_plan", "service[" + i + "][service_no]").equals(null)) {
					schedResultSet.putAt(getValueFromHttpRequestValue("edit_plan", "service[" + i + "][service_no]"), i)
				}
			}
			return schedResultSet.sort()
		}
		
		String md_VerifyRateSchedAmountExpED2(String testcaseid) {
			
					Map<String, String> suppFieldValues = new TreeMap<String, String>()
			
					int rateSchedCount = getRateScheduleNo().size()
					def plan_no = getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
					String rateSchedQuery = "SELECT * FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SCHEDULE_NO IN (SELECT SCHEDULE_NO FROM ARIACORE.PLAN_RATE_SCHED_MAP WHERE PLAN_NO = " + plan_no + ") ORDER BY SCHEDULE_NO, SERVICE_NO, FROM_UNIT"
					ConnectDB database = new ConnectDB()
					ResultSet result = database.executePlaQuery(rateSchedQuery)
					int i=1;
					while (result.next()) {
						logi.logInfo("Schedule Amount " + i + " " +  result.getString("RATE_PER_UNIT"))
						suppFieldValues.putAt("RateSchedAmount " + i, result.getString("RATE_PER_UNIT"))
						i++
					}
					return suppFieldValues.toString()
				}
		
		String md_VerifySuppFieldValuesActEditED2(String testcaseid) {
			
			Map<String, List<String>> suppFieldValues = library.Constants.Constant.hashResultSuppObjFields
	
			int i = 0
			while(!getValueFromHttpRequestValue("edit_plan", "supplemental_obj_field[" + i + "][field_no]").equals(null)) {
				logi.logInfo(" Supp Field  - " + getValueFromHttpRequestValue("edit_plan", "supplemental_obj_field[" + i + "][field_no]"))
				int j =0
				List<String> valueList = new ArrayList<>();
				while(!getValueFromHttpRequestValue("edit_plan", "supplemental_obj_field[" + i + "][field_value][" + j + "]").equals(null)) {
					logi.logInfo(" Supp Values - " + getValueFromHttpRequestValue("edit_plan", "supplemental_obj_field[" + i + "][field_value][" + j + "]"))
	
					valueList.add(getValueFromHttpRequestValue("edit_plan", "supplemental_obj_field[" + i + "][field_value][" + j + "]"));
					j++
				}
				suppFieldValues.put(getValueFromHttpRequestValue("edit_plan", "supplemental_obj_field[" + i + "][field_no]"), valueList )
				i++
			}
			suppFieldValues.each
			{k,v ->
				String values = v
				if (values.contains(",")) {
					values = values.replaceAll(", ", "&")
					suppFieldValues.put(k,values)
				}
			}
			logi.logInfo("After replace : " + suppFieldValues)
			return suppFieldValues.sort().toString()
		}
		
		def md_VerifyParentPlans_from_Edit_Directive2(String testcaseid) {
			logi.logInfo("Entering md_VerifyChildPlans_from_Edit_Directive_API")
			LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
			List<String> child_plans=[]
			String apiname=testcaseid.split("-")[2]
			def directive = getValueFromHttpRequestValue("edit_plan", "edit_directives")
			logi.logInfo("Directive selected is " +directive)
			logi.logInfo("API Name:"+apiname)
			logi.logInfo("Entering directive 2")
			int createIndex=0,editIndex=0;
			while(!getValueFromHttpRequestValue("create_new_plan", "parent_plans[" + createIndex + "]").equals(null))
			{
				child_plans.add(getValueFromHttpRequestValue("create_new_plan", "parent_plans[" + createIndex + "]"))
				createIndex++
			}
			while(!getValueFromHttpRequestValue("edit_plan", "parent_plans[" + editIndex + "]").equals(null))
			{
				child_plans.add(getValueFromHttpRequestValue("edit_plan", "parent_plans[" + editIndex + "]"))
				editIndex++
			}
			int i=0
			ResultSet fld_vals
			for(n in child_plans) {
				if(n != null) {
					hashResult.put("parent_plans"+i, n)
				}
				i++
			}
			logi.logInfo("Hashmap is : " + hashResult)
			return hashResult.sort()
		}
		
		def md_VerifyPlanExclusion_API(String testcaseid) {
			LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
			List<String> plan_exclusion=[]
			String apiname=testcaseid.split("-")[2]
			logi.logInfo("API Name:"+apiname)
			for(i in 0..1) {
				plan_exclusion.add(getValueFromHttpRequestValue(apiname, "exclusion_plans[" + i + "]"))
			}
			int i=0
			ResultSet fld_vals
			for(n in plan_exclusion) {
				if(n != null) {
					hashResult.put("exclusion_plans"+i, n)
				}
				i++
			}
			logi.logInfo("exclusion_plans API Hashmap is : " + hashResult)
			return hashResult.sort()
	
		}
		
		def md_VerifyPlanExclusion_from_Edit_Directive2(String testcaseid) {
			logi.logInfo("Entering md_VerifyChildPlans_from_Edit_Directive_API")
			LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
			List<String> child_plans=[]
			String apiname=testcaseid.split("-")[2]
			def directive = getValueFromHttpRequestValue("edit_plan", "edit_directives")
			logi.logInfo("Directive selected is " +directive)
			logi.logInfo("API Name:"+apiname)
			logi.logInfo("Entering directive 2")
			int createIndex=0,editIndex=0;
			while(!getValueFromHttpRequestValue("create_new_plan", "exclusion_plans[" + createIndex + "]").equals(null))
			{
				child_plans.add(getValueFromHttpRequestValue("create_new_plan", "exclusion_plans[" + createIndex + "]"))
				createIndex++
			}
			while(!getValueFromHttpRequestValue("edit_plan", "exclusion_plans[" + editIndex + "]").equals(null))
			{
				child_plans.add(getValueFromHttpRequestValue("edit_plan", "exclusion_plans[" + editIndex + "]"))
				editIndex++
			}
			int i=0
			ResultSet fld_vals
			for(n in child_plans) {
				if(n != null) {
					hashResult.put("exclusion_plans"+i, n)
				}
				i++
			}
			logi.logInfo("Hashmap is : " + hashResult)
			return hashResult.sort()
		}
		
		def md_VerifyPlanExclusion_from_DB(String testcaseid) {
			LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
			def client_no = getValueFromHttpRequestValue("create_new_plan", "client_no")
			String planExclQuery = "SELECT * FROM ARIACORE.SUPP_PLAN_EXCL_MAP WHERE CLIENT_NO=" + client_no + " AND SUPP_PLAN_NO = " + getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
			ResultSet result = db.executePlaQuery(planExclQuery)
			int i = 0
			while (result.next()) {
				hashResult.put("exclusion_plans" + i , result.getString("GROUP_NO"))
				i++
			}
			return hashResult.sort()
		}
		
		String md_VerifyNewRateSeqUnitsActEd2Dev4647_3(String testcaseid) {
			md_VerifyNewRateSeqUnitsActEd2Dev4647(testcaseid, 3)
		}
		
		String md_VerifyNewRateSeqUnitsActEd2Dev4647_2(String testcaseid) {
			md_VerifyNewRateSeqUnitsActEd2Dev4647(testcaseid, 2)
		}
		
		String md_VerifyNewRateSeqUnitsActEd2Dev4647_1(String testcaseid) {
			md_VerifyNewRateSeqUnitsActEd2Dev4647(testcaseid, 1)
		}
		
		String md_VerifyNewRateSeqUnitsActEd2Dev4647(String testcaseid, int n) {
			
			Map<String, String> suppFieldValues = library.Constants.Constant.hashResultCreatePlanRateUnits.sort()
			Map<String, String> rateSchedhash = getRateScheduleNo()
			Map<String, String> sortedSerivceHash = getSortedSerivceOrderIndex()
			Map<String, String> sortedSerivceHashEditPlan = getSortedSerivceOrderIndexEditPlan()
			if(sortedSerivceHashEditPlan.size() == 0) {
				sortedSerivceHashEditPlan = getSortedSerivceOrderFromDB()
			}
			suppFieldValues = new TreeMap<String, String>()
			Iterator iterator1 = rateSchedhash.entrySet().iterator();
			int rateSchedIndex = 0
			while (iterator1.hasNext())
			{
				Map.Entry item1 = (Map.Entry) iterator1.next();
				Iterator iterator = sortedSerivceHash.entrySet().iterator();
				while (iterator.hasNext())
				{
					int count  = 0
					Map.Entry item = (Map.Entry) iterator.next();
					for(int i = 1 ; i <= n ; i++ ) {
						String fromVal, toVal
						
						if(rateSchedIndex == 0) {
							logi.logInfo("EDit Uk :" +item.getKey());
							logi.logInfo("Edit_uk : "+getValueFromHttpRequestValue("edit_plan", "service[0][service_no]"))
							logi.logInfo("Edit sched: "+rateSchedhash.get("test"+(rateSchedIndex+1)))
							logi.logInfo("Edit srhjehj : "+getValueFromHttpRequestValue("edit_plan", "schedule[0][schedule_no]"))
							if((rateSchedhash.get("test"+(rateSchedIndex+1))==getValueFromHttpRequestValue("edit_plan", "schedule[0][schedule_no]"))&&(item.getKey()==getValueFromHttpRequestValue("edit_plan", "service[0][service_no]"))){
								logi.logInfo("Item  value ---- First schedule edit_plan" + rateSchedIndex)
								logi.logInfo(fromVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][from]")
								logi.logInfo(toVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][to]")
								
								fromVal = getValueFromHttpRequestValue("edit_plan", "service[" + count + "][tier][" + (i - 1) + "][from]")
								toVal = getValueFromHttpRequestValue("edit_plan", "service[" + count + "][tier][" + (i - 1) + "][to]")
								
							}else{
							logi.logInfo("Item  value --- first schedule create new plan plan- " + rateSchedIndex)
							logi.logInfo(fromVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][from]")
							logi.logInfo(toVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][to]")
							
							fromVal = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][from]")
							toVal = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][to]")
							}
						} else {
						logi.logInfo("Test serive_Uk :" +item.getKey());
						logi.logInfo("test requezt_uk : "+getValueFromHttpRequestValue("edit_plan", "service[0][service_no]"))
						if((rateSchedhash.get("test"+(rateSchedIndex+1))==getValueFromHttpRequestValue("edit_plan", "schedule[0][schedule_no]"))&&(item.getKey()==getValueFromHttpRequestValue("edit_plan", "service[0][service_no]"))){
							logi.logInfo("Item  value ---- edited rate schedule " + rateSchedIndex)
							logi.logInfo(fromVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][0][from]")
							logi.logInfo(toVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][0][to]")
							
							fromVal = getValueFromHttpRequestValue("edit_plan", "service[" + count + "][tier][" + (i - 1) + "][schedule][0][from]")
							toVal = getValueFromHttpRequestValue("edit_plan", "service[" + count + "][tier][" + (i - 1) + "][schedule][0][to]")
						
						}else{
							logi.logInfo("Item  value enter to create new plan ---- " + rateSchedIndex)
							logi.logInfo(fromVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchedIndex + "][from]")
							logi.logInfo(toVal + "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchedIndex + "][to]")
							
							fromVal = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchedIndex + "][from]")
							toVal = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + (i - 1) + "][schedule][" + rateSchedIndex + "][to]")
						}
						}
						
						if(toVal.equals(null)) { toVal = 0 }
						if(i == 1) {
							if(fromVal.equals(null)) {
								fromVal = 1
								suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + item.getKey() + " rate tier " + i, "from " + fromVal + " to " + toVal)
								logi.logInfo(item1.getValue() + " " + item.getKey() + " tier : " + i + " " + fromVal + " to " + toVal);
							} else {
								suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + item.getKey() + " rate tier " + i, "from " + fromVal + " to " + toVal)
								logi.logInfo(item1.getValue() + " " + item.getKey() + " tier : " + i + " " + fromVal + " to " + toVal);
							}
						} else {
							if (!fromVal.equals(null)) {
								suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + item.getKey() + " rate tier " + i, "from " + fromVal + " to " + toVal)
								logi.logInfo(item1.getValue() + " " + item.getKey() + " tier : " + i + " " + fromVal + " to " + toVal);
							}
						}
						
					}
					count ++
				}
				rateSchedIndex++
			}
	//		suppFieldValues = library.Constants.Constant.hashResultCreatePlanRateUnits.sort()
			return suppFieldValues.sort().toString()
	}

	
		String md_VerifyRateSchedAmountActAdd_Sched(String testcaseid) {
			logi.logInfo("md_VerifyRateSchedAmountAct ")
			Map<String, String> suppFieldValues = new TreeMap<String, String>()
			Map<String, String> sortedSerivceHash = getSortedSerivceOrderIndex()
			int rateSchedCount = getRateScheduleNo().size()
			int serviceCount = sortedSerivceHash.size()
			if(sortedSerivceHash.size() == 0) {
				sortedSerivceHash = getSortedSerivceOrderFromDB()
			}
			String apiname=testcaseid.split("-")[2]
	
			logi.logInfo("Service Index " + sortedSerivceHash.toString())
			int k = 1
			for(int i = 0 ; i < rateSchedCount ; i++) {
	
				Iterator iterator = sortedSerivceHash.entrySet().iterator();
				while (iterator.hasNext())
				{
					Map.Entry item = (Map.Entry) iterator.next();
					logi.logInfo(item.getKey() + " - " + item.getValue())
					for(int l = 0 ; l < 3 ; l++) {
						String key = "RateSched " + (i+1) +" Service No " + item.getKey() + " Rate tier "+(l+1)
						String value = null
						if(apiname == "edit_plan" && i == 2){
							value = getValueFromHttpRequestValue("edit_plan", "service[" + item.getValue() + "][tier][" + l + "][schedule][" + i + "][amount]")
						}else{
						value = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + l + "][schedule][" + i + "][amount]")
						}
						logi.logInfo("Rate Sched " + key + " Value " + value)
						if(!value.equals(null)) {
							suppFieldValues.putAt(key, value)
							k++
						}
					}
				}
			}
			return suppFieldValues.toString()
		}
		
		
				
		
		
		String md_VerifyRateSchedAmountActEditDir2_AddService(String testCaseId) {
			logi.logInfo("md_VerifyRateSchedAmountAct_add service ")
			
			List testCaseCmb = Constant.lhmAllHttpInputRequest_Global.keySet().asList()
			List httpValues = Constant.lhmAllHttpInputRequest_Global.values().asList()
			List<String> propertyList = []
			String apiname=testCaseId.split("-")[2]
			logi.logInfo("API Name:"+apiname)
	
			for(int j=0; j<testCaseCmb.size(); j++) {
				logi.logInfo("case Cmb value is :: "+ testCaseCmb[j])
				logi.logInfo(testCaseId)
				if(testCaseCmb[j].toString().equals(testCaseId)) {
					com.eviware.soapui.impl.wsdl.teststeps.HttpTestRequest request = httpValues[j]
					logi.logInfo("Request is ::::" + request)
					for(prp in request.getPropertyList()) {
						if(prp.name.contains("[service_no]")) {
							propertyList.add(prp.name)
						}
					}
				}
			}
			Map<String, String> suppFieldValues = new TreeMap<String, String>()
			Map<String, String> sortedSerivceHash = getSortedSerivceOrderIndex()
			int rateSchedCount = getRateScheduleNo().size()
			int serviceCount = sortedSerivceHash.size()
			if(sortedSerivceHash.size() == 0) {
				sortedSerivceHash = getSortedSerivceOrderFromDB()
			}
			suppFieldValues = library.Constants.Constant.hashResultCreatePlanRateUnitsNew
			logi.logInfo("Service Index " + sortedSerivceHash.toString())
		int k = 1
		for(int i = 0 ; i < rateSchedCount ; i++) {

			logi.logInfo("request Size : " + propertyList.size())
			for(j in 0..(propertyList.size()-1)) {
				if(getValueFromHttpRequestValue("edit_plan", "service["+ j +"][service_no]") != null){
					for(int l = 0 ; l < 3 ; l++) {
						String key = "RateSched " + (i+1) +" Service No " + getValueFromHttpRequestValue("edit_plan", "service["+ j +"][service_no]") + " Rate tier "+(l+1)
						String value = getValueFromHttpRequestValue("edit_plan", "service[" + j + "][tier][" + l + "][schedule][" + i + "][amount]")
						logi.logInfo("Rate Sched " + key + " Value " + value)
						if(!value.equals(null)) {
							suppFieldValues.putAt(key, value)
							k++
						}
					}
				}
			}
		}
			return suppFieldValues.sort().toString()
		}
		
		
		String md_VerifyRateSeqUnitsActNewAddService(String testCaseId) {
			
			Map<String, String> suppFieldValues = new TreeMap<String, String>()
			Map<String, String> rateSchedhash = getRateScheduleNo()
			Map<String, String> sortedSerivceHash = getSortedSerivceOrderIndex()
			
			List testCaseCmb = Constant.lhmAllHttpInputRequest_Global.keySet().asList()
			List httpValues = Constant.lhmAllHttpInputRequest_Global.values().asList()
			List<String> propertyList = []
			String apiname=testCaseId.split("-")[2]
			suppFieldValues = library.Constants.Constant.hashResultCreatePlanRateUnits
			logi.logInfo("API Name:"+apiname)
	
			for(int j=0; j<testCaseCmb.size(); j++) {
				logi.logInfo("case Cmb value is :: "+ testCaseCmb[j])
				logi.logInfo(testCaseId)
				if(testCaseCmb[j].toString().equals(testCaseId)) {
					com.eviware.soapui.impl.wsdl.teststeps.HttpTestRequest request = httpValues[j]
					logi.logInfo("Request is ::::" + request)
					for(prp in request.getPropertyList()) {
						if(prp.name.contains("[service_no]")) {
							propertyList.add(prp.name)
						}
					}
				}
			}
			if(sortedSerivceHash.size() == 0) {
				sortedSerivceHash = getSortedSerivceOrderFromDB()
			}
		apiname=testCaseId.split("-")[2]
			Iterator iterator1 = rateSchedhash.entrySet().iterator();
			int rateSchIndex = 0
			while (iterator1.hasNext())
			{
				Map.Entry item1 = (Map.Entry) iterator1.next();
				logi.logInfo("request Size : " + propertyList.size())
			for(j in 0..(propertyList.size()-1)) {
				if(getValueFromHttpRequestValue("edit_plan", "service["+ j +"][service_no]") != null){
					for(int i = 1 ; i <= 3 ; i++ ) {
						String fromVal, toVal
						if(rateSchIndex == 0) {
							logi.logInfo("Item  value ---- " + rateSchIndex)
							logi.logInfo(fromVal + "service[" + j + "][tier][" + (i - 1) + "][from]")
							logi.logInfo(toVal + "service[" + j + "][tier][" + (i - 1) + "][to]")
							
							fromVal = getValueFromHttpRequestValue("edit_plan", "service[" + j + "][tier][" + (i - 1) + "][from]")
							toVal = getValueFromHttpRequestValue("edit_plan", "service[" + j + "][tier][" + (i - 1) + "][to]")
							
						} else {
							logi.logInfo("Item  value ---- " + rateSchIndex)
							logi.logInfo(fromVal + "service[" + j + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][from]")
							logi.logInfo(toVal + "service[" + j + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][to]")
							
							fromVal = getValueFromHttpRequestValue("edit_plan", "service[" + j + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][from]")
							toVal = getValueFromHttpRequestValue("edit_plan", "service[" + j + "][tier][" + (i - 1) + "][schedule][" + rateSchIndex + "][to]")
						}
						
	
						if(toVal.equals(null)) { toVal = 0 }
						if(i == 1) {
							if(fromVal.equals(null)) {
								fromVal = 1
								suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + getValueFromHttpRequestValue("edit_plan", "service["+ j +"][service_no]") + " rate tier " + i, "from " + fromVal + " to " + toVal)
								logi.logInfo(item1.getValue() + " " + getValueFromHttpRequestValue("edit_plan", "service["+ j +"][service_no]") + " tier : " + i + " " + fromVal + " to " + toVal);
							} else {
								suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + getValueFromHttpRequestValue("edit_plan", "service["+ j +"][service_no]") + " rate tier " + i, "from " + fromVal + " to " + toVal)
								logi.logInfo(item1.getValue() + " " + getValueFromHttpRequestValue("edit_plan", "service["+ j +"][service_no]") + " tier : " + i + " " + fromVal + " to " + toVal);
							}
						} else {
							if (!fromVal.equals(null)) {
								suppFieldValues.putAt("Schedule No " + item1.getValue() + " Service No " + getValueFromHttpRequestValue("edit_plan", "service["+ j +"][service_no]") + " rate tier " + i, "from " + fromVal + " to " + toVal)
								logi.logInfo(item1.getValue() + " " + getValueFromHttpRequestValue("edit_plan", "service["+ j +"][service_no]") + " tier : " + i + " " + fromVal + " to " + toVal);
							}
						}
					}
				}
				}
				rateSchIndex++
			}
			
			return suppFieldValues.toString()
		}
		String md_editplanerrormsg(String testCaseId)
		{
			String error_msg
			String sch_no= getValueFromHttpRequestValue("edit_plan","schedule[0][schedule_no]")
			logi.logInfo "Schedule_no" + sch_no
			error_msg = "Rate schedule - "+ sch_no + " can not be removed from the plan. Either it is assigned to an account or  to a plan as rollover rate schedule. Rate schedule is currently being used on an assigned plan or rate schedule was previously used on the assigned plan or is assigned to a plan as a rollover rate schedule"
			return error_msg
		
		
		}
		
		def md_get_rate_schedule_DB(String testCaseId)
		{
			return getRateScheduleNo()
		}
		def md_get_rate_schedule_API1(String testCaseId)
		{
			return getRateScheduleNo()
		}
		def md_get_rate_schedule_ID_DB(String testCaseId)
		{
			return getRateScheduleNo1()
		}
		def md_get_rate_schedule_ID_API1(String testCaseId)
		{
			return getRateScheduleNo1()
		}
		def md_get_removed_rate_schedule_ID_DB(String testCaseId)
		{
			return getRemovedRateSchedule()
		}
		def md_get_removed_rate_schedule_ID_API1(String testCaseId)
		{
			return getRemovedRateSchedule()
		}
		
		String md_get_rateschedule_count(String testCaseId)
		{
					ConnectDB dbConnection=new ConnectDB()
					VerificationMethods vm =new VerificationMethods()
					logi.logInfo "md_get_rateschedule_count"
					String client_no=Constant.mycontext.expand('${Properties#Client_No}')
					String plan_no = vm.getValueFromResponse("create_new_plan",ExPathRpcEnc.CREATE_NEW_PLAN_PLANNO1)
					String count1 = db.executeQueryP2("Select count(schedule_no) FROM ARIACORE.PLAN_RATE_SCHED_MAP where plan_no = "+plan_no+" and client_no ="+client_no )
					dbConnection.closeConnection()
					return count1
		}
		String md_editplanerrormsg1(String testCaseId)
		{
					logi.logInfo "md_get_rateschedule_count"
					ConnectDB dbConnection=new ConnectDB()
					VerificationMethods vm =new VerificationMethods()
					String sch_no= getValueFromHttpRequestValue("edit_plan","schedule[0][schedule_no]")
					String client_no=Constant.mycontext.expand('${Properties#Client_No}')
					String plan_no = vm.getValueFromResponse("create_new_plan",ExPathRpcEnc.CREATE_NEW_PLAN_PLANNO1)
					logi.logInfo "Plan No" + plan_no
					String count1 = db.executeQueryP2("Select currency_cd from ariacore.client_plan where plan_no = "+plan_no )
					logi.logInfo "Plan No" + plan_no
					String ErrorMsg = "Default Rate Schedule of Currencies - \""+count1+"\" cannot be deleted."
					dbConnection.closeConnection()
					return ErrorMsg
		}
		String md_VerifyRateSchedDescriptionExp(String testcaseid)
		{
		
				Map<String, String> suppFieldValues = new TreeMap<String, String>()
		
				int rateSchedCount = getRateScheduleNo().size()
				def plan_no = getValueFromResponse("create_new_plan", ExPathAdminToolsApi.CREATE_PLAN_PLAN_NO1)
				String rateSchedQuery = "SELECT * FROM ARIACORE.NEW_RATE_SCHED_RATES WHERE SCHEDULE_NO IN (SELECT SCHEDULE_NO FROM ARIACORE.PLAN_RATE_SCHED_MAP WHERE PLAN_NO = " + plan_no + ") ORDER BY SCHEDULE_NO, SERVICE_NO"
				ConnectDB database = new ConnectDB()
				ResultSet result = database.executePlaQuery(rateSchedQuery)
				int i=0;
				String oldSchedNo=""
				while (result.next()) {
					if(oldSchedNo!=result.getString("SCHEDULE_NO")){
						oldSchedNo = result.getString("SCHEDULE_NO")
						i++
					}
					logi.logInfo("Schedule Amount " + i + " " +  result.getString("DESCRIPTION"))
					suppFieldValues.putAt("RateSched " + i +" Service No " + result.getString("SERVICE_NO") + " Rate tier "+result.getInt("RATE_SEQ_NO"), result.getString("DESCRIPTION"))
					
				}
				return suppFieldValues.toString()
		}
		String md_VerifyRateSchedDescriptionAct(String testcaseid, int n) {
			logi.logInfo("md_VerifyRateSchedAmountAct "+n)
			Map<String, String> suppFieldValues = new TreeMap<String, String>()
			Map<String, String> sortedSerivceHash = getSortedSerivceOrderIndex()
			int rateSchedCount = getRateScheduleNo().size()
			int serviceCount = sortedSerivceHash.size()
			if(sortedSerivceHash.size() == 0) {
				sortedSerivceHash = getSortedSerivceOrderFromDB()
			}
	
			logi.logInfo("Service Index " + sortedSerivceHash.toString())
			int k = 1
			for(int i = 0 ; i < rateSchedCount ; i++) {
	
				Iterator iterator = sortedSerivceHash.entrySet().iterator();
				while (iterator.hasNext())
				{
					Map.Entry item = (Map.Entry) iterator.next();
					logi.logInfo(item.getKey() + " - " + item.getValue())
					for(int l = 0 ; l < n ; l++) {
						String key = "RateSched " + (i+1) +" Service No " + item.getKey() + " Rate tier "+(l+1)
						String value = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + l + "][schedule][" + i + "][amount]")
						String desc = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + l + "][schedule][" + i + "][description]")
						logi.logInfo("Rate Sched " + key + " Value " + value)
						if(!value.equals(null))
						{
							
								logi.logInfo("Rate Sched " + key + " Value " + value)
								suppFieldValues.putAt(key, desc)
								k++
							
						}
						
					}
				}
			}
			library.Constants.Constant.hashResultCreatePlanRateUnitsNew = suppFieldValues
			return suppFieldValues.toString()
		}
		String md_VerifyRateSchedDescriptionAct_2(String testcaseid)
		{
			logi.logInfo("Entering amount act 2")
			return md_VerifyRateSchedDescriptionAct(testcaseid,2)
		}
		
		
		String md_VerifyRateSchedDescriptionAct_3(String testcaseid)
		{
			logi.logInfo("Entering amount act 3")
			return md_VerifyRateSchedDescriptionAct(testcaseid,3)
		}
		
		
		String md_VerifyRateSchedDescriptionAct_4(String testcaseid)
		{
			logi.logInfo("Entering amount act 4")
			return md_VerifyRateSchedDescriptionAct(testcaseid,4)
		}
		String md_VerifyRateSchedDescriptionEditAct(String testcaseid,int n) {
			logi.logInfo("md_VerifyRateSchedDescriptionEditAct "+n)
			Map<String, String> suppFieldValues = new TreeMap<String, String>()
			Map<String, String> sortedSerivceHash = getSortedSerivceOrderIndex()
			int rateSchedCount = getRateScheduleNo().size()
			int serviceCount = sortedSerivceHash.size()
			if(sortedSerivceHash.size() == 0) {
				sortedSerivceHash = getSortedSerivceOrderFromDB()
			}
	
			logi.logInfo("Service Index " + sortedSerivceHash.toString())
			int k = 1
			for(int i = 0 ; i < rateSchedCount ; i++) {
	
				Iterator iterator = sortedSerivceHash.entrySet().iterator();
				while (iterator.hasNext())
				{
					Map.Entry item = (Map.Entry) iterator.next();
					logi.logInfo(item.getKey() + " - " + item.getValue())
					for(int l = 0 ; l < n ; l++) {
						String key = "RateSched " + (i+1) +" Service No " + item.getKey() + " Rate tier "+(l+1)
						String value = getValueFromHttpRequestValue("edit_plan", "service[" + item.getValue() + "][tier][" + l + "][schedule][" + i + "][amount]")
						String desc = getValueFromHttpRequestValue("edit_plan", "service[" + item.getValue() + "][tier][" + l + "][schedule][" + i + "][description]")
						logi.logInfo("Rate Sched " + key + " Value " + value)
						if(!value.equals(null))
						{
							
								logi.logInfo("Rate Sched " + key + " Value " + value)
								suppFieldValues.putAt(key, desc)
								k++
							
						}
						else
						{
							String value1 = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + l + "][schedule][" + i + "][amount]")
							if(!value1.equals(null))
							{
								String desc1 = getValueFromHttpRequestValue("create_new_plan", "service[" + item.getValue() + "][tier][" + l + "][schedule][" + i + "][description]")
								suppFieldValues.putAt(key, desc1)
							}
						}
						
					}
				}
			}
			library.Constants.Constant.hashResultCreatePlanRateUnitsNew = suppFieldValues
			return suppFieldValues.toString()
		}
		String md_VerifyRateSchedDescriptionActedit_2(String testcaseid)
		{
			logi.logInfo("Entering amount act 2")
			return md_VerifyRateSchedDescriptionEditAct(testcaseid,2)
		}
		
		
		String md_VerifyRateSchedDescriptionActedit_3(String testcaseid)
		{
			logi.logInfo("Entering amount act 3")
			return md_VerifyRateSchedDescriptionEditAct(testcaseid,3)
		}
		
		
		String md_VerifyRateSchedDescriptionActedit_4(String testcaseid)
		{
			logi.logInfo("Entering amount act 4")
			return md_VerifyRateSchedDescriptionEditAct(testcaseid,4)
		}
		
		
// Rate tiers verification Update Inventory DB method


             def md_updated_rate_tiers_details_from_DB(String testcaseid) {
                     
             logi.logInfo("Entered into tjin loop")
                        
             LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
             String item_no = getValueFromResponse("update_inventory_item", ExPathAdminToolsApi.UPDATE_INVENTORY_ITEM_ITEM_NO1)
             String updated_currency_cd = getValueFromHttpRequestValue("update_inventory_item", "currency_cd").toUpperCase()
                     String updated_item_name = getValueFromHttpRequestValue("update_inventory_item", "item_name")
             String schedule_name = updated_item_name + "_" + updated_currency_cd
             logi.logInfo("CREATE SCHEDULEM NAME+++++"+schedule_name)


             String schedule_no = db.executeQueryP2("SELECT SCHEDULE_NO FROM ARIACORE.INVENTORY_ITEM_RATE_SCHEDULE WHERE SCHEDULE_NAME="+schedule_name)
             String currency_cd = db.executeQueryP2("SELECT CURRENCY_CD FROM ARIACORE.INVENTORY_ITEM_RATE_SCHEDULE WHERE SCHEDULE_NAME="+schedule_name)
String tax_inclusive_ind = db.executeQueryP2("SELECT TAX_INCLUSIVE_IND FROM ARIACORE.INVENTORY_ITEM_RATE_SCHEDULE WHERE SCHEDULE_NAME="+schedule_name)


             String from_unit = db.executeQueryP2("SELECT FROM_UNIT FROM ARIACORE.INVENTORY_ITEM_RATE_SCHED_RATE WHERE SCHEDULE_NO =" +schedule_no)
             String to_unit = db.executeQueryP2("SELECT TO_UNIT FROM ARIACORE.INVENTORY_ITEM_RATE_SCHED_RATE WHERE SCHEDULE_NO =" +schedule_no)
             String rate_per_unit = db.executeQueryP2("SELECT RATE_PER_UNIT FROM ARIACORE.INVENTORY_ITEM_RATE_SCHED_RATE WHERE SCHEDULE_NO ="+schedule_no)
              
             logi.logInfo(" Updated Item, schedule_no, Currency_cd and tax_ind : " + item_no + " :: " + schedule_no + " :: " + currency_cd + " :: " + 

tax_inclusive_ind)
             logi.logInfo("Updated from_unit, to_unit and rate_per_unit : " + from_unit + " :: " + to_unit + " :: " + rate_per_unit)
                     
             hashResult.put("From Unit", from_unit)
             hashResult.put("To Unit", to_unit)
             hashResult.put("Rate Per Unit", rate_per_unit)
             hashResult.put("Currency Code", currency_cd)
             hashResult.put("Tax Inclusive Indicator", tax_inclusive_ind)
                                         
             return hashResult.sort()
             }






// Rate tiers verification Create Inventory DB method


             def md_created_rate_tiers_details_from_DB(String testcaseid) {
                   
             LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
             String item_no = getValueFromResponse("create_inventory_item", ExPathAdminToolsApi.CREATE_INVENTORY_ITEM_ITEM_NO1)
             String created_currency_cd = getValueFromHttpRequestValue("create_inventory_item", "currency_cd").toUpperCase()
             String created_item_name = getValueFromHttpRequestValue("create_inventory_item", "item_name")

             String schedule_name = created_item_name + "_" + created_currency_cd
             logi.logInfo("UPDATE SCHEDULEM NAME+++++"+schedule_name)

    String schedule_no = db.executeQueryP2("SELECT SCHEDULE_NO FROM ARIACORE.INVENTORY_ITEM_RATE_SCHEDULE WHERE SCHEDULE_NAME="+schedule_name)
    String currency_cd = db.executeQueryP2("SELECT CURRENCY_CD FROM ARIACORE.INVENTORY_ITEM_RATE_SCHEDULE WHERE SCHEDULE_NAME="+schedule_name)
    String tax_inclusive_ind = db.executeQueryP2("SELECT TAX_INCLUSIVE_IND FROM ARIACORE.INVENTORY_ITEM_RATE_SCHEDULE WHERE SCHEDULE_NAME="+schedule_name)
    String from_unit = db.executeQueryP2("SELECT FROM_UNIT FROM ARIACORE.INVENTORY_ITEM_RATE_SCHED_RATE WHERE SCHEDULE_NO =" +schedule_no)
    String to_unit = db.executeQueryP2("SELECT TO_UNIT FROM ARIACORE.INVENTORY_ITEM_RATE_SCHED_RATE WHERE SCHEDULE_NO =" +schedule_no)
    String rate_per_unit = db.executeQueryP2("SELECT RATE_PER_UNIT FROM ARIACORE.INVENTORY_ITEM_RATE_SCHED_RATE WHERE SCHEDULE_NO="+schedule_no)



    logi.logInfo("Created Item, schedule_no, Currency_cd and tax_ind : " + item_no + " :: " + schedule_no + " :: " + currency_cd + " :: " + 

tax_inclusive_ind)
    logi.logInfo("Created from_unit, to_unit and rate_per_unit : " + from_unit + " :: " + to_unit + " :: " + rate_per_unit)
                     
                     hashResult.put("From Unit", from_unit)
                     hashResult.put("To Unit", to_unit)
                     hashResult.put("Rate Per Unit", rate_per_unit)
                     hashResult.put("Currency Code", currency_cd)
                     hashResult.put("Tax Inclusive Indicator", tax_inclusive_ind)
                                         
                     return hashResult.sort()

                     }

		
		
		
		
		
//		Rate tiers verification create_API method
		def md_rate_tiers_details_from_create_API(String testcaseid) {
			
			String created_schedule = getValueFromHttpRequestValue("create_inventory_item", "schedule")
			logi.logInfo("CREATED SCHEDULE VAL: " +created_schedule)
									
			HashMap <String, String> resultHash = new HashMap<String, String>()
			
			String item_price = getValueFromHttpRequestValue("create_inventory_item", "item_price")
			String currency_cd = getValueFromHttpRequestValue("create_inventory_item", "currency_cd")
			String tax_inclusive_ind = getValueFromHttpRequestValue("create_inventory_item", "tax_inclusive_ind")
								
			if(created_schedule.equals(null))
				{
				String from_unit = 1.toString()
				String to_unit = null
				resultHash.put("From Unit", from_unit)
				resultHash.put("To Unit", to_unit)
				resultHash.put("Rate Per Unit", item_price)
				resultHash.put("Currency Code", currency_cd)
				resultHash.put("Tax Inclusive Indicator", tax_inclusive_ind)
				}
				else 
				{
				String tier_from = getValueFromHttpRequestValue("create_inventory_item", "schedule[0][tier][0][from]")
				String tier_to = getValueFromHttpRequestValue("create_inventory_item", "schedule[0][tier][0][to]")
				String tier_amount = getValueFromHttpRequestValue("create_inventory_item", "schedule[0][tier][0][amount]")
								
				resultHash.put("From Unit", tier_from)
				resultHash.put("To Unit", tier_to)
				resultHash.put("Rate Per Unit", tier_amount)
				resultHash.put("Currency Code", currency_cd)
				resultHash.put("Tax Inclusive Indicator", tax_inclusive_ind)
				}
		
			return resultHash.sort()
		 }
 
		//	Rate tiers verification update_API method
		def md_rate_tiers_details_from_update_API(String testcaseid) {
			
			String updated_schedule = getValueFromHttpRequestValue("create_inventory_item", "schedule")
			logi.logInfo("UPDATED SCHEDULE VAL: " +updated_schedule)
						
			HashMap <String, String> resultHash = new HashMap<String, String>()
									
			String updated_item_price = getValueFromHttpRequestValue("update_inventory_item", "item_price")
			String updated_currency_cd = getValueFromHttpRequestValue("update_inventory_item", "currency_cd")
			String updated_tax_inclusive_ind = getValueFromHttpRequestValue("update_inventory_item", "tax_inclusive_ind")
			logi.logInfo("Item_price, Currency_cd and tax_ind : " + updated_item_price + " :: " + updated_currency_cd + " :: " + updated_tax_inclusive_ind)
			
			 if(updated_schedule.equals(null))
				{
				String from_unit = 1.toString()
				String to_unit = null
				resultHash.put("From Unit", from_unit)
				resultHash.put("To Unit", to_unit)
				resultHash.put("Rate Per Unit", updated_item_price)
				resultHash.put("Currency Code", updated_currency_cd)
			    resultHash.put("Tax Inclusive Indicator", updated_tax_inclusive_ind)
				}
				else
				{
				String tier_from = getValueFromHttpRequestValue("update_inventory_item", "schedule[0][tier][0][from]")
				String tier_to = getValueFromHttpRequestValue("update_inventory_item", "schedule[0][tier][0][to]")
				String tier_amount = getValueFromHttpRequestValue("update_inventory_item", "schedule[0][tier][0][amount]")

				resultHash.put("From Unit", tier_from)
				resultHash.put("To Unit", tier_to)
				resultHash.put("Rate Per Unit", tier_amount)
				resultHash.put("Currency Code", updated_currency_cd)
        		resultHash.put("Tax Inclusive Indicator", updated_tax_inclusive_ind)
				}
					
			return resultHash.sort()
		 }
	
		
		
		//Method to get the total invoice amount from API
		public String uv_getTotalInvoicefromAPI(String countInv) {
			
			boolean success = false;
			int tot
			String total
			String invAmt,ordAmt
			logi.logInfo("Entering getTotalInvoicefromAPI funtion");
			try {
				 
				logi.logInfo "Test no : " + countInv.toInteger()
				
				for(int i = 1; i<=countInv.toInteger(); i++){
					invAmt = getValueFromResponse("create_acct_complete_m","//ns1:create_acct_complete_mResponse[1]/out_acct[1]/ns1:out_acct_row[1]/ns1:invoice_info[1]/ns1:invoice_info_row[" + i + "]/ns1:total[1]")
					tot = tot + invAmt.toInteger()
				}
				
				invAmt = getValueFromResponse("create_order_m","//ns1:create_order_mResponse[1]/total_charges_after_tax[1]")
				
				int tot1 = invAmt.toInteger();
				tot = tot + tot1;
				
				logi.logInfo "Invoice total : " + tot;
				total = tot + " ";
				total = total.trim()
																		   
			}
			
			catch(Exception e) {
				logi.logInfo("PayThroughInvoice Method Exception: "+e.printStackTrace());
				total = tot + " ";
				total = total.trim()
				return total;
			}
		
			return total;
		}
			
		//Method to get the total invoice amount from DB
		public String uv_getTotalInvoicefromDB(String tcID) {
			
			ResultSet resultSet1,resultSet2
			int total = 0
			String tot
			
			logi.logInfo("Entering getTotalInvoicefromDB funtion");
			
			try {
			//Gets account NO
			String acctNo = getValueFromResponse("create_acct_complete_m","//*/*:acct_no[1]")
			logi.logInfo "Account NO : " + acctNo;
			
			//Connects DB and execute the query to fetch Invoice NO
			ConnectDB db = new ConnectDB()
			//String query = "select INVOICE_NO from ariacore.gl where acct_no = " + acctNo;
			String query = "select distinct INVOICE_NO from ariacore.gl_pending_detail where PLAN_INSTANCE_NO in (select PLAN_INSTANCE_NO from ariacore.plan_instance where acct_no=" + acctNo + ") union select INVOICE_NO from ariacore.gl where acct_no = " + acctNo;
		
			resultSet1 = db.executePlaQuery(query);
			
			List <String> listItem = []
			
			while(resultSet1.next()){
			   listItem.add(resultSet1.getString(1))
			}
				
			logi.logInfo "Size : " + listItem.size()
			for(int i=0;i<=listItem.size()-1;i++){
				logi.logInfo "test 1 : " + listItem.get(i)
			}
			
			String str = listItem.join(",");
			logi.logInfo "invoice List : " + str
			
			//query = "select (select sum(debit) from ariacore.gl_detail where invoice_no in ( " + str + ")) + (select sum(debit) from ariacore.gl_pending_detail where invoice_no in (" + str + ")) as result "
			query = "select (F+P) as result1 from ( select nvl(sum(debit),0) as F from ariacore.gl_detail where invoice_no in ( " + str + ")),(select nvl(sum(debit),0) as P from ariacore.gl_pending_detail where invoice_no in (" + str + ") )"
			resultSet2 = db.executePlaQuery(query);
			resultSet2.next()
			tot = resultSet2.getString(1)
			
			/*for(int i=0;i<=listItem.size()-1;i++){
		   
				 query = "select debit from ariacore.gl_detail where invoice_no = " + listItem.get(i)
				logi.logInfo "test 1 : " + listItem.get(i)
				resultSet2 = db.executePlaQuery(query);
				while(resultSet2.next()){
				total = total + resultSet2.getString(1).toInteger();
				}
				logi.logInfo "test 11 : " + resultSet2.getString(1).toInteger()
				
			}*/
			
			logi.logInfo "Invoice total : " + tot
			/*tot = total + " "
			tot = tot.trim()*/
																		   
			}
			
			catch(Exception e) {
				logi.logInfo("PayThroughInvoice Method Exception: "+e.printStackTrace());
			}
		
			return tot;
		}
		
	String md_get_field_no(String tcID){
		logi.logInfo("test Info")
		String fieldNo = getValueFromResponse("create_supp_obj_field_m","//*/*:field_no[1]")
		return fieldNo
	}

	//Gets the Data type of the product field
	String md_get_datatype(String tcID){

		logi.logInfo("test Info dtype")
		String fieldNo = getValueFromResponse("create_supp_obj_field_m","//*/*:field_no[1]")
		ResultSet rs = db.executeQuery("select DATATYPE from ariacore.CLIENT_OBJ_SUPP_FIELDS where field_no = "+ fieldNo);

		rs.next();
		String dType = rs.getString(1);

		return dType;

	}

	//Gets the Input Type of product field
	String md_get_input_Type(String tcID){
		String fieldNo = getValueFromResponse("create_supp_obj_field_m","//*/*:field_no[1]")
		ResultSet rs = db.executeQuery("select FORM_INPUT_TYPE from ariacore.CLIENT_OBJ_SUPP_FIELDS where field_no = "+ fieldNo);

		rs.next();
		String inputType = rs.getString(1);

		return inputType;
	}

	//Gets the maximum selections of product field
	String md_get_max_sel_value(String tcID){
		String fieldNo = getValueFromResponse("create_supp_obj_field_m","//*/*:field_no[1]")
		ResultSet rs = db.executeQuery("select MAX_NUM_VALS from ariacore.CLIENT_OBJ_SUPP_FIELDS where field_no = "+ fieldNo);

		rs.next();
		String max = rs.getString(1);

		return max;
	}
	
	def md_get_entity_no_m(String testcaseid)
	{
		logi.logInfo("Entering Into md_get_entity_no_m")
		def client_no=getValueFromHttpRequestValue("create_legal_entity_m", "client_no")
		logi.logInfo "Client No " + client_no
		String name=getValueFromHttpRequestValue("create_legal_entity_m", "legal_entity_name").toString()
		logi.logInfo "Legal entity name" + name
		String entity_no = db.executeQueryP2("SELECT LEGAL_ENTITY_NO FROM ARIACORE.CLIENT_LEGAL_ENTITY WHERE LEGAL_ENTITY_NAME='"+name+"' and client_no ="+client_no)
		logi.logInfo "Legal entity No" + entity_no
		return entity_no
	}

	//Gets the minimum selections of product field
	String md_get_min_sel_value(String tcID){
		String fieldNo = getValueFromResponse("create_supp_obj_field_m","//*/*:field_no[1]")
		ResultSet rs = db.executeQuery("select MIN_NUM_VALS from ariacore.CLIENT_OBJ_SUPP_FIELDS where field_no = "+ fieldNo);

		rs.next();
		String min = rs.getString(1);

		return min;
	}

		
//		def md_getPlanDetailsfromDB(String testcaseid) 
//		{
//		String plan_no
//		HashMap <String, String> resultHash = new HashMap<String, String>()
//		plan_no = getValueFromHttpRequestValue("create_new_plan_m", "plan_no")
//		ConnectDB db=new ConnectDB()
//		ResultSet rs = db.executeQuery("SELECT * FROM ARIACORE.INVENTORY_ITEMS where ITEM_NO= " + item_no + " AND CLIENT_NO=" + client_no)
//		while(rs.next()) {
//			hashResult.put("Item No", rs.getString('ITEM_NO'))
//			hashResult.put("Label", rs.getString('LABEL'))
//			hashResult.put("Client Sku", rs.getString('CLIENT_SKU'))
//			hashResult.put("Item type", rs.getString('ARC_IND'))
//			hashResult.put("Description", rs.getString('LONG_DESC'))
//			hashResult.put("Service No", rs.getString('SERVICE_NO'))
//			hashResult.put("Active Index", rs.getString('NO_DISPLAY_IND'))
//			hashResult.put("Subunit Qty", rs.getString('SUBUNIT_QTY'))
//			hashResult.put("Subunit Label", rs.getString('SUBUNIT_LABEL'))
//			hashResult.put("Client Defined ID", rs.getString('CLIENT_ITEM_ID'))
//			hashResult.put("Stock Level Track", rs.getString('STOCK_LEVEL'))
//		}
		
		
		
		
		
		
		
		
//		}
		
}









