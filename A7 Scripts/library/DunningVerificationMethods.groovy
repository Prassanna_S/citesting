package library

import java.awt.geom.Arc2D.Double;
import java.lang.reflect.Method
import java.sql.ResultSet;
import java.sql.ResultSetMetaData
import java.sql.SQLException
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.text.SimpleDateFormat
import java.text.DateFormat
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import groovy.xml.StreamingMarkupBuilder

import org.apache.ivy.core.module.descriptor.ExtendsDescriptor;

import library.Constants.Constant;
import library.Constants.ExpValueConstants;
import library.Constants.ExPathRpc;
import library.Constants.ExPathRpcEnc;
import library.Constants.Constant.RESULT_TYPE;
import library.VerificationMethods
import library.ReadData

public class DunningVerificationMethods extends ServiceCreditsVerificationMethods
{
	LogResult logi = new LogResult(Constant.logFilePath)
	static ConnectDB db = null;
	public DecimalFormat df=new DecimalFormat("#.##")

	public DunningVerificationMethods(ConnectDB db) {
		super(db)
	}
	
	String md_current_balance(String testCaseId)
	{
		def accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		ConnectDB db = new ConnectDB()
		String invoice_no=db.executeQueryP2(invoiceQuery)
		String invoice_sum="SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no
		String invoice_amt=db.executeQueryP2(invoice_sum)
		String balance_query="SELECT SUM(BALANCE) FROM ARIACORE.ACCT_BALANCE WHERE ACCT_NO= "+accountNo+ " AND CLIENT_NO= "+clientNo
		String balance_amt=db.executeQueryP2(balance_query)
		double balance=balance_amt.toDouble()-invoice_amt.toDouble()
		return balance.toString()
		
	}
	
	String md_balance_check(String testCaseId)
	{
		def accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		if(accountNo=='NoVal')
		{
			accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
		}
		logi.logInfo " account no"+accountNo
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		ConnectDB db = new ConnectDB()
		String balance_query="SELECT count(UNPAID_AMOUNT) FROM ARIACORE.ACCT_UNPAID_CHARGES WHERE ACCT_NO= "+accountNo+ " AND CLIENT_NO= "+clientNo
		String balance_amt=db.executeQueryP2(balance_query)
		String flag='FALSE'
		if(balance_amt.toInteger()>0)
		{
		flag='TRUE'
		}
		return flag
		
	}
	
	String md_surcharge_applied(String testCaseId)
	{
		def accountNo = getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		ConnectDB db = new ConnectDB()
		String invoice_no=db.executeQueryP2(invoiceQuery)
		String sur_query="SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND SURCHARGE_RATE_SEQ_NO=1"
		String sur_check=db.executeQueryP2(sur_query)
		String flag='FALSE'
		if(sur_check!=null)
		{
			flag='TRUE'
		}
		return flag
	}


	
	//Dunning
	def md_Verify_dunning_duration_days(String testcaseId)
	{
		
			logi.logInfo("Inside method to count the dunning days")
			// Getting the account number and client id
			def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			def clientNo = Constant.mycontext.expand('${Properties#Client_No}')

			logi.logInfo " account no"+accountNo
			if(accountNo =='NoVal')
			{
				   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
				   logi.logInfo " hierarchy account no"+accountNo
			}
			
			DecimalFormat df = new DecimalFormat("#.##");
			ConnectDB db = new ConnectDB();
			List dunningNum=[]
			logi.logInfo " dunning count"
			String dunning_count =  "SELECT count(amount) from ariacore.acct_transaction where acct_no = " +accountNo+ "AND CLIENT_NO= "+clientNo+" and transaction_type = 15 order by create_date desc"
			String dunning_quy =db.executeQueryP2(dunning_count)
			logi.logInfo "dunning count"+dunning_quy
			
			String dunning_amt =  "SELECT amount from ariacore.acct_transaction where acct_no = " +accountNo+ "AND CLIENT_NO= "+clientNo+" and transaction_type = 15 order by create_date desc"
			ResultSet rs1=db.executePlaQuery(dunning_amt)
			logi.logInfo "Executed result set"
			while(rs1.next())
			{
				   logi.logInfo" Inside result set of dunning"
				   String dunning_no=rs1.getString(1)
				   dunningNum.add(dunning_no)
			}
			logi.logInfo "dunning amount"+dunningNum.toString()
			String dunningdays
			
			//int i = 1;
			//int j = i+1;
			
			if( dunning_quy >1)
			{
			switch(dunning_quy.toInteger())
			{
							  
			   case 2 :
			   
			   String dunning_days = "SELECT EXTRACT(DAY FROM (Max(a.create_date) - Min(a.create_date))) as days from (SELECT row_number() over(order by create_date asc)  sequencew,create_date FROM ariacore.acct_transaction	WHERE acct_no = " +accountNo+ "AND CLIENT_NO= "+clientNo+" AND transaction_type = 15)a where a.sequencew between 1 and 2"
			   dunningdays=db.executeQueryP2(dunning_days)
			   logi.logInfo("dunning days "+dunningdays)
			   return dunningdays
			   break;
			   
			   case 3 :
			   
			   String dunning_days = "SELECT EXTRACT(DAY FROM (Max(a.create_date) - Min(a.create_date))) as days from (SELECT row_number() over(order by create_date asc)  sequencew,create_date FROM ariacore.acct_transaction	WHERE acct_no = " +accountNo+ " AND client_no = "+clientNo+" AND transaction_type = 15)a where a.sequencew between 2 and 3"
			   dunningdays=db.executeQueryP2(dunning_days)
			   logi.logInfo("dunning days "+dunningdays)
			   return dunningdays
			   break;
			  
			   case 4 :
			   
			   String dunning_days = "SELECT EXTRACT(DAY FROM (Max(a.create_date) - Min(a.create_date))) as days from (SELECT row_number() over(order by create_date asc)  sequencew,create_date FROM ariacore.acct_transaction	WHERE acct_no = " +accountNo+ " AND client_no = "+clientNo+" AND transaction_type = 15)a where a.sequencew between 3 and 4"
			   dunningdays=db.executeQueryP2(dunning_days)
			   logi.logInfo("dunning days "+dunningdays)
			   return dunningdays
			   break;
			   
			}
			  
			}
			   
	}
	
	def md_dunning_void_total_amount(String testcaseId)
	{
		
			logi.logInfo("Inside method void total amount")
			// Getting the account number and client id
			def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			
			if(accountNo =='NoVal')
			{
				   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
			}
			
			DecimalFormat df = new DecimalFormat("#.##");
			ConnectDB db = new ConnectDB();
			String amt,amt1, dunning_amount, total_amount
			def ttamount
			String void_amount = "SELECT nvl(sum(a.amount),0) from(SELECT amount,row_number()over (order by create_date desc) as seq  from ariacore.acct_trans_void_history_view where acct_no = " +accountNo+ "    and client_no = "+clientNo+" )a where a.seq=1"
			//String void_amount = "SELECT amount from ariacore.acct_trans_void_history_view where acct_no = " +accountNo+ " and client_no = "+clientNo+" order by create_date desc"
			amt = db.executeQueryP2(void_amount)
			logi.logInfo(" void entry AMount "+amt)
			logi.logInfo(" void entry amaaaaaaaaaaaaount "+amt.toDouble())
			if(amt == '0')
			{
			String transaction_amt = "SELECT sum(amount) from ariacore.acct_transaction where acct_no = " +accountNo+ "  and client_no = "+clientNo+"  and transaction_type = 1  order by create_date asc"
			amt1=db.executeQueryP2(transaction_amt)
			logi.logInfo("inside if void amount "+amt1.toDouble())
			amt=amt1
			logi.logInfo("inside if VALUE CAHNGED void amount " +amt.toDouble())
			String dunning_amt = "SELECT sum(amount) from ariacore.dunning_charge where acct_no= " +accountNo+ "  and client_no = "+clientNo+" order by create_date desc"
			dunning_amount = db.executeQueryP2(dunning_amt)
			}
			else
			{
				logi.logInfo(" void amount "+amt.toDouble())
				String dunning_amt = "SELECT amount from ariacore.dunning_charge where acct_no= " +accountNo+ "  and client_no = "+clientNo+" order by create_date desc"
				dunning_amount = db.executeQueryP2(dunning_amt)
			}

			logi.logInfo(" dunning amount "+dunning_amount.toDouble())
			total_amount = (amt.toDouble()) + (dunning_amount.toDouble())
			logi.logInfo(" total amount "+total_amount.toDouble())
			//ttamount = df.format(total_amount)
			
			return df.format(total_amount.toDouble()).toString()
				
		
	}


	def md_get_acct_details_all_api(String testcaseId)
	{
		
			logi.logInfo("Inside method md_get_acct_details_all_api")
			// Getting the account number and client id
			HashMap row =new HashMap()
			def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			
			if(accountNo =='NoVal')
			{
				   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
			}
			
			DecimalFormat df = new DecimalFormat("#.##");
				
			String FN  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_FIRSTNAME1)
			logi.logInfo("First Name => "+FN.toString())
			row.put("First Name",FN.toString())
	
			String LN  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_LASTNAME1)
			logi.logInfo("Last Name => "+LN.toString())
			row.put("Last Name",LN.toString())
			
			String RLC  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_RESPLEVELCODE1)
			logi.logInfo("Resp level code => "+RLC.toString())
			row.put("Resp level code",RLC.toString())
			
			String alt_email  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_ALTEMAIL1)
			logi.logInfo("alt_email => "+alt_email.toString())
			row.put("Alt_email",alt_email.toString())
			
			String status_cd  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_STATUSCD1)
			logi.logInfo("status_cd => "+status_cd.toString())
			row.put("Status_cd",status_cd.toString())
			
			String bill_day  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_BILLDAY1)
			logi.logInfo("bill_day => "+bill_day.toString())
			row.put("bill_day",bill_day.toString())
			
			String balance  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_BALANCE1)
			logi.logInfo("balance => "+balance.toString())
			row.put("balance",balance.toString())
						
			String status_label  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_STATUSCD_LABEL)
			logi.logInfo("status_label => "+status_label.toString())
			row.put("status_label",status_label.toString())
						
			String plan_no  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_PLANNO1)
			logi.logInfo("plan_no => "+plan_no.toString())
			row.put("plan_no",plan_no.toString())
						
			String plan_name  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_PLANNAME1)
			logi.logInfo("plan_name => "+plan_name.toString())
			row.put("plan_name",plan_name.toString())
			
			String plan_units  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_PLANUNITS1)
			logi.logInfo("plan_units => "+plan_units.toString())
			row.put("plan_units",plan_units.toString())
			
			String pay_method  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_PAYMETHOD1)
			logi.logInfo("pay_method => "+pay_method.toString())
			row.put("pay_method",pay_method.toString())
			
			String pay_method_name  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_PAYMETHODNAME1)
			logi.logInfo("pay_method_name => "+pay_method_name.toString())
			row.put("pay_method_name",pay_method_name.toString())
						
			String is_test_acct  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_ISTESTACCT1)
			logi.logInfo("is_test_acct => "+is_test_acct.toString())
			row.put("is_test_acct",is_test_acct.toString())
			
			String last_arrears_bill_thru_date  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_LASTARREARSBILLTHRUDATE1)
			logi.logInfo("last_arrears_bill_thru_date => "+last_arrears_bill_thru_date.toString())
			row.put("last_arrears_bill_thru_date",last_arrears_bill_thru_date.toString())
			
			String last_bill_date  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_LASTBILLDATE1)
			logi.logInfo("last_bill_date => "+last_bill_date.toString())
			row.put("last_bill_date",last_bill_date.toString())
			
			String last_bill_thru_date  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_LASTBILLTHRUDATE1)
			logi.logInfo("last_bill_thru_date => "+last_bill_thru_date.toString())
			row.put("last_bill_thru_date",last_bill_thru_date.toString())
			
			String next_bill_date  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_NEXTBILLDATE1)
			logi.logInfo("next_bill_date => "+next_bill_date.toString())
			row.put("next_bill_date",next_bill_date.toString())
			
			String plan_date  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_PLAN_DATE)
			logi.logInfo("plan_date => "+plan_date.toString())
			row.put("plan_date",plan_date.toString())
			
			String status_date  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_STATUS_DATE)
			logi.logInfo("status_date => "+status_date.toString())
			row.put("status_date",status_date.toString())
			
			String notify_method  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_NOTIFY_METHOD)
			logi.logInfo("notify_method => "+notify_method.toString())
			row.put("notify_method",notify_method.toString())
			
			String notify_method_name  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_NOTIFY_METHOD_NAME)
			logi.logInfo("notify_method_name => "+notify_method_name.toString())
			row.put("notify_method_name",notify_method_name.toString())
			
			String billing_email  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_BILLINGEMAIL1)
			logi.logInfo("billing_email => "+billing_email.toString())
			row.put("billing_email",billing_email.toString())
			
			String billing_first_name  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_BILLINGFIRSTNAME1)
			logi.logInfo("billing_first_name => "+billing_first_name.toString())
			row.put("billing_first_name",billing_first_name.toString())
			
			String billing_last_name  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_BILLINGLASTNAME1)
			logi.logInfo("billing_last_name => "+billing_last_name.toString())
			row.put("billing_last_name",billing_last_name.toString())
						
			String billing_city  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_BILLINGCITY1)
			logi.logInfo("billing_city => "+billing_city.toString())
			row.put("billing_city",billing_city.toString())
			
			String billing_state  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_BILLINGSTATE1)
			logi.logInfo("billing_state => "+billing_state.toString())
			row.put("billing_state",billing_state.toString())
		
			String billing_zip  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_BILLINGZIP1)
			logi.logInfo("billing_zip => "+billing_zip.toString())
			row.put("billing_zip",billing_zip.toString())
				
			String billing_country  = getValueFromResponse('get_acct_details_all',ExPathRpcEnc.GET_ACCT_DETAILS_ALL_BILLINGCOUNTRY1)
			logi.logInfo("billing_country => "+billing_country.toString())
			row.put("billing_country",billing_country.toString())
				
			logi.logInfo("md_get_acct_details_all_api => "+row)
			return row.sort()
		
	}
	
	def md_get_acct_details_all_DB(String testcaseId)
	{
		
			logi.logInfo("Inside method md_get_acct_details_all_DB")
			// Getting the account number and client id
			HashMap row =new HashMap()
			def accountNo=getValueFromResponse('create_acct_complete',ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
			def clientNo = Constant.mycontext.expand('${Properties#Client_No}')
			
			if(accountNo =='NoVal')
			{
				   accountNo = getValueFromResponse("create_acct_hierarchy",ExPathRpcEnc.CAH_ACCT_NO1)
			}
			
			DecimalFormat df = new DecimalFormat("#.##");
			ConnectDB db = new ConnectDB();
						
			String FN_Q =  "SELECT First_Name FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String FN = db.executeQueryP2(FN_Q)
			logi.logInfo("First Name => "+FN.toString())
			row.put("First Name",FN.toString())
			
			String LN_Q =  "SELECT last_name FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String LN = db.executeQueryP2(LN_Q)
			logi.logInfo("Last Name => "+LN.toString())
			row.put("Last Name",LN.toString())
			
			String RLC_Q =  "SELECT resp_level_cd FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String RLC = db.executeQueryP2(RLC_Q)
			logi.logInfo("Resp level code => "+RLC.toString())
			row.put("Resp level code",RLC.toString())
			
			String altemail_Q =  "SELECT alt_email FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String alt_email = db.executeQueryP2(altemail_Q)
			logi.logInfo("alt_email => "+alt_email.toString())
			row.put("Alt_email",alt_email.toString())
			
			String status_cd_Q =  "SELECT status_cd FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String status_cd = db.executeQueryP2(status_cd_Q)
			logi.logInfo("status_cd => "+status_cd.toString())
			row.put("Status_cd",status_cd.toString())
			
			String balance_Q =  "SELECT balance FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String balance = db.executeQueryP2(balance_Q)
			logi.logInfo("balance => "+balance.toString())
			row.put("balance",balance.toString())
			
			String bill_day_Q =  "SELECT bill_day FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String bill_day = db.executeQueryP2(bill_day_Q)
			logi.logInfo("bill_day => "+bill_day.toString())
			row.put("bill_day",bill_day.toString())
					
			String status_label_Q =  "SELECT status_label FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String status_label = db.executeQueryP2(status_label_Q)
			logi.logInfo("status_label => "+status_label.toString())
			row.put("status_label",status_label.toString())
					
			String plan_no_Q =  "SELECT plan_no FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String plan_no = db.executeQueryP2(plan_no_Q)
			logi.logInfo("plan_no => "+plan_no.toString())
			row.put("plan_no",plan_no.toString())
					
			String plan_name_Q =  "SELECT plan_name FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String plan_name = db.executeQueryP2(plan_name_Q)
			logi.logInfo("plan_name => "+plan_name.toString())
			row.put("plan_name",plan_name.toString())
					
			String plan_units_Q =  "SELECT plan_units FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String plan_units = db.executeQueryP2(plan_units_Q)
			logi.logInfo("plan_units => "+plan_units.toString())
			row.put("plan_units",plan_units.toString())
					
			String pay_method_Q =  "SELECT pay_method FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String pay_method = db.executeQueryP2(pay_method_Q)
			logi.logInfo("pay_method => "+pay_method.toString())
			row.put("pay_method",pay_method.toString())
					
			String pay_method_name_Q =  "SELECT pay_method_name FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String pay_method_name = db.executeQueryP2(pay_method_name_Q)
			logi.logInfo("pay_method_name => "+pay_method_name.toString())
			row.put("pay_method_name",pay_method_name.toString())
							
			String is_test_acct_Q =  "SELECT is_test_acct FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String is_test_acct = db.executeQueryP2(is_test_acct_Q)
			logi.logInfo("is_test_acct => "+is_test_acct.toString())
			row.put("is_test_acct",is_test_acct.toString())
			
			String last_arrears_bill_thru_date_Q =  "SELECT  to_char(last_arrears_bill_thru_date,'yyyy-mm-dd') FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String last_arrears_bill_thru_date = db.executeQueryP2(last_arrears_bill_thru_date_Q)
			logi.logInfo("last_arrears_bill_thru_date => "+last_arrears_bill_thru_date.toString())
			row.put("last_arrears_bill_thru_date",last_arrears_bill_thru_date.toString())
			
			String last_bill_date_Q =  "SELECT  to_char(last_bill_date,'yyyy-mm-dd') FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String last_bill_date = db.executeQueryP2(last_bill_date_Q)
			logi.logInfo("last_bill_date => "+last_bill_date.toString())
			row.put("last_bill_date",last_bill_date.toString())
			
			String last_bill_thru_date_Q =  "SELECT to_char(last_bill_thru_date,'yyyy-mm-dd') FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String last_bill_thru_date = db.executeQueryP2(last_bill_thru_date_Q)
			logi.logInfo("last_bill_thru_date => "+last_bill_thru_date.toString())
			row.put("last_bill_thru_date",last_bill_thru_date.toString())
			
			String next_bill_date_Q =  "SELECT to_char(next_bill_date,'yyyy-mm-dd') FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String next_bill_date = db.executeQueryP2(next_bill_date_Q)
			logi.logInfo("next_bill_date => "+next_bill_date.toString())
			row.put("next_bill_date",next_bill_date.toString())
			
			String plan_date_Q =  "SELECT to_char(plan_date,'yyyy-mm-dd') FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String plan_date = db.executeQueryP2(plan_date_Q)
			logi.logInfo("plan_date => "+plan_date.toString())
			row.put("plan_date",plan_date.toString())
			
			String status_date_Q =  "SELECT to_char(status_date,'yyyy-mm-dd') FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String status_date = db.executeQueryP2(status_date_Q)
			logi.logInfo("status_date => "+status_date.toString())
			row.put("status_date",status_date.toString())
			
			String notify_method_Q =  "SELECT notify_method FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String notify_method = db.executeQueryP2(notify_method_Q)
			logi.logInfo("notify_method => "+notify_method.toString())
			row.put("notify_method",notify_method.toString())
			
			String notify_method_name_Q =  "SELECT notify_method_name FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String notify_method_name = db.executeQueryP2(notify_method_name_Q)
			logi.logInfo("notify_method_name => "+notify_method_name.toString())
			row.put("notify_method_name",notify_method_name.toString())
			
			String billing_email_Q =  "SELECT billing_email FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String billing_email = db.executeQueryP2(billing_email_Q)
			logi.logInfo("billing_email => "+billing_email.toString())
			row.put("billing_email",billing_email.toString())
			
			String billing_first_name_Q =  "SELECT billing_first_name FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String billing_first_name = db.executeQueryP2(billing_first_name_Q)
			logi.logInfo("billing_first_name => "+billing_first_name.toString())
			row.put("billing_first_name",billing_first_name.toString())
			
			String billing_last_name_Q =  "SELECT billing_last_name FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String billing_last_name = db.executeQueryP2(billing_last_name_Q)
			logi.logInfo("billing_last_name => "+billing_last_name.toString())
			row.put("billing_last_name",billing_last_name.toString())
			
			String billing_city_Q =  "SELECT billing_city FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String billing_city = db.executeQueryP2(billing_city_Q)
			logi.logInfo("billing_city => "+billing_city.toString())
			row.put("billing_city",billing_city.toString())
			
			String billing_state_Q =  "SELECT billing_state FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String billing_state = db.executeQueryP2(billing_state_Q)
			logi.logInfo("billing_state => "+billing_state.toString())
			row.put("billing_state",billing_state.toString())
			
			String billing_zip_Q =  "SELECT billing_zip FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String billing_zip = db.executeQueryP2(billing_zip_Q)
			logi.logInfo("billing_zip => "+billing_zip.toString())
			row.put("billing_zip",billing_zip.toString())
			
			String billing_country_Q =  "SELECT billing_country FROM ARIACORE.ACCT_DETAILS where acct_no = "+accountNo + "and client_no = "+clientNo
			String billing_country = db.executeQueryP2(billing_country_Q)
			logi.logInfo("billing_country => "+billing_country.toString())
			row.put("billing_country",billing_country.toString())
			
			logi.logInfo("md_get_acct_details_all_DB => "+row)
			return row.sort()
				
						
	}
	
	def md_invoiceAmount_WithSurcharge(String testcaseId)
	{
		String accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String tax_exemption_level = getValueFromRequest("create_acct_complete",ExPathRpcEnc.IR_TAX_EXEMPTION_LEVEL)
		List seqNum=[],service=[],plan=[]
		String credit
		String sur_check
		double taxAmount,invoiceAmount,coupon_credit,ans,actualAmount=0
		String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		ConnectDB db = new ConnectDB()
		String invoice_no=db.executeQueryP2(invoiceQuery)
		String plan_query="SELECT DISTINCT(PLAN_NO) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND PLAN_NO IS  NOT NULL"
		ResultSet rs=db.executePlaQuery(plan_query)
		logi.logInfo "Executed result set...."
		while(rs.next())
		{
			logi.logInfo" Inside result set of plan....."
			String plan_no=rs.getString(1)
			plan.add(plan_no)
		}
		logi.logInfo "Plan list : "+plan.toString()

		for(int i=0;i<plan.size();i++)
		{
			service.clear()
			String service_query="SELECT service_no from ariacore.gl_detail where invoice_no= "+invoice_no+ "and service_no not in (0,400,401,402,403,405) and plan_no="+plan.get(i).toString()+ " AND DEBIT >0"
			rs=db.executePlaQuery(service_query)
			while(rs.next())
			{
				logi.logInfo "Inside result set..."
				String service_no=rs.getString(1)
				service.add(service_no)
			}
			logi.logInfo "Service list : " +service.toString()
			logi.logInfo "Service listi :"+i+"service" +service.toString()

			for(int j=0;j<service.size();j++)
			{
					String act_query="SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+" AND SERVICE_NO= "+service.get(j).toString()+" AND PLAN_NO="+plan.get(i).toString()
					String service_fee=db.executeQueryP2(act_query)
					actualAmount+=service_fee.toDouble()
				
				}
			}
		/*String sur_query="SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND SURCHARGE_RATE_SEQ_NO=1"
		sur_check=db.executeQueryP2(sur_query)
		//String flag='FALSE'
		if(sur_check!=null)
		{
			actualAmount+=sur_check.toDouble()
		}*/
	
		
		logi.logInfo "Invoice_no " +invoice_no
		
		 String seq_query="SELECT SEQ_NUM FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO="+invoice_no+"AND SERVICE_NO  IN (400,401) ORDER BY SEQ_NUM DESC"
			  rs=db.executePlaQuery(seq_query)
			 while(rs.next())
			 {
				 logi.logInfo" Inside result set of SEQ_NUM....."
				 String seq_no=rs.getString(1)
				 seqNum.add(seq_no)
			 }
		 
		 for(int k=0;k<seqNum.size();k++)
		 {
			 String tax_query="SELECT NVL(SUM(DEBIT),0) FROM ARIACORE.GL_TAX_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND SEQ_NUM= "+seqNum.get(k).toString()
			 taxAmount=taxAmount+(db.executeQueryP2(tax_query)).toDouble()
			 logi.logInfo "Tax amount :" +taxAmount
		 }
		 
		 String coupon_amt_query="SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO ="+invoice_no+" AND SERVICE_NO=0"
		 String coupon_present=db.executeQueryP2(coupon_amt_query)
		 if(coupon_present!=null)
		 {
			 String couponqry = "SELECT COUPON_CD FROM ariacore.ACCT_COUPONS WHERE ACCT_NO="+accountNo
			 String coupon_cd=db.executeQueryP2(couponqry)
			 logi.logInfo "Coupon : "+ coupon_cd
			 String credit_query="SELECT distinct(amount) from ariacore.credits where acct_no="+accountNo+" AND orig_coupon_cd= '"+coupon_cd+"'"
			 credit=db.executeQueryP2(credit_query)
		 }
		 else credit=0
		 
		 String sur_query="SELECT DEBIT FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+ " AND SURCHARGE_RATE_SEQ_NO=1"
		 sur_check=db.executeQueryP2(sur_query)
		 //String flag='FALSE'
		 if(sur_check!=null)
		 {
			 actualAmount+=sur_check.toDouble()
		 }
	 
		 logi.logInfo "Actual amount "+actualAmount
		 logi.logInfo "Credit amount "+credit
		 
		 invoiceAmount=(actualAmount-credit.toDouble())+taxAmount
		 DecimalFormat df = new DecimalFormat("#.##");
		 return df.format(invoiceAmount).toString()
		
		
	}
	
	def md_credit_sequence_check (String testcaseId)
	{
		String flag='FALSE'
		String accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		ConnectDB db = new ConnectDB()
		String invoice_no=db.executeQueryP2(invoiceQuery)
		String credit_seq_query="SELECT CREDITED_SEQ_NUM FROM ARIACORE.GL_CREDIT_DETAIL WHERE INVOICE_NO= "+invoice_no+ " ORDER BY GL_DTL_CREDIT_SEQ_NUM ASC"
		String credit_seq=db.executeQueryP2(credit_seq_query)
		logi.logInfo "The credited_seq_num is "+credit_seq
		String plan_query="SELECT PLAN_NO FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no+" AND SEQ_NUM= "+credit_seq
		String plan_no=db.executeQueryP2(plan_query)
		logi.logInfo "The plan no from invoice is "+plan_no
		String acct_query="SELECT PLAN_NO FROM ARIACORE.ACCT_DETAILS WHERE ACCT_NO= "+accountNo+ "AND CLIENT_NO= "+clientNo
		String acct_plan_no=db.executeQueryP2(acct_query)
		logi.logInfo "The plan no from acct is "+acct_plan_no
		if(plan_no.equals(acct_plan_no))
		{
			flag='TRUE'
		}
		return flag
	}
	
	def md_cash_credit_check(String testcaseId)
	{
		logi.logInfo "Inside md_cash_credit_check"
		double balance_amt;
		String accountNo=getValueFromResponse("create_acct_complete",ExPathRpcEnc.CREATE_ACCT_COMPLETE_ACCTNO1)
		String clientNo = Constant.mycontext.expand('${Properties#Client_No}')
		String invoiceQuery="SELECT MAX(INVOICE_NO) FROM ARIACORE.GL WHERE ACCT_NO = " +accountNo+ "AND CLIENT_NO= "+clientNo
		ConnectDB db = new ConnectDB()
		String invoice_no=db.executeQueryP2(invoiceQuery)
		String bal_query="SELECT SUM(DEBIT) FROM ARIACORE.GL_DETAIL WHERE INVOICE_NO= "+invoice_no
		String invoice_amt=db.executeQueryP2(bal_query)
		logi.logInfo "The invoice amt "+invoice_amt
		String paid_query="SELECT PAID_AMOUNT FROM ARIACORE.ACCT_UNPAID_CHARGES WHERE ACCT_NO= "+accountNo
		String paid_amt=db.executeQueryP2(paid_query)
		logi.logInfo "The paid amt "+paid_amt
		balance_amt=invoice_amt.toDouble()-paid_amt.toDouble()
		logi.logInfo "The Balance amt issssss "+balance_amt.toString()
		return balance_amt.toString()
	}
	
	def md_inventory_image_details_from_API1(String testcaseid) {
		String image_url = getValueFromResponse("get_client_item_images","//*/item[2]/image_url[1]")
		String thumb_image_url = getValueFromResponse("get_client_item_images","//*/item[1]/image_url[1]")
		String image_text = getValueFromResponse("get_client_item_images","//*/item[1]/image_text[1]")
		String default_image_index = getValueFromResponse("get_client_item_images","//*/item[1]/default_ind[1]")
		String active_image_index = getValueFromResponse("get_client_item_images","//*/item[1]/image_class_seq_no[1]")

		HashMap <String, String> resultHash = new HashMap<String, String>()
		resultHash.put("IMAGE_URL0", image_url)
		resultHash.put("IMAGE_URL1", thumb_image_url)
		resultHash.put("IMAGE_TEXT0", image_text)
		resultHash.put("IMAGE_TEXT1", image_text)
		resultHash.put("DEFAULT_IND0", default_image_index)
		resultHash.put("DEFAULT_IND1", default_image_index)
		resultHash.put("DISPLAY_IND0", active_image_index)
		resultHash.put("DISPLAY_IND1", active_image_index)
		return resultHash.sort()
	}
	def md_inventory_image_details_from_DB1(String testcaseid)
	{
		logi.logInfo "Entering into md_inventory_image_details_from_DB"
		String apiname=testcaseid.split("-")[2]
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		ConnectDB db = new ConnectDB()
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		String item_no
		item_no = getValueFromRequest("get_client_item_images","//item_no")
		logi.logInfo("Item Number " + item_no )
		if(item_no == 'NoVal')
		{
			String client_item_id = getValueFromRequest("get_client_item_images","//client_item_id")
			logi.logInfo("Item and Client number : " + client_item_id )
			String query = "SELECT ITEM_NO FROM ARIACORE.INVENTORY_ITEMS WHERE CLIENT_ITEM_ID = " +' client_item_id '+" and client_no = " +client_no
			item_no = db.executeQueryP2(query)
		}
		logi.logInfo("Item and Client number : " + item_no + " :: " + client_no)
		int i = 0
		ResultSet rs = db.executePlaQuery("SELECT * from ariacore.inventory_item_images where item_no=" + item_no + " AND CLIENT_NO=" + client_no + " order by image_class_seq_no")

		while(rs.next()) {
			hashResult.put("IMAGE_URL" + i, rs.getString('IMAGE_URL'))
			hashResult.put("IMAGE_TEXT" + i, rs.getString('IMAGE_TEXT'))
			hashResult.put("DEFAULT_IND" + i, rs.getString('DEFAULT_IND'))
			hashResult.put("DISPLAY_IND" + i, rs.getString('DISPLAY_IND'))
			i++
		}
		logi.logInfo("Hashmap is : " + hashResult)

		return hashResult.sort()
		}
	
	
	def md_inventory_supp_field_details_from_API(String testcaseid) 
	{
		String field_name = getValueFromResponse("get_client_item_supp_fields","//*/item[1]/field_name[1]")
		String field_desc = getValueFromResponse("get_client_item_supp_fields","//*/item[1]/field_desc[1]")
		String field_order_no = getValueFromResponse("get_client_item_supp_fields","//*/item[1]/field_order_no[1]")
		String field_value = getValueFromResponse("get_client_item_supp_fields","//*/item[1]/field_value[1]")
		
		HashMap <String, String> resultHash = new HashMap<String, String>()
		resultHash.put("FIELD_NAME", field_name)
		resultHash.put("FIELD_DESC", field_desc)
		resultHash.put("FIELD_ORDER_NO", field_order_no)
		resultHash.put("FIELD_VALUE", field_value)
		return resultHash.sort()
	}
	def md_inventory_supp_field_details_from_DB(String testcaseid)
	{
		logi.logInfo "Entering into md_inventory_image_details_from_DB"
		String apiname=testcaseid.split("-")[2]
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		ConnectDB db = new ConnectDB()
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		String item_no,field_no
		item_no = getValueFromRequest("get_client_item_supp_fields","//filter_item_no")
		logi.logInfo("Item Number " + item_no )
		if(item_no == 'NoVal')
		{
			String client_item_id = getValueFromRequest("get_client_item_supp_fields","//filter_client_item_id")
			logi.logInfo("Item and Client number : " + client_item_id )
			String query = "SELECT ITEM_NO FROM ARIACORE.INVENTORY_ITEMS WHERE CLIENT_ITEM_ID = '" + client_item_id +"' and client_no = " +client_no
			item_no = db.executeQueryP2(query)
		}
		logi.logInfo("Item and Client number : " + item_no + " :: " + client_no)
		String query1 = "SELECT FIELD_NO FROM ARIACORE.CLIENT_ITEM_SUPP_FIELDS_VAL WHERE ITEM_NO = " + item_no +" and client_no = " +client_no
		field_no =  db.executeQueryP2(query1)
		int i = 0
		ResultSet rs = db.executeQuery("SELECT FIELD_NAME,DESCRIPTION from ariacore.CLIENT_OBJ_SUPP_FIELDS where field_no =" + field_no + " AND CLIENT_NO=" + client_no )
		while(rs.next())
		{
			logi.logInfo("Entering nto the loop")
			hashResult.put("FIELD_NAME" , rs.getString('FIELD_NAME'))
			hashResult.put("FIELD_DESC", rs.getString('DESCRIPTION'))
		}
		logi.logInfo("Hashmap is : " + hashResult)
		
		ResultSet rs1 = db.executeQuery("SELECT PRIMACY,VALUE_TEXT FROM ARIACORE.CLIENT_ITEM_SUPP_FIELDS_VAL WHERE ITEM_NO = " + item_no +" and client_no = " +client_no)
		while(rs1.next())
		{
			hashResult.put("FIELD_ORDER_NO" , rs1.getString('PRIMACY'))
			hashResult.put("FIELD_VALUE", rs1.getString('VALUE_TEXT'))
		}
			
		logi.logInfo("Hashmap is : " + hashResult)

		return hashResult.sort()
		}
	
	def md_inventory_price_details_from_DB1(String testcaseid)
	{
		logi.logInfo "Entering into md_inventory_price_details_from_DB1"
		String apiname=testcaseid.split("-")[2]
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		ConnectDB db = new ConnectDB()
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		String item_no
		item_no = getValueFromRequest("get_client_item_prices","//filter_item_no")
		logi.logInfo("Item Number " + item_no )
		if(item_no == 'NoVal')
		{
			String client_item_id = getValueFromRequest("get_client_item_prices","//filter_client_item_id")
			logi.logInfo("Item and Client number : " + client_item_id )
			String query = "SELECT ITEM_NO FROM ARIACORE.INVENTORY_ITEMS WHERE CLIENT_ITEM_ID = '" + client_item_id +"' and client_no = " +client_no
			item_no = db.executeQueryP2(query)
		}
		logi.logInfo("Item and Client number : " + item_no + " :: " + client_no)
		int i = 1
		ResultSet rs = db.executePlaQuery("SELECT * from ariacore.inventory_item_prices where item_no=" + item_no + " AND CLIENT_NO=" + client_no)

		while(rs.next()) {
			hashResult.put("Currency_Code " + i, rs.getString('CURRENCY_CD'))
			hashResult.put("Amount " + i, rs.getString('PRICE'))
			i++
		}
		logi.logInfo("Hashmap is : " + hashResult)

		return hashResult.sort()
		}
	
	def md_inventory_price_details_from_API1(String testcaseid)
	{
		logi.logInfo "Entering into md_inventory_price_details_from_API1"
		ConnectDB db = new ConnectDB()
		String item_no
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		item_no = getValueFromRequest("get_client_item_prices","//filter_item_no")
		logi.logInfo("Item Number " + item_no )
		if(item_no == 'NoVal')
		{
			String client_item_id = getValueFromRequest("get_client_item_prices","//filter_client_item_id")
			logi.logInfo("Item and Client number : " + client_item_id )
			String query = "SELECT ITEM_NO FROM ARIACORE.INVENTORY_ITEMS WHERE CLIENT_ITEM_ID = '" + client_item_id +"' and client_no = " +client_no
			item_no = db.executeQueryP2(query)
		}
		logi.logInfo("Item and Client number : " + item_no + " :: " + client_no)
		String query = "SELECT COUNT(currency_cd) from ariacore.inventory_item_prices where item_no=" + item_no + " AND CLIENT_NO=" + client_no
		String count1 = db.executeQueryP2(query)
		logi.logInfo("Total count  " + count1 )
		for(int i = 1; i<=count1.toInteger() ; i++ )
		{
			
			String Currency_cd = getValueFromResponse("get_client_item_prices","//*/item["+ i +"]/currency_cd[1]")
			hashResult.put("Currency_Code " + i, Currency_cd)
			String Price = getValueFromResponse("get_client_item_prices"," //*/item["+i+"]/price[1]")
			hashResult.put("Amount " + i, Price)
		
		}
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}
	def md_inventory_price_details_from_Currency_DB1(String testcaseid)
	{
		logi.logInfo "Entering into md_inventory_price_details_from_DB1"
		String apiname=testcaseid.split("-")[2]
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		ConnectDB db = new ConnectDB()
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		String item_no,curr_cd
		item_no = getValueFromRequest("get_client_item_prices","//filter_item_no")
		curr_cd = getValueFromRequest("get_client_item_prices","//filter_currency_cd")
		logi.logInfo("Item Number " + item_no )
		if(item_no == 'NoVal')
		{
			String client_item_id = getValueFromRequest("get_client_item_prices","//filter_client_item_id")
			logi.logInfo("Item and Client number : " + client_item_id )
			String query = "SELECT ITEM_NO FROM ARIACORE.INVENTORY_ITEMS WHERE CLIENT_ITEM_ID = '" + client_item_id +"' and client_no = " +client_no
			item_no = db.executeQueryP2(query)
		}
		logi.logInfo("Item and Client number : " + item_no + " :: " + client_no)
		int i = 1
		ResultSet rs = db.executePlaQuery("SELECT * from ariacore.inventory_item_prices where item_no=" + item_no + " AND CURRENCY_CD = '" + curr_cd + "' AND CLIENT_NO = " + client_no)

		while(rs.next()) {
			hashResult.put("Currency_Code " + i, rs.getString('CURRENCY_CD'))
			hashResult.put("Amount " + i, rs.getString('PRICE'))
			i++
		}
		logi.logInfo("Hashmap is : " + hashResult)

		return hashResult.sort()
		}
	def md_inventory_price_details_from_Currency_API1(String testcaseid)
	{
		logi.logInfo "Entering into md_inventory_price_details_from_API1"
		ConnectDB db = new ConnectDB()
		String item_no,curr_cd
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		item_no = getValueFromRequest("get_client_item_prices","//filter_item_no")
		curr_cd = getValueFromRequest("get_client_item_prices","//filter_currency_cd")
		logi.logInfo("Item Number " + item_no )
		if(item_no == 'NoVal')
		{
			String client_item_id = getValueFromRequest("get_client_item_prices","//filter_client_item_id")
			logi.logInfo("Item and Client number : " + client_item_id )
			String query = "SELECT ITEM_NO FROM ARIACORE.INVENTORY_ITEMS WHERE CLIENT_ITEM_ID = '" + client_item_id +"' and client_no = " +client_no
			item_no = db.executeQueryP2(query)
		}
		logi.logInfo("Item and Client number : " + item_no + " :: " + client_no)
		String query = "SELECT COUNT(currency_cd) from ariacore.inventory_item_prices where item_no=" + item_no + " AND CURRENCY_CD = '" + curr_cd + "' AND CLIENT_NO = " + client_no
		String count1 = db.executeQueryP2(query)
		logi.logInfo("Total count  " + count1 )
		for(int i = 1; i<=count1.toInteger() ; i++ )
		{
			
			String Currency_cd = getValueFromResponse("get_client_item_prices","//*/item["+ i +"]/currency_cd[1]")
			hashResult.put("Currency_Code " + i, Currency_cd)
			String Price = getValueFromResponse("get_client_item_prices"," //*/item["+i+"]/price[1]")
			hashResult.put("Amount " + i, Price)
		
		}
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}
	
	def md_inventory_basic_details_from_API1(String testcaseid)
	{
			String item_no = getValueFromResponse("get_client_items_basic","//*/item[1]/item_no[1]")
			String item_label = getValueFromResponse("get_client_items_basic","//*/item[1]/item_label[1]")
			String service_no = getValueFromResponse("get_client_items_basic","//*/item[1]/service_no[1]")
			String client_sku = getValueFromResponse("get_client_items_basic","//*/item[1]/client_sku[1]")
			String item_type = getValueFromResponse("get_client_items_basic","//*/item[1]/item_type[1]")
			String item_desc = getValueFromResponse("get_client_items_basic","//*/item[1]/item_desc[1]")
			String active_ind = getValueFromResponse("get_client_items_basic"," //*/item[1]/active_ind[1]")
			String modify_price_indicator = getValueFromResponse("get_client_items_basic","//*/item[1]/modify_price_ind[1]")
			String client_item_id =getValueFromResponse("get_client_items_basic","//*/item[1]/client_item_id[1]")
			
			HashMap <String, String> resultHash = new HashMap<String, String>()
			resultHash.put("ITEM_NO", item_no)
			resultHash.put("ITEM_LABEL", item_label)
			resultHash.put("SERVICE_NO", service_no)
			resultHash.put("CLIENT_SKU", client_sku)
			resultHash.put("ITEM_TYPE", item_type)
			resultHash.put("ITEM_DESC", item_desc)
			resultHash.put("ACTIVE_IND", active_ind)
			resultHash.put("MODIFY_IND", modify_price_indicator)
			resultHash.put("CLIENT_ITEM_ID", client_item_id)
			return resultHash.sort()
		}
	
	def md_inventory_basic_details_from_DB1(String testcaseid)
	{
		logi.logInfo "Entering into md_inventory_basic_details_from_DB1"
		String apiname=testcaseid.split("-")[2]
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		ConnectDB db = new ConnectDB()
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		String item_no,client_item_id
		item_no = getValueFromRequest("get_client_items_basic","//filter_item_no")
		logi.logInfo("Item Number " + item_no )
		if(item_no == 'NoVal')
		{
			client_item_id = getValueFromRequest("get_client_items_basic","//filter_client_item_id")
			logi.logInfo("Item and Client number : " + client_item_id )
			String query = "SELECT ITEM_NO FROM ARIACORE.INVENTORY_ITEMS WHERE CLIENT_ITEM_ID = '" + client_item_id +"' and client_no = " +client_no
			item_no = db.executeQueryP2(query)
		}
		logi.logInfo("Item and Client number : " + item_no + " :: " + client_no)
		
		ResultSet rs = db.executePlaQuery("SELECT * FROM ARIACORE.INVENTORY_ITEMS WHERE item_no = '" + item_no +"' and client_no = " +client_no)

		while(rs.next())
		{
			hashResult.put("ITEM_NO", rs.getString('ITEM_NO'))
			hashResult.put("ITEM_LABEL", rs.getString('LABEL'))
			hashResult.put("SERVICE_NO", rs.getString('SERVICE_NO'))
			hashResult.put("CLIENT_SKU", rs.getString('CLIENT_SKU'))
			hashResult.put("ITEM_TYPE", rs.getString('PREPAY_ITEM_IND'))
			hashResult.put("ITEM_DESC", rs.getString('LONG_DESC'))
			hashResult.put("ACTIVE_IND", rs.getString('ALLOW_EU_DIRECT_PURCHASE_IND'))
			hashResult.put("MODIFY_IND", rs.getString('PRICE_PROMPT_IND'))
			hashResult.put("CLIENT_ITEM_ID", rs.getString('CLIENT_ITEM_ID'))
			
		}
		logi.logInfo("Hashmap is : " + hashResult)

		return hashResult.sort()
		}
	
	def md_inventory_all_details_from_DB1(String testcaseid)
	{
		logi.logInfo "Entering into md_inventory_basic_details_from_DB1"
		String apiname=testcaseid.split("-")[2]
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		ConnectDB db = new ConnectDB()
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		String item_no,client_item_id
		item_no = getValueFromRequest("get_client_items_all","//filter_item_no")
		logi.logInfo("Item Number " + item_no )
		if(item_no == 'NoVal')
		{
			client_item_id = getValueFromRequest("get_client_items_all","//filter_client_item_id")
			logi.logInfo("Item and Client number : " + client_item_id )
			String query = "SELECT ITEM_NO FROM ARIACORE.INVENTORY_ITEMS WHERE CLIENT_ITEM_ID = '" + client_item_id +"' and client_no = " +client_no
			item_no = db.executeQueryP2(query)
		}
		logi.logInfo("Item and Client number : " + item_no + " :: " + client_no)
		
		ResultSet rs = db.executePlaQuery("SELECT * FROM ARIACORE.INVENTORY_ITEMS WHERE item_no = '" + item_no +"' and client_no = " +client_no)

		while(rs.next())
		{
			hashResult.put("ITEM_NO", rs.getString('ITEM_NO'))
			hashResult.put("ITEM_LABEL", rs.getString('LABEL'))
			hashResult.put("SERVICE_NO", rs.getString('SERVICE_NO'))
			hashResult.put("CLIENT_SKU", rs.getString('CLIENT_SKU'))
			hashResult.put("ITEM_TYPE", rs.getString('PREPAY_ITEM_IND'))
			hashResult.put("ITEM_DESC", rs.getString('LONG_DESC'))
			hashResult.put("ACTIVE_IND", rs.getString('ALLOW_EU_DIRECT_PURCHASE_IND'))
			hashResult.put("MODIFY_IND", rs.getString('PRICE_PROMPT_IND'))
			hashResult.put("CLIENT_ITEM_ID", rs.getString('CLIENT_ITEM_ID'))
			
		}
		int i = 1
		ResultSet rs1 = db.executePlaQuery("SELECT * from ariacore.inventory_item_prices where item_no=" + item_no + " AND CLIENT_NO=" + client_no)

		while(rs1.next())
		{
			hashResult.put("Currency_Code " + i, rs1.getString('CURRENCY_CD'))
			hashResult.put("Amount " + i, rs1.getString('PRICE'))
			i++
		}

		logi.logInfo("Hashmap is : " + hashResult)

		return hashResult.sort()
		}
	
	
	def md_inventory_all_details_from_API1(String testcaseid)
	{
		ConnectDB db = new ConnectDB()
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		String item_no1
		item_no1 = getValueFromRequest("get_client_items_all","//filter_item_no")
		logi.logInfo("Item Number " + item_no1 )
		
		if(item_no1 == 'NoVal')
		{
		String client_item_id = getValueFromRequest("get_client_items_all","//filter_client_item_id")
		logi.logInfo("Item and Client number : " + client_item_id )
		String query = "SELECT ITEM_NO FROM ARIACORE.INVENTORY_ITEMS WHERE CLIENT_ITEM_ID = '" + client_item_id +"' and client_no = " +client_no
		item_no1 = db.executeQueryP2(query)
		}
		logi.logInfo("Item and Client number : " + item_no1 + " :: " + client_no)
		String query = "SELECT COUNT(currency_cd) from ariacore.inventory_item_prices where item_no=" + item_no1 + " AND CLIENT_NO = " + client_no
		String count1 = db.executeQueryP2(query)
		logi.logInfo("Total count  " + count1 )
		
		
		String item_no = getValueFromResponse("get_client_items_all","//*/item[1]/item_no[1]")
		String item_label = getValueFromResponse("get_client_items_all","//*/item[1]/item_label[1]")
		String service_no = getValueFromResponse("get_client_items_all","//*/item[1]/service_no[1]")
		String client_sku = getValueFromResponse("get_client_items_all","//*/item[1]/client_sku[1]")
		String item_type = getValueFromResponse("get_client_items_all","//*/item[1]/item_type[1]")
		String item_desc = getValueFromResponse("get_client_items_all","//*/item[1]/item_desc[1]")
		String active_ind = getValueFromResponse("get_client_items_all"," //*/item[1]/active_ind[1]")
		String modify_price_indicator = getValueFromResponse("get_client_items_all","//*/item[1]/modify_price_ind[1]")
		String client_item_id =getValueFromResponse("get_client_items_all","//*/item[1]/client_item_id[1]")
		
		HashMap <String, String> resultHash = new HashMap<String, String>()
		resultHash.put("ITEM_NO", item_no)
		resultHash.put("ITEM_LABEL", item_label)
		resultHash.put("SERVICE_NO", service_no)
		resultHash.put("CLIENT_SKU", client_sku)
		resultHash.put("ITEM_TYPE", item_type)
		resultHash.put("ITEM_DESC", item_desc)
		resultHash.put("ACTIVE_IND", active_ind)
		resultHash.put("MODIFY_IND", modify_price_indicator)
		resultHash.put("CLIENT_ITEM_ID", client_item_id)
		
		for(int i = 1; i<=count1.toInteger() ; i++ )
		{
		
		String Currency_cd = getValueFromResponse("get_client_items_all"," //*/item["+ i +"]/currency_cd[1]")
		resultHash.put("Currency_Code " + i, Currency_cd)
		String Price = getValueFromResponse("get_client_items_all","//*/item["+ i +"]/price[1]")
		resultHash.put("Amount " + i, Price)
	
		}
		return resultHash.sort()
	}
	
	def md_client_plan_service_rates_from_API1(String testcaseid)
	{
		logi.logInfo "Entering into md_client_plan_service_rates_from_API1"
		ConnectDB db = new ConnectDB()
		String plan_no,service_no
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		plan_no = getValueFromRequest("get_client_plan_service_rates","//plan_no")
		service_no=getValueFromRequest("get_client_plan_service_rates","//service_no")
		logi.logInfo("Plan Number " + plan_no )
		if(plan_no == 'NoVal')
		{
			String client_plan_id = getValueFromRequest("get_client_plan_service_rates","//client_plan_id")
			logi.logInfo("plan_no and Client number : " + client_plan_id )
			String query = "SELECT PLAN_NO FROM ARIACORE.CLIENT_PLAN WHERE CLIENT_PLAN_ID = '" + client_plan_id +"' and client_no = " +client_no
			plan_no = db.executeQueryP2(query)
		}
		if(service_no == 'NoVal')
		{
			String client_service_id = getValueFromRequest("get_client_plan_service_rates","//client_service_id")
			logi.logInfo("service number and Client number : " + client_service_id )
			String query1 = "SELECT SERVICE_NO FROM ARIACORE.CLIENT_SERVICE WHERE CLIENT_SERVICE_ID = '" + client_service_id +"' and client_no = " +client_no
			service_no = db.executeQueryP2(query1)
		}
		logi.logInfo("Service number and Client number : " + service_no + " :: " + client_no)
		String query2 = "SELECT SCHEDULE_NO from ariacore.PLAN_RATE_SCHED_MAP where plan_no=" + plan_no + " AND CLIENT_NO=" + client_no
		String Schedule_no = db.executeQueryP2(query2)
		logi.logInfo("Schedule number  " + Schedule_no )
		String query3 = "SELECT COUNT(RECORD_NO) from ariacore.NEW_RATE_SCHED_RATES where service_no=" + service_no + " and schedule_no =" + Schedule_no +" AND CLIENT_NO=" + client_no
		String count1=db.executeQueryP2(query3)
		for(int i = 1; i<=count1.toInteger() ; i++ )
		{
			
			String rate_Seq = getValueFromResponse("get_client_plan_service_rates","//*/item["+i+"]/rate_seq_no[1]")
			hashResult.put("Rate_Seq_No " + i, rate_Seq)
			String from_unit = getValueFromResponse("get_client_plan_service_rates","//*/item["+i+"]/from_unit[1]")
			hashResult.put("From_unit " + i, from_unit)
			String to_unit = getValueFromResponse("get_client_plan_service_rates","//*/item["+i+"]/to_unit[1]")
			hashResult.put("To_unit " + i, to_unit)
			String rate_per_unit = getValueFromResponse("get_client_plan_service_rates","//*/item["+i+"]/rate_per_unit[1]")
			hashResult.put("RatePerUnit " + i, rate_per_unit)
		
		}
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}
	def md_client_plan_service_rates_from_DB1(String testcaseid)
	{
		logi.logInfo "Entering into md_client_plan_service_rates_from_DB1"
		ConnectDB db = new ConnectDB()
		String plan_no,service_no
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		plan_no = getValueFromRequest("get_client_plan_service_rates","//plan_no")
		service_no=getValueFromRequest("get_client_plan_service_rates","//service_no")
		logi.logInfo("Plan Number " + plan_no )
		if(plan_no == 'NoVal')
		{
			String client_plan_id = getValueFromRequest("get_client_plan_service_rates","//client_plan_id")
			logi.logInfo("plan_no and Client number : " + client_plan_id )
			String query = "SELECT PLAN_NO FROM ARIACORE.CLIENT_PLAN WHERE CLIENT_PLAN_ID = '" + client_plan_id +"' and client_no = " +client_no
			plan_no = db.executeQueryP2(query)
		}
		if(service_no == 'NoVal')
		{
			String client_service_id = getValueFromRequest("get_client_plan_service_rates","//client_service_id")
			logi.logInfo("service number and Client number : " + client_service_id )
			String query1 = "SELECT SERVICE_NO FROM ARIACORE.CLIENT_SERVICE WHERE CLIENT_SERVICE_ID = '" + client_service_id +"' and client_no = " +client_no
			service_no = db.executeQueryP2(query1)
		}
		logi.logInfo("Service number and Client number : " + service_no + " :: " + client_no)
		String query2 = "SELECT SCHEDULE_NO from ariacore.PLAN_RATE_SCHED_MAP where plan_no=" + plan_no + " AND CLIENT_NO=" + client_no
		String Schedule_no = db.executeQueryP2(query2)
		logi.logInfo("Schedule number  " + Schedule_no )
		int i = 1
		ResultSet rs1 = db.executePlaQuery("SELECT * from ariacore.NEW_RATE_SCHED_RATES where service_no=" + service_no + " and schedule_no =" + Schedule_no +" AND CLIENT_NO=" + client_no)

		while(rs1.next())
		{
			
			hashResult.put("Rate_Seq_No " + i,rs1.getString('RATE_SEQ_NO'))
			hashResult.put("From_unit " + i, rs1.getString('FROM_UNIT'))
			(rs1.getString('TO_UNIT')!= null)?hashResult.put("To_unit " + i, rs1.getString('TO_UNIT')):hashResult.put("To_unit " + i, "NoVal")
			hashResult.put("RatePerUnit " + i, rs1.getString('RATE_PER_UNIT'))
			
			i++
		}
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}
	
	def md_get_rate_schedules_for_plan_from_API1(String testcaseid)
	{
		logi.logInfo "Entering into md_get_rate_schedules_for_plan_from_API1"
		ConnectDB db = new ConnectDB()
		String plan_no
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		plan_no = getValueFromRequest("get_rate_schedules_for_plan","//plan_no")
		logi.logInfo("Item Number " + plan_no )
		if(plan_no == 'NoVal')
		{
			String client_plan_id = getValueFromRequest("get_rate_schedules_for_plan","//client_plan_id")
			logi.logInfo("plan_no and Client number : " + client_plan_id )
			String query = "SELECT PLAN_NO FROM ARIACORE.CLIENT_PLAN WHERE CLIENT_PLAN_ID = '" + client_plan_id +"' and client_no = " +client_no
			plan_no = db.executeQueryP2(query)
		}
		logi.logInfo("Plan_no and Client number : " + plan_no + " :: " + client_no)
		String query = "SELECT COUNT(SCHEDULE_NO) from ariacore.PLAN_RATE_SCHED_MAP where PLAN_NO=" + plan_no + " AND CLIENT_NO=" + client_no
		String count1 = db.executeQueryP2(query)
		logi.logInfo("Total count  " + count1 )
		for(int i = 1; i<=count1.toInteger() ; i++ )
		{
			
			String schedule_no = getValueFromResponse("get_rate_schedules_for_plan","//*/item["+ i +"]//schedule_no[1]")
			hashResult.put("Schedule_no " + i, schedule_no)
			String Schedule_name = getValueFromResponse("get_rate_schedules_for_plan"," //*/item["+i+"]/schedule_name[1]")
			hashResult.put("Schedule_name " + i, Schedule_name)
			String Currency_cd = getValueFromResponse("get_rate_schedules_for_plan"," //*/item["+i+"]/schedule_currency[1]")
			hashResult.put("Currency_cd " + i, Currency_cd)
			String Schedule_id = getValueFromResponse("get_rate_schedules_for_plan"," //*/item["+i+"]/client_rate_schedule_id[1]")
			hashResult.put("Client_Schedule_id " + i, Schedule_id)

		}
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}
	def md_get_rate_schedules_for_plan_from_DB1(String testcaseid)
	{
		logi.logInfo "Entering into md_get_rate_schedules_for_plan_from_API1"
		ConnectDB db = new ConnectDB()
		String plan_no,temp_sch
		List sch_no= []
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		plan_no = getValueFromRequest("get_rate_schedules_for_plan","//plan_no")
		logi.logInfo("Item Number " + plan_no )
		if(plan_no == 'NoVal')
		{
			String client_plan_id = getValueFromRequest("get_rate_schedules_for_plan","//client_plan_id")
			logi.logInfo("plan_no and Client number : " + client_plan_id )
			String query = "SELECT PLAN_NO FROM ARIACORE.CLIENT_PLAN WHERE CLIENT_PLAN_ID = '" + client_plan_id +"' and client_no = " +client_no
			plan_no = db.executeQueryP2(query)
		}
		logi.logInfo("Plan_no and Client number : " + plan_no + " :: " + client_no)
		String query = "SELECT COUNT(SCHEDULE_NO) from ariacore.PLAN_RATE_SCHED_MAP where PLAN_NO=" + plan_no + " AND CLIENT_NO=" + client_no
		String count1 = db.executeQueryP2(query)
		logi.logInfo("Total count  " + count1 )
		ResultSet rs1 = db.executePlaQuery("SELECT * from ariacore.PLAN_RATE_SCHED_MAP where PLAN_NO=" + plan_no + " AND CLIENT_NO=" + client_no )
			while(rs1.next())
			{
				temp_sch = rs1.getString('SCHEDULE_NO')
				logi.logInfo("SCHEDULE NO  " + temp_sch )
				sch_no.add(temp_sch)
			}
			logi.logInfo("Total count  " + count1 )
		
		for(int schdl = 0; schdl<sch_no.size(); schdl++ )
		{
			ResultSet rs2 = db.executePlaQuery("SELECT * from ariacore.NEW_RATE_SCHEDULES where CLIENT_NO=" + client_no +" and schedule_no=" +sch_no[schdl])
			while(rs2.next())
			{
					hashResult.put("Schedule_no " + (schdl+1),rs2.getString('SCHEDULE_NO'))
					hashResult.put("Schedule_name " + (schdl+1), rs2.getString('SCHEDULE_NAME'))
					hashResult.put("Currency_cd " + (schdl+1),rs2.getString('CURRENCY_CD'))
					hashResult.put("Client_Schedule_id " + (schdl+1), rs2.getString('CLIENT_RATE_SCHEDULE_ID'))
			}
			
			

		}
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}
	
	def md_get_service_for_plan_from_DB1(String testcaseid)
	{
		logi.logInfo "Entering into md_get_service_for_plan_from_DB1"
		ConnectDB db = new ConnectDB()
		String plan_no,temp_sch
		List plan_noo= []
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		plan_no = getValueFromRequest("get_client_plan_services","//plan_no")
		logi.logInfo("Item Number " + plan_no )
		if(plan_no == 'NoVal')
		{
			String client_plan_id = getValueFromRequest("get_client_plan_services","//client_plan_id")
			logi.logInfo("plan_no and Client number : " + client_plan_id )
			String query = "SELECT PLAN_NO FROM ARIACORE.CLIENT_PLAN WHERE CLIENT_PLAN_ID = '" + client_plan_id +"' and client_no = " +client_no
			plan_no = db.executeQueryP2(query)
		}
		ResultSet rs1 = db.executePlaQuery("SELECT * from ariacore.PLAN_SERVICES where PLAN_NO=" + plan_no + " AND CLIENT_NO=" + client_no )
			while(rs1.next())
			{
				temp_sch = rs1.getString('SERVICE_NO')
				logi.logInfo("SERVICE NO  " + temp_sch )
				plan_noo.add(temp_sch)
			}
			logi.logInfo("Total count  " + plan_noo.size() )
		
		for(int schdl = 0; schdl<plan_noo.size(); schdl++ )
		{
			ResultSet rs2 = db.executePlaQuery("SELECT * from ariacore.SERVICES where custom_to_client_no=" + client_no +" and service_no=" +plan_noo[schdl])
			while(rs2.next())
			{
					hashResult.put("Service_NO " + (schdl+1),rs2.getString('SERVICE_NO'))
					hashResult.put("Service_Desc " + (schdl+1), rs2.getString('COMMENTS'))
					hashResult.put("Recurring_IND " + (schdl+1),rs2.getString('RECURRING'))
					hashResult.put("Usage_IND	 " + (schdl+1), rs2.getString('USAGE_BASED'))
					hashResult.put("Tax_IND " + (schdl+1),rs2.getString('TAXABLE'))
					hashResult.put("Setup_IND " + (schdl+1), rs2.getString('IS_SETUP'))
					hashResult.put("Order_IND " + (schdl+1),rs2.getString('IS_ORDER_BASED'))
					hashResult.put("Cancel_IND	 " + (schdl+1), rs2.getString('IS_CANCELLATION'))
					hashResult.put("Minimum_IND " + (schdl+1),rs2.getString('IS_MIN_FEE'))
					hashResult.put("COA_CD " + (schdl+1), rs2.getString('COA_ID'))
					hashResult.put("Client_service_id " + (schdl+1),rs2.getString('CLIENT_SERVICE_ID'))
					
			}
			
			

		}
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}
	
	def md_get_service_for_plan_from_API(String testcaseid)
	{
		logi.logInfo "Entering into md_get_service_for_plan_from_API"
		ConnectDB db = new ConnectDB()
		String plan_no,temp_sch
		List plan_noo= []
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		plan_no = getValueFromRequest("get_client_plan_services","//plan_no")
		logi.logInfo("Item Number " + plan_no )
		if(plan_no == 'NoVal')
		{
			String client_plan_id = getValueFromRequest("get_client_plan_services","//client_plan_id")
			logi.logInfo("plan_no and Client number : " + client_plan_id )
			String query = "SELECT PLAN_NO FROM ARIACORE.CLIENT_PLAN WHERE CLIENT_PLAN_ID = '" + client_plan_id +"' and client_no = " +client_no
			plan_no = db.executeQueryP2(query)
		}
		String query = "SELECT count(service_no) from ariacore.PLAN_SERVICES where PLAN_NO=" + plan_no + " AND CLIENT_NO=" + client_no
		String count1 = db.executeQueryP2(query)
		logi.logInfo("Total count  " + count1 )
		
		for(int i = 1; i<=count1.toInteger(); i++ )
		{
					String service_no = getValueFromResponse("get_client_plan_services","//*/item["+ i +"]//service_no[1]")
					hashResult.put("Service_NO " + i,service_no)
					String service_desc = getValueFromResponse("get_client_plan_services","//*/item["+ i +"]//service_desc[1]")
					hashResult.put("Service_Desc " + i,service_desc)
					String recurring_ind = getValueFromResponse("get_client_plan_services","//*/item["+ i +"]//is_recurring_ind[1]")
					hashResult.put("Recurring_IND " + i,recurring_ind)
					String usage_ind = getValueFromResponse("get_client_plan_services","//*/item["+ i +"]//is_usage_based_ind[1]")
					hashResult.put("Usage_IND	 " + i,usage_ind)
					String tax_ind = getValueFromResponse("get_client_plan_services","//*/item["+ i +"]//taxable_ind[1]")
					hashResult.put("Tax_IND " + i,tax_ind)
					String set_up = getValueFromResponse("get_client_plan_services","//*/item["+ i +"]//is_setup_ind[1]")
					hashResult.put("Setup_IND " + i, set_up)
					String order_ind = getValueFromResponse("get_client_plan_services","//*/item["+ i +"]//is_order_based_ind[1]")
					hashResult.put("Order_IND " + i,order_ind)
					String cancel_ind = getValueFromResponse("get_client_plan_services","//*/item["+ i +"]//is_cancellation_ind[1]")
					hashResult.put("Cancel_IND	 " + i,cancel_ind)
					String min_ind = getValueFromResponse("get_client_plan_services","//*/item["+ i +"]//is_min_fee_ind[1]")
					hashResult.put("Minimum_IND " + i,min_ind)
					String coa_id = getValueFromResponse("get_client_plan_services","//*/item["+ i +"]//coa_id[1]")
					hashResult.put("COA_CD " + i, coa_id)
					String client_service_id = getValueFromResponse("get_client_plan_services","//*/item["+ i +"]//client_service_id[1]")
					hashResult.put("Client_service_id " + i,client_service_id)
					
					
			}
			
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}
	
	
	
	
	
	def md_get_avail_child_plans_for_plan_from_DB1(String testcaseid)
	{
		logi.logInfo "Entering into md_get_avail_child_plans_for_plan_from_DB1"
		String apiname=testcaseid.split("-")[2]
		logi.logInfo("API Name:"+apiname)
		ConnectDB db = new ConnectDB()
		String plan_no,temp_sch
		List plan_noo= []
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		plan_no = getValueFromRequest(apiname,"//in_plan_no")
		logi.logInfo("Item Number " + plan_no )
		if(plan_no == 'NoVal')
		{
			String client_plan_id = getValueFromRequest(apiname,"//in_client_plan_id")
			logi.logInfo("plan_no and Client number : " + client_plan_id )
			String query = "SELECT PLAN_NO FROM ARIACORE.CLIENT_PLAN WHERE CLIENT_PLAN_ID = '" + client_plan_id +"' and client_no = " +client_no
			plan_no = db.executeQueryP2(query)
		}
		ResultSet rs1 = db.executePlaQuery("SELECT * from ariacore.PLAN_SUPP_PLAN_MAP where MASTER_PLAN_NO=" + plan_no + " AND CLIENT_NO=" + client_no )
			while(rs1.next())
			{
				temp_sch = rs1.getString('SUPP_PLAN_NO')
				logi.logInfo("SERVICE NO  " + temp_sch )
				plan_noo.add(temp_sch)
			}
			logi.logInfo("Total count  " + plan_noo.size() )
		
		for(int schdl = 0; schdl<plan_noo.size(); schdl++ )
		{
			ResultSet rs2 = db.executePlaQuery("SELECT * from ariacore.CLIENT_PLAN where CLIENT_NO=" + client_no +" and PLAN_NO=" +plan_noo[schdl])
			while(rs2.next())
			{
					hashResult.put("Plan_no " + (schdl+1),rs2.getString('PLAN_NO'))
					hashResult.put("Plan_name " + (schdl+1), rs2.getString('PLAN_NAME'))
					hashResult.put("Billing_Interval " + (schdl+1),rs2.getString('BILLING_INTERVAL'))
					hashResult.put("Currency_CD	 " + (schdl+1), rs2.getString('CURRENCY'))
					hashResult.put("Client_plan_id " + (schdl+1),rs2.getString('CLIENT_PLAN_ID'))
					hashResult.put("Proration_invoice_timing " + (schdl+1), rs2.getString('PRORATION_INVOICE_TIMING_CD'))
					
					
			}
			
			

		}
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}
	def md_get_avail_child_plans_for_plan_from_AP1(String testcaseid)
	{
		logi.logInfo "Entering into md_get_avail_child_plans_for_plan_from_DB1"
		String apiname=testcaseid.split("-")[2]
		logi.logInfo("API Name:"+apiname)
		ConnectDB db = new ConnectDB()
		String plan_no,temp_sch
		List plan_noo= []
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		plan_no = getValueFromRequest(apiname,"//in_plan_no")
		logi.logInfo("Item Number " + plan_no )
		if(plan_no == 'NoVal')
		{
			String client_plan_id = getValueFromRequest(apiname,"//in_client_plan_id")
			logi.logInfo("plan_no and Client number : " + client_plan_id )
			String query = "SELECT PLAN_NO FROM ARIACORE.CLIENT_PLAN WHERE CLIENT_PLAN_ID = '" + client_plan_id +"' and client_no = " +client_no
			plan_no = db.executeQueryP2(query)
		}
		String query = "SELECT count(supp_plan_no) from ariacore.PLAN_SUPP_PLAN_MAP where MASTER_PLAN_NO=" + plan_no + " AND CLIENT_NO=" + client_no
		String count1 = db.executeQueryP2(query)
		logi.logInfo("Total count  " + count1 )
		
		for(int i = 1; i<=count1.toInteger(); i++ )
		{
					String Plan_no = getValueFromResponse(apiname,"//*/item["+ i +"]//plan_no[1]")
					hashResult.put("Plan_no " + i,Plan_no)
					String Plan_name = getValueFromResponse(apiname,"//*/item["+ i +"]//plan_name[1]")
					hashResult.put("Plan_name " + i,Plan_name)
					String Billing_Interval = getValueFromResponse(apiname,"//*/item["+ i +"]//billing_interval[1]")
					hashResult.put("Billing_Interval " + i,Billing_Interval)
					String Currency_CD = getValueFromResponse(apiname,"//*/item["+ i +"]//currency_cd[1]")
					hashResult.put("Currency_CD	 " + i,Currency_CD)
					String Client_plan_id = getValueFromResponse(apiname,"//*/item["+ i +"]//client_plan_id[1]")
					hashResult.put("Client_plan_id " + i,Client_plan_id)
					String Proration_invoice_timing = getValueFromResponse(apiname,"//*/item["+ i +"]//proration_invoice_timing_cd[1]")
					hashResult.put("Proration_invoice_timing " + i, Proration_invoice_timing)
			}
			
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}

	def md_get_client_plan_basic_from_API(String testcaseid)
	{
		logi.logInfo "Entering into md_get_client_plan_basic_from_API"
		ConnectDB db = new ConnectDB()
		String plan_no,temp_sch
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		plan_no = getValueFromRequest("get_client_plans_basic","//plan_no")
		logi.logInfo("Item Number " + plan_no )
		if(plan_no == 'NoVal')
		{
			String client_plan_id = getValueFromRequest("get_client_plans_basic","//client_plan_id")
			logi.logInfo("plan_no and Client number : " + client_plan_id )
			String query = "SELECT PLAN_NO FROM ARIACORE.CLIENT_PLAN WHERE CLIENT_PLAN_ID = '" + client_plan_id +"' and client_no = " +client_no
			plan_no = db.executeQueryP2(query)
		}
	
					String plan_no1 = getValueFromResponse("get_client_plans_basic","//*/item[1]/plan_no[1]")
					hashResult.put("Plan_no",plan_no1)
					String Plan_Name = getValueFromResponse("get_client_plans_basic","//*/item[1]/plan_name[1]")
					hashResult.put("Plan_Name" ,Plan_Name)
					String Plan_desc = getValueFromResponse("get_client_plans_basic","//*/item[1]/plan_desc[1]")
					hashResult.put("Plan_desc" ,Plan_desc)
					String supp_plan_ind = getValueFromResponse("get_client_plans_basic","//*/item[1]/supp_plan_ind[1]")
					hashResult.put("Supp_plan_Ind" ,supp_plan_ind)
					String billing_interval = getValueFromResponse("get_client_plans_basic","//*/item[1]/billing_interval[1]")
					hashResult.put("Billing_Interval" ,billing_interval)
					String rollover_month = getValueFromResponse("get_client_plans_basic","//*/item[1]/rollover_months[1]")
					hashResult.put("Rollover_Month" , rollover_month)
					String rollover_plan = getValueFromResponse("get_client_plans_basic","//*/item[1]/rollover_plan_no[1]")
					hashResult.put("Rollover_Plan" ,rollover_plan)
					String currency_cd = getValueFromResponse("get_client_plans_basic","//*/item[1]/currency_cd[1]")
					hashResult.put("Currency_Code",currency_cd)
					String client_plan_id = getValueFromResponse("get_client_plans_basic","//*/item[1]/client_plan_id[1]")
					hashResult.put("Client_Plan_Id" ,client_plan_id)
					String proration_invoice_timing = getValueFromResponse("get_client_plans_basic","//*/item[1]/proration_invoice_timing_cd[1]")
					hashResult.put("Proration_invoice_timing",proration_invoice_timing)

			
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}
	
	
	def md_get_client_plan_basic_from_DB(String testcaseid)
	{
		logi.logInfo "Entering into md_get_client_plan_basic_from_DB"
		ConnectDB db = new ConnectDB()
		String plan_no,temp_sch
		List plan_noo= []
		String client_no = Constant.mycontext.expand('${Properties#Client_No}')
		LinkedHashMap<String,String> hashResult = new LinkedHashMap<String, String>()
		plan_no = getValueFromRequest("get_client_plans_basic","//plan_no")
		logi.logInfo("Item Number " + plan_no )
		if(plan_no == 'NoVal')
		{
			String client_plan_id = getValueFromRequest("get_client_plans_basic","//client_plan_id")
			logi.logInfo("plan_no and Client number : " + client_plan_id )
			String query = "SELECT PLAN_NO FROM ARIACORE.CLIENT_PLAN WHERE CLIENT_PLAN_ID = '" + client_plan_id +"' and client_no = " +client_no
			plan_no = db.executeQueryP2(query)
		}
	
		ResultSet rs1 = db.executePlaQuery("SELECT * from ariacore.CLIENT_PLAN where PLAN_NO=" + plan_no + " AND CLIENT_NO=" + client_no )
		while(rs1.next())
		{
			
			hashResult.put("Plan_no",rs1.getString('PLAN_NO'))
			hashResult.put("Plan_Name", rs1.getString('PLAN_NAME'))
			hashResult.put("Plan_desc", rs1.getString('EU_HTML_LONG'))
			hashResult.put("Supp_plan_Ind", rs1.getString('SUPP_PLAN_IND'))
			hashResult.put("Billing_Interval", rs1.getString('BILLING_INTERVAL'))
			hashResult.put("Rollover_Month", rs1.getString('ROLLOVER_MONTHS'))
			hashResult.put("Rollover_Plan", rs1.getString('ROLLOVER_PLAN_NO'))
			hashResult.put("Currency_Code", rs1.getString('CURRENCY'))
			hashResult.put("Client_Plan_Id", rs1.getString('CLIENT_PLAN_ID'))
			hashResult.put("Proration_invoice_timing", rs1.getString('PRORATION_INVOICE_TIMING_CD'))
			
			
		}
			
		logi.logInfo("Hashmap is : " + hashResult)
		return hashResult.sort()
	}


	
	
}//DunningVerificationMethods class end
